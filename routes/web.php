<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Talent\TalentDashboardController;
use App\Http\Controllers\Front\FrontController;
use App\Http\Controllers\Moderator\ModeratorDashboardController;
use App\Http\Controllers\Talent\TalentAvailabilityController;
use App\Http\Controllers\GenericController;
use App\Http\Controllers\Common\SearchController;
use App\Http\Controllers\Talent\TalentProfileController;
use App\Http\Controllers\Talent\TalentServicesPricingController;
use App\Http\Controllers\Common\BookingRequestController;
use App\Http\Controllers\Talent\Bookings\TalentBookingController;
use App\Http\Controllers\Moderator\Bookings\ModeratorBookingsController;
use App\Http\Controllers\Talent\WorkHistoryController;
use App\Http\Controllers\Common\Chat\ChatController;
use App\Http\Controllers\Talent\Bookings\TalentBookingManagementController;
use App\Http\Controllers\Moderator\Bookings\ModeratorBookingManagementController;
use App\Http\Controllers\Moderator\Chat\ModeratorChatController;
use App\Http\Controllers\Common\SocialLoginController;
use App\Http\Controllers\Talent\Chat\TalentChatController;
use App\Http\Controllers\Common\DisputeQueryController;
use App\Http\Controllers\Common\ReviewController;
use App\Http\Controllers\Front\StripeController;
use App\Http\Controllers\Common\FavouriteUsersController;
use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\Payment\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// test route, no functionality in actual website
Route::view('/front-end-test', 'app.test-front-end.cookies');
Route::view('/update-pages', 'app/test-front-end/page-sepration/update_services_info');
Route::get('transfer-amount',[FrontController::class,'transferAmount']);
Route::get('/payout-email',[FrontController::class, 'payoutEmail'])->name('payout-email');

/*
    *
    *******************************************************************************
    |                                Admin Routes
    *******************************************************************************
    | Here are all of the Admin routes used in the project.
    | The used routes for the Admin are sub-domain based.
    *
*/
Route::group(['domain' => config('custom.admin.domain')], function () {
    Route::group([], function () {
        Route::get('/login', [AdminAuthController::class, 'renderLoginPage'])
            ->name('admin.login');
        Route::post('/login', [AdminAuthController::class, 'adminLoginAttempt'])
            ->name('admin.login.post')->middleware('throttle:4,1');
    });

    //    admin protected routes
    Route::group(['middleware' => ['admin', 'auth.admin', 'verified']], function () {
        Route::get('/', [AdminDashboardController::class, 'renderAdminDashboard'])
            ->name('admin-dashboard');
        Route::get('/users', [AdminDashboardController::class, 'renderUsersList'])
            ->name('admin.users');
        Route::get('/bookings-list', [AdminDashboardController::class, 'renderBookingsList'])
            ->name('admin.bookings-list');
        Route::get('/mail-subscribers', [AdminDashboardController::class, 'renderMailSubscribers'])
            ->name('admin.mail-subscribers');
        Route::get('/subscribers/send-email', [AdminDashboardController::class, 'renderSendSubscribersEmail'])
            ->name('admin.subscribers.send-email');
        Route::post('/subscribers/send-email', [AdminDashboardController::class, 'sendSubscribersEmail'])
            ->name('admin.subscribers.send-email.post');
        Route::get('/service-charges/setting', [AdminDashboardController::class, 'serviceChargesSettings'])
            ->name('admin.service-charges.setting');
        Route::post('/service-charges/setting', [AdminDashboardController::class, 'serviceChargesSettingsSave'])
            ->name('admin.service-charges.post');

        Route::get('/admin/payouts-listing',[AdminDashboardController::class,'renderPayoutsListing'])
            ->name('renderAdminPayoutsListing');
        Route::get('/admin/payouts-exception-listing',[AdminDashboardController::class,'renderPayoutsExceptionListing'])
            ->name('renderAdminPayoutsExceptionListing');
        Route::get('/payouts/process-manual', [PaymentController::class, 'processTalentManualPayout'])
            ->name('admin.payouts.process');
        Route::get('/payouts/moderator/process-manual', [PaymentController::class, 'processModeratorManualPayout'])
            ->name('admin.payouts.moderator.process');

        Route::get('/profile', [AdminDashboardController::class, 'renderProfile'])
            ->name('admin.profile');
        Route::post('/profile/save', [AdminDashboardController::class, 'profileSave'])
            ->name('admin.profile.save');

        Route::get('/user/featured/make', [AdminDashboardController::class, 'makeUserFeatured'])
            ->name('admin.user.featured.make');
        Route::get('/user/featured/undo', [AdminDashboardController::class, 'undoUserFeatured'])
            ->name('admin.user.featured.undo');

        Route::get('/chat/messages/render', [ChatController::class, 'renderAdminChatMessages'])
            ->name('admin.chat.messages');
    });
});


/*
    *
    *******************************************************************************
    |                                Social login handling routes
    *******************************************************************************
    *
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('/login/{provider}', [SocialLoginController::class, 'redirect'])
        ->name('social.login');
    Route::get('/login/{provider}/callback', [SocialLoginController::class, 'socialCallback'])
        ->name('facebook.social.callback');
    Route::get('/facebook/data-deletion', [SocialLoginController::class, 'facebookDataDeletion'])
        ->name('fb-data-deletion');
    Route::get('/facebook/user/de-authorized', [SocialLoginController::class, 'facebookUserDeAuthorized'])
        ->name('fb.user.de-authorized');

    Route::get('/social/login/instagram', [SocialLoginController::class, 'redirectToInstagramProvider'])
        ->name('instagram.login');
    Route::get('/social/login/instagram/callback', [SocialLoginController::class, 'instagramProviderCallback'])
        ->name('instagram.login.callback');
    Route::get('/instagram/data-deletion', [SocialLoginController::class, 'instagramDataDeletion'])
        ->name('insta-data-deletion');
    Route::get('/instagram/user/de-authorized', [SocialLoginController::class, 'instagramDataDeletion'])
        ->name('instagram.user.de-authorized');
});


/*
    *
    *******************************************************************************
    |                                Stripe callback handling routes
    *******************************************************************************
    *
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('stripe/connectAccount-callback', [StripeController::class,'verifyStripeToken'])
        ->name('user.stripe.verify');
});

/*
    *
    *******************************************************************************
    |                             Generic | common routes
    *******************************************************************************
    *
*/
Route::group([], function () {
    Route::get('/time-conversion', [GenericController::class, 'calculateTime'])
        ->name('time-conversion');
    Route::get('/search-talent', [SearchController::class, 'searchTalent'])
        ->name('search-talent');
    Route::get('/talent/public-profile', [TalentProfileController::class, 'talentPublicProfile'])
        ->name('talent.public-profile');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/booking/request/start', [BookingRequestController::class, 'bookingRequestPageRender'])
            ->name('booking-request.start')
            ->middleware('verified', 'auth.moderator');
        Route::post('/booking/request/date-selection', [BookingRequestController::class, 'bookingRequestSelectDate'])
            ->name('booking-request.date-selection.post');
        Route::get('/booking/request/available-slots', [BookingRequestController::class, 'bookingRequestAvailableSlotsRender'])
            ->name('booking-request.available-slots');
        Route::post('/booking/request/slot-selection', [BookingRequestController::class, 'bookingSlotSelection'])
            ->name('booking-request.slot-selection.post');
        Route::get('/booking/request/detail', [BookingRequestController::class, 'bookingRequestDetailRender'])
            ->name('booking-request.detail');
        Route::post('/booking/request/detail', [BookingRequestController::class, 'bookingRequestDetailAdd'])
            ->name('booking-request.detail.post');
        Route::get('/booking/request/payment', [BookingRequestController::class, 'bookingPaymentRender'])
            ->name('booking-request.payment');
        Route::post('/booking/payment/charge', [BookingRequestController::class, 'bookingPaymentCharge'])
            ->name('booking.payment.charge');

        Route::get('/booking/detail', [BookingRequestController::class, 'bookingDetailRender'])
            ->name('booking.detail');

        Route::post('/dispute/query', [DisputeQueryController::class, 'disputeQuerySubmit'])
            ->name('dispute.query.submit');

        Route::get('/favorite/user/add', [FavouriteUsersController::class, 'addFavouriteUser'])
            ->name('favourite.user.add');
        Route::get('/favorite/user/remove', [FavouriteUsersController::class, 'removeFavouriteUser'])
            ->name('favourite.user.remove');
    });

});


/*
    *
    *******************************************************************************
    |                                FRONT ROUTES
    *******************************************************************************
    *
*/
Route::group([], function () {
    Route::get('/', [FrontController::class, 'homeViewRender'])
        ->name('home');
    Route::get('/about', [FrontController::class, 'aboutViewRender'])
        ->name('about.get');
    Route::get('/contact', [FrontController::class, 'contactViewRender'])
        ->name('contact.get');
    Route::post('/contact', [FrontController::class, 'contactQuerySubmit'])
        ->name('contact.post')
        ->middleware(['throttle:3,1']);
    Route::get('/faq', [FrontController::class, 'faqViewRender'])
        ->name('faq.get');
    Route::get('/terms-conditions', [FrontController::class, 'termsConditionsViewRender'])
        ->name('terms-conditions.get');
    Route::get('/privacy-policy', [FrontController::class, 'privacyPolicyViewRender'])
        ->name('privacy-policy.get');

    Route::post('/subscribe/mail-list', [FrontController::class, 'subscribeMailingList'])
        ->name('subscribe.mail-list.post');

    Route::post('/session/set-timezone', [FrontController::class, 'sessionSetTimezone'])
        ->name('session.set-timezone');
});


/*
    *
    *******************************************************************************
    |                                AUTHENTICATION ROUTES
    *******************************************************************************
    *
*/
Route::group(['namespace' => 'Authentication'], function () {
    Route::get('/signup','AuthController@signupFormRender')->name('signup');
    Route::post('/signup','AuthController@signupAction')->name('signupPost');
    Route::get('/login','AuthController@loginForm')->name('login');
    Route::post('/login-post','AuthController@login')->name('loginPost');
    Route::get('/password/forgot','AuthController@forgotPasswordFormRender')->name('forgot.password');
    Route::post('/password/forgot','AuthController@forgotPassword')->name('forgot.password.action');
    Route::get('/forgot-token/verify','AuthController@verifyForgotToken')->name('forgot-token.verify');
    Route::get('/password/reset','AuthController@resetPasswordFormRender')->name('reset.password');
    Route::post('/password/reset','AuthController@resetPasswordAction')->name('reset.password.action')->middleware('verified.reset.token');

    Route::get('/email/verify','AuthController@verificationNotice')
        ->middleware('auth')
        ->name('verification.notice');
    Route::get('/email/verify/{id}/{hash}','AuthController@verifyVerification')
        ->middleware(['auth', 'signed'])
        ->name('verification.verify');
    Route::post('/email/verification-notification','AuthController@verificationSend')
        ->middleware(['auth', 'throttle:6,1'])
        ->name('verification.send');
    Route::get('/email/verification-notification/resend','AuthController@verificationResend')
        ->middleware(['auth', 'throttle:6,1'])
        ->name('verification.resend');
    Route::get('/logout','AuthController@logout')
        ->middleware(['auth'])
        ->name('logout');
});


/*
    *
    *******************************************************************************
    |                                TALENT ROUTES
    *******************************************************************************
    *
*/
Route::group(['prefix' => 'talent'], function () {
    Route::group(['middleware' => ['auth', 'verified', 'auth.talent']], function () {
        Route::get('/dashboard', [TalentDashboardController::class, 'renderTalentDashboard'])
            ->name('talent-dashboard');

        Route::get('/profile/edit', [TalentDashboardController::class, 'editTalentProfile'])
            ->name('talent.profile.edit');
        Route::post('/profile/basic/update', [TalentDashboardController::class, 'talentProfileBasicUpdate'])
            ->name('talent.profile.basic.update');

        Route::get('/profile/availability', [TalentAvailabilityController::class, 'renderAvailability'])
            ->name('talent.availability');
        Route::post('/profile/availability/set', [TalentAvailabilityController::class, 'talentAvailabilitySet'])
            ->name('talent.availability.set');

        Route::get('/profile/services-pricing', [TalentServicesPricingController::class, 'renderTalentServicesPricing'])
            ->name('talent.services-pricing');
        Route::post('/profile/services-pricing/set', [TalentServicesPricingController::class, 'talentServicesPricingSet'])
            ->name('talent.services-pricing.set');

        Route::get('/profile/work-gallery', [WorkHistoryController::class, 'renderTalentWorkGallery'])
            ->name('talent.profile.work-gallery');
        Route::get('/profile/work-files/upload', [WorkHistoryController::class, 'renderTalentWorkFilesUpload'])
            ->name('talent.profile.work-files');
        Route::post('/profile/work-files/save', [WorkHistoryController::class, 'talentWorkFilesSave'])
            ->name('talent.profile.work-files.save');
        Route::get('/work-file/delete', [WorkHistoryController::class, 'talentWorkFileDelete'])
            ->name('talent.work-file.delete');

        Route::get('/social-connectivity', [TalentDashboardController::class, 'renderSocialConnectivity'])
            ->name('talent.social-connectivity');

        Route::get('/bookings/requests', [TalentBookingController::class, 'talentBookingRequests'])
            ->name('talent.bookings.requests');
        Route::get('/bookings/search', [TalentBookingController::class, 'talentBookingsListing'])
            ->name('talent.bookings.search');
        Route::get('/booking/request/action', [TalentBookingController::class, 'actionBookingRequest'])
            ->name('talent.booking.request.accept');
        Route::get('/booking/management', [TalentBookingManagementController::class, 'renderTalentBookingManagement'])
            ->name('talent.booking.management');
        Route::get('/booking-management/talent-present', [TalentBookingManagementController::class, 'talentPresent'])
            ->name('talent.booking-management.talent-present');
        Route::get('/booking-management/mark-completed', [TalentBookingManagementController::class, 'talentMarkTimeCompleted'])
            ->name('talent.booking.mark-completed');

        Route::get('/chat/conversations', [TalentChatController::class, 'renderTalentChatConversations'])
            ->name('talent.chat.conversations');
        Route::get('/chat/messages', [TalentChatController::class, 'renderTalentChatMessages'])
            ->name('talent.chat.messages');
    });
});


/*
    *
    *******************************************************************************
    |                                MODERATOR ROUTES
    *******************************************************************************
    *
*/
Route::group(['prefix' => 'moderator'], function () {
    Route::group(['middleware' => ['auth', 'verified', 'auth.moderator']], function () {
        Route::get('/dashboard', [ModeratorDashboardController::class, 'renderModeratorDashboard'])
            ->name('moderator-dashboard');

        Route::get('/profile', [ModeratorDashboardController::class, 'renderModeratorProfile'])
            ->name('moderator.profile');
        Route::post('/profile', [ModeratorDashboardController::class, 'moderatorProfileSave'])
            ->name('moderator.profile.save');

        Route::get('/bookings/requests', [ModeratorBookingsController::class, 'moderatorBookingRequests'])
            ->name('moderator.bookings.requests');
        Route::get('/bookings/search', [ModeratorBookingsController::class, 'projectsListing'])
            ->name('moderator.bookings.search');
        Route::get('/booking/management', [ModeratorBookingManagementController::class, 'moderatorBookingManagement'])
            ->name('moderator.booking.management');
        Route::get('/booking/timer-start', [ModeratorBookingManagementController::class, 'startBookingTimer'])
            ->name('moderator.booking.timer-start');
        Route::get('/booking/timer-stop', [ModeratorBookingManagementController::class, 'stopBookingTimer'])
            ->name('moderator.booking.timer-stop');
        Route::get('/booking/approve-completion', [ModeratorBookingManagementController::class, 'approveCompletion'])
            ->name('moderator.booking.approve-completion');
        Route::post('/booking/submit-event-review', [ReviewController::class, 'moderatorAddEventReview'])
            ->name('moderator.booking.submit-review');

        Route::get('/chat/conversations/render', [ModeratorChatController::class, 'renderModeratorChatConversations'])
            ->name('moderator.chat.conversations');
        Route::get('/chat/messages/render', [ModeratorChatController::class, 'renderModeratorChatMessages'])
            ->name('moderator.chat.messages');

        Route::get('/favorite-users', [ModeratorDashboardController::class, 'moderatorFavouriteUsers'])
            ->name('moderator.favourite-users');


    });
});


/*
    *
    *******************************************************************************
    |                                CHAT ROUTES
    *******************************************************************************
    *
*/
Route::group(['prefix' => 'chat'], function () {
    Route::get('/conversations', [ChatController::class, 'renderChatConversations'])
        ->name('chat.conversations');
    Route::get('/messages', [ChatController::class, 'renderChatMessages'])
        ->name('chat.messages');
    Route::get('/conversation/start', [ChatController::class, 'startChatConversation'])
        ->name('chat.conversation.start');
    Route::post('/conversation/start', [ChatController::class, 'saveMessage'])
        ->name('chat.message.save');
    Route::get('/conversation/messages', [ChatController::class, 'renderConversationMessages'])
        ->name('conversation.load');
});



