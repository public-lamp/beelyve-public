<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

// clear all the log file in the /storage/logs
//Artisan::command('logs:clear', function() {
//    exec('rm -f ' . storage_path('logs/*.log'));
//    exec('rm -f ' . base_path('*.log'));
//    $this->comment('Logs have been cleared!');
//})->describe('Clear log files');

// clear all the log file in the /storage/logs
Artisan::command('logs:clear', function() {
    $command = 'truncate -s 0 ' .storage_path('logs/*.log');
    exec($command);
    $this->comment('Logs have been cleared!');
})->describe('Clear all log files in the /storage/logs folder');


// clear queue-worker logs
Artisan::command('queue-logs:clear', function() {
    exec('truncate -s 0 ' . storage_path('logs/queueWorker.log'));
    $this->comment('Queue-worker Logs cleared!');
})->describe('Clear Queue-worker log files');
