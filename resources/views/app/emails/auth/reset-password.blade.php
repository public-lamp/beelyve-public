@extends('app.emails.email-template-skeleton')

@section('title', 'Verify Account Email')



@section('main-content')

    <table width="900" border="0"  cellspacing="0" cellpadding="0" align="center" style="background:#fff; overflow: hidden; margin-top: 60px;
border:1px solid rgba(255, 187, 0, 0.5);
box-shadow: 0px 3px 6px 0px rgba(133, 133, 133, 0.12),inset 0px 0px 1px 0px rgba(255, 187, 0, 0.57);">
        <tbody>
        <tr>
            <td width="500" height="110" valign="center" align="center" style=" padding: 20px 20px 20px;  ">
                <span class="temp_logo" style=" padding: 20px 0px 30px; display: block;border-bottom: 1px solid #cecece; max-width: 500px;"><a href="{{ route('home') }}"><img src="{{ asset('project-assets/images/email_temp_logo.jpg') }}" alt="BeeLyve" style="display: block; width: 210px;" /></a></span>
            </td>
        </tr>
        <tr>
            <td valign="top" height="40"></td>
        </tr>
        <tr>
            <td width="800" valign="top" ><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="600" height="50">
                            <h2 style=" margin: auto; text-align: center; padding: 0px 100px 20px;font-size: 32px; color: #000; display: block; font-family: 'Roboto', sans-serif;  max-width: 500px;">Reset Password</h2>
                        </td>
                    </tr>
                    <tr>
                        <td width="600" height="">
                            <p style=" margin: 0px; margin: auto; text-align: center; padding: 0px 100px 0px;font-size: 20px; line-height: 1.5; color: #7e7c7c; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">To reset password click below</p>
                        </td>
                    </tr>
                    </tbody>
                </table></td>
        </tr>
        <tr>
            <td height="40" valign="top"></td>
        </tr>
        <tr>
            <td width="800" valign="top" ><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="600" height="50">
                            <a href="{{ $token }}" class="button" style="border-radius: 4px; color: #fff; display: block; overflow: hidden; text-decoration: none; background: #000; width: 200px; margin: auto; text-align: center; padding: 10px;">Reset Password</a>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" valign="top"></td>
                    </tr>
                    <tr>
                        <td width="600" height="">
                            <p style=" margin: 0px; margin: auto; text-align: center; padding: 0px 100px 0px;font-size: 20px; line-height: 1.5; color: #7e7c7c; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">Ignore if you did not requested Reset password</p>
                        </td>
                    </tr>

                    </tbody>
                </table></td>
        </tr>
        <tr>
            <td height="150" valign="top"></td>
        </tr>

        <tr>
            <td width="800" valign="top" ><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="800" height="" >
                            <strong style="font-size: 20px; line-height: 60px; margin: 0px; background: #000; color: #fff; display: block; font-family: 'Roboto', sans-serif; font-weight: 400 ; text-align: center;">&#169;{{date('Y').' '.config('app.name')}}, all rights reserved.</strong>
                        </td>
                    </tr>
                    </tbody>
                </table></td>
        </tr>
        </tbody>
    </table>
@endsection


