@extends('app.emails.email-template-skeleton')

@section('title', 'Verify Account Email')

@section('main-content')
<table width="900" border="0"  cellspacing="0" cellpadding="0" align="center" style="background:#fff; overflow: hidden; margin-top: 60px; margin-bottom: 50px;
border:1px solid #8aa4ee;
box-shadow: 0px 3px 6px 0px rgba(133, 133, 133, 0.12),inset 0px 0px 1px 0px rgba(255, 187, 0, 0.57);">
    <tbody>
        <tr>
            <td width="500" height="110" valign="center" align="center" style=" padding: 20px 20px 20px;  ">
                <span class="temp_logo" style=" padding: 20px 0px 30px; display: block;border-bottom: 1px solid #cecece; max-width: 500px;"><a href="{{ route('home') }}"><img src="{{ asset('project-assets/images/email_temp_logo.jpg') }}" alt="BeeLyve" style="display: block; width: 210px;" /></a></span>
            </td>
        </tr>
        <tr>
            <td valign="top" height="40"></td>
        </tr>
        <tr>
            <td width="800" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td width="600" height="50">
                                <h2 style=" margin: auto; text-align: center; padding: 0px 100px 20px;font-size: 24px; color: #000; display: block;  font-weight: 500; max-width: 500px;">{{ $title }}</h2>
                            </td>
                        </tr>
                        <tr>
                            <td width="600" height="">
                                <p style=" margin: 0px; margin: auto; text-align: center; padding: 0px 100px 0px;font-size: 18px; line-height: 1.5; color: #606060; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">{{ $bodyText }}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="40" valign="top"></td>
        </tr>
        <tr>
            <td width="800" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td width="600" height="50">
                                @if($toRoleName === 'moderator')
                                    <a href="{{ $moderatorBookingManagementLink }}" style=" text-align: center; font-weight: 500; border-radius: 50px; background: #3d69e7; display: block; font-size: 18px; padding: 18px 25px; max-width: 260px; margin: auto; color: #fff; text-decoration:none !important;">Manage Booking</a>
                                @elseif($toRoleName === 'talent')
                                    <a href="{{ $talentBookingManagementLink }}" style=" text-align: center; font-weight: 500; border-radius: 50px; background: #3d69e7; display: block; font-size: 18px; padding: 18px 25px; max-width: 260px; margin: auto; color: #fff; text-decoration:none !important;">Manage Booking</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td height="60" valign="top"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>


        {{--    compleet footer starts --}}
        <tr>
            <td height="120" valign="top"></td>
        </tr>
        <tr>
            <td width="600" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="500" height=""  style="padding: 0px 20px;">
                            <p style=" border-top: 1px solid #cecece; margin: 0px; margin: auto; text-align: center; padding: 20px 0px 0px !important; font-size: 16px; line-height: 1.5; color: #606060; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">832 Thompson Drive, San Fransisco CA 94107, United States</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="500" height="">
                            <p style=" margin: 0px; margin: auto; text-align: center; padding: 0px 100px 0px;font-size: 14px; line-height: 1.5; color: #606060; display: block; font-weight: 400; max-width: 500px;">
                                <a href="javascript:void(0)" style="text-decoration: underline;">Unsubscribe</a>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20" valign="top"></td>
        </tr>
        <tr>
            <td width="800" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="800" height="" >
                            <strong style="font-size: 20px; line-height: 60px; margin: 0px; background: #000; color: #fff; display: block; font-family: 'Roboto', sans-serif; font-weight: 400 ; text-align: center;">&#169;{{ date('Y').' '.config('app.name') }}, all rights reserved.</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

    </tbody>
</table>
@endsection
