<!doctype html>

<html>
<head>
    <meta charset="utf-8">
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">

    @yield('pageStyle')

</head>

<body style="margin:0px; padding:0px; background:#fff;">

@yield('main-content')

</body>
</html>
