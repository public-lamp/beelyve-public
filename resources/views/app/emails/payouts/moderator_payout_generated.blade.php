@extends('app.emails.email-template-skeleton')

@section('pageStyle')
    <style rel="stylesheet">
        .innertable1 tr td:last-child p { text-align: right;}
        .innertable1 tr { vertical-align: top;}
        @media only screen and (min-width: 300px) and (max-width: 767px) {
            body {
                padding: 20px !important;
            }
            table {
                width: 100% !important;
                margin: 0px !important;
            }
            table td p,
            table td strong {
                padding: 0px 10px !important;
                font-size: 13px !important;
            }
            table td h2 {
                padding: 0px 10px !important;
                font-size: 18px !important;
            }
            .innertable1 td:first-child,
            .innertable1 td:last-child {
                width: auto !important;
            }
            .btn_press { max-width: 190px !important;}
            .temp_logo img { width: 100px !important;}
        }
    </style>
@endsection

@section('title', 'Payout Generated')

@section('main-content')
    <table width="900" border="0"  cellspacing="0" cellpadding="0" align="center" style="background:#fff; overflow: hidden; margin-top: 60px; margin-bottom: 50px;
            border:1px solid #8aa4ee;
            box-shadow: 0px 3px 6px 0px rgba(133, 133, 133, 0.12),inset 0px 0px 1px 0px rgba(255, 187, 0, 0.57);">
        <tbody>
        <tr>
            <td width="500" height="110" valign="center" align="center" style=" padding: 20px 20px 20px;  ">
                <span class="temp_logo" style=" padding: 20px 0px 30px; display: block;border-bottom: 1px solid #cecece; max-width: 500px;"><a href="{{ route('home') }}"><img src="{{ asset('project-assets/images/email_temp_logo.jpg') }}" alt="BeeLyve" style="display: block; width: 210px;" /></a></span>
            </td>
        </tr>
        <tr>
            <td valign="top" height="10"></td>
        </tr>

        <tr>
            <td width="800" valign="top">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="600" height="50">
                            <h2 style=" margin: auto; text-align: center; padding: 0px 100px 20px;font-size: 24px; color: #000; display: block;  font-weight: 500; max-width: 500px;">{{ $title }}</h2>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table style="width: 600px; margin: auto;" class="innertable1">
                                <tbody>
                                <tr>
                                    <td width="60"></td>
                                    <td>
                                        <table style="width: 100%;">
                                            <tbody>
                                            <tr>
                                                <td width="200">
                                                    <p style="margin: 0px;"><b> Booking title:</b></p>
                                                </td>
                                                <td width="400">
                                                    <p style="margin: 0px;">{{ $booking->title }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="margin: 0px;"><b>Payout amount:</b></p>
                                                </td>
                                                <td>
                                                    <p style="margin: 0px;">${{ $booking->amount }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="margin: 0px;"><b>Payout status:</b></p>
                                                </td>
                                                <td>
                                                    <p style="margin: 0px;">{{ $payout['payoutStatus']['status'] }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="40"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="margin: 0px;"><b>Description note:</b></p>
                                                </td>
                                                <td>
                                                    <p style="margin: 0px;">{{ $moderatorPayoutNote }}</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="60"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <td width="600" height="50">
                        <a href="{{ route('booking.detail', ['booking_id' => $booking['id']]) }}" style=" text-align: center; font-weight: 500; border-radius: 50px; background: #3d69e7; display: block; font-size: 18px; padding: 18px 25px; max-width: 260px; margin: auto; color: #fff; text-decoration:none !important;">Booking Detail</a>
                    </td>
                    </tbody>
                </table>
            </td>
        </tr>


        {{-- email footer --}}
        <tr>
            <td height="120" valign="top"></td>
        </tr>
        <tr>
            <td width="600" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="500" height=""  style="padding: 0px 20px;">
                            <p style=" border-top: 1px solid #cecece; margin: 0px; margin: auto; text-align: center; padding: 20px 0px 0px !important; font-size: 16px; line-height: 1.5; color: #606060; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">832 Thompson Drive, San Fransisco CA 94107, United States</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="500" height="">
                            <p style=" margin: 0px; margin: auto; text-align: center; padding: 0px 100px 0px;font-size: 14px; line-height: 1.5; color: #606060; display: block; font-weight: 400; max-width: 500px;">
                                <a href="javascript:void(0)" style="text-decoration: underline;">Unsubscribe</a>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20" valign="top"></td>
        </tr>
        <tr>
            <td width="800" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="800" height="" >
                            <strong style="font-size: 20px; line-height: 60px; margin: 0px; background: #000; color: #fff; display: block; font-family: 'Roboto', sans-serif; font-weight: 400 ; text-align: center;">&#169;{{ date('Y').' '.config('app.name') }}, all rights reserved.</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection
