@extends('app.emails.email-template-skeleton')

@section('title', config('app.name').' - Subscribed Email')

@section('pageStyle')
    <style type="text/css">
        .innertable1 tr td:last-child p { text-align: right; }
        .innertable1 tr { vertical-align: top; }
        @media only screen and (min-width: 300px) and (max-width: 767px) {
            body {
                padding: 20px !important;
            }
            table {
                width: 100% !important;
                margin: 0px !important;
            }
            table td p,
            table td strong {
                padding: 0px 10px !important;
                font-size: 13px !important;
            }
            table td h2 {
                padding: 0px 10px !important;
                font-size: 18px !important;
            }
            .innertable1 td:first-child,
            .innertable1 td:last-child {
                width: auto !important;
            }
            .btn_press { max-width: 190px !important;}
            .temp_logo img { width: 170px !important;}
        }
    </style>
@endsection

@section('main-content')
    <table width="900" border="0"  cellspacing="0" cellpadding="0" align="center" style="background:#fff; overflow: hidden; margin-top: 60px; margin-bottom: 50px;
                border:1px solid #8aa4ee;
                box-shadow: 0px 3px 6px 0px rgba(133, 133, 133, 0.12),inset 0px 0px 1px 0px rgba(255, 187, 0, 0.57);">
        <tbody>
        <tr>
            <td width="500" height="110" valign="center" align="center" style=" padding: 20px 20px 20px;  ">
                    <span class="temp_logo" style=" padding: 20px 0px 30px; display: block;border-bottom: 1px solid #cecece; max-width: 500px;">
                        <a href="{{ route('home') }}"><img src="{{ asset('project-assets/images/email_temp_logo.jpg') }}" alt="BeeLyve" style="display: block; width: 130px;" /></a>
                    </span>
            </td>
        </tr>

        <tr>
            <td valign="top" height="10"></td>
        </tr>

        <tr>
            <td width="800" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="600" height="50">
                            <h2 style=" margin: auto; text-align: center; padding: 0px 100px 20px;font-size: 24px; color: #000; display: block;  font-weight: 500; max-width: 500px;">{{ $title }}</h2>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="10"></td>
                    </tr>
                    <tr>
                        <td width="600" height="50" style="padding: 0px 20px;">
                            <h2 style=" margin: auto; padding: 0px 0px 0px !important;font-size: 18px; color: #000; display: block;  font-weight: 500; max-width: 500px;">Title: <b>{{ $emailTitle }}</b></h2>
                        </td>
                    </tr>
                    <tr>
                        <td width="600" height="" style="padding: 0px 20px;">
                            <p style=" margin: 0px; margin: auto; padding: 0px 0px 30px !important;font-size: 16px; line-height: 1.5; color: #606060; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">{{ $message }}</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        {{--    complete footer starts --}}
        <tr>
            <td height="120" valign="top"></td>
        </tr>
        <tr>
            <td width="600" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="500" height=""  style="padding: 0px 20px;">
                            <p style=" border-top: 1px solid #cecece; margin: 0px; margin: auto; text-align: center; padding: 20px 0px 0px !important; font-size: 16px; line-height: 1.5; color: #606060; display: block; font-family: 'Roboto', sans-serif;  font-weight: 400; max-width: 500px;">832 Thompson Drive, San Fransisco CA 94107, United States</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="500" height="">
                            <p style=" margin: 0px; margin: auto; text-align: center; padding: 0px 100px 0px;font-size: 14px; line-height: 1.5; color: #606060; display: block; font-weight: 400; max-width: 500px;">
                                <a href="javascript:void(0)" style="text-decoration: underline;">Unsubscribe</a>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20" valign="top"></td>
        </tr>
        <tr>
            <td width="800" valign="top" >
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="800" height="" >
                            <strong style="font-size: 20px; line-height: 60px; margin: 0px; background: #000; color: #fff; display: block; font-family: 'Roboto', sans-serif; font-weight: 400 ; text-align: center;">&#169;{{ date('Y').' '.config('app.name') }}, all rights reserved.</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        {{-- footer ends --}}

        </tbody>
    </table>
@endsection
