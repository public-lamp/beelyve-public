
{{-- bootstrap4 --}}
<link rel="stylesheet" type="text/css" href="{{ asset('project-assets/css/bootstrap.min.css') }}" />
<link rel="stylesheet" type="application/octet-stream" href="{{ asset('project-assets/css/bootstrap.min.css.map') }}" />
{{-- font-awesome --}}
<link rel="stylesheet" type="text/css" href="{{ asset('project-assets/css/font-awesome.min.css') }}" />
{{-- JQuery timepicker css --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css" />
{{-- jqueryui css --}}
<link rel="stylesheet" href="{{ asset('project-assets/css/jquery-ui.css') }}" />
{{-- index css --}}
<link rel="stylesheet" type="text/css" href="{{ asset('project-assets/css/style.css') }}" />
{{-- responsive css --}}
<link rel="stylesheet" type="text/css" href="{{ asset('project-assets/css/responsive.css') }}" />

{{-- toaster style link --}}
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
{{--sweat alets style--}}
{{--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.css" />--}}
{{-- JQuery UI --}}
{{--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />--}}
