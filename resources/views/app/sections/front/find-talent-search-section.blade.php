{{-- find talent page, search section --}}
<form method="GET" action="{{ route('search-talent') }}">
    <div class="mainSearch_input">
        <input type="text" name="search_text" value="@if( isset($searchText) ) {{ $searchText }} @endif" placeholder="Search Talent…">
    </div>
    <div class="sorted_dropdown">
        <div class="custom_dropdown">
            <select name="search_filters[talent_service_category]">
                <option selected disabled>Service category</option>
                @foreach( \App\Models\Talent\TalentCategory::all() as $talentCat )
                    <option value="{{ $talentCat->id  }}" @if( isset($searchFilters['talent_service_category']) && $searchFilters['talent_service_category'] == $talentCat['id']) selected @endif >
                        {{ $talentCat->name  }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="sorted_dropdown dropdown_sort">
        <div class="custom_dropdown">
            <select name="search_filters[sort_by]">
                <option selected disabled>Sort by</option>
                @foreach( $filters = \App\Models\Generic\TalentSearchFilter::all() as $filterKey => $searchFilter )
                    @if(isset($searchFilter['parent_filter_key']) && $filters[$filterKey-1]['parent_filter_key'] !== $searchFilter['parent_filter_key'])
                        <optgroup label="{{ $searchFilter['parent_filter_name'] }}"></optgroup>
                        <option value="{{ $searchFilter['id'] }}">{{ $searchFilter['name'] }}</option>
                    @else
                        <option value="{{ $searchFilter['id'] }}" @if( isset($searchFilters['sort_by']) && $searchFilters['sort_by'] == $searchFilter['id'] ) selected @endif >{{ $searchFilter['name'] }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="mainSearch_bttn">
        <input type="submit" value="Search">
    </div>
</form>



