<!-- banner slider -->
<div class="banner_section ">
    <dvi class="search_popout">
        <div class="search_overlay"></div>
        <div class="search_inner">
            <a href="javascript:void(0)" class="close_search">&nbsp;</a>
            @include('app.sections.front.home-banner-search')
        </div>
    </dvi>

    <div class="bannerSlider_outer owl-carousel banner_slider sett_home_banner owl-theme">
        <div class="item">
            <div class="banner_slider_section">
                <span class="banner_img"><img src="{{ asset('project-assets/images/home_img1.png') }}" alt="img" /></span>
                <div class="banner_info">
                    <div class="auto_container">
                        <div class="banner_info_detail">
                            <label>welcome to beelyve</label>
                            <h1>Book your favorite influencers for virtual events</h1>
                            <p>Access your favorite stars and book  them for any occasion</p>
                            <a href="{{ route('search-talent') }}" class="default_bttn">Browse Talent </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="banner_slider_section">
                <span class="banner_img"><img src="{{ asset('project-assets/images/home_img2.png') }}" alt="img" /></span>
                <div class="banner_info">
                    <div class="auto_container">
                        <div class="banner_info_detail">
                            <label>BEELYVE FOR BUSINESS</label>
                            <h1>Put your brand in the spotlight</h1>
                            <p>Unlock access to starts and convert customers by leveraging the power of Beelyve</p>
                            <a href="{{ route('search-talent') }}" class="default_bttn">Browse Talent </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
