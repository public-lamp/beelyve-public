{{-- banner search section --}}
<form method="GET" action="{{ route('search-talent') }}">
    <div class="mainSearch">
        <div class="mainSearch_input">
            <input type="text" name="search_text" value="" placeholder="Search Talent…" />
        </div>
        <div class="mainSearch_bttn">
            <input type="submit" value="Search" />
        </div>
    </div>
</form>


