<!-- trusted section -->
<div class="trustedSection_info">
    <div class="auto_container">
        <div class="trustedSection">
            <ul>
                <li>
                    <a href="#"><img src="{{ asset('project-assets/images/log_google.png') }}" alt="#" /></a>
                </li>
                <li>
                    <a href="#"><img src="{{ asset('project-assets/images/logo_netflix.png') }}" alt="#" /></a>
                </li>
                <li>
                    <a href="#"><img src="{{ asset('project-assets/images/logo_payPal.png') }}" alt="#" /></a>
                </li>
            </ul>
        </div>
    </div>
</div>
