
{{-- main navbar --}}
<div class="header">
    <div class="auto_container">
        <div class="header_detail">
            <div class="header_detail_inner">
                <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('project-assets/images/logo.png') }}" alt="logo_img" class="logo_web" />
                        <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="logo" class="logo_mbile" />
                    </a>
                </div>

                <div class="headerInfo_inner">
                    <div class="menuIcon"></div>
                    <div class="menu_nav">
                        <a href="javascript:void(0)" class="closeNav">&nbsp</a>
                        <ul>
                            <li><a href="{{ route('home') }}" class=" @if( url()->current() == route('home')) nav_active @endif ">Home</a></li>
                            <li><a href="{{ route('about.get') }}" class=" @if( url()->current() == route('about.get')) nav_active @endif ">About</a></li>
                            <li><a href="{{ route('faq.get') }}" class=" @if( url()->current() == route('faq.get')) nav_active @endif ">FAQs</a></li>
                        </ul>
                    </div>

                    @if( auth()->check() )
                        <div class="nav_notification_outer">
                            <div class="profile_info">
                                <span>
                                    <b><img src="{{ isset( auth()->user()->image ) ? auth()->user()->image : asset('project-assets/images/avatar_white.png') }}" alt="user-avatar" /></b>
                                    <small>{{ auth()->user()->full_name }}</small>
                                    <i><img src="{{ asset('project-assets/images/chevron_down.png') }}" alt="^" /></i>
                                </span>
                                <div class="profileDropdown">
                                    <ul>
                                        <li>
                                            <a href="@if( auth()->user()->isAdmin() ) {{ route('admin-dashboard') }} @endif">Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}">Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>

