<!-- Edit Profile popup -->
<div class="custom_popup editPopup" style=" display: block;">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h2>Edit Profile</h2>
                </div>
                <form id="updateTalentProfileForm">
                    @csrf
                    <div class="edit_profile_head">
                        <div class="choose_profile">
                            <div class="profile_img">
                                <span>
                                    <b class="my_file">
                                        <input type='file' name="user_image" onchange="readImage(this);" />
                                        <small><i class="fa fa-camera fa-3" aria-hidden="true"></i>Select</small>
                                    </b>
                                    <img id="user_image_pan" src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="user-image" />
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="popup_form">
                        <ul>
                            <li class="w-50">
                                <div class="custom_field">
                                    <strong>First Name</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="first_name" id="first_name" />
                                    </div>
                                </div>
                            </li>
                            <li class="w-50">
                                <div class="custom_field">
                                    <strong>Last Name</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="last_name" id="last_name" />
                                    </div>
                                </div>
                            </li>
                            <li class="w-50">
                                <div class="custom_field">
                                    <strong>User Name</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="{{ auth()->user()->user_name }}" name="user_name" id="user_name" />
                                    </div>
                                </div>
                            </li>
                            <li class="w-50">
                                <div class="custom_field">
                                    <strong>Country</strong>
                                    <div class="custom_field_input">
                                        <select class="form-control" name="country" id="" >
                                            <option selected disabled>Select your country</option>
                                            @foreach( \App\Models\Generic\Country::all() as $country )
                                                <option>{{ $country->name  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>Position</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="position" id="position" />
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>Select Experience</strong>
                                    <div class="custom_field_input">
                                        <select class="form-control" name="experience_id" id="experience_id" >
                                            @foreach( \App\Models\Generic\Experience::all() as $experience )
                                                <option value="{{ $experience->id }}">{{ $experience->name  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>Tagline</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="tagline" id="tagline" />
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>Facebook Profile Link</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="fb_profile_link" id="fb_profile_link" />
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>Instagram Profile Link</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="insta_profile_link" id="insta_profile_link" />
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong> linkedin Profile Link</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="linkedin_profile_link" id="linkedin_profile_link" />
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>My Service</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="user_service" id="user_service" />
                                    </div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom_field">
                                    <strong>About</strong>
                                    <div class="custom_field_input">
                                       <textarea class="custom_field_input" name="user_about" id="user_about"></textarea>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <br>

                    <div class="custom_submit">
                        <input type="submit" value="Submit" />
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function readImage(imageInput) {
        let updatedImageUrl = imageInput.value;
        // $('#user_image_pan').attr('src', updatedImageUrl);
    }

    {{-- ajax --}}
    $('#updateTalentProfileForm').on('submit', function (e) {
        $('.errorAlertText').html('');
        e.preventDefault();
        let formData =  new FormData( this );
        blockUi( true );

        $.ajax({
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
            processData: false,
            contentType: false,
            cache: false,
            enctype: 'multipart/form-data',
            url: "{{ route('talent.profile.update') }}",
            data: formData,
            success: function (response) {
                if (response.status) {
                    blockUi( false );
                    toasterSuccess( true, response.message);
                    $('#editPopup').css({ "display": "none" });
                    return window.location.reload;
                } else if (!response.status) {
                    blockUi( false );
                    return toasterSuccess( false, response.error);
                }
            },
            error: function (data) {

                $.each(data.responseJSON.errors, function (key, value) {
                    blockUi( false );
                    let element = document.getElementById(key);
                    let errorAlertHtml = validationText(value, 'error');
                    $(element).after(errorAlertHtml);
                    if ( value.includes(key) ) {
                        $(element).val(value);
                    }
                });
            }
        });
    });
    {{-- Ends ajax --}}

</script>
