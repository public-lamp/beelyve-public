<style>
    .lds-dual-ring {
  display: inline-block;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 8px;
  border-radius: 50%;
  border: 6px solid #f98a29 ;
  border-color: #f98a29  transparent #f98a29  transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
.custom_loader { display: none; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 999; background: rgba(0,0,0, 0.5); }
.loader_inner { display: flex; align-items: center; justify-content: center; width: 100%; height: 100%; }
</style>

<div class="custom_loader" id="custom_loader">
    <div class="loader_inner">
        <div class="lds-dual-ring"></div>
    </div>
</div>
