<!-- Status popup -->
<style type="text/css">
    .status_info { padding-bottom: 40px;}
    .status_info_icon { width: 100%; text-align: center;}
    .status_info p { padding-bottom: 40px; text-align: center;}
    .status_info_icon span { margin: 0px 20px; font-size: 60px; display: inline-block;}
    .status_popup .custom_submit a   { max-width: 150px; margin: auto;}
    .status_popup .custom_submit { border-top: 1px solid #e9e4e4; padding-top: 30px;}
</style>

<div class="custom_popup status_popup" id="successAlert" style="display: none;">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h3>Success</h3>
                </div>
                <div class="status_info">
                    <p id="successMessageContainer"></p>
                    <div class="status_info_icon">
                        <span class="text-success"><i class="fa fa-check-circle-o" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="custom_submit">
                    <a href="javascript:void(0);" onclick="hideSuccessAlert()">Ok</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function showSuccessAlert(message) {
        $('#successMessageContainer').html(message);
        $('#successAlert').show();
    }

    function hideSuccessAlert() {
        $('#successAlert').hide();
        if ( '{{ session()->get('refreshPage') }}' ) {
            window.location.reload();
        }
    }
</script>
