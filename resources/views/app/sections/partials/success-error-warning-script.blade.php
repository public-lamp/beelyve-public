<script type="text/javascript">
    function successMsg(_msg) {
        window.toastr.success(_msg);
    }

    function errorMsg(_msg) {
        window.toastr.error(_msg);
    }

    function warningMsg(_msg) {
        window.toastr.warning(_msg);
    }
    @if( session()->has('success') )
        successMsg("{{ session()->get('success') }}");
    @endif

    @if(session()->has('error'))
        errorMsg("{{ session()->get('error') }}");
    @endif

    @if(session()->has('warning'))
        warningMsg("{{ session()->get('error') }}");
    @endif
</script>
