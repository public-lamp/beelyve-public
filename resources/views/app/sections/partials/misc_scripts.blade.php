<script type="text/javascript">
    {{-- remove elememnts using class name --}}
    function removeAlertElementsByClass(className){
        let removableElements = document.getElementsByClassName(className);
        while(removableElements.length > 0){
            removableElements[0].parentNode.removeChild(removableElements[0]);
        }
    }

    //    getBrowserTimeZone
    function getBrowserTimeZone() {
        let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        // save time zone in session
        $.ajax({
            method: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
            url: "{{ route('session.set-timezone') }}",
            data: { browserTimeZone: timeZone },
        });

        return timeZone;
    }

    @if( !session()->has('browserTimeZone') )
        getBrowserTimeZone();
    @endif
</script>
