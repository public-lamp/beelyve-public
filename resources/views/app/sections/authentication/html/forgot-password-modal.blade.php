<!-- forgot password popup -->
<div class="custom_popup" id="forgot_modal">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h2>Forgot Password</h2>
                    <p>Please enter your email address and we'll send you a link to reset your password</p>
                </div>
                <form id="forgotPasswordForm">
                    @csrf
                    <div class="popup_form">
                        <ul>
                            <li>
                                <div class="custom_field">
                                    <strong>Your Email</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" id="forgot_email" name="forgot_email" />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <br>
                    <div class="custom_submit">
                        <input type="submit" value="Submit" id="forgotPasswordBtn"/>
                    </div>
                </form>
                <div class="do_you_agree">
                    <strong>Back to <a href="javascript:void(0)" class="login_modal_link">Sign in</a></strong>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- forgot scripts section --}}
@include('app.sections.authentication.scripts.forgot-script')


