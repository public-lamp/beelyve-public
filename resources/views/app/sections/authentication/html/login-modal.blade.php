<!-- sign in popup -->
<div class="custom_popup @if(session()->has('loginAlertMessage')) addAlert @endif" style="" id="login_modal">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">

            {{-- login alert message --}}
            @if(session()->has('loginAlertMessage'))
                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                    <strong>Holy guacamole!</strong>{{ session()->get('loginAlertMessage') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h2>Log In</h2>
                </div>
                <form id="loginForm">
                    @csrf
                    <div class="popup_form">
                        <ul>
                            <li>
                                <div class="custom_field">
                                    <strong>Email</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" name="login_email" id="login_email" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="custom_field">
                                    <strong>Your Password</strong>
                                    <div class="custom_field_input">
                                        <input type="password" value="" name="login_password" id="login_password" />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="remember_forgot">
                        <div class="remember_me">
                            <label class="checkbox-label">
                                <input type="hidden" name="remember_me" value="0" />
                                <input type="checkbox" value="1" name="remember_me"> Remember Me
                                <span class="checkbox-custom rectangular"></span>
                            </label>
                        </div>
                        <div class="forgotPassw">
                            <a href="javascript:void(0)" class="forgot_modal_link">Forgot password?</a>
                        </div>
                    </div>

                    <div class="custom_submit">
                        <input type="submit" value="Sign in to your account" id="loginFormBtn" />
                    </div>
                </form>

                <div class="do_you_agree">
                    <strong>Do you have an account? <a href="javascript:void(0)" class="signup_modal_link">Sign up</a></strong>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- login scripts --}}
@include('app.sections.authentication.scripts.login-script')

