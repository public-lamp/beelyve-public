<!-- sign up popup -->
<div class="custom_popup" id="signup_modal">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h2>Sign Up</h2>
                </div>
                <form id="signupForm">
                    @csrf
                    <div class="popup_form">
                        <ul>
                            <li>
                                <div class="custom_field">
                                    <strong>Username</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" id="user_name" name="user_name" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="custom_field">
                                    <strong>Email</strong>
                                    <div class="custom_field_input">
                                        <input type="text" value="" id="email" name="email" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="custom_field">
                                    <strong>Your Password</strong>
                                    <div class="custom_field_input">
                                        <input type="password" value="" id="password" name="password" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="custom_field">
                                    <strong>Confirm Password</strong>
                                    <div class="custom_field_input">
                                        <input type="password" value="" id="password_confirmation" name="password_confirmation" />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="choose_type">
                        <div class="chooseOptionDetail">
                            <label>
                                <input type="radio" class="option-input radio" name="role_id" value="2" checked>
                                Talent
                            </label>
                        </div>
                        <div class="chooseOptionDetail">
                            <label>
                                <input type="radio" class="option-input radio" name="role_id" value="3">
                                {{ \App\Models\Auth\Role::where('id', 3)->first()->name }}
                            </label>
                        </div>
                    </div>
                    <div class="custom_submit">
                        <input type="submit" value="Create your account" id="signupFormBtn" />
                    </div>
                </form>
                <div class="do_you_agree">
                    <p>By clicking "Create Your Account", you agree to our <a href="{{ route('terms-conditions.get') }}">Terms & Conditions</a> and <a href="{{ route('privacy-policy.get') }}">Privacy Policy.</a></p>
                    <strong>Do you have an account?
                        <a href="javascript:void(0)" class="login_modal_link">Login</a>
                    </strong>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- signup ajax section --}}
@include('app.sections.authentication.scripts.signup-script')





