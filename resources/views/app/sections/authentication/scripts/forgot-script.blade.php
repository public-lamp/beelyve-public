
{{-- forgot-password scripts --}}
<script type="text/javascript">

    // check and show forgot modal
    function showForgotModal() {
        $('.custom_popup').hide();
        $(' #forgot_modal ').fadeIn();
    }
    @if( session()->remove('show_forgot_modal') )
        {{ session()->forget('show_forgot_modal') }}
        showForgotModal();
    @endif
    // Ends check and show forgot modal

</script>

<script>
    // page ajax start
    $(document).ready(function() {

        $('#forgotPasswordBtn').on('click', function (e) {
            // set input fields to normal
            $('.errorAlertText').html('');
            e.preventDefault();
            let formData = $('#forgotPasswordForm').serialize();
            blockUi( true );
            $.ajax({
                type: 'POST',
                url: "{{ route('forgot.password') }}",
                data: formData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        window.location.href = "{{ route('home') }}";
                        return toasterSuccess( true, response.message);
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if (value.includes(key)) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, value);
                    });
                }
            });
        });
    });

</script>
