{{-- login scripts --}}
<script type="text/javascript">

    // render and check teh login modal
    function showLoginModal() {
        $('.custom_popup').hide();
        $(' #login_modal ').fadeIn();
    }
    @if( session()->remove('show_login_modal') )
        showLoginModal();
    @endif

    // page ajax start
    $('#loginFormBtn').on('click', function (e) {
        // set input fields to normal
        $('.errorAlertText').html('');
        e.preventDefault();
        blockUi( true );
        let loginFormData = $('#loginForm').serializeArray();
        let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        loginFormData.push({ name: 'time_zone', value: timeZone });

        $.ajax({
            method: 'post',
            headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
            url: "{{ route('loginPost') }}",
            data: loginFormData,
            success: function (response) {
                blockUi( false );
                if ( !response.status ) {
                    return toasterSuccess( false, response.error);
                }
                toasterSuccess( true,  response.message);
                window.location.href = response.data.next_url;
            },
            error: function (data) {
                $.each(data.responseJSON.errors, function (key, value) {
                    blockUi( false );
                    let element = document.getElementById(key);
                    let errorAlertHtml = validationText(value, 'error');
                    $(element).after(errorAlertHtml);
                    if (value.includes(key)) {
                        $(element).val(value);
                    }
                    toasterSuccess( false, value);
                });
            }
        });

    });


</script>

