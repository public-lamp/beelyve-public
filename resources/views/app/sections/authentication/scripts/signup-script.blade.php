{{-- signup scripts --}}
<script type="text/javascript">

    // check and show signup modal
    function showSignupModal() {
        $('.custom_popup').hide();
        $(' #signup_modal ').fadeIn();
    }
    // check and show signup modal
    function hideSignupModal() {
        $('.custom_popup').hide();
    }
    @if( session()->remove('show_signup_modal') )
        showSignupModal();
    @endif
    // Ends check and show signup modal

    {{-- signup ajax --}}
    $('#signupFormBtn').on('click', function (e) {
        $('.errorAlertText').html('');
        e.preventDefault();
        blockUi( true );
        let signupFormData = $('#signupForm').serializeArray();
        let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        signupFormData.push({ name: 'time_zone', value: timeZone });

        $.ajax({
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
            url: "{{ route('signupPost') }}",
            data: signupFormData,
            success: function (response) {
                if (response.status) {
                    blockUi( false );
                    hideSignupModal();
                    showSuccessAlert(response.message);
                    {{ session()->flash('refreshPage', true) }}
                } else if (!response.status) {
                    blockUi( false );
                    toasterSuccess( false, response.error);
                }
            },
            error: function (data) {
                $.each(data.responseJSON.errors, function (key, value) {
                    blockUi( false );
                    let element = document.getElementById(key);
                    let errorAlertHtml = validationText(value, 'error');
                    $(element).after(errorAlertHtml);
                    if ( value.includes(key) ) {
                        $(element).val(value);
                    }
                });
            }
        });
    });
    {{-- Ends signup ajax --}}

</script>

