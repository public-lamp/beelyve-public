{{--session messages handling--}}
<script type="text/javascript">
    function successMsg(_msg) {
        window.toastr.success(_msg);
    }

    function errorMsg(_msg) {
        window.toastr.error(_msg);
    }

    function warningMsg(_msg) {
        window.toastr.warning(_msg);
    }

    @if( session()->has('success') )
        @if ( session()->has('showSuccessAlert') )
            showSuccessAlert("{{ session()->remove('success') }}");
        @else
            successMsg("{{ session()->get('success') }}");
        @endif
    @endif

    @if( session()->has('error') )
        @if ( session()->has('showErrorAlert') )
            showErrorAlert("{{ session()->remove('error') }}");
        @else
            errorMsg("{{ session()->get('error') }}");
        @endif
    @endif

    @if( session()->has('warning') )
        errorMsg("{{ session()->get('warning') }}");
    @endif

</script>

