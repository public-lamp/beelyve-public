{{-- weekly update section --}}
<div class="update_newsLetter_section">
    <div class="auto_container">
        <div class="update_newsLetter_detail">
            <div class="update_newsLetter_heading">
                <h2>Join our mailing list</h2>
                <p>Be the first to know about the newest stars and best deal on Beelyve</p>
            </div>

            <div class="update_newsLetter_field">
                <form id="subscribeMailListForm">
                    @csrf
                    <div class="mainSearch">
                        <div class="mainSearch_input">
                            <input type="text" name="subscriber_email" id="subscriber_email" value="" placeholder="Enter your email address..." />
                        </div>
                        <div class="mainSearch_bttn">
                            <input type="submit" value="SUBSCRIBE" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@section('ajax')
    {{-- mail list subscription script --}}
    <script type="text/javascript">
        // page ajax start
        $('#subscribeMailListForm').submit(function (e) {
            e.preventDefault();
            blockUi( true );
            let subscribeMailListData = $('#subscribeMailListForm').serializeArray();

            $.ajax({
                method: 'post',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('subscribe.mail-list.post') }}",
                data: subscribeMailListData,
                success: function (response) {
                    blockUi( false );
                    if ( response.status ) {
                        showSuccessAlert(response.message);
                        $('#subscriber_email').val(null); // empty email filed empty
                    }
                    else if( !response.status ) {
                        showErrorAlert(response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        showErrorAlert(value);
                    });
                }
            });
        });
    </script>
@endsection
