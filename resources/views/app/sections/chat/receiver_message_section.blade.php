<li>
    <div class="chat_list_info">
        <div class="chatAvatar">
            <div class="featureProfile_avatar">
                    <span>
                        <img src="{{ isset($message['otherUser']['image']) ? $message['otherUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                        <b class="sattus_online"></b>
                    </span>
                <strong>{{ $message['otherUser']['first_name']. ' ' .$message['otherUser']['last_name'] }}</strong>
            </div>
        </div>
        <div class="chat_conversation">
            <div class="chat_conversation_info user2">
                @if($message['messageType']['slug'] === 'file')
                    <a href="{{ $message['message'] }}">File</a>
                @elseif($message['messageType']['slug'] === 'image')
                    <div class="chat_message_img">
                        <img src="{{ $message['message'] }}" alt="img">
                    </div>
                @else
                    <strong>{{ $message['message'] }}</strong>
                @endif
            </div>
            <div class="chat_seen">
                <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
            </div>
        </div>
    </div>
</li>
