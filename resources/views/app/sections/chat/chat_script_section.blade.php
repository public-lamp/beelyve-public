
{{-- on page load --}}
<script type="text/javascript">
    $( document ).ready(function() {

        // scroll if have chat section
        let messagesList = document.getElementById('messagesList');
        if (messagesList) {
            messageScrollRelocate(100);
        }
    });
</script>

{{-- ajax and helping scripts --}}
<script type="text/javascript">

    {{-- enable and disable send message buttin --}}
    $('#text_message').on('change keyup', function() {
        let textAreaText = $.trim($('#text_message').val());
        if ( !textAreaText ) {
            disableSendMessageBtn();
        } else {
            enableSendMessageBtn();
        }
    });
    $('#message_file').on('change', function() {
        enableSendMessageBtn();
    });

    {{-- save message, stars --}}
    $('#sendMessageForm').on('submit', function (e) {
        e.preventDefault();
        let messageData =  new FormData( this );
        $.ajax({
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
            processData: false,
            contentType: false,
            cache: false,
            enctype: 'multipart/form-data',
            url: "{{ route('chat.message.save') }}",
            data: messageData,
            success: function (response) {
                if ( Boolean(response.status) ) {
                    $.each(response.data.lastMessagesHtml, function (key, value) {
                        appendMessage(value);
                    });
                    clearMessageField();
                    messageScrollRelocate();
                    disableSendMessageBtn();
                } else if ( !Boolean(response.status) ) {
                    blockUi( false );
                    toasterSuccess( false, response.error);
                }
            },
            error: function (data) {
                $.each(data.responseJSON.errors, function (key, value) {
                    toasterSuccess( false, value);
                });
            }
        });
    });

    {{-- append message in the messages list --}}
    function appendMessage(messageHtml) {
        $('#messagesList').append(messageHtml);
    }

    {{-- clear send-message input --}}
    function clearMessageField() {
        $('#text_message').val(null);
        $('#message_file').val(null);
        $('#file_name_container').html('');
    }

    {{-- disable send message btn --}}
    function disableSendMessageBtn() {
        $('#sendMessageBtn').attr('disabled', true);
        $('#sendMessageBtn').addClass('disabeled');
    }

    {{-- enable send message btn --}}
    function enableSendMessageBtn() {
        $('#sendMessageBtn').attr('disabled', false);
        $('#sendMessageBtn').removeClass('disabeled');
    }

    {{-- scroll controll to last message --}}
    function messageScrollRelocate(speed = 2000) {
        let messagesList = document.getElementById('messagesList');
        // messagesList.scrollTop = messagesList.scrollHeight;
        $('#messagesList').animate({scrollTop: messagesList.scrollHeight}, speed);
    }
</script>

{{-- pusher new messages event handling --}}
<script type="text/javascript">
    $( document ).ready(function() {
        subscribeToCurrentChatChannel();
    });

    function subscribeToCurrentChatChannel() {
        let appEnvironment = '{{ config('app.env') }}';
        if ( appEnvironment !== 'production' ) {
            Pusher.logToConsole = true;
        }

        let pusher = new Pusher('{{ config('services.pusher.key') }}', {
            cluster: '{{ config('services.pusher.cluster') }}',
            forceTLS: true
        });

        let activeConversationId = $('#active_conversation_input').val();
        let channelId = '{{ auth()->id() }}'+ "-" +activeConversationId;
        console.log(channelId);
        let channel = pusher.subscribe(channelId);

        channel.bind('newMessage', function (data) {
            $.each(data.newMessageData.messagesHtml, function (key, value) {
                appendMessage(value);
                messageScrollRelocate();
            });
        });
    }

</script>
