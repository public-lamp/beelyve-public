{{--chat messages section--}}
<div class="chat-modification">
    <div class="event_message_chat">
        <div class="searchChat">
            <div class="custom_field_input">
                <input type="text" value="">
                <b class="search_icon"><i class="fa fa-search" aria-hidden="true"></i></b>
            </div>
        </div>
        <div class="event_message_chat_list">
            <ul>
                @foreach($conversations as $conversation)
                    <li>
                        <a href="javascript:void(0)" id="{{ $conversation['id'] }}_conversation" class="@if($conversation['id'] == $lastActiveConversation['id'] ) active_chat @endif" onclick="loadConversationMessages({{ $conversation['id'] }})">
                            <div class="feartureProflie_info">
                                <div class="featureProfile_avatar">
                                    <span>
                                        <img src="{{ isset($conversation['otherUser']['image']) ? $conversation['otherUser']['image'] : asset('project-assets/images/avatar_white.png') }}" alt="img">
                                        <b class="sattus_online"></b>
                                    </span>
                                </div>
                                <div class="featureProfile_avatar_text">
                                    <strong>{{ isset($conversation['otherUser']['first_name']) ? $conversation['otherUser']['first_name']. ' ' .$conversation['otherUser']['last_name'] : $conversation['otherUser']['user_name']  }}</strong>
                                    @if( isset($conversation['lastMessage']) )
                                        <small>
                                            @if(intval($conversation['lastMessage']['message_type_id']) === 3) <!-- file -->
                                                @if($conversation['lastMessage']['current_user_is_sender'])
                                                    Sent attachment
                                                @else
                                                    Received attachment
                                                @endif
                                            @elseif(intval($conversation['lastMessage']['message_type_id']) === 2) <!-- image -->
                                                @if($conversation['lastMessage']['current_user_is_sender'])
                                                    Sent image
                                                @else
                                                    Received image
                                                @endif
                                            @else  <!-- text -->
                                                {{ $conversation['lastMessage']['message'] }}
                                            @endif
                                        </small>
                                    @else
                                        <small>No chat history</small>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="event_activity_fl">
        <div class="activity_chat_box">
            {{-- other converser info --}}
            <div class="event_order_head">
                <div class="event_order_avatar">
                    <div class="chatAvatar">
                        <div class="featureProfile_avatar">
                        <span>
                            <img src="{{ isset($lastActiveConversation['otherUser']['image']) ? $lastActiveConversation['otherUser']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="img">
                            <b class="sattus_online"></b>
                        </span>
                        </div>
                    </div>
                    <strong>{{ $lastActiveConversation['otherUser']['first_name']. ' ' .$lastActiveConversation['otherUser']['last_name'] }}
                        <b class="showOnline">Online</b>
                    </strong>
                </div>

                <div class="order_menu_bttn">
                    <small><img src="{{ asset('project-assets/images/menu_elipsis.png') }}" alt="img"></small>
                </div>
            </div>
            <div class="activity_chat_box_info" id="messagesOuterContainer">
                <div class="chat_list">
                    <ul id="messagesList">
                        @foreach( $lastActiveConversationMessages as $message )
                            @if($message['current_user_is_sender'])
                                <li>
                                    <div class="chat_list_info user1">
                                        <div class="chatAvatar">
                                            <div class="featureProfile_avatar">
                                            <span>
                                                <img src="{{ isset($message['currentUser']['image']) ? $message['currentUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                <b class="sattus_online"></b>
                                            </span>
                                                <strong>You</strong>
                                            </div>
                                        </div>
                                        <div class="chat_conversation">
                                            @if($message['messageType']['slug'] === 'file')
                                                <a href="{{ $message['message'] }}">File</a>
                                            @elseif($message['messageType']['slug'] === 'image')
                                                <div class="chat_message_img">
                                                    <img src="{{ $message['message'] }}" alt="img">
                                                </div>
                                            @else
                                                <div class="chat_conversation_info">
                                                    <strong>{{ $message['message'] }}</strong>
                                                </div>
                                            @endif
                                            <div class="chat_seen">
                                                <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li>
                                    <div class="chat_list_info">
                                        <div class="chatAvatar">
                                            <div class="featureProfile_avatar">
                                            <span>
                                                <img src="{{ isset($message['otherUser']['image']) ? $message['otherUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                <b class="sattus_online"></b>
                                            </span>
                                                <strong>{{ $message['otherUser']['first_name']. ' ' .$message['otherUser']['last_name'] }}</strong>
                                            </div>
                                        </div>
                                        <div class="chat_conversation">
                                            @if($message['messageType']['slug'] === 'file')
                                                <a href="{{ $message['message'] }}">File</a>
                                            @elseif($message['messageType']['slug'] === 'image')
                                                <div class="chat_message_img">
                                                    <img src="{{ $message['message'] }}" alt="img">
                                                </div>
                                            @else
                                                <div class="chat_conversation_info user2">
                                                    <strong>{{ $message['message'] }}</strong>
                                                </div>
                                            @endif
                                            <div class="chat_seen">
                                                <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <form id="sendMessageForm">
                <div class="chat_form">
                    <input type="hidden" name="conversation_id" id="active_conversation_input" value="{{ $lastActiveConversation['id'] }}"/>
                    <div class="chat_form_message">
                        <textarea name="text_message" id="text_message" placeholder="Message..."rows="2"></textarea>
                    </div>
                    <div class="upload_logo">
                        <b id="file_name_container"></b>
                        <label for="">
                            <input type="file" name="message_file" id="message_file" onchange="readURL(this);" accept="image/x-png,image/gif,image/jpeg,image/jpg" />
                            <img src="{{ asset('project-assets/images/addFile_icon.png') }}" alt="img" />
                        </label>
                    </div>
                    <div class="chat_form_bttn">
                        <button type="submit" class="default_bttn disabeled" id="sendMessageBtn" disabled>Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- load conversation messages --}}
<script type="text/javascript">

    {{-- update conversation messages --}}
    function updateConversationMessages(messagesHtml) {
        $('#messagesList').html(messagesHtml);
    }

    {{-- make currently selected conversation as active conversation --}}
    function makeCurrentConversationSelected(conversationId) {
        if ($('#'+ conversationId +'_conversation').hasClass('active_chat')) {
            return false;
        } else {
            $('.event_message_chat_list ul li a').removeClass('active_chat'); // remove active-chat from all the conversation list
            $('#'+ conversationId +'_conversation').addClass('active_chat'); // add active-chat class to current conversation list
        }
    }

    // check current conversation is active and return boolean
    function checkConversationIsActive(conversationId) {
        return !!$('#' + conversationId + '_conversation').hasClass('active_chat');
    }

    // load the currently selected conversation messages
    function loadConversationMessages(conversationId) {
        if (checkConversationIsActive(conversationId)) {
            return false;
        } else {
            makeCurrentConversationSelected(conversationId);
            const loadConversationData = {conversation_id: conversationId};
            blockUi( true );
            $.ajax({
                type: 'GET',
                url: "{{ route('conversation.load') }}",
                data: loadConversationData,
                success: function (response) {
                    blockUi( false );
                    if ( response.status ) {
                        let conversationMessagesHtml = response.data.conversationMessagesHtml;
                        updateConversationMessages(conversationMessagesHtml);
                        messageScrollRelocate(200);
                    }
                    else if ( !response.status ) {
                        toasterSuccess( false, response.error);
                    }
                    else {
                        toasterSuccess( false, 'Something went wrong, please try again');
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        // blockUi( false );
                        toasterSuccess( false, value);
                    });
                }
            });
        }
    }

</script>
