{{--conversation section--}}
<div class="banner_section">
    <div class="banner_slider_section bannerOverlay">
        <span class="banner_img"><img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="img" /></span>

        <div class="banner_info">
            <div class="auto_container">
                <div class="banner_info_detail">
                    <h1>Messages</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="">
    <div class="auto_container">
        <div class="dasbord_container_detail messagePage">

            <div class="db_tittle">
                <div class="db_tittle_heading">
                    <h4>Message List</h4>
                </div>
                <div class="sorted_dropdown dropdown_sort">
                    <div class="custom_dropdown">
                        <select>
                            <option>Sort by: Date</option>
                            <option>Sort by: Date 1</option>
                            <option>Sort by: Date 2</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="message_section">
                <ul>
                    <li>
                        <div class="message_section_info">
                            <div class="feartureProflie_info">
                                <div class="featureProfile_avatar">
                                    <span>
                                        <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                        <b class="sattus_online"></b>
                                    </span>
                                </div>
                                <div class="featureProfile_avatar_text">
                                    <strong>Jack Black</strong>
                                    <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="message_section_info">
                            <div class="feartureProflie_info">
                                <div class="featureProfile_avatar">
                                    <span>
                                        <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                        <b class="sattus_online"></b>
                                    </span>
                                </div>
                                <div class="featureProfile_avatar_text">
                                    <strong>Jack Black</strong>
                                    <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="message_section_info">
                            <div class="feartureProflie_info">
                                <div class="featureProfile_avatar">
                                    <span>
                                        <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                        <b class="sattus_online"></b>
                                    </span>
                                </div>
                                <div class="featureProfile_avatar_text">
                                    <strong>Jack Black</strong>
                                    <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="message_section_info">
                            <div class="feartureProflie_info">
                                <div class="featureProfile_avatar">
                                    <span>
                                        <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                        <b class="sattus_online"></b>
                                    </span>
                                </div>
                                <div class="featureProfile_avatar_text">
                                    <strong>Jack Black</strong>
                                    <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="message_section_info">
                            <div class="feartureProflie_info">
                                <div class="featureProfile_avatar">
                                    <span>
                                        <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                        <b class="sattus_online"></b>
                                    </span>
                                </div>
                                <div class="featureProfile_avatar_text">
                                    <strong>Jack Black</strong>
                                    <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
