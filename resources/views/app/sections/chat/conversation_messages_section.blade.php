{{-- conversation messages list--}}
@foreach( $messages as $message )
    @if($message['current_user_is_sender'])
        <li>
            <div class="chat_list_info user1">
                <div class="chatAvatar">
                    <div class="featureProfile_avatar">
                        <span>
                            <img src="{{ isset($message['currentUser']['image']) ? $message['currentUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                            <b class="sattus_online"></b>
                        </span>
                        <strong>You</strong>
                    </div>
                </div>
                <div class="chat_conversation">
                    @if($message['messageType']['slug'] === 'file')
                        <a href="{{ $message['message'] }}">File</a>
                    @elseif($message['messageType']['slug'] === 'image')
                        <div class="chat_message_img">
                            <img src="{{ $message['message'] }}" alt="img">
                        </div>
                    @else
                        <div class="chat_conversation_info">
                            <strong>{{ $message['message'] }}</strong>
                        </div>
                    @endif
                    <div class="chat_seen">
                        <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
                    </div>
                </div>
            </div>
        </li>
    @else
        <li>
            <div class="chat_list_info">
                <div class="chatAvatar">
                    <div class="featureProfile_avatar">
                        <span>
                            <img src="{{ isset($message['otherUser']['image']) ? $message['otherUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                            <b class="sattus_online"></b>
                        </span>
                        <strong>{{ $message['otherUser']['first_name']. ' ' .$message['otherUser']['last_name'] }}</strong>
                    </div>
                </div>
                <div class="chat_conversation">
                    @if($message['messageType']['slug'] === 'file')
                        <a href="{{ $message['message'] }}">File</a>
                    @elseif($message['messageType']['slug'] === 'image')
                        <div class="chat_message_img">
                            <img src="{{ $message['message'] }}" alt="img">
                        </div>
                    @else
                        <div class="chat_conversation_info user2">
                            <strong>{{ $message['message'] }}</strong>
                        </div>
                    @endif
                    <div class="chat_seen">
                        <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
                    </div>
                </div>
            </div>
        </li>
    @endif
@endforeach
