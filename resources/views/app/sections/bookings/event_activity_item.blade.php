{{-- event activity list item --}}
@if(auth()->user()->isModerator())
    <li>
        <div class="activity_info_status">
            <div class="status_info">
                <span class="@if(in_array($eventActivity['activity_type_id'], [1, 2])) status_inactive @else status_active @endif"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
            </div>
            <div class="activity_info_perform_outer">
                <div class="activity_info_perform">
                    <strong>{{ $eventActivity['activityType']['about'] }}</strong>
                    <span>{{ parseTimeWithTimeZone($eventActivity['performed_at'], auth()->user()->time_zone) }}</span>
                </div>
{{--                <span class="float-right mr-4 mt-1">{{ formatDateTime($eventActivity['performed_at'], 'D m, Y') }}</span>--}}
                <span class="float-right mr-4 mt-1">{{ parseDateWithTimeZone($eventActivity['performed_at'], auth()->user()->time_zone) }}</span>
            </div>
        </div>
    </li>
@elseif(auth()->user()->isTalent())
    <li>
        <div class="activity_info_status">
            <div class="status_info">
                <span class="@if(in_array($eventActivity['activity_type_id'], [3,4, 5, 7])) status_inactive @else status_active @endif"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
            </div>
            <div class="activity_info_perform_outer">
                <div class="activity_info_perform">
                    <strong>{{ $eventActivity['activityType']['about'] }}</strong>
                    <span>{{ parseTimeWithTimeZone($eventActivity['performed_at'], auth()->user()->time_zone) }}</span>
                </div>
                <span class="float-right mr-4 mt-1">{{ parseDateWithTimeZone($eventActivity['performed_at'], auth()->user()->time_zone) }}</span>
            </div>
        </div>
    </li>
@else
    <li>
        <div class="activity_info_status">
            <div class="status_info">
                <span class="status_inactive"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
            </div>
            <div class="activity_info_perform_outer">
                <div class="activity_info_perform">
                    <strong>{{ $eventActivity['activityType']['about'] }}</strong>
                    <span>{{ parseTimeWithTimeZone($eventActivity['performed_at'], auth()->user()->time_zone) }}</span>
                </div>
                <span class="float-right mr-4 mt-1">{{ parseDateWithTimeZone($eventActivity['performed_at'], auth()->user()->time_zone) }}</span>
            </div>
        </div>
    </li>
@endif
