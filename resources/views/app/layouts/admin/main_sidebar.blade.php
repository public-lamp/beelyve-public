{{-- sidebar --}}
<div class="db_nav">
    <div class="db_nav_inner">
        <div class="db_nav_menu">
            <ul>
                <li class="@if( url()->current() == route('admin.users')) active_nav @endif"><a href="{{ route('admin.users') }}" class="ftl_icon"><b><i class="fa fa-users" aria-hidden="true"></i></b>Users<i></i></a></li>
                <li class="@if( url()->current() == route('admin.bookings-list')) active_nav @endif"><a href="{{ route('admin.bookings-list') }}" class="wallet_icon"><b><i class="fa fa-list-ol" aria-hidden="true"></i></b>Bookings<i></i></a></li>
                <li class="@if( url()->current() == route('admin.mail-subscribers')) active_nav @endif"><a href="{{ route('admin.mail-subscribers') }}" class="wallet_icon"><b><i class="fa fa-envelope" aria-hidden="true"></i></b>Mail Subscribers<i></i></a></li>
                <li class="@if( url()->current() == route('admin.subscribers.send-email')) active_nav @endif"><a href="{{ route('admin.subscribers.send-email') }}" class="wallet_icon"><b><i class="fa fa-paper-plane" aria-hidden="true"></i></b>Send Subscribers Email<i></i></a></li>
                <li class="@if( url()->current() == route('renderAdminPayoutsListing')) active_nav @endif"><a href="{{ route('renderAdminPayoutsListing') }}" class="wallet_icon"><b><i class="fa fa-money" aria-hidden="true"></i></b>Payouts<i></i></a></li>
                <li class="@if( url()->current() == route('renderAdminPayoutsExceptionListing')) active_nav @endif"><a href="{{ route('renderAdminPayoutsExceptionListing') }}" class="wallet_icon"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></b>Payouts Exceptions<i></i></a></li>
                <li class="@if( url()->current() == route('admin.service-charges.setting')) active_nav @endif"><a href="{{ route('admin.service-charges.setting') }}" class="wallet_icon"><b><i class="fa fa-cog" aria-hidden="true"></i></b>Service Charges Setting<i></i></a></li>
                <li><a href="{{ route('logout') }}" class="wallet_icon"><b><i class="fa fa-sign-out" aria-hidden="true"></i></b>Log Out</a></li>
            </ul>
        </div>
    </div>
</div>
