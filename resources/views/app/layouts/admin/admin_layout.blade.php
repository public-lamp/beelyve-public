<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <meta name="_token" content="{{ csrf_token() }}">
    {{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    @yield('meta')

    <title>{{ config('app.name') }} | @yield('title')</title>

    {{--    favicon--}}
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('project-assets/images/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('project-assets/css/admin_dashboard.css') }}">

    @yield('page-styles')

    <script type="text/javascript" src="{{ asset('project-assets/js/jquery-3.4.1.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/bootstrap.min.js') }}" ></script>
    @yield('page-scripts')

    @include('app.styles')

    {{--    datatables CSS cdns --}}
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.1.4/css/rowGroup.dataTables.min.css">
</head>

<body class="db_body">

<div class="wrapper">
    @yield('page-header')

    @yield('main-content')

    @yield('page-footer')

    {{-- custom loader complete html with styles --}}
    @include('app.sections.partials.custom-loader')
</div>

{{-- main scripts --}}
@include('app.scripts')

{{--    datatables JS cdns --}}
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/rowgroup/1.1.4/js/dataTables.rowGroup.min.js"></script>

@include('app.footers.misc-project-sections')

{{-- custom success alert --}}
@include('app.sections.partials.custom_success_alert')
{{-- custom warning alert --}}
@include('app.sections.partials.custom_warning_alert')
{{-- custom error alert --}}
@include('app.sections.partials.custom_error_alert')

{{-- page misc scripts section --}}
@yield('page-misc-scripts')

{{--page included sections and components--}}
@yield('rendered-sections')

{{--page ajax section--}}
@yield('ajax')

{{-- session messages & alerts handling --}}
@include('app.sections.footers.popup-messages-section')

{{-- misc. scripts to use in every page --}}
@include('app.sections.partials.misc_scripts')

{{-- scripts that removes unnecessary url parts --}}
@include('app.sections.partials.url_filter')

</body>

</html>
