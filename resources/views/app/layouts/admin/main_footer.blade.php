<footer>
    <div class="footer fixedFooter db_footer">
        <div class="footerDown">
            <div class="auto_container">
                <div class="footerDown_detail">
                    <div class="copyRight2">
                        <strong>&#169;2021-{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>, all rights reserved.</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
