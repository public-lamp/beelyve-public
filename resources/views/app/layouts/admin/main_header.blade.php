<header>
    <div class="dashboardHeader">
        <div class="auto_container">
            <div class="dashboardHeader_detail">
                <div class="dashboardHeader_data">
                    <div class="db_logo">
                        {{-- <a href="{{ config('app.url').'/' }}"><img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="logo" /></a> --}}
                        <a href="javascript:void(0);"><img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="logo" /></a>
                    </div>
                    <div class="db_tittle2">
                        <h3><a href="{{ route('admin-dashboard') }}">Dashboard</a></h3>
                    </div>

                    <div class="menuIcon"></div>
                    <a href="javascript:void(0)" class="closeNav">&nbsp;</a>

                    <div class="profile_menu">
                        <div class="dropdownMenu sett_profile_db">
                            <div class="profile_img ">
                                <span>
                                    <img src="{{ isset(auth()->user()->image) ? auth()->user()->image : asset('project-assets/images/profile_avatar.png') }}" alt="admin-avatar">
                                </span>
                            </div>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)">{{ auth()->user()->full_name }}</a>
                                    <ul>
                                        <li><a href="{{ route('admin.profile') }}">profile</a></li>
                                        <li><a href="{{ route('logout') }}">log out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
