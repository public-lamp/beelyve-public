
<script type="text/javascript" src="{{ asset('project-assets/js/jquery-3.4.1.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('project-assets/js/bootstrap.min.js') }}" ></script>
<script type="application/octet-stream" src="{{ asset('project-assets/js/bootstrap.min.js.map') }}" ></script>
<script type="text/javascript" src="{{ asset('project-assets/js/owl.carousel.js') }}" ></script>
<script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" ></script>
<script type="text/javascript" src="{{ asset('project-assets/js/jquery-ui.js') }}" ></script>
{{-- jQuery timepicker --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.js"></script>
{{-- sweat alert --}}
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.js" crossorigin="anonymous" defer></script>--}}
{{-- toastr script --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="application/octet-stream" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.js.map"></script>
{{-- blockui script --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

{{-- project index script --}}
<script type="text/javascript" src="{{ asset('project-assets/js/myscript.js') }}" ></script>


