<header>
    {{-- simple navbar, with home page link only --}}
    <div class="header">
        <div class="auto_container">
            <div class="header_detail">
                <div class="header_detail_inner">
                    <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('project-assets/images/logo.png') }}" alt="#" class="logo_web" />
                        <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="#" class="logo_mbile" />
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
