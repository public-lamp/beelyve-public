<header>
    {{-- navbar --}}
    <div class="header">
        <div class="auto_container">
            <div class="header_detail dbheader_sett">

                <div class="header_detail_inner">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('project-assets/images/logo.png') }}" alt="image" class="logo_web" />
                            <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="image" class="logo_mbile" />
                        </a>
                    </div>

                    <div class="headerInfo_inner">
                    <div class="menuIcon"></div>
                        <div class="menu_nav">
                            <a href="javascript:void(0)" class="closeNav">&nbsp;</a>
                            <ul>
                                <li><a href="{{ route('moderator-dashboard') }}" class=" @if( url()->current() == route('moderator-dashboard') ) nav_active @endif ">Dashboard</a></li>
                                <li><a href="{{ route('moderator.bookings.search', ['booking_status' => 'upcoming']) }}" class="@if( url()->current() == route('moderator.bookings.search') ) nav_active @endif">Manage Bookings</a></li>
                                <li><a href="{{ route('moderator.bookings.requests', ['booking_status' => 'requested']) }}" class="@if( url()->current() == route('moderator.bookings.requests') ) nav_active @endif">Booking Requests</a></li>
                                <li><a href="{{ route('moderator.chat.messages') }}" class="@if( url()->current() == route('moderator.chat.messages') ) nav_active @endif">Messages</a></li>
                                <li><a href="{{ route('moderator.favourite-users') }}" class="@if( url()->current() == route('moderator.favourite-users') ) nav_active @endif">Favorites</a></li>
                                <li><a href="{{ route('search-talent') }}" class=" @if( url()->current() == route('search-talent')) nav_active @endif ">Find Talent</a></li>
                            </ul>
                        </div>
                        <div class="nav_notification_outer">
                            <div class="profile_info">
                                <span>
                                    <b><img src="{{ isset( auth()->user()->image ) ? auth()->user()->image : asset('project-assets/images/avatar_white.png') }}" alt="user-image" /></b>
                                    <small>{{ auth()->user()->user_name }}</small>
                                    <i><img src="{{ asset('project-assets/images/chevron_down.png') }}" alt="avatar" /></i>
                                </span>
                                <div class="profileDropdown">
                                    <ul>
                                        <li><a href="{{ route('moderator.profile') }}">Profile</a></li>
                                        <li><a href="{{ route('home') }}">Home</a></li>
                                        <li><a href="{{ route('logout') }}">Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>

