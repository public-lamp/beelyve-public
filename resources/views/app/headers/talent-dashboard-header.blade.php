<header>
    <div class="header talend_dashboard">
        <div class="auto_container">
            <div class="header_detail">
                <div class="header_detail_inner">
                    <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('project-assets/images/logo.png') }}" alt="image" class="logo_web" />
                        <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="image" class="logo_mbile" />
                    </a>
                    </div>

                    <div class="headerInfo_inner">
                        <div class="menuIcon"></div>
                        <div class="menu_nav">
                            <a href="javascript:void(0)" class="closeNav">&nbsp;</a>
                            <ul>
                                <li><a href="{{ route('talent-dashboard') }}" class="@if( url()->current() == route('talent-dashboard') ) nav_active @endif">Dashboard</a></li>
                                <li><a href="{{ route('talent.bookings.search', ['booking_status' => 'upcoming']) }}" class="@if( url()->current() == route('talent.bookings.search') ) nav_active @endif">Manage Orders</a></li>
                                <li><a href="{{ route('talent.bookings.requests', ['booking_status' => 'requested']) }}" class="@if( url()->current() == route('talent.bookings.requests') ) nav_active @endif">Booking Requests</a></li>
                                <li><a href="{{ route('talent.chat.messages') }}" class="@if( url()->current() == route('talent.chat.messages') ) nav_active @endif">Messages</a></li>
                            </ul>
                        </div>
                        <div class="nav_notification_outer">
                            {{--                            <div class="notiication_info">--}}
                            {{--                                <a href="javascript:void(0)">--}}
                            {{--                                    <i class="fa fa-bell-o" aria-hidden="true"></i>--}}
                            {{--                                    <b>0</b>--}}
                            {{--                                </a>--}}
                            {{--                            </div>--}}
                            <div class="profile_info">
                                    <span>
                                        <b><img src="{{ isset( auth()->user()->image ) ? auth()->user()->image : asset('project-assets/images/avatar_white.png') }}" alt="user-image" /></b>
                                        <small>{{ auth()->user()->user_name }}</small>
                                        <i><img src="{{ asset('project-assets/images/chevron_down.png') }}" alt="user-image" /></i>
                                    </span>
                                <div class="profileDropdown">
                                    <ul>
                                        <li><a href="{{ route('talent.profile.edit') }}">Profile</a></li>
                                        <li><a href="{{ route('home') }}">Home</a></li>
                                        <li><a href="{{ route('logout') }}">Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
