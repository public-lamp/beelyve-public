
@if( auth()->check() )

    {{-- talent header --}}
    @if( auth()->user()->isTalent() )
        @include('app.headers.talent-auth-header')
    @endif

    {{-- moderator header --}}
    @if( auth()->user()->isModerator() )
        @include('app.headers.moderator-auth-header')
    @endif

    {{-- admin header --}}
    @if( auth()->user()->isAdmin() )
        <header>
            @include('app.sections.headers.admin_front_nav')
        </header>
    @endif

@else
    {{-- main header content --}}
    <header>
        @include('app.sections.headers.main-navbar')
    </header>
@endif
