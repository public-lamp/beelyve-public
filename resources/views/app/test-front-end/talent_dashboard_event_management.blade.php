<!doctype html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <title>Moderator</title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/bootstrap.min.css') }}">
    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/style.css') }}">
    <link rel="stylesheet " href="css/responcive.css "> 
    <!-- font awesome-->
    <link rel="stylesheet " href="{{ asset('project-assets/css/font-awesome.min.css') }}"> 


    <script src="js/jquery-3.4.1.min.js"></script>


</head>

<body>
    <div class="wrapper">
        <header>
            <div class="header">
                <div class="auto_container">
                    <div class="header_detail">
                        <div class="header_detail_inner">
                            <div class="logo">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('project-assets/images/logo.png') }}" alt="#" class="logo_web" /> 
                                <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="#" class="logo_mbile" />
                            </a>
                            </div>

                            <div class="menu_nav">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#" class="nav_active">Manage Order</a></li>
                                    <li><a href="#">Booking Request</a></li>
                                    <li><a href="#">Message</a></li> 
                                </ul>
                            </div>

                            <div class="nav_authantication_bttns"  style="display: none;">
                                <ul>
                                    <li><a href="#" class="login_bttn">Log In</a></li>
                                    <li><a href="#" class="signUp_bttn">Sign Up</a></li>
                                </ul>
                            </div>

                            <div class="nav_notification_outer">

                                <div class="notiication_info">
                                    <a href="#">
                                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                                        <b>2</b>
                                    </a>
                                </div>

                                <div class="profile_info">
                                    <span>
                                        <b><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></b>
                                        <small>Angela</small>
                                        <i><img src="{{asset('project-assets/images/chevron_down.png')}}" alt="#" /></i>
                                    </span>
                                    <div class="profileDropdown">
                                        <ul>
                                            <li><a href="#">Profile </a></li>
                                            <li><a href="#">Settings</a></li>
                                            <li><a href="#">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main>
            <div class="main_container">
 
                <div class="event_page db_event_page dasbord_container">
                    <div class="auto_container">
                        <div class="dasbord_container_detail ">

                            <div class="db_tittle">
                                <div class="db_tittle_heading">
                                    <h5>Event #1234/S</h5>
                                </div>
                                <div class="our_earning">
                                    <strong>April 6, 9:00 pm</strong>
                                </div>
                            </div>


                            <div class="event_section_head">
                                <div class="db_profile "> 
                                    <div class="featureWeek_info"> 
                                        <div class="feartureProflie_info">
                                            <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ asset('project-assets/images/profile_avatar.png')}}" alt="#">  
                                                </span>
                                            </div> 
                                        </div>
                                        <div class="featureProfile_avatar_inner">
                                            <div class="featureProfile_avatar_text">
                                                <strong>Angela Moss</strong>
                                                <small>Photographer</small>
                                            </div>
                                        </div>
                                    
                                    </div> 
                                </div>

                                <div class="event_section_head_right">

                                    <div class="duration_timer">
                                        <label>Duration 30 mints</label>
                                        <ul>
                                            <li>
                                                <span>00</span>
                                                <strong>Hours</strong>
                                            </li>
                                            <li>
                                                <span>30</span>
                                                <strong>Minutes</strong>
                                            </li>
                                            <li>
                                                <span>00</span>
                                                <strong>Seconds</strong>
                                            </li>
                                        </ul>
                                    </div>


                                    <div class="dispute_alert">
                                        <span>
                                            <img src="{{ asset('project-assets/images/alert_icon.png')}}" alt="#" />
                                            Dispute
                                        </span>
                                    </div>
                                    
                                </div>




                            </div>


                            <div class="db_event_bttns">
                                
                                <div class="event_bttns">
                                    <a href="#" class="default_bttn  approve_bttn">I’m Present </a>
                                    <a href="#" class="default_bttn ">Time Complete   </a>
                                </div>
                            </div>


                            <div class="event_activity_section">
                                <div class="event_activity_fl">
                                    <div class="event_activity_tabs">
                                        <ul>
                                            <li><a href="#" class="activity_active">Activity</a></li>
                                            <li><a href="#">Service Details</a></li>
                                            <li><a href="#">Moderator Rules</a></li>
                                        </ul>
                                    </div>

                                    <div class="activity_chat_box">
                                        <div class="activity_chat_box_info">
                                            <label class="activity_date">
                                                April 6
                                            </label>

                                            <div class="activity_info_list">
                                                <ul>
                                                    <li>
                                                        <div class="activity_info_status">
                                                            <div class="status_info">
                                                                <span class="status_active"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                            </div>

                                                            <div class="activity_info_perform_outer">
                                                                <div class="activity_info_perform">
                                                                    <strong>Talent is Present</strong>
                                                                    <span>9:00  <i>am</i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="activity_info_status">
                                                            <div class="status_info">
                                                                <span class="status_inactive"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                            </div>

                                                            <div class="activity_info_perform_outer">
                                                                <div class="activity_info_perform">
                                                                    <strong>Moderator Conform </strong>
                                                                    <span>9:01  <i>am</i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="activity_info_status">
                                                            <div class="status_info">
                                                                <span class="status_inactive"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                            </div>

                                                            <div class="activity_info_perform_outer">
                                                                <div class="activity_info_perform">
                                                                    <strong>Time Started</strong>
                                                                    <span>9:01  <i>am</i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
  

                                            <div class="chat_list">
                                                <ul>
                                                    <li>
                                                        <div class="chat_list_info">
                                                            <div class="chatAvatar">
                                                                <div class="featureProfile_avatar">
                                                                    <span>
                                                                        <img src="{{ asset('project-assets/images/profile_avatar.png')}}" alt="#">
                                                                        <b class="sattus_online"></b>
                                                                    </span>
                                                                    <strong>Bobby</strong>
                                                                </div>
                                                            </div>
                                                            <div class="chat_conversation">
                                                                <div class="chat_conversation_info error_msg">
                                                                    <strong>Sorry lost connection, please wait me 1 mint</strong>
                                                                </div>
                                                                <div class="chat_seen">
                                                                    <strong>Today, 10:30 am</strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>



                                                    <li>
                                                        <div class="chat_list_info user1">
                                                            <div class="chatAvatar">
                                                                <div class="featureProfile_avatar">
                                                                    <span>
                                                                        <img src="{{ asset('project-assets/images/profile_avatar.png')}}" alt="#">
                                                                        <b class="sattus_online"></b>
                                                                    </span>
                                                                    <strong>You</strong>
                                                                </div>
                                                            </div>
                                                            <div class="chat_conversation">
                                                                <div class="chat_conversation_info ">
                                                                    <strong>Ok i will wait</strong>
                                                                </div>
                                                                <div class="chat_seen">
                                                                    <strong>Today, 10:30 am</strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="chat_list_info">
                                                            <div class="chatAvatar">
                                                                <div class="featureProfile_avatar">
                                                                    <span>
                                                                        <img src="{{ asset('project-assets/images/profile_avatar.png')}}" alt="#">
                                                                        <b class="sattus_online"></b>
                                                                    </span>
                                                                    <strong>Bobby</strong>
                                                                </div>
                                                            </div>
                                                            <div class="chat_conversation">
                                                                <div class="chat_conversation_info ">
                                                                    <strong>Sorry lost connection</strong>
                                                                </div>
                                                                <div class="chat_seen">
                                                                    <strong>Today, 10:30 am</strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
 
                                        </div>

                                        <div class="chat_form">
                                            <div class="chat_form_message">
                                                <textarea placeholder="Message..."rows="2"></textarea>
                                            </div>
                                            <div class="chat_form_bttn">
                                                <button type="submit" class="default_bttn">Send</button>
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>
 
                            </div>
 
                             
                        </div>
                    </div>
                </div>
 

                
            </div>
        </main>


        <footer>
            <div class="footer_outer">  

                <div class="copyRight">
                    <div class="auto_container">
                        <div class="copyRight_info">
                            <p>© 2020 All Rights Reserved <a href="#">Moderator live</a> </p>
                        </div>
                    </div>
                </div>

            </div>
        </footer>





          <!-- dispute popup -->


 <div class="custom_popup rateReview_popup">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h3>Dispute</h3> 
                </div>  
                

                <div class="popup_form">
                    <ul>  

                        <li>
                            <div class="custom_field">
                                <strong>Query</strong>
                                <div class="custom_field_textarea">
                                    <textarea></textarea>
                                </div>
                            </div>
                        </li> 
                    </ul>
                </div>
  
                 

                <div class="custom_submit">
                    <input type="submit" value="Submit" />
                </div> 

            </div>
        </div>
    </div>
</div>




 


    </div>





    <script src="{{ asset('project-assets/js/myscript.js') }}"></script> 
    <!-- bootstrap -->
    <script src="j{{ asset('project-assets/js/bootstrap.min.js') }}"></script> 




</body>

</html> 