<!doctype html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <title>Moderator</title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/bootstrap.min.css') }}">
    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/style.css') }}">
    <link rel="stylesheet " href="{{ asset('project-assets/css/responsive.css') }}"> 
    <!-- font awesome-->
    <link rel="stylesheet " href="{{ asset('project-assets/css/font-awesome.min.css') }}"> 


 


</head>

<body>
    <div class="wrapper">
        <header>
            <div class="header">
                <div class="auto_container">
                    <div class="header_detail">
                        <div class="header_detail_inner">
                            <div class="logo">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('project-assets/images/logo.png') }}" alt="#" class="logo_web" /> 
                                <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="#" class="logo_mbile" />
                            </a>
                            </div>

                            <div class="menu_nav">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#" class="nav_active">Manage Order</a></li>
                                    <li><a href="#">Booking Request</a></li>
                                    <li><a href="#">Message</a></li> 
                                </ul>
                            </div>

                            <div class="nav_authantication_bttns"  style="display: none;">
                                <ul>
                                    <li><a href="#" class="login_bttn">Log In</a></li>
                                    <li><a href="#" class="signUp_bttn">Sign Up</a></li>
                                </ul>
                            </div>

                            <div class="nav_notification_outer">

                                <div class="notiication_info">
                                    <a href="#">
                                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                                        <b>2</b>
                                    </a>
                                </div>

                                <div class="profile_info">
                                    <span>
                                        <b><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></b>
                                        <small>Angela</small>
                                        <i><img src="{{asset('project-assets/images/chevron_down.png')}}" alt="#" /></i>
                                    </span>
                                    <div class="profileDropdown">
                                        <ul>
                                            <li><a href="#">Profile </a></li>
                                            <li><a href="#">Settings</a></li>
                                            <li><a href="#">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main>
            <div class="main_container">
 
                <div class="event_pages db_messageChat">
                    <div class="auto_container">
                        <div class="dasbord_container_detail ">

                        <div class="available_feature_sorted">
                                <div class="sorted_nav tanelt_sort on_book_list">
                                    @include('app.sections.front.find-talent-search-section')
                                    <div class="icon_list_grid">
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-th iconGrid" aria-hidden="true"></i>
                                            <i class="fa fa-th-list iconList" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>



                            <div class="">
                            <div class="dashboard_tabs_data">
                                                <div class="dashboard_dataTable_head">
                                                    <div class="data_col w-35">
                                                        <div class="formSorted">
                                                            <strong>Sort by:</strong>
                                                            <div class="custom_dropdown">
                                                                <select>
                                                                    <option>Date</option>
                                                                    <option>Time</option> 
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="data_col w-15">
                                                        <h5>Start date & time</h5>
                                                    </div>
                                                    <div class="data_col w-15">
                                                        <h5>Complete Date</h5>
                                                    </div>
                                                    <div class="data_col w-7">
                                                        <h5>Total</h5>
                                                    </div>
                                                    <div class="data_col w-12">
                                                        <h5>Status</h5>
                                                    </div> 
                                                </div>

                                                <div class="dashboard_tabs_dataList">
                                                    <ul>
                                                        <li>
                                                            <div class="dashboard_tabs_dataInfo">
                                                                <div class="data_col w-35">
                                                                    <div class="data_profile_info">
                                                                         <span><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                         <div class="data_profile_infoText">
                                                                             <h4>Windy Sue</h4>
                                                                             <p>Business Coach</p>
                                                                         </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-7">
                                                                    <small>$60</small>
                                                                </div>
                                                                <div class="data_col w-12">
                                                                    <div class="activeBttn">
                                                                        <a href="#" class="default_bttn pending_status"> Process</a>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </li> 
                                                        <li>
                                                            <div class="dashboard_tabs_dataInfo">
                                                                <div class="data_col w-35">
                                                                    <div class="data_profile_info">
                                                                         <span><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                         <div class="data_profile_infoText">
                                                                             <h4>Windy Sue</h4>
                                                                             <p>Business Coach</p>
                                                                         </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-7">
                                                                    <small>$60</small>
                                                                </div>
                                                                <div class="data_col w-12">
                                                                    <div class="activeBttn">
                                                                        <a href="#" class="default_bttn "> Process</a>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </li> 
                                                        <li>
                                                            <div class="dashboard_tabs_dataInfo">
                                                                <div class="data_col w-35">
                                                                    <div class="data_profile_info">
                                                                         <span><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                         <div class="data_profile_infoText">
                                                                             <h4>Windy Sue</h4>
                                                                             <p>Business Coach</p>
                                                                         </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-7">
                                                                    <small>$60</small>
                                                                </div>
                                                                <div class="data_col w-12">
                                                                    <div class="activeBttn">
                                                                        <a href="#" class="default_bttn pending_status"> Process</a>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </li> 
                                                        <li>
                                                            <div class="dashboard_tabs_dataInfo">
                                                                <div class="data_col w-35">
                                                                    <div class="data_profile_info">
                                                                         <span><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                         <div class="data_profile_infoText">
                                                                             <h4>Windy Sue</h4>
                                                                             <p>Business Coach</p>
                                                                         </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-7">
                                                                    <small>$60</small>
                                                                </div>
                                                                <div class="data_col w-12">
                                                                    <div class="activeBttn">
                                                                        <a href="#" class="default_bttn "> Process</a>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </li> 
                                                        <li>
                                                            <div class="dashboard_tabs_dataInfo">
                                                                <div class="data_col w-35">
                                                                    <div class="data_profile_info">
                                                                         <span><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                         <div class="data_profile_infoText">
                                                                             <h4>Windy Sue</h4>
                                                                             <p>Business Coach</p>
                                                                         </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-15">
                                                                    <small>April 6, 12:30 am</small>
                                                                </div>
                                                                <div class="data_col w-7">
                                                                    <small>$60</small>
                                                                </div>
                                                                <div class="data_col w-12">
                                                                    <div class="activeBttn">
                                                                        <a href="#" class="default_bttn "> Process</a>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </li> 
                                                    </ul>
                                                </div>
                                            </div> 
                            </div>
                             
                        </div>
                    </div>
                </div>
 

                
            </div>
        </main>


        <footer>
            <div class="footer_outer">  

                <div class="copyRight">
                    <div class="auto_container">
                        <div class="copyRight_info">
                            <p>© 2020 All Rights Reserved <a href="#">Moderator live</a> </p>
                        </div>
                    </div>
                </div>

            </div>
        </footer>

</div>


<script src="  {{ asset('project-assets/js/jquery-3.4.1.min.js') }}"></script>

    <script src="{{ asset('project-assets/js/myscript.js') }}"></script> 
    <!-- bootstrap -->
    <script src="{{ asset('project-assets/js/bootstrap.min.js') }}"></script> 

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.js"></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/owl.carousel.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" ></script>
</body>

</html>