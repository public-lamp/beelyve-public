<!doctype html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <title>Moderator</title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/bootstrap.min.css') }}">
    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/style.css') }}">
    <link rel="stylesheet " href="{{ asset('project-assets/css/responsive.css') }}">
    <!-- font awesome-->
    <link rel="stylesheet " href="{{ asset('project-assets/css/font-awesome.min.css') }}">

</head>

<body>
    <div class="wrapper">
        <header>
            <div class="header">
                <div class="auto_container">
                    <div class="header_detail">
                        <div class="header_detail_inner">
                            <div class="logo">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('project-assets/images/logo.png') }}" alt="#" class="logo_web" />
                                <img src="{{ asset('project-assets/images/mobile_logo.png') }}" alt="#" class="logo_mbile" />
                            </a>
                            </div>

                            <div class="menu_nav">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#" class="nav_active">Manage Order</a></li>
                                    <li><a href="#">Booking Request</a></li>
                                    <li><a href="#">Message</a></li>
                                </ul>
                            </div>

                            <div class="nav_authantication_bttns"  style="display: none;">
                                <ul>
                                    <li><a href="#" class="login_bttn">Log In</a></li>
                                    <li><a href="#" class="signUp_bttn">Sign Up</a></li>
                                </ul>
                            </div>

                            <div class="nav_notification_outer">

                                <div class="notiication_info">
                                    <a href="#">
                                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                                        <b>2</b>
                                    </a>
                                </div>

                                <div class="profile_info">
                                    <span>
                                        <b><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></b>
                                        <small>Angela</small>
                                        <i><img src="{{asset('project-assets/images/chevron_down.png')}}" alt="#" /></i>
                                    </span>
                                    <div class="profileDropdown">
                                        <ul>
                                            <li><a href="#">Profile </a></li>
                                            <li><a href="#">Settings</a></li>
                                            <li><a href="#">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main>
            <div class="main_container">

                <div class="event_pages db_messageChat">
                    <div class="auto_container">
                        <div class="dasbord_container_detail ">

                            <div class="db_tittle">
                                <div class="db_tittle_heading">
                                    <h3>Booking Details</h3>
                                </div>
                            </div>

                            <div class="bookingDetail_section">
                                <div class="bookingDetail_section_fl">
                                    <div class="bookingDetail_fl_info">
                                        <div class="booking_description">
                                            <strong><b>Title:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit</strong>
                                            <h4>Booking Note (By Moderator)</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                        </div>


                                        <div class="slots_info">
                                            <h4>Booking Slot Details</h4>
                                            <ul>
                                                <li>
                                                    <strong>Date</strong>
                                                    <small>Date</small>
                                                </li>
                                                <li>
                                                    <strong>Time from</strong>
                                                    <small>12:00</small>
                                                </li>
                                                <li>
                                                    <strong>Time To</strong>
                                                    <small>01:00</small>
                                                </li>
                                            </ul>
                                        </div>



                                        <div class="slots_info">
                                            <h4>Booking Services Detail</h4>
                                            <ul>
                                                <li>
                                                    <strong>Services Plan</strong>
                                                    <small>Basic</small>
                                                </li>
                                                <li>
                                                    <strong>Services Charges</strong>
                                                    <small>$100</small>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="mainFeatures">
                                            <h6>Main Features</h6>
                                            <ul>
                                                <li>Lorem Ipsum</li>
                                                <li>Lorem Ipsum dollor sit amet.</li>
                                            </ul>
                                        </div>


                                        <div class="booking_description descrp">
                                            <h6>Discription</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="bookingDetail_section_fr">
                                    <div class="db_profile ">
                                        <div class="featureWeek_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{asset('project-assets/images/profile_avatar.png')}}" alt="#">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="featureProfile_avatar_inner">
                                                <div class="featureProfile_avatar_text">
                                                    <strong>David Hutapea</strong>
                                                    <small>Fashion Designer</small>
                                                </div>

                                                <div class="profile_rating">
                                                    <div class="profile_rating_info">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        </ul>
                                                        <strong>4.3</strong>
                                                    </div>
                                                </div>

                                                <div class="achieved_task_info">
                                                    <ul>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Completed Projects</span>
                                                                <strong>(569)</strong>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Member Since</span>
                                                                <strong>Oct 2021</strong>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Amount Earned</span>
                                                                <strong>$50k</strong>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="viewProfile">
                                        <a href="#">View Profile</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>



            </div>
        </main>


        <footer>
            <div class="footer_outer">

                <div class="copyRight">
                    <div class="auto_container">
                        <div class="copyRight_info">
                            <p>© 2020 All Rights Reserved <a href="#">Moderator live</a> </p>
                        </div>
                    </div>
                </div>

            </div>
        </footer>

</div>


<script src="  {{ asset('project-assets/js/jquery-3.4.1.min.js') }}"></script>

    <script src="{{ asset('project-assets/js/myscript.js') }}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('project-assets/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.js"></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/owl.carousel.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" ></script>
</body>

</html>
