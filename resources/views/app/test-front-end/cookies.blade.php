{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Faq')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img">
                        <img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="#" />
                    </span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Terms & Conditions<p><a href="#">www.Moderatorlive.com</a></p></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">
                        <div class="terms_condition_detail">
                            <h4>Effective date: <b>1st September 2021</b></h4>
                            
                                <p>This Cookie Policy explains how Moderatorlive.com (“we”, “us” or “our”) uses cookies and
similar technologies in connection with the <a href="#">www.Moderatorlive.com</a>  website.</p>

<h4>What are cookies?</h4>

<p>Cookies are small text files placed on your computer by websites and sometimes by emails.
They provide useful information to organizations, which helps to make your visits to their
websites more effective and efficient. We use cookies to ensure that we are able to understand
how you use our websites and to ensure that we can make improvements to the websites.</p>
<p>Cookies do not contain any personal or confidential information about you.</p>
 

<h4>How we use cookies</h4>
<p>We use cookies to ensure that you get the best from our website. The first time that you visit our
website you will be asked to consent to our use of cookies and we suggest that you agree to
allow the cookies to be active on your device whilst you visit and browse our website to ensure
that you experience our website fully.</p>
<p>The types of cookies that we may use include:</p>

                            <ul class="list_info">
                                <li>
                                    <h4>Session cookies</h4> 
                                    <p>Session cookies last only for the duration of your visit and are deleted when you close
your browser. These facilitate various tasks such as allowing a website to identify that a
user of a particular device is navigating from page to page, supporting website security
or basic functionality.</p>    
                                </li> 
                                
                                <li>
                                    <h4>Persistent cookies</h4> 
                                    <p>Persistent cookies last after you have closed your browser, and allow a website to
remember your actions and preferences. Sometimes persistent cookies are used by
websites to provide targeted advertising based upon the browsing history of the device.
We use persistent cookies to allow us to analyze users visit to our site. These cookies
help us to understand how customers arrive at and use our site so we can improve the
overall service.</p>    
                                </li> 
                                <li>
                                    <h4>Strictly necessary cookies</h4> 
                                    <p>These cookies are essential in order to enable you to move around the website and use
its features, and ensuring the security of your experience. Without these cookies
services you have asked for, such as applying for products and managing your
accounts, cannot be provided. These cookies don’t gather information about you for the purposes of marketing.</p>    
                                </li> 
                                <li>
                                    <h4>Performance cookies</h4> 
                                    <p>These cookies collect information about how visitors use a web site, for instance which
pages visitors go to most often, and if they get error messages from web pages. All
information these cookies collect is only used to improve how a website works, the user
experience and to optimize our advertising. By using our websites you agree that we can
place these types of cookies on your device, however you can block these cookies using
your browser settings.</p>    
                                </li> 
                                <li>
                                    <h4>Functionality cookies</h4> 
                                    <p>These cookies allow the website to remember choices you make (such as your
username). The information these cookies collect is anonymized (i.e. it does not contain
your name, address etc.) and they do not track your browsing activity across other
websites. By using our websites you agree that we can place these types of cookies on
your device, however you can block these cookies using your browser settings.</p>    
                                </li> 
                                <li>
                                    <h4>Targeting cookies</h4> 
                                    <p>These cookies collect several pieces of information about your browsing habits. [They
are usually placed by third party advertising networks]. They remember that you have
visited a website and this information is shared with other organizations such as media
publishers. These organizations do this in order to provide you with targeted adverts
more relevant to you and your interests.</p>    
                                </li> 
                                <li>
                                    <h4>Third party cookies</h4> 
                                    <p>Please note that third parties (including, for example, advertising networks and providers
of external services like web traffic analysis services) may also use cookies, over which
we have no control. These cookies are likely to be analytical/performance cookies or
targeting cookies.</p>    
                                </li> 
                            </ul>

                            <h4>Managing Cookies</h4>
                            <p>You can control and/or delete cookies as you wish – for details, see aboutcookies.org. You can
delete all cookies that are already on your computer and you can set most browsers to prevent
them from being placed. If you do this, however, you may have to manually adjust some
preferences every time you visit our website or use our Platform and some services and
functionalities we offer may not work.</p>
                            <p>To restrict or handle cookies, please see the ‘Help’ section of your internet browser.</p>
                            <p>For any further information, please contact us <a href="#"> Support@Moderatorlive.com</a></p>

                             
 
</div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
{{--    <script type="text/javascript" src="https://js/jquery.mCustomScrollbar.concat.min.js"></script>--}}
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
@endsection



