<!doctype html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <title>Moderator</title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/bootstrap.min.css') }}">
    <!-- custom css -->
    <link rel="stylesheet" href="{{ asset('project-assets/css/style.css') }}">
    <link rel="stylesheet " href="css/responcive.css "> 
    <!-- font awesome-->
    <link rel="stylesheet " href="{{ asset('project-assets/css/font-awesome.min.css') }}"> 


    <script src="js/jquery-3.4.1.min.js"></script>


</head>

<body>
    <div class="wrapper">
        <header>
            <div class="header">
                <div class="auto_container">
                    <div class="header_detail">
                        <div class="header_detail_inner">
                            <div class="logo">
                                <a href="#"><img src="{{asset('project-assets/images/logo.png')}}" alt="#" /></a>
                            </div>

                            <div class="menu_nav">
                                <ul>
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#" class="nav_active">Manage Order</a></li>
                                    <li><a href="#">Booking Request</a></li>
                                    <li><a href="#">Message</a></li> 
                                </ul>
                            </div>

                            <div class="nav_authantication_bttns"  style="display: none;">
                                <ul>
                                    <li><a href="#" class="login_bttn">Log In</a></li>
                                    <li><a href="#" class="signUp_bttn">Sign Up</a></li>
                                </ul>
                            </div>

                            <div class="nav_notification_outer">

                                <div class="notiication_info">
                                    <a href="#">
                                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                                        <b>2</b>
                                    </a>
                                </div>

                                <div class="profile_info">
                                    <span>
                                        <b><img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="#" /></b>
                                        <small>Angela</small>
                                        <i><img src="{{asset('project-assets/images/chevron_down.png')}}" alt="#" /></i>
                                    </span>
                                    <div class="profileDropdown">
                                        <ul>
                                            <li><a href="#">Profile </a></li>
                                            <li><a href="#">Settings</a></li>
                                            <li><a href="#">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main>
            <div class="main_container">
 
                <div class="dasbord_container">
                    <div class="auto_container">
                        <div class="dasbord_container_detail db_booking_request">

                            <div class="db_tittle">
                                <div class="db_tittle_heading">
                                    <h4>Booking Request</h4>
                                </div> 
                                <div class="sorted_dropdown dropdown_sort">
                                    <div class="custom_dropdown">
                                        <select>
                                            <option>Sort by: Date</option>
                                            <option>Sort by: Date 1</option>
                                            <option>Sort by: Date 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            
                            <div class="message_section">
                                <ul>
                                    <li>
                                        <div class="message_section_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{asset('project-assets/images/profile_avatar.png')}}" alt="#"> 
                                                    </span>
                                                </div>
                                                <div class="featureProfile_avatar_text">
                                                    <strong>Jack Black</strong> 
                                                    <small>Event Name</small>
                                                </div> 
                                            </div>
                                            <div class="event_bttns">
                                                <a href="#" class="default_bttn approve_bttn">Reject </a>
                                                <a href="#" class="default_bttn ">Accept  </a>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="message_section_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{asset('project-assets/images/profile_avatar.png')}}" alt="#"> 
                                                    </span>
                                                </div>
                                                <div class="featureProfile_avatar_text">
                                                    <strong>Jack Black</strong> 
                                                    <small>Event Name</small>
                                                </div> 
                                            </div>
                                            <div class="event_bttns">
                                                <a href="#" class="default_bttn approve_bttn">Reject </a>
                                                <a href="#" class="default_bttn ">Accept  </a>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="message_section_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{asset('project-assets/images/profile_avatar.png')}}" alt="#"> 
                                                    </span>
                                                </div>
                                                <div class="featureProfile_avatar_text">
                                                    <strong>Jack Black</strong> 
                                                    <small>Event Name</small>
                                                </div> 
                                            </div>
                                            <div class="event_bttns">
                                                <a href="#" class="default_bttn approve_bttn">Reject </a>
                                                <a href="#" class="default_bttn ">Accept  </a>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="message_section_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{asset('project-assets/images/profile_avatar.png')}}" alt="#"> 
                                                    </span>
                                                </div>
                                                <div class="featureProfile_avatar_text">
                                                    <strong>Jack Black</strong> 
                                                    <small>Event Name</small>
                                                </div> 
                                            </div>
                                            <div class="event_bttns">
                                                <a href="#" class="default_bttn approve_bttn">Reject </a>
                                                <a href="#" class="default_bttn ">Accept  </a>
                                            </div>
                                        </div>
                                    </li> 
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
 

                
            </div>
        </main>


        <footer>
            <div class="footer_outer">  

                <div class="copyRight">
                    <div class="auto_container">
                        <div class="copyRight_info">
                            <p>© 2020 All Rights Reserved <a href="#">Moderator live</a> </p>
                        </div>
                    </div>
                </div>

            </div>
        </footer>





<!-- dispute popup -->


<div class="custom_popup eventDetail_popup">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h3>Event Details</h3> 
                </div>  
                
 
                <div class="event_detail_profile">
                    <div class="feartureProflie_info">
                        <div class="featureProfile_avatar">
                            <span>
                                <img src="{{asset('project-assets/images/profile_avatar.png')}}" alt="#"> 
                            </span>
                        </div>
                        <div class="featureProfile_avatar_text">
                            <strong>Windy Sue</strong> 
                            <small>Event Name</small>
                        </div> 
                    </div>
                </div>

                <div class="event_detail_list">
                    <div class="orderDetail_summary">
                        <ul>
                            <li>
                                <div class="summary_text">
                                    <strong>Event type</strong>
                                    <b>Any</b>
                                </div>
                                
                            </li>
                            <li>
                                <div class="summary_text">
                                    <strong>Date & time</strong>
                                    <b>April 6, 9:00 am</b>
                                </div>
                            </li>
                            <li>
                                <div class="summary_text">
                                    <strong>Total Price</strong>
                                    <b>$25.75</b>
                                </div>
                            </li>
                            <li>
                                <div class="summary_text">
                                    <strong>Event rules</strong>
                                    <b>your rules</b>
                                </div>
                            </li>
                            <li>
                                <div class="summary_text">
                                    <strong>Goals</strong>
                                    <b>your goals</b>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
  
                  
            </div>
        </div>
    </div>
</div>




 


    </div>





    <script src="{{ asset('project-assets/js/myscript.js') }}"></script> 
    <!-- bootstrap -->
    <script src="j{{ asset('project-assets/js/bootstrap.min.js') }}"></script> 




</body>

</html>