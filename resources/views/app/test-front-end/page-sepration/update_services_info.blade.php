@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Dashboard')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/select2/select2.css') }}" />

    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>

@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/select2/select2.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>


@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Update Profile</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="update_profile_tabs">
                                    <ul>
                                        <li><a href="#updateTab_1">Basic Profile</a></li>
                                         <li><a href="#updateTab_2">Availability</a></li>
                                         <li><a href="#updateTab_3" class="updateTab_active">Services Pricing</a></li>
                                         <li><a href="#updateTab_4">Upload files</a></li>
                                         <li><a href="#updateTab_5">Gallery</a></li>
                                         <li><a href="#updateTab_6">Social Connectivity</a></li>
                                    </ul>
                                </div> 

                                {{-- Services pricing section start --}}
                                <div class="update_profile_data" id="updateTab_3">
                                    <form id="talentServicesPricingForm">
                                        @csrf
                                        <div class="availability_detail">
                                            <div class="services_pricing_sec">
                                                @if( $talentProfile['talentServicesPricing']->isNotEmpty() )
                                                    @foreach($talentProfile['talentServicesPricing'] as $servicePricingType)
                                                        <div class="services_pricing_detail">
                                                            <div class="popup_form">
                                                                <h4 class="basic_tittle">{{ $servicePricingType['servicesPricingType']['name'] }} Services</h4>
                                                                <ul>
                                                                    <input type="hidden" value="{{ $servicePricingType['servicesPricingType']['id'] }}" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][id]" />
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Time Duration</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][duration]" id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_duration" >
                                                                                    <option selected disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesPricingSlot::all() as $slot)
                                                                                        <option value="{{ $slot->id }}" @if($servicePricingType['services_pricing_slot_id'] == $slot->id) selected @endif>{{ $slot->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Price</strong>
                                                                            <div class="custom_field_input unitPrice">
                                                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                                                <input type="text" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][price]"
                                                                                       value="{{ $servicePricingType['price'] }}"
                                                                                       id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_price" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Communication Medium</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select class="selectMultiple" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][mediums][]" id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_medium" multiple="multiple">
                                                                                    <option disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesMedium::all() as $serMedium)
                                                                                        <option value="{{ $serMedium->id }}"
                                                                                            @foreach($servicePricingType['servicesPricingType']['talentServicesMediums'] as $medium)
                                                                                                @if($medium['servicesMedium']['id'] == $serMedium->id) selected @endif
                                                                                            @endforeach >
                                                                                            {{ $serMedium->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                     </li>

                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Main Features</strong>
                                                                            <div class="custom_field_input addMain_feature">
                                                                                @foreach($servicePricingType['main_features'] as $featureKey => $feature)
                                                                                    <input type="text" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][main_features][{{ $featureKey }}]"
                                                                                           id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_main_features_{{ $featureKey }}"
                                                                                           value="{{ $feature }}"
                                                                                           placeholder="{{ $featureKey }}" />
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Description</strong>
                                                                            <div class="custom_field_textarea">
                                                                                <textarea name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][description]" id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_description">
                                                                                    {{ $servicePricingType['description'] }}
                                                                                </textarea>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    @foreach(\App\Models\Talent\ServicesPricingType::all() as $pricingTypeKey => $pricingType)
                                                        <div class="services_pricing_detail">
                                                            <div class="popup_form">
                                                                <h4 class="basic_tittle">{{ $pricingType->name }} Services</h4>
                                                                <ul>
                                                                    <input type="hidden" value="{{ $pricingType->id }}" name="pricing[{{ $pricingType->slug }}][id]" />
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Time Duration</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select name="pricing[{{ $pricingType->slug }}][duration]" id="pricing_{{ $pricingType->slug }}_duration" >
                                                                                    <option selected disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesPricingSlot::all() as $slot)
                                                                                        <option value="{{ $slot->id }}">{{ $slot->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Price</strong>
                                                                            <div class="custom_field_input unitPrice">
                                                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][price]" id="pricing_{{ $pricingType->slug }}_price" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Communication Medium</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select class="selectMultiple" name="pricing[{{ $pricingType->slug }}][mediums][]" id="pricing_{{ $pricingType->slug }}_mediums" multiple="multiple">
                                                                                    <option disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesMedium::all() as $serMedium)
                                                                                        <option value="{{ $serMedium->id }}">{{ $serMedium->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Main Features</strong>
                                                                            <div class="custom_field_input addMain_feature">
                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][main_features][]" id="pricing_{{ $pricingType->slug }}_main_features_0" placeholder="1" />

                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][main_features][]" id="pricing_{{ $pricingType->slug }}_main_features_1" placeholder="2" />

                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][main_features][]" id="pricing_{{ $pricingType->slug }}_main_features_2" placeholder="3" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Description</strong>
                                                                            <div class="custom_field_textarea">
                                                                                <textarea name="pricing[{{ $pricingType->slug }}][description]" id="pricing_{{ $pricingType->slug }}_description"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                         </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Submit" id="talentServicesPricingFormBtn" />
                                        </div>
                                    </form>
                                </div> 
                                
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- work images script --}}
        <script type="text/javascript">
            // media gallery image
            // $('#work_images').ssi_uploader();
        </script>

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')

    <script type="text/javascript">
        // select2 multi-tag select
        $(document).ready(function() {
            $('.selectMultiple').select2();
        })
    </script>

    {{--    video upload scripts --}}
    <script type="text/javascript">
        var fileInputVideo = document.getElementById('filePostVideo');
        var previewVideo = document.getElementById('video-preview-post');
        previewVideo.style.display = "block";

        fileInputVideo.addEventListener('change', function (e) {
                let videoSizeInMbs = e.target.files[0].size/1000000; // convert to MBs
                FileUploadVideoPath = fileInputVideo.value;
                var VideoExtension = FileUploadVideoPath.substring(FileUploadVideoPath.lastIndexOf('.') + 1).toLowerCase();
                //The file uploaded is an image
                if (VideoExtension != "png" && VideoExtension != "avi" && VideoExtension != "flv" && VideoExtension != "wmv" && VideoExtension != "mov" && VideoExtension != "mp4") {
                    document.getElementById('forVideoAlert').innerHTML = `<div class="alert alert-warning"><strong>Warning!</strong> Only .AVI, .FLV, .MOV, .MP4 and WMV files are allowed.</div>`;;
                    setTimeout(function () {
                        $('#forVideoAlert').fadeOut('slow');
                    }, 5000);
                    return;
                }

                if ( videoSizeInMbs > 10 ) {
                    document.getElementById('forVideoAlert').innerHTML = `<div class="alert alert-warning"><strong>Warning!</strong>Video size can not be more than 10 MB.</div>`;;
                    setTimeout(function () {
                        $('#forVideoAlert').fadeOut('slow');
                    }, 5000);
                    return;
                }

                var url = URL.createObjectURL(e.target.files[0]);
                document.getElementById('real_upload_video').style.display = "none";
                document.getElementById('dummy_upload_video').style.display = "block";
                previewVideo.innerHTML = `<div class="closing" onclick="removeItemInPost('video')"><i class="fa fa-close"></i></div><video  controls style="width:100%;"><source src="` +url+ `" type="video/mp4">Your browser does not support HTML5 video.</video>`;;
            }
        );

        function removeItemInPost(item, group = null) {
            document.getElementById('video-preview-post').innerHTML = "";
            document.getElementById('dummy_upload_video').style.display = "none";
            document.getElementById('real_upload_video').style.display = 'block';
            document.getElementById('filePostVideo').value = "";
        }
    </script>

@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">

        {{-- Basic profile-info ajax, stars --}}
        $('#talentBasicInfoForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let basicInfoData =  new FormData( this );
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.basic.update') }}",
                data: basicInfoData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        // return window.location.reload();
                         window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                    });
                }
            });
        });
        {{-- Basic profile-info ajax, Ends --}}


        {{-- availability ajax, stars --}}
        $('#talentAvailabilityForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let availabilityData =  $('#talentAvailabilityForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.availability.set') }}",
                data: availabilityData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        console.log(keyId+": "+value);
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- availability ajax, Ends --}}


        {{-- Services pricing ajax stars --}}
        $('#talentServicesPricingForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let servicePricingData =  $('#talentServicesPricingForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.services-pricing.set') }}",
                data: servicePricingData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        if ( keyId.includes('_mediums_') ) {
                            keyId = keyId.slice(0, -1);
                        }
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        // toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Services pricing ajax, Ends --}}


        {{-- Work history stars --}}
        $('#talentWorkFilesForm').on('submit', function (e) {
            e.preventDefault();
            let workHistoryData =  new FormData( $('#talentWorkFilesForm')[0] );
            blockUi( true );
            $.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.work-files.save') }}",
                data: workHistoryData,
                success: function (response) {
                    if ( Boolean(response.status) ) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.reload();
                    } else if ( !Boolean(response.status) ) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Work history, Ends --}}

    </script>


    <script type="text/javascript">
        var formdata = new FormData();

        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function(){
                    $(".preview_img").addClass('my_img');
                    $("#previewImg").attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
        $(document).ready(function(){
            $(".preview_img i").click(function(){
                $('.preview_img').removeClass('my_img');
            });
        });
    </script>


    <script type="text/javascript">
        var countFile=0;
        $(".file_input").on("change", function(event) {
            var files = event.target.files[0]; // also use like    var files = $('#imgInp')[0].files[0];
            var fileName = files.name;
            var fsize = files.size;
            var tmppath = URL.createObjectURL(files);
            var ext = fileName.split('.').pop().toLowerCase();
            if ((fsize / 1024) / 1024 <= 1) {
                if (countFile <= 5) {
                    if (ext == 'jpg' || ext == 'png' || ext == 'jpeg') {

                       $('.main-div').show();
                        var html = '<span class="close-spn-close"><span class="card_img remove-file-icon"  data-id="' + countFile + '">x</span><img class="preview-imgs" src="' + tmppath + '"></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    } else {
                        chatBox1.find('.main-div').show();
                        var html = '<span class="downloaded-items1234 clearfix file_download close-f-attch close-spn-close" href="#"><span class="card_file remove-file-icon"   data-id="' + countFile + '">x</span><span class="left1">' + fileName + '</span></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    }
                    countFile++;
                 //   $(this).parents('div.chat-main-div-new').find('textarea.emojionearea').focus();
                } else {
                    errorMsg('Only five files allow at a time');
                }
            } else {
                errorMsg('Select file less than 1 MB');
            }
        });

        $(document).on("click", ".remove-file-icon", function(e) {
            var div_length = $(this).closest('.main-div').children().length;
            $(this).closest('.close-spn-close').remove();
            countFile--;
            div_length--;
            if (div_length == 0) {
                // $('.chat-main-div-new').find('.main-div').html('');
                // $('.chat-main-div-new').find('.main-div').hide();
                // $('.chat-main-div-new').find(".file_input").val('');
            }
        });

        $(function() {
            $('#sort_more').change(function(){
                $('.sorting').hide();
                $('#' + $(this).val()).show();
            });
        });
        $(document).ready(function(){
            $('body').addClass('chat_body');
        });

    </script>

@endsection








