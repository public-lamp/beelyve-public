@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Dashboard')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/select2/select2.css') }}" />

    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>

@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/select2/select2.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>


@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Update Profile</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="update_profile_tabs">
                                    <ul>
                                        <li><a href="#updateTab_1">Basic Profile</a></li>
                                         <li><a href="#updateTab_2" class="updateTab_active">Availability</a></li>
                                         <li><a href="#updateTab_3">Services Pricing</a></li>
                                         <li><a href="#updateTab_4">Upload files</a></li>
                                         <li><a href="#updateTab_5">Gallery</a></li>
                                         <li><a href="#updateTab_6">Social Connectivity</a></li>
                                    </ul>
                                </div> 

                                {{-- Availabbility section starts --}}
                                <div class="update_profile_data" id="updateTab_2">
                                    <form id="talentAvailabilityForm">
                                        @csrf

                                        <div class="availability_detail">
                                            <div class="availability_options">
                                                <ul>
                                                     @foreach( \App\Models\Generic\AvailabilityCategory::all() as $availabilityCategory )
                                                         <li>
                                                            <div class="chooseOptionDetail">
                                                                <label>
                                                                    <input type="radio" class="option-input radio" value="{{ $availabilityCategory->id }}"
                                                                           name="availability_category"
                                                                           data-avail-name = '{{ $availabilityCategory->name }}'
                                                                           @if( isset($talentProfile['availabilityCategory']) && $availabilityCategory->id == $talentProfile['availabilityCategory']['availability_category_id'] ) checked @endif />
                                                                    {{ $availabilityCategory->name }}
                                                                </label>
                                                            </div>
                                                        </li>
                                                     @endforeach
                                                </ul>
                                            </div>

                                            <div class="availability_info">
                                                @if( isset($talentProfile['availabilityCategory']) && (!empty($talentProfile['userAvailabilityDays']) && $talentProfile['availabilityCategory']['availability_category_id'] == 2) )
                                                    <div class="avaiable_days" style="display: block;">
                                                        <div class="avaiable_daysInner">

                                                            <div class="d-flex days_tab">
                                                                <div class="btn_days">
                                                                    @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                        <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                            {{ substr($weekDay->name, 0, 3) }}
                                                                            <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                                   name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                                   data-child="{{ $weekDay->slug }}_parent" />
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            @foreach($talentProfile['userAvailabilityDays'] as $userAvailDayKey => $availabilityDay)
                                                                <div class="{{ $availabilityDay['weekDay']['slug'] }}_parent main_parents" id="{{ $availabilityDay['weekDay']['slug'] }}_parent" style="display: none;">
                                                                    <div class="sound-signal">
                                                                        <strong class="reset_time"><a href="javascript:void(0);">Reset</a></strong>
                                                                    </div>
                                                                    <div class="time_sec">
                                                                        @if( !$availabilityDay['availabilitySlots']->isEmpty() )
                                                                            @foreach($availabilityDay['availabilitySlots'] as $slotKey => $availabilitySlot)
                                                                                <div class="price_from">
                                                                                    <span>Time From</span>
                                                                                    <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                           name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_from][]"
                                                                                           id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                           data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                           value="{{ $availabilitySlot['time_from'] }}"
                                                                                           readonly="" />
                                                                                </div>
                                                                                <div class="price_from time_to">
                                                                                    <span> Time To</span>
                                                                                    <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                           name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_to][]"
                                                                                           id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                           value="{{ $availabilitySlot['time_to'] }}"
                                                                                           readonly="" />
                                                                                </div>
                                                                            @endforeach
                                                                        @else
                                                                            <div class="price_from">
                                                                                <span>Time From</span>
                                                                                <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                       name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_from][]"
                                                                                       id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                       data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                       placeholder="00:00" readonly="" />
                                                                            </div>
                                                                            <div class="price_from time_to">
                                                                                <span> Time To</span>
                                                                                <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                       name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_to][]"
                                                                                       id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                       placeholder="00:00" readonly="" />
                                                                            </div>
                                                                        @endif
                                                                        <div class="add_hours_btn">
                                                                            <div class="cssCircle add_hours" data-name="{{ $availabilityDay['weekDay']['slug'] }}">
                                                                                Add
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avaiable_days" style="/* display: block; */">
                                                        <div class="avaiable_daysInner">
                                                            <div class="d-flex days_tab">
                                                                <div class="btn_days">
                                                                    @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                        <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                            {{ substr($weekDay->name, 0, 3) }}
                                                                            <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                                   name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                                   data-child="{{ $weekDay->slug }}_parent" />
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                <div class="{{ $weekDay->slug }}_parent main_parents" id="{{ $weekDay->slug }}_parent" style="display: none;">
                                                                    <div class="sound-signal">
                                                                        <strong class="reset_time"><a href="javascript:void(0);">Reset</a></strong>
                                                                    </div>
                                                                    <div class="time_sec">
                                                                        <div class="price_from">
                                                                            <span>Time From</span>
                                                                            <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                   name="days[{{ $weekDay->slug }}][time][time_from][]"
                                                                                   id="days_{{ $weekDay->slug }}_time_time_from_0"
                                                                                   data-time-to-id="days_{{ $weekDay->slug }}_time_time_to_0"
                                                                                   placeholder="00:00" readonly="" />
                                                                        </div>
                                                                        <div class="price_from time_to">
                                                                            <span> Time To</span>
                                                                            <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                   name="days[{{ $weekDay->slug }}][time][time_to][]"
                                                                                   id='days_{{ $weekDay->slug }}_time_time_to_0'
                                                                                   placeholder="00:00" readonly="" />
                                                                        </div>
                                                                        <div class="add_hours_btn">
                                                                            <div class="cssCircle add_hours" data-name="{{ $weekDay->slug }}">
                                                                                Add
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Submit" id="talentAvailabilityFormBtn" />
                                        </div>
                                    </form>
                                </div>
                                {{-- Avaiability Section ends --}} 

                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- work images script --}}
        <script type="text/javascript">
            // media gallery image
            // $('#work_images').ssi_uploader();
        </script>

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')

    <script type="text/javascript">
        // select2 multi-tag select
        $(document).ready(function() {
            $('.selectMultiple').select2();
        })
    </script>

    {{--    video upload scripts --}}
    <script type="text/javascript">
        var fileInputVideo = document.getElementById('filePostVideo');
        var previewVideo = document.getElementById('video-preview-post');
        previewVideo.style.display = "block";

        fileInputVideo.addEventListener('change', function (e) {
                let videoSizeInMbs = e.target.files[0].size/1000000; // convert to MBs
                FileUploadVideoPath = fileInputVideo.value;
                var VideoExtension = FileUploadVideoPath.substring(FileUploadVideoPath.lastIndexOf('.') + 1).toLowerCase();
                //The file uploaded is an image
                if (VideoExtension != "png" && VideoExtension != "avi" && VideoExtension != "flv" && VideoExtension != "wmv" && VideoExtension != "mov" && VideoExtension != "mp4") {
                    document.getElementById('forVideoAlert').innerHTML = `<div class="alert alert-warning"><strong>Warning!</strong> Only .AVI, .FLV, .MOV, .MP4 and WMV files are allowed.</div>`;;
                    setTimeout(function () {
                        $('#forVideoAlert').fadeOut('slow');
                    }, 5000);
                    return;
                }

                if ( videoSizeInMbs > 10 ) {
                    document.getElementById('forVideoAlert').innerHTML = `<div class="alert alert-warning"><strong>Warning!</strong>Video size can not be more than 10 MB.</div>`;;
                    setTimeout(function () {
                        $('#forVideoAlert').fadeOut('slow');
                    }, 5000);
                    return;
                }

                var url = URL.createObjectURL(e.target.files[0]);
                document.getElementById('real_upload_video').style.display = "none";
                document.getElementById('dummy_upload_video').style.display = "block";
                previewVideo.innerHTML = `<div class="closing" onclick="removeItemInPost('video')"><i class="fa fa-close"></i></div><video  controls style="width:100%;"><source src="` +url+ `" type="video/mp4">Your browser does not support HTML5 video.</video>`;;
            }
        );

        function removeItemInPost(item, group = null) {
            document.getElementById('video-preview-post').innerHTML = "";
            document.getElementById('dummy_upload_video').style.display = "none";
            document.getElementById('real_upload_video').style.display = 'block';
            document.getElementById('filePostVideo').value = "";
        }
    </script>

@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">

        {{-- Basic profile-info ajax, stars --}}
        $('#talentBasicInfoForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let basicInfoData =  new FormData( this );
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.basic.update') }}",
                data: basicInfoData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        // return window.location.reload();
                         window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                    });
                }
            });
        });
        {{-- Basic profile-info ajax, Ends --}}


        {{-- availability ajax, stars --}}
        $('#talentAvailabilityForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let availabilityData =  $('#talentAvailabilityForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.availability.set') }}",
                data: availabilityData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        console.log(keyId+": "+value);
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- availability ajax, Ends --}}


        {{-- Services pricing ajax stars --}}
        $('#talentServicesPricingForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let servicePricingData =  $('#talentServicesPricingForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.services-pricing.set') }}",
                data: servicePricingData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        if ( keyId.includes('_mediums_') ) {
                            keyId = keyId.slice(0, -1);
                        }
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        // toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Services pricing ajax, Ends --}}


        {{-- Work history stars --}}
        $('#talentWorkFilesForm').on('submit', function (e) {
            e.preventDefault();
            let workHistoryData =  new FormData( $('#talentWorkFilesForm')[0] );
            blockUi( true );
            $.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.work-files.save') }}",
                data: workHistoryData,
                success: function (response) {
                    if ( Boolean(response.status) ) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.reload();
                    } else if ( !Boolean(response.status) ) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Work history, Ends --}}

    </script>


    <script type="text/javascript">
        var formdata = new FormData();

        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function(){
                    $(".preview_img").addClass('my_img');
                    $("#previewImg").attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
        $(document).ready(function(){
            $(".preview_img i").click(function(){
                $('.preview_img').removeClass('my_img');
            });
        });
    </script>


    <script type="text/javascript">
        var countFile=0;
        $(".file_input").on("change", function(event) {
            var files = event.target.files[0]; // also use like    var files = $('#imgInp')[0].files[0];
            var fileName = files.name;
            var fsize = files.size;
            var tmppath = URL.createObjectURL(files);
            var ext = fileName.split('.').pop().toLowerCase();
            if ((fsize / 1024) / 1024 <= 1) {
                if (countFile <= 5) {
                    if (ext == 'jpg' || ext == 'png' || ext == 'jpeg') {

                       $('.main-div').show();
                        var html = '<span class="close-spn-close"><span class="card_img remove-file-icon"  data-id="' + countFile + '">x</span><img class="preview-imgs" src="' + tmppath + '"></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    } else {
                        chatBox1.find('.main-div').show();
                        var html = '<span class="downloaded-items1234 clearfix file_download close-f-attch close-spn-close" href="#"><span class="card_file remove-file-icon"   data-id="' + countFile + '">x</span><span class="left1">' + fileName + '</span></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    }
                    countFile++;
                 //   $(this).parents('div.chat-main-div-new').find('textarea.emojionearea').focus();
                } else {
                    errorMsg('Only five files allow at a time');
                }
            } else {
                errorMsg('Select file less than 1 MB');
            }
        });

        $(document).on("click", ".remove-file-icon", function(e) {
            var div_length = $(this).closest('.main-div').children().length;
            $(this).closest('.close-spn-close').remove();
            countFile--;
            div_length--;
            if (div_length == 0) {
                // $('.chat-main-div-new').find('.main-div').html('');
                // $('.chat-main-div-new').find('.main-div').hide();
                // $('.chat-main-div-new').find(".file_input").val('');
            }
        });

        $(function() {
            $('#sort_more').change(function(){
                $('.sorting').hide();
                $('#' + $(this).val()).show();
            });
        });
        $(document).ready(function(){
            $('body').addClass('chat_body');
        });

    </script>

@endsection








