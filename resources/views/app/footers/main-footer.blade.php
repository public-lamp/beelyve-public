{{-- main footer --}}
<footer>
    <div class="footer_outer">

        <!-- main footer section -->
        <div class="footer_up">
            <div class="auto_container">
                <div class="footer_up_detail">
                    <div class="footer_up_column column1">
                        <div class="footer_info">
                            <a href="#" class="footerLogo">
                                <img src="{{ asset('project-assets/images/logo.png') }}" alt="#">
                            </a>
                             <p>A World of Talent At Your Fingertips</p>
                            <div class="social_info">
                                <ul>
                                    <li>
                                        <a href="{{ env('APP_FB_LINK') }}" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ env('APP_TWITTER_LINK') }}" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ env('APP_YOUTUBE_LINK') }}" class="youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ env('APP_INSTA_LINK') }}" class="insta"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer_up_column">
                        <div class="footerLinks">
                            <h5>Links</h5>
                            <ul>
                                @if( auth()->check() )
                                    <li><a href="{{ route('logout') }}">Logout</a></li>
                                @else
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('signup') }}">SignUp</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="footer_up_column">
                        <div class="footerLinks">
                            <h5>Company</h5>
                            <ul>
                                <li><a href="{{ route('terms-conditions.get') }}">Terms & Conditions</a></li>
                                <li><a href="{{ route('privacy-policy.get') }}">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="footer_up_column">
                        <div class="get_in_touch">
                            <ul>
                                <li><span><i class="fa fa-map-marker" aria-hidden="true"></i><p>832 Thompson Drive, San Fransisco
                                            CA 94107, United States</p></span></li>
                                <li><a href="tel:+123 345123 556"><i class="fa fa-phone" aria-hidden="true"></i><p>+123 345123 556</p></a></li>
                                <li><a href="mailto:support@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i><p>support@gmail.com</p></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyRight">
            <div class="auto_container">
                <div class="copyRight_info">
                    <p>&#169;2021-{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>, all rights reserved.</p>
                </div>
            </div>
        </div>
    </div>

{{--    --}}{{-- session messages section --}}
{{--    @include('app.sections.footers.popup-messages-section')--}}
</footer>
