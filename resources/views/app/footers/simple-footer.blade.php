<footer>
    <div class="footer_outer footer_db">

        <div class="copyRight">
            <div class="auto_container">
                <div class="copyRight_info">
                    <p>&#169;2021-{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>, all rights reserved.</p>
                </div>
            </div>
        </div>

    </div>
</footer>
