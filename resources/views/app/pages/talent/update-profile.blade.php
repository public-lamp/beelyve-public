@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Profile')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/select2/select2.css') }}" />

    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>

@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/select2/select2.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>


@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Update Profile</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="update_profile_tabs">
                                    <ul>
                                        <li><a href="#updateTab_1" class="updateTab_active">Basic Profile</a></li>
                                         <li><a href="#updateTab_2">Availability</a></li>
                                         <li><a href="#updateTab_3">Services Pricing</a></li>
                                         <li><a href="#updateTab_4">Upload files</a></li>
                                         <li><a href="#updateTab_5">Gallery</a></li>
                                         <li><a href="#updateTab_6">Social Connectivity</a></li>
                                    </ul>
                                </div>

                                {{-- <p>{{ dd( session()->get('userProfileIsComplete') ) }}</p> --}}

                                {{--Talent basic info section starts--}}
                                <div class="update_profile_data" id="updateTab_1" style="display: block;">
                                    <form id="talentBasicInfoForm">

                                        <div class="edit_profile_head">
                                            <div class="choose_profile">
                                                <div class="profile_img">
                                                    <span>
                                                        <b class="my_file">
                                                            <input type='file' onchange="readURL(this);" name="user_image" />
                                                            <small><i class="fa fa-camera fa-3" aria-hidden="true"></i>Change</small>
                                                        </b>
                                                        <img id="blah" src="{{ isset( $talentProfile['image'] ) ? $talentProfile['image'] : asset('project-assets/images/avatar_white.png') }}" alt="your image" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="popup_form">
                                            <ul>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>First Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ auth()->user()->first_name ? auth()->user()->first_name : '' }}" name="first_name" id="first_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Last Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ auth()->user()->last_name ? auth()->user()->last_name : '' }}" name="last_name" id="last_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>User Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{  auth()->check() ? auth()->user()->user_name : '' }}" name="user_name" id="user_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Select talent category</strong>
                                                        <div class="custom_dropdown">
                                                            <select name="talent_category_id" id="talent_category_id" >
                                                                <option selected disabled>Select talent category</option>
                                                                @foreach( \App\Models\Talent\TalentCategory::all() as $talentCat )
                                                                    <option value="{{ $talentCat->id  }}" @if( isset( $talentProfile['talentProfessionalInfo'], $talentProfile['talentProfessionalInfo']['talent_category_id'] ) && $talentProfile['talentProfessionalInfo']['talent_category_id'] == $talentCat->id ) selected @endif >
                                                                        {{ $talentCat->name  }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Country</strong>
                                                        <div class="custom_dropdown">
                                                            <select name="country" id="country" >
                                                                <option selected disabled>Select your country</option>
                                                                @foreach( \App\Models\Generic\Country::all() as $country )
                                                                    <option value="{{ $country->name }}" @if( isset($talentProfile['userAddress'], $talentProfile['userAddress']['country']) && $talentProfile['userAddress']['country'] === $country->name ) selected @endif >
                                                                        {{ $country->name  }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Starting rate</strong>
                                                        <div class="custom_field_input unitPrice">
                                                            <i class="fa fa-usd" aria-hidden="true"></i>
                                                            <input type="text" value="{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['start_rate'] : '' }}" name="talent_start_rate" id="talent_start_rate" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Tagline</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['tagline'] : '' }}"
                                                                   list="existingTaglinesList" name="tagline" id="tagline" />
                                                            <datalist id="existingTaglinesList">
                                                                @foreach($existingTaglines as $tagline)
                                                                    <option value="{{ $tagline }}">
                                                                @endforeach
                                                            </datalist>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Facebook Profile Link</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentBasicInfo']) ? $talentProfile['talentBasicInfo']['fb_profile_link'] : '' }}" name="fb_profile_link" id="fb_profile_link" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Instagram Profile Link</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentBasicInfo']) ? $talentProfile['talentBasicInfo']['insta_profile_link'] : '' }}" name="insta_profile_link" id="insta_profile_link" />
                                                        </div>
                                                    </div>
                                                </li>
{{--                                                <li class="">--}}
{{--                                                    <div class="custom_field">--}}
{{--                                                        <strong> linkedin Profile Link</strong>--}}
{{--                                                        <div class="custom_field_input">--}}
{{--                                                            <input type="text" value="{{ isset($talentProfile['talentBasicInfo']) ? $talentProfile['talentBasicInfo']['linkedin_profile_link'] : '' }}" name="linkedin_profile_link" id="linkedin_profile_link" />--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>My Service</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['service'] : '' }}" name="talent_service" id="talent_service" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Experience</strong>
                                                        <div class="custom_field_textarea">
                                                            <textarea name="experience" id="experience">{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['experience'] : '' }}</textarea>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>About</strong>
                                                        <div class="custom_field_textarea">
                                                            <textarea name="talent_about" id="talent_about">{{ isset($talentProfile['about']) ? $talentProfile['about'] : '' }}</textarea>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Save" id="updateTalentProfileFormBtn" />
                                        </div>

                                        {{--view more/next button--}}
                                        {{-- <div class="viewMore">--}}
                                        {{-- <a href="javascript:void(0)" class="show_next">Next<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>--}}
                                        {{-- </div>--}}

                                    </form>
                                </div>
                                {{--Talent basic info section ends--}}


                                {{-- Availabbility section starts --}}
                                <div class="update_profile_data" id="updateTab_2">
                                    <form id="talentAvailabilityForm">
                                        @csrf

                                        <div class="availability_detail">
                                            <div class="availability_options">
                                                <ul>
                                                     @foreach( \App\Models\Generic\AvailabilityCategory::all() as $availabilityCategory )
                                                         <li>
                                                            <div class="chooseOptionDetail">
                                                                <label>
                                                                    <input type="radio" class="option-input radio" value="{{ $availabilityCategory->id }}"
                                                                           name="availability_category"
                                                                           data-avail-name = '{{ $availabilityCategory->name }}'
                                                                           @if( isset($talentProfile['availabilityCategory']) && $availabilityCategory->id == $talentProfile['availabilityCategory']['availability_category_id'] ) checked @endif />
                                                                    {{ $availabilityCategory->name }}
                                                                </label>
                                                            </div>
                                                        </li>
                                                     @endforeach
                                                </ul>
                                            </div>

                                            <div class="availability_info">
                                                @if( isset($talentProfile['availabilityCategory']) && (!empty($talentProfile['userAvailabilityDays']) && $talentProfile['availabilityCategory']['availability_category_id'] == 2) )
                                                    <div class="avaiable_days" style="display: block;">
                                                        <div class="avaiable_daysInner">

                                                            <div class="d-flex days_tab">
                                                                <div class="btn_days">
                                                                    @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                        <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                            {{ substr($weekDay->name, 0, 3) }}
                                                                            <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                                   name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                                   data-child="{{ $weekDay->slug }}_parent" />
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            @foreach($talentProfile['userAvailabilityDays'] as $userAvailDayKey => $availabilityDay)
                                                                <div class="{{ $availabilityDay['weekDay']['slug'] }}_parent main_parents" id="{{ $availabilityDay['weekDay']['slug'] }}_parent" style="display: none;">
                                                                    <div class="sound-signal">
                                                                        <strong class="reset_time"><a href="javascript:void(0);">Reset</a></strong>
                                                                    </div>
                                                                    <div class="time_sec">
                                                                        @if( !$availabilityDay['availabilitySlots']->isEmpty() )
                                                                            @foreach($availabilityDay['availabilitySlots'] as $slotKey => $availabilitySlot)
                                                                                <div class="price_from">
                                                                                    <span>Time From</span>
                                                                                    <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                           name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_from][]"
                                                                                           id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                           data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                           value="{{ $availabilitySlot['time_from'] }}"
                                                                                           readonly="" />
                                                                                </div>
                                                                                <div class="price_from time_to">
                                                                                    <span> Time To</span>
                                                                                    <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                           name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_to][]"
                                                                                           id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                           value="{{ $availabilitySlot['time_to'] }}"
                                                                                           readonly="" />
                                                                                </div>
                                                                            @endforeach
                                                                        @else
                                                                            <div class="price_from">
                                                                                <span>Time From</span>
                                                                                <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                       name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_from][]"
                                                                                       id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                       data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                       placeholder="00:00" readonly="" />
                                                                            </div>
                                                                            <div class="price_from time_to">
                                                                                <span> Time To</span>
                                                                                <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                       name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_to][]"
                                                                                       id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                       placeholder="00:00" readonly="" />
                                                                            </div>
                                                                        @endif
                                                                        <div class="add_hours_btn">
                                                                            <div class="cssCircle add_hours" data-name="{{ $availabilityDay['weekDay']['slug'] }}">
                                                                                Add
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avaiable_days" style="/* display: block; */">
                                                        <div class="avaiable_daysInner">
                                                            <div class="d-flex days_tab">
                                                                <div class="btn_days">
                                                                    @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                        <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                            {{ substr($weekDay->name, 0, 3) }}
                                                                            <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                                   name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                                   data-child="{{ $weekDay->slug }}_parent" />
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                <div class="{{ $weekDay->slug }}_parent main_parents" id="{{ $weekDay->slug }}_parent" style="display: none;">
                                                                    <div class="sound-signal">
                                                                        <strong class="reset_time"><a href="javascript:void(0);">Reset</a></strong>
                                                                    </div>
                                                                    <div class="time_sec">
                                                                        <div class="price_from">
                                                                            <span>Time From</span>
                                                                            <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                   name="days[{{ $weekDay->slug }}][time][time_from][]"
                                                                                   id="days_{{ $weekDay->slug }}_time_time_from_0"
                                                                                   data-time-to-id="days_{{ $weekDay->slug }}_time_time_to_0"
                                                                                   placeholder="00:00" readonly="" />
                                                                        </div>
                                                                        <div class="price_from time_to">
                                                                            <span> Time To</span>
                                                                            <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                   name="days[{{ $weekDay->slug }}][time][time_to][]"
                                                                                   id='days_{{ $weekDay->slug }}_time_time_to_0'
                                                                                   placeholder="00:00" readonly="" />
                                                                        </div>
                                                                        <div class="add_hours_btn">
                                                                            <div class="cssCircle add_hours" data-name="{{ $weekDay->slug }}">
                                                                                Add
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Submit" id="talentAvailabilityFormBtn" />
                                        </div>
                                    </form>
                                </div>
                                {{-- Avaiability Section ends --}}


                                {{-- Services pricing section start --}}
                                <div class="update_profile_data" id="updateTab_3">
                                    <form id="talentServicesPricingForm">
                                        @csrf
                                        <div class="availability_detail">
                                            <div class="services_pricing_sec">
                                                @if( $talentProfile['talentServicesPricing']->isNotEmpty() )
                                                    @foreach($talentProfile['talentServicesPricing'] as $servicePricingType)
                                                        <div class="services_pricing_detail">
                                                            <div class="popup_form">
                                                                <h4 class="basic_tittle">{{ $servicePricingType['servicesPricingType']['name'] }} Services</h4>
                                                                <ul>
                                                                    <input type="hidden" value="{{ $servicePricingType['servicesPricingType']['id'] }}" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][id]" />
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Time Duration</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][duration]" id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_duration" >
                                                                                    <option selected disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesPricingSlot::all() as $slot)
                                                                                        <option value="{{ $slot->id }}" @if($servicePricingType['services_pricing_slot_id'] == $slot->id) selected @endif>{{ $slot->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Price</strong>
                                                                            <div class="custom_field_input unitPrice">
                                                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                                                <input type="text" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][price]"
                                                                                       value="{{ $servicePricingType['price'] }}"
                                                                                       id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_price" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Communication Medium</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select class="selectMultiple" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][mediums][]" id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_medium" multiple="multiple">
                                                                                    <option disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesMedium::all() as $serMedium)
                                                                                        <option value="{{ $serMedium->id }}"
                                                                                            @foreach($servicePricingType['servicesPricingType']['talentServicesMediums'] as $medium)
                                                                                                @if($medium['servicesMedium']['id'] == $serMedium->id) selected @endif
                                                                                            @endforeach >
                                                                                            {{ $serMedium->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                     </li>

                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Main Features</strong>
                                                                            <div class="custom_field_input addMain_feature">
                                                                                @foreach($servicePricingType['main_features'] as $featureKey => $feature)
                                                                                    <input type="text" name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][main_features][{{ $featureKey }}]"
                                                                                           id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_main_features_{{ $featureKey }}"
                                                                                           value="{{ $feature }}"
                                                                                           placeholder="{{ $featureKey }}" />
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Description</strong>
                                                                            <div class="custom_field_textarea">
                                                                                <textarea name="pricing[{{ $servicePricingType['servicesPricingType']['slug'] }}][description]" id="pricing_{{ $servicePricingType['servicesPricingType']['slug'] }}_description">{{ $servicePricingType['description'] }}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    @foreach(\App\Models\Talent\ServicesPricingType::all() as $pricingTypeKey => $pricingType)
                                                        <div class="services_pricing_detail">
                                                            <div class="popup_form">
                                                                <h4 class="basic_tittle">{{ $pricingType->name }} Services</h4>
                                                                <ul>
                                                                    <input type="hidden" value="{{ $pricingType->id }}" name="pricing[{{ $pricingType->slug }}][id]" />
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Time Duration</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select name="pricing[{{ $pricingType->slug }}][duration]" id="pricing_{{ $pricingType->slug }}_duration" >
                                                                                    <option selected disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesPricingSlot::all() as $slot)
                                                                                        <option value="{{ $slot->id }}">{{ $slot->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="w-50">
                                                                        <div class="custom_field">
                                                                            <strong>Price</strong>
                                                                            <div class="custom_field_input unitPrice">
                                                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][price]" id="pricing_{{ $pricingType->slug }}_price" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Communication Medium</strong>
                                                                            <div class="custom_dropdown">
                                                                                <select class="selectMultiple" name="pricing[{{ $pricingType->slug }}][mediums][]" id="pricing_{{ $pricingType->slug }}_mediums" multiple="multiple">
                                                                                    <option disabled>Select Duration</option>
                                                                                    @foreach(\App\Models\Talent\ServicesMedium::all() as $serMedium)
                                                                                        <option value="{{ $serMedium->id }}">{{ $serMedium->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Main Features</strong>
                                                                            <div class="custom_field_input addMain_feature">
                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][main_features][]" id="pricing_{{ $pricingType->slug }}_main_features_0" placeholder="1" />

                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][main_features][]" id="pricing_{{ $pricingType->slug }}_main_features_1" placeholder="2" />

                                                                                <input type="text" name="pricing[{{ $pricingType->slug }}][main_features][]" id="pricing_{{ $pricingType->slug }}_main_features_2" placeholder="3" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="">
                                                                        <div class="custom_field">
                                                                            <strong>Description</strong>
                                                                            <div class="custom_field_textarea">
                                                                                <textarea name="pricing[{{ $pricingType->slug }}][description]" id="pricing_{{ $pricingType->slug }}_description"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                         </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Submit" id="talentServicesPricingFormBtn" />
                                        </div>
                                    </form>
                                </div>


                                {{-- Previous work section start --}}
                                <div class="update_profile_data" id="updateTab_4">
                                    <form id="talentWorkFilesForm">
                                        @csrf
                                        <div class="addProfile_media">
                                            <h4>Image Upload</h4>
                                            <div class="upload_logo">
                                                <div class="chat-main-div-new">
                                                    <label class="chat_attach_btn">
                                                        <input class="file_input form-control form-control-lg" type="file" name="work_images[]" style="display: none;">
                                                        <i class="fa fa-upload"></i>
                                                    </label>
                                                    <div class="main-div"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="addProfile_media ">
                                            <h4>Video Upload</h4>
                                            <div class="w-100 float-left" id="forVideoAlert"></div>
                                            <div class="upload_logo">
                                                <div class="ck_editor_main_div">
                                                    <div id="video-preview-post" class="text-center" style="display:none; margin-bottom: 20px;"></div>
                                                    <div id="preview"></div>
                                                </div>
                                                <ul class="publish_ul">
                                                    <li id="real_upload_video">
                                                        <label class="">
                                                            <input type="file" id="filePostVideo" name="work_video" accept="video/*" >
                                                            <i class="fa fa-video-camera" aria-hidden="true"></i>
                                                            <small></small>
                                                        </label>
                                                        <b>Add Video Here</b>
                                                    </li>
                                                    <li id="dummy_upload_video" style="display:none;">
                                                        <label class="file_upload_btn" style="background:#eff5f72b;cursor: not-allowed;">
                                                            <i class="fa fa-video-camera" aria-hidden="true"></i>
                                                        </label>
                                                        <b>Add Video Here</b>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Upload data" />
                                        </div>
                                    </form>
                                </div>


                                <!-- Gallery tab -->
                                <div class="update_profile_data" id="updateTab_5">
                                    <div class="updated_gallery_img">
                                        <h4>Uploaded Images</h4>
                                        <div class="updated_gallery_img_list">
                                            @if( $workImagesCount > 0 )
                                                <ul>
                                                    @foreach( $talentProfile['talentWorkFiles'] as $workFile)
                                                        @if($workFile['type'] === 'image')
                                                            <li>
                                                                <div class="gallery_img_info">
                                                                    <span><img src="{{ asset($workFile['file_url']) }}" alt="image" /></span>
                                                                    {{-- <a href="#" class="edit_icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>--}}
                                                                    <a href="{{ route('talent.work-file.delete', ['id' => $workFile['id']]) }}" class="trash_icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                                    <strong>Lorem Ipsum</strong>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <div class="no_data_here">
                                                    <p>No uploaded images found</p>
                                                    {{-- <div class="custom_submit ">--}}
                                                    {{-- <a href="#">Upload Image</a>--}}
                                                    {{-- </div>--}}
                                                </div>
                                            @endif
                                        </div>
                                        <br>

                                        <h4>Uploaded Video</h4>
                                        <div class="updated_gallery_img_list update_myVideo">
                                            <ul>
                                                @if( $workVideosCount > 0 )
                                                    @foreach( $talentProfile['talentWorkFiles'] as $workFile)
                                                        @if($workFile['type'] === 'video')
                                                            <li>
                                                                <div class="gallery_img_info">
                                                                    <span>
                                                                        <iframe width="560" height="315" src="{{ asset($workFile['file_url']) }}" frameborder="0" allowfullscreen></iframe></iframe>
                                                                    </span>
                                                                    {{-- <a href="#" class="edit_icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>--}}
                                                                    <a href="{{ route('talent.work-file.delete', ['id' => $workFile['id']]) }}" class="trash_icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                                    <strong>Lorem Ipsum</strong>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div class="no_data_here">
                                                        <p>No Data Uploaded yet.</p>
                                                        {{-- <div class="custom_submit ">--}}
                                                        {{-- <a href="#">Upload Video</a>--}}
                                                        {{-- </div>--}}
                                                    </div>
                                                @endif
                                            </ul>
                                        </div>

                                        {{-- <div class="custom_submit ">--}}
                                        {{--  <input type="submit" value="Save" id="" />--}}
                                        {{-- </div>--}}
                                    </div>
                                </div>

                                <!-- social connectivity tab -->
                                <div class="update_profile_data" id="updateTab_6">
                                    <div class="social_activity_info">
                                        <h4><b class="fb_bg"><i class="fa fa-facebook" aria-hidden="true"></i></b>Facebook Connectivity</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et scelerisque enim. Pellentesque sit amet ultrices augue.
                                                Nam consectetur erat ex, nec porttitor dolor tempus sit amet. Duis molestie bibendum consectetur. In convallis imperdiet efficitur.
                                                Maecenas a dolor at turpis consequat interdum. Nam faucibus enim ex, et eleifend felis aliquet quis.</p>
                                        <div class="custom_submit">
                                            {{-- <input type="submit" value="Save" id=""> --}}
                                            <a href="{{ route('social.login', ['provider' => 'facebook']) }}">Connect With Facebook</a>
                                        </div>
                                    </div>
                                    <div class="social_activity_info">
                                        <h4><b class="instagram_bg"><i class="fa fa-instagram" aria-hidden="true"></i></b>Instagram Connectivity</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et scelerisque enim. Pellentesque sit amet ultrices augue.
                                                Nam consectetur erat ex, nec porttitor dolor tempus sit amet. Duis molestie bibendum consectetur. In convallis imperdiet efficitur.
                                                Maecenas a dolor at turpis consequat interdum. Nam faucibus enim ex, et eleifend felis aliquet quis.</p>
                                        <div class="custom_submit">
                                            <!-- <input type="submit" value="Save" id=""> -->
                                            <a href="javascript:void(0)">Connect With Instagram</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- work images script --}}
        <script type="text/javascript">
            // media gallery image
            // $('#work_images').ssi_uploader();
        </script>

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')

    <script type="text/javascript">
        // select2 multi-tag select
        $(document).ready(function() {
            $('.selectMultiple').select2();
        })
    </script>

    {{--    video upload scripts --}}
    <script type="text/javascript">
        var fileInputVideo = document.getElementById('filePostVideo');
        var previewVideo = document.getElementById('video-preview-post');
        previewVideo.style.display = "block";

        fileInputVideo.addEventListener('change', function (e) {
                let videoSizeInMbs = e.target.files[0].size/1000000; // convert to MBs
                FileUploadVideoPath = fileInputVideo.value;
                var VideoExtension = FileUploadVideoPath.substring(FileUploadVideoPath.lastIndexOf('.') + 1).toLowerCase();
                //The file uploaded is an image
                if (VideoExtension != "png" && VideoExtension != "avi" && VideoExtension != "flv" && VideoExtension != "wmv" && VideoExtension != "mov" && VideoExtension != "mp4") {
                    document.getElementById('forVideoAlert').innerHTML = `<div class="alert alert-warning"><strong>Warning!</strong> Only .AVI, .FLV, .MOV, .MP4 and WMV files are allowed.</div>`;;
                    setTimeout(function () {
                        $('#forVideoAlert').fadeOut('slow');
                    }, 5000);
                    return;
                }

                if ( videoSizeInMbs > 10 ) {
                    document.getElementById('forVideoAlert').innerHTML = `<div class="alert alert-warning"><strong>Warning!</strong>Video size can not be more than 10 MB.</div>`;;
                    setTimeout(function () {
                        $('#forVideoAlert').fadeOut('slow');
                    }, 5000);
                    return;
                }

                var url = URL.createObjectURL(e.target.files[0]);
                document.getElementById('real_upload_video').style.display = "none";
                document.getElementById('dummy_upload_video').style.display = "block";
                previewVideo.innerHTML = `<div class="closing" onclick="removeItemInPost('video')"><i class="fa fa-close"></i></div><video  controls style="width:100%;"><source src="` +url+ `" type="video/mp4">Your browser does not support HTML5 video.</video>`;;
            }
        );

        function removeItemInPost(item, group = null) {
            document.getElementById('video-preview-post').innerHTML = "";
            document.getElementById('dummy_upload_video').style.display = "none";
            document.getElementById('real_upload_video').style.display = 'block';
            document.getElementById('filePostVideo').value = "";
        }
    </script>

@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">

        {{-- Basic profile-info ajax, stars --}}
        $('#talentBasicInfoForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let basicInfoData =  new FormData( this );
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.basic.update') }}",
                data: basicInfoData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        // return window.location.reload();
                         window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                    });
                }
            });
        });
        {{-- Basic profile-info ajax, Ends --}}


        {{-- availability ajax, stars --}}
        $('#talentAvailabilityForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let availabilityData =  $('#talentAvailabilityForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.availability.set') }}",
                data: availabilityData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        console.log(keyId+": "+value);
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- availability ajax, Ends --}}


        {{-- Services pricing ajax stars --}}
        $('#talentServicesPricingForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let servicePricingData =  $('#talentServicesPricingForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.services-pricing.set') }}",
                data: servicePricingData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.href = "{{ route('talent-dashboard') }}"
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        if ( keyId.includes('_mediums_') ) {
                            keyId = keyId.slice(0, -1);
                        }
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        // toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Services pricing ajax, Ends --}}


        {{-- Work history stars --}}
        $('#talentWorkFilesForm').on('submit', function (e) {
            e.preventDefault();
            let workHistoryData =  new FormData( $('#talentWorkFilesForm')[0] );
            blockUi( true );
            $.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.work-files.save') }}",
                data: workHistoryData,
                success: function (response) {
                    if ( Boolean(response.status) ) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.reload();
                    } else if ( !Boolean(response.status) ) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Work history, Ends --}}

    </script>


    <script type="text/javascript">
        var formdata = new FormData();

        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function(){
                    $(".preview_img").addClass('my_img');
                    $("#previewImg").attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
        $(document).ready(function(){
            $(".preview_img i").click(function(){
                $('.preview_img').removeClass('my_img');
            });
        });
    </script>


    <script type="text/javascript">
        var countFile=0;
        $(".file_input").on("change", function(event) {
            var files = event.target.files[0]; // also use like    var files = $('#imgInp')[0].files[0];
            var fileName = files.name;
            var fsize = files.size;
            var tmppath = URL.createObjectURL(files);
            var ext = fileName.split('.').pop().toLowerCase();
            if ((fsize / 1024) / 1024 <= 1) {
                if (countFile <= 5) {
                    if (ext == 'jpg' || ext == 'png' || ext == 'jpeg') {

                       $('.main-div').show();
                        var html = '<span class="close-spn-close"><span class="card_img remove-file-icon"  data-id="' + countFile + '">x</span><img class="preview-imgs" src="' + tmppath + '"></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    } else {
                        chatBox1.find('.main-div').show();
                        var html = '<span class="downloaded-items1234 clearfix file_download close-f-attch close-spn-close" href="#"><span class="card_file remove-file-icon"   data-id="' + countFile + '">x</span><span class="left1">' + fileName + '</span></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    }
                    countFile++;
                 //   $(this).parents('div.chat-main-div-new').find('textarea.emojionearea').focus();
                } else {
                    errorMsg('Only five files allow at a time');
                }
            } else {
                errorMsg('Select file less than 1 MB');
            }
        });

        $(document).on("click", ".remove-file-icon", function(e) {
            var div_length = $(this).closest('.main-div').children().length;
            $(this).closest('.close-spn-close').remove();
            countFile--;
            div_length--;
            if (div_length == 0) {
                // $('.chat-main-div-new').find('.main-div').html('');
                // $('.chat-main-div-new').find('.main-div').hide();
                // $('.chat-main-div-new').find(".file_input").val('');
            }
        });

        $(function() {
            $('#sort_more').change(function(){
                $('.sorting').hide();
                $('#' + $(this).val()).show();
            });
        });
        $(document).ready(function(){
            $('body').addClass('chat_body');
        });

    </script>

@endsection








