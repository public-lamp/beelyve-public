@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Messages')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    {{--    @include('app.headers.simple-header')--}}
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <div class="event_pages db_messageChat">
                <div class="auto_container">
                    <div class="dasbord_container_detail">

                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h5>Messages List</h5>
                            </div>
                            {{-- <div class="sorted_dropdown">--}}
                            {{-- <div class="custom_dropdown">--}}
                            {{-- <select>--}}
                            {{-- <option>Sort by: Date</option>--}}
                            {{-- <option>Sort by: Date 1</option>--}}
                            {{-- <option>Sort by: Date 2</option>--}}
                            {{-- </select>--}}
                            {{-- </div>--}}
                            {{-- </div>--}}
                        </div>

                        @if( count($conversations) > 0 )
                            @include('app.sections.chat.messages_section')
                        @else
                            <div class="event_activity_section text-center">
                                <p>No Chat history found</p>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    @include('app.sections.chat.chat_script_section')
@endsection



