@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Requests')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    {{--    @include('app.headers.simple-header')--}}
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <div class="event_pages db_messageChat">
                <div class="auto_container">

                    {{-- Projects listings starts --}}
                    <div class="dasbord_container_detail">

                        {{-- booking fileters section --}}
                        <div class="available_feature_sorted">
                            <div class="sorted_nav tanelt_sort on_book_list">
                                <form method="GET" action="{{ route('talent.bookings.requests') }}">
                                    <div class="sorted_dropdown dropdown_sort ml-0">
                                        <div class="custom_dropdown">
                                            <select name="booking_status">
                                                <option selected disabled>Search booking</option>
                                                <option value="requested" @if($bookingsStatus === 'requested') selected @endif>Booking Requests</option>
                                                <option value="rejected" @if($bookingsStatus === 'rejected') selected @endif>Rejected Bookings</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mainSearch_input">
                                        {{-- <input type="text" name="search_text" value="" placeholder="Search Talent…">--}}
                                    </div>
                                    <div class="sorted_dropdown"></div>
                                    <div class="mainSearch_bttn">
                                        <input type="submit" value="Search">
                                    </div>
                                </form>
                            </div>
                        </div>

                        {{-- Booking listings section --}}
                        <div class="dashboard_tabs_outer">
                            {{-- active projects listing --}}
                            @if($bookingsStatus === 'requested')
                                <div class="dashboard_tabs_data">
                                    <div class="dashboard_dataTable_head">
                                        <div class="data_col w-25">
                                            Moderator
                                        </div>

                                        <div class="data_col w-20">
                                            <h5>Start date & time</h5>
                                        </div>
                                        <div class="data_col w-10">
                                            <h5>Total</h5>
                                        </div>
                                        <div class="data_col w-30">
                                            <h5>Actions</h5>
                                        </div>
                                        <div class="data_col w-15">
                                            <h5>Status</h5>
                                        </div>
                                    </div>

                                    {{-- Project listing section --}}
                                    <div class="dashboard_tabs_dataList">
                                        <ul>
                                            @if( $bookings->isNotEmpty() )
                                                @foreach($bookings as $booking)
                                                    <li>
                                                        <div class="dashboard_tabs_dataInfo">
                                                            <div class="data_col w-25">
                                                                <div class="data_profile_info">
                                                                    <span><img src="{{ isset($booking['moderator']['image']) ? $booking['moderator']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                    <div class="data_profile_infoText">
                                                                        <h4>{{ $booking['moderator']['first_name']. ' ' .$booking['moderator']['last_name'] }}</h4>
                                                                        <p>{{ $booking['title'] }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="data_col w-20">
                                                                <small>{{ parseDateTimeWithTimeZone($booking['time_from'], auth()->user()->time_zone) }}</small>
                                                            </div>
                                                            <div class="data_col w-10">
                                                                <small>${{ $booking['servicePricing']['price'] }}</small>
                                                            </div>
                                                            <div class="data_col w-30">
                                                                <div class="action_view">
                                                                    <a href="{{ route('booking.detail', ['booking_id' => $booking->id]) }}">
                                                                        <button type="button" class="btn btn-info">View</button>
                                                                        {{--  <i class="fa fa-eye" aria-hidden="true"></i>--}}
                                                                    </a>
                                                                    @if( in_array($booking['booking_status_id'], [3])  )
                                                                        <a href="{{ route('talent.booking.request.accept', ['booking_id' => $booking->id, 'accept' => true]) }}" class="action_accept" id="acceptRequest">
                                                                            {{-- <i class="fa fa-handshake-o" aria-hidden="true"></i>--}}
                                                                            <button type="button" class="btn btn-success">Accept</button>
                                                                        </a>
                                                                        <a href="{{ route('talent.booking.request.accept', ['booking_id' => $booking->id, 'accept' => false]) }}" class="action_reject" id="rejectRequest">
                                                                            <button type="button" class="btn btn-danger">Reject</button>
                                                                            {{-- <i class="fa fa-ban" aria-hidden="true"></i>--}}
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="data_col w-15">
                                                                <div class="activeBttn">
                                                                    <a href="javascript:void(0)" class="default_bttn">{{ $booking['bookingStatus']['name'] }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <!-- No projects section -->
                                                <div class="no_projecty_info">
                                                    <strong>No projects to show</strong>
                                                </div>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="">
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination">{{ $bookings->appends(request()->except('page'))->links() }}</ul>
                                        </nav>
                                    </div>
                                </div>
                            @endif

                            {{-- active projects listing --}}
                            @if($bookingsStatus === 'rejected')
                                <div class="dashboard_tabs_data">
                                    <div class="dashboard_dataTable_head">
                                        <div class="data_col w-35"></div>
                                        <div class="data_col w-20">
                                            <h5>Completion Time</h5>
                                        </div>
                                        <div class="data_col w-10">
                                            <h5>Total</h5>
                                        </div>
                                        <div class="data_col w-20">
                                            <h5>Actions</h5>
                                        </div>
                                        <div class="data_col w-15">
                                            <h5>Status</h5>
                                        </div>
                                    </div>

                                    <div class="dashboard_tabs_dataList">
                                        <ul>
                                            @if( $bookings->isNotEmpty() )
                                                @foreach($bookings as $booking)
                                                    <li>
                                                        <div class="dashboard_tabs_dataInfo">
                                                            <div class="data_col w-35">
                                                                <div class="data_profile_info">
                                                                    <span><img src="{{ isset($booking['moderator']['image']) ? $booking['moderator']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                    <div class="data_profile_infoText">
                                                                        <h4>{{ $booking['moderator']['name'] }}</h4>
                                                                        <p>{{ $booking['title'] }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="data_col w-20">
                                                                <small>{{ parseDateTimeWithTimeZone($booking['time_to'], auth()->user()->time_zone) }}</small>
                                                            </div>
                                                            <div class="data_col w-15">
                                                                <small>${{ $booking['servicePricing']['price'] }}</small>
                                                            </div>
                                                            <div class="data_col w-15">
                                                                <div class="action_view">
                                                                    <a href="{{ route('booking.detail', ['booking_id' => $booking->id]) }}">
                                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="data_col w-15">
                                                                <div class="activeBttn">
                                                                    <a href="javascript:void(0)" class="default_bttn">{{ $booking['bookingStatus']['name'] }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <!-- No projects section -->
                                                <div class="no_projecty_info">
                                                    <strong>No projects to show</strong>
                                                </div>
                                            @endif
                                        </ul>
                                        <div class="">
                                            <nav aria-label="Page navigation example">
                                                <ul class="pagination">{{ $bookings->appends(request()->except('page'))->links() }}</ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">
        $('#acceptRequest').click(function (e) {
            blockUi(true);
        });
        $('#rejectRequest').click(function (e) {
            blockUi(true);
        });
    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection



