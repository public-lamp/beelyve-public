@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Management')

@section('page-styles')
@endsection

@section('page-scripts')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <div class="event_page db_event_page dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">

                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h5>Event #{{ $booking['id'] }}</h5>
                            </div>
                            <div class="our_earning">
                                <strong>{{ parseDateTimeWithTimeZone($booking['time_from'], auth()->user()->time_zone) }}</strong>
                            </div>
                        </div>

                        <div class="event_section_head">
                            <div class="db_profile">
                                <div class="featureWeek_info">
                                    <div class="feartureProflie_info">
                                        <div class="featureProfile_avatar">
                                            <span>
                                                <img src="{{ isset($booking['moderator']['image']) ? $booking['moderator']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featureProfile_avatar_inner">
                                        <div class="featureProfile_avatar_text">
                                            <strong>{{ $booking['moderator']['user_name'] }}</strong>
                                            <small>Moderator</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="display: none;">
                                <form id="bookingInfoForm">
                                    <input type="hidden" name="booking_id" value="{{ $booking['id'] }}" />
                                    <input type="hidden" name="booking_duration" value="{{ $booking['total_minutes_duration'] }}" />
                                    <input type="hidden" id="booking_activity_array" data-activity="{{ $booking['timerActivity'] }}" />
                                    @if(isset($booking['timerActivity']))
                                        <input type="hidden" id="timer_in_progress" value="{{ $booking['timerActivity']['in_progress'] }}" />
                                        <input type="hidden" id="expected_end_time" value="{{ isset($booking['timerActivity']['expected_end_time']) ? date('D, d M Y  H:i:s T', strtotime($booking['timerActivity']['expected_end_time'])) : null }}" />
                                        <input type="hidden" id="remaining_time_minutes" value="{{ $booking['timerActivity']['remaining_time_minutes'] }}" />
                                        <input type="hidden" id="remaining_hours" value="{{ $booking['timerActivity']['remaining_hours'] }}" />
                                        <input type="hidden" id="remaining_minutes" value="{{ $booking['timerActivity']['remaining_minutes'] }}" />
                                        <input type="hidden" id="remaining_seconds" value="{{ $booking['timerActivity']['remaining_seconds'] }}" />
                                    @endif
                                </form>
                            </div>

                            <div class="event_section_head_right">
                                <div class="duration_timer">
                                    <label>Duration {{ $booking['bookingServicesDetail']['service_pricing_slot']->duration }} mints</label>
                                    <ul>
                                        <li>
                                            <span id="timer_hours">0</span>
                                            <strong>Hours</strong>
                                        </li>
                                        <li>
                                            <span id="timer_minutes">0</span>
                                            <strong>Minutes</strong>
                                        </li>
                                        <li>
                                            <span id="timer_seconds">0</span>
                                            <strong>Seconds</strong>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dispute_alert">
                                    @if( in_array($booking['booking_status_id'], [4, 6, 7]) ) <!-- if booking assigned, in-progress or pending approval by moderator -->
                                        <span id="dispute_btn">
                                            <img src="{{ asset('project-assets/images/alert_icon.png')}}" alt="Dispute" />Dispute
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="db_event_bttns">
                            <div class="event_bttns">
                                @if( in_array($booking['booking_status_id'], [4, 6]) ) <!-- if booking assigned or in-progress -->
                                    <a href="javascript:void(0);" class="default_bttn" id="talentPresentBtn">I’m Present</a>
                                    <a href="javascript:void(0)" class="default_bttn" id="talentMarkTimeCompletedBtn">Time Complete</a>
                                @elseif( in_array($booking['booking_status_id'], [7]) ) <!-- if booking assigned or in-progress -->
                                    <a href="javascript:void(0);" class="default_bttn" id="talentPresentBtn">I’m Present</a>
                                    <a href="javascript:void(0)" class="default_bttn approve_bttn no_cursor" id="talentMarkTimeCompletedBtn">Time Complete</a>
                                @else
                                    <a href="javascript:void(0);" class="default_bttn approve_bttn no_cursor" id="talentPresentBtn">I’m Present</a>
                                    <a href="javascript:void(0)" class="default_bttn approve_bttn no_cursor" id="talentMarkTimeCompletedBtn">Time Complete</a>
                                @endif
                            </div>
                        </div>

                        <div class="event_activity_section">
                            {{-- left section --}}
                            <div class="event_activity_fl">
                                <div class="event_activity_tabs">
                                    <ul>
                                        <li><a href="#activity_tab" class="activity_active">Event Activities</a></li>
                                         <li><a href="#services_tab">Event Chat</a></li>
                                    </ul>
                                </div>
                                <div class="activity_chat_box">
                                    <div class="chatBox_tabs" style="display: block" id="activity_tab">
                                        <div class="activity_chat_box_info setting_chat">
                                            {{-- <label class="activity_date">Event Activities</label> --}}
                                            <div class="activity_info_top" id="talentEventActivityTopList">
                                                <div class="activity_info_list">
                                                    <ul id="event_activities_list">
                                                        @if(count($booking['eventActivities']) > 0)
                                                            @foreach($booking['eventActivities'] as $eventActivity)
                                                                <li>
                                                                    <div class="activity_info_status">
                                                                        <div class="status_info">
                                                                            <span class="@if(in_array($eventActivity['activity_type_id'], [1, 2])) status_active @else status_inactive @endif"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                                        </div>
                                                                        <div class="activity_info_perform_outer">
                                                                            <div class="activity_info_perform">
                                                                                <strong>{{ $eventActivity['activityType']['about'] }}</strong>
                                                                                <span>{{ parseTimeWithTimeZone($eventActivity['performed_at'], session()->get('browserTimeZone')) }}</span>
                                                                            </div>
                                                                            <span class="float-right mr-4 mt-1">{{ parseDateWithTimeZone($eventActivity['performed_at'], session()->get('browserTimeZone')) }}</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        @else
                                                            {{-- no activities section--}}
                                                            <li>
                                                                <div class="no_data_here">
                                                                    <p>No activity found</p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chatBox_tabs" id="services_tab">
                                        {{-- bottom section --}}
                                        <div class="activity_info_bottom setting_chat2" id="talentEventActivityBottomList">
                                            <div class="chat_list">
                                                <ul id="messagesList">
                                                    @if(!empty($bookingChatMessages))
                                                        @foreach($bookingChatMessages as $message)
                                                            @if($message['current_user_is_sender'])
                                                                <li>
                                                                    <div class="chat_list_info user1">
                                                                        <div class="chatAvatar">
                                                                            <div class="featureProfile_avatar">
                                                                                <span>
                                                                                    <img src="{{ isset($message['currentUser']['image']) ? $message['currentUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                                                    <b class="sattus_online"></b>
                                                                                </span>
                                                                                <strong>You</strong>
                                                                            </div>
                                                                        </div>
                                                                        <div class="chat_conversation">
                                                                            @if($message['messageType']['slug'] === 'file')
                                                                                <a href="{{ $message['message'] }}">File</a>
                                                                            @elseif($message['messageType']['slug'] === 'image')
                                                                                <div class="chat_message_img">
                                                                                    <img src="{{ $message['message'] }}" alt="img">
                                                                                </div>
                                                                            @else
                                                                                <div class="chat_conversation_info">
                                                                                    <strong>{{ $message['message'] }}</strong>
                                                                                </div>
                                                                            @endif
                                                                            <div class="chat_seen">
                                                                                <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @else
                                                                <li>
                                                                    <div class="chat_list_info">
                                                                        <div class="chatAvatar">
                                                                            <div class="featureProfile_avatar">
                                                                                <span>
                                                                                    <img src="{{ isset($message['otherUser']['image']) ? $message['otherUser']['image']:  asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                                                    <b class="sattus_online"></b>
                                                                                </span>
                                                                                <strong>{{ $message['otherUser']['first_name']. ' ' .$message['otherUser']['last_name'] }}</strong>
                                                                            </div>
                                                                        </div>
                                                                        <div class="chat_conversation">
                                                                            @if($message['messageType']['slug'] === 'file')
                                                                                <a href="{{ $message['message'] }}">File</a>
                                                                            @elseif($message['messageType']['slug'] === 'image')
                                                                                <div class="chat_message_img">
                                                                                    <img src="{{ $message['message'] }}" alt="img">
                                                                                </div>
                                                                            @else
                                                                                <div class="chat_conversation_info user2">
                                                                                    <strong>{{ $message['message'] }}</strong>
                                                                                </div>
                                                                            @endif
                                                                            <div class="chat_seen">
                                                                                <strong>{{ parseDateTimeWithTimeZone($message['created_at'], session()->get('browserTimeZone')) }}</strong>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        {{-- no activities section--}}
                                                        <li id="noChatLi">
                                                            <div class="no_data_here">
                                                                <p>No chat found</p>
                                                            </div>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>

                                        {{-- chat-message section --}}
                                        <form id="sendMessageForm">
                                            <div class="chat_form">
                                                <input type="hidden" name="conversation_id" id="active_conversation_input" value="{{ $conversation['id'] }}"/>
                                                <div class="chat_form_message">
                                                    <textarea name="text_message" id="text_message" placeholder="Message..."rows="2"></textarea>
                                                </div>
                                                <div class="upload_logo">
                                                    <b id="file_name_container"></b>
                                                    <label for="">
                                                        <input type="file" name="message_file" id="message_file" onchange="readURL(this);" accept="image/x-png,image/gif,image/jpeg,image/jpg" />
                                                        <img src="{{ asset('project-assets/images/addFile_icon.png') }}" alt="img" />
                                                    </label>
                                                </div>
                                                <div class="chat_form_bttn">
                                                    <button type="submit" class="default_bttn disabeled" id="sendMessageBtn" disabled>Send</button>
                                                    <button type="button" class="default_bttn disabeled" id="sendingMessageBtn" disabled style="display: none;">Sending</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            {{-- right section --}}
                            <div class="event_activity_fr">
                                <div class="event_order_detail">
                                    <div class="event_order_head">
                                        <strong>Order Details</strong>
                                        <div class="order_menu_bttn">
                                            <small><img src="{{ asset('project-assets/images/menu_elipsis.png') }}" alt="img" /></small>
                                        </div>
                                    </div>

                                    <div class="event_order_info">
                                        <div class="event_order_img">
                                            <span><img src="{{ isset($booking['moderator']['image']) ? $booking['moderator']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="img" /></span>
                                        </div>
                                        <div class="event_order_text">
                                            <p>{{ $booking['detail_note'] }}</p>
                                        </div>
                                    </div>

                                    <div class="orderDetail_summary">
                                        <ul>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Talent Name</strong>
                                                    <b>{{ $booking['moderator']['full_name'] }}</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Date</strong>
                                                    <b>{{ parseDateWithTimeZone($booking['time_from'], session()->get('browserTimeZone')) }}</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Total Price</strong>
                                                    <b>${{ $booking['amount'] }}</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Order#</strong>
                                                    <b>{{ $booking['id'] }}</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Status</strong>
                                                    <b>{{ $booking['bookingStatus']['name'] }}</b>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="on_chatPage">
                                        <a href="{{ route('booking.detail', ['booking_id' => $booking['id']]) }}" class="default_bttn">View Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!-- dispute popup -->
        <div class="custom_popup rateReview_popup disputePopup">
            <div class="custom_popup_inner">
                <div class="custom_popup_detail">
                    <div class="custom_popup_info">
                        <div class="custom_popup_tittle">
                            <h3>Dispute</h3>
                        </div>
                        <form id="disputeForm">
                            <div class="popup_form">
                                {{-- hidden booking id field--}}
                                <input type="hidden" name="booking_id" value="{{ $booking['id'] }}" />
                                <ul>
                                    <li>
                                        <div class="custom_field">
                                            <div class="custom_field_input">
                                                <input name="dispute_title" id="dispute_title" placeholder="Dispute title..." />
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom_field">
                                            <div class="custom_field_textarea">
                                                <textarea name="dispute_query" id="dispute_query" placeholder="Your query here..."></textarea>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="custom_submit">
                                <a href="javascript:void(0)" id="disputeFormSubmitBtn">Submit</a>
                                <a href="javascript:void(0)" class="approve_bttn cancel_bttn">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">

        {{--        hide popup --}}
        function hidePopup() {
            $("body").removeClass("hiddden");
            $(".custom_popup").hide();
        }


        // scroll control to bottom, on a list
        function listScrollRelocate(listElementId, speed = 2000) {
            let list = document.getElementById(listElementId);
            // list.scrollTop = list.scrollHeight;
            $('#' +listElementId).animate({scrollTop: list.scrollHeight}, speed);
        }

        // ********************************************************************************
        //                              Chat script, starts
        // *********************************************************************************
        {{-- enable and disable send message buttin --}}
        let fileMessageElement =  $('#message_file');
        let textMessageElement =  $('#text_message');

        textMessageElement.on('change keyup', function() {
            let textAreaText = $.trim(textMessageElement.val());
            if ( !textAreaText ) {
                disableSendMessageBtn();
            } else {
                enableSendMessageBtn();
            }
        });
        fileMessageElement.on('change', function() {
            enableSendMessageBtn();
        });

        {{-- append message in the messages list --}}
        function appendMessage(messageHtml) {
            $('#messagesList').append(messageHtml);
        }

        {{-- clear send-message input --}}
        function clearMessageField() {
            $('#text_message').val(null);
            $('#message_file').val(null);
            $('#file_name_container').html('');
        }

        {{-- disable send message btn --}}
        function disableSendMessageBtn() {
            $('#sendMessageBtn').attr('disabled', true);
            $('#sendMessageBtn').addClass('disabeled');
        }

        {{-- enable send message btn --}}
        function enableSendMessageBtn() {
            $('#sendMessageBtn').attr('disabled', false);
            $('#sendMessageBtn').removeClass('disabeled');
        }

        {{-- disable send message btn --}}
        function showSendingMessageBtn() {
            $('#sendMessageBtn').hide();
            $('#sendingMessageBtn').show();
            $('#text_message').css({"cursor": "not-allowed"});
        }

        {{-- enable send message btn --}}
        function hideSendingMessageBtn() {
            $('#text_message').css({"cursor": "default"});
            $('#sendingMessageBtn').hide();
            $('#sendMessageBtn').show();
        }

        {{-- hide no messages text --}}
        function removeNoChatTextElement() {
            let noChatElement = document.getElementById('noChatLi');
            if (noChatElement) {
                noChatElement.remove();
            }
        }

        {{-- scroll controll to last message --}}
        function messageScrollRelocate(speed = 2000) {
            let messagesList = document.getElementById('messagesList');
            // messagesList.scrollTop = messagesList.scrollHeight;
            $('#messagesList').animate({scrollTop: messagesList.scrollHeight}, speed);
        }

        {{-- save message, stars --}}
        $('#sendMessageForm').on('submit', function (e) {
            e.preventDefault();
            let messageData =  new FormData( this );
            showSendingMessageBtn();
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('chat.message.save') }}",
                data: messageData,
                success: function (response) {
                    if ( Boolean(response.status) ) {
                        $.each(response.data.lastMessagesHtml, function (key, value) {
                            appendMessage(value);
                        });
                        clearMessageField();
                        messageScrollRelocate();
                        hideSendingMessageBtn();
                        removeNoChatTextElement();
                        disableSendMessageBtn();
                    } else if ( !Boolean(response.status) ) {
                        blockUi( false );
                        toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        toasterSuccess( false, value);
                    });
                }
            });
        });
    </script>
    {{-- ***************** chat script ends *********************** --}}



    <script type="text/javascript">
        let counterEvent; // variable of counter-event
        let currentBookingData = @json($booking);

        // update/set timer time-params
        function setTimeCounterTime(hours, minutes, seconds) {
            document.getElementById("timer_hours").innerHTML = hours;
            document.getElementById("timer_minutes").innerHTML = minutes;
            document.getElementById("timer_seconds").innerHTML = seconds;
        }

        // start timer-counter
        function startTimer() {
            // Update the count down every 1 second
            counterEvent = setInterval(function() {
                let hours = $('#timer_hours').html();
                let minutes = $('#timer_minutes').html();
                let seconds = $('#timer_seconds').html();

                // check and calculate time
                if (hours > 0 || minutes > 0 || seconds > 0) {
                    let calculatedTime = getCounterRemainingTime(hours, minutes, seconds);
                    // set time in counter
                    setTimeCounterTime(calculatedTime.hours, calculatedTime.minutes, calculatedTime.seconds);
                } else { // stop timer
                    let stopBookingData = $('#bookingInfoForm').serializeArray();
                    const stopTimeNow = new Date();
                    let currentUtcDateTime = stopTimeNow.toUTCString();
                    stopBookingData.push({name: 'stopped_at', value: currentUtcDateTime}, {name: 'time_completed', value: true});

                    stopTimer(); // stop time-counter, function call
                }
            }, 1000);
        }

        // timer-counter calculator
        function getCounterRemainingTime(hours, minutes, seconds) {
            if (seconds > 0 ) {
                seconds--;
            }
            if (seconds == 0 && minutes >= 1) {
                minutes = minutes - 1;
                seconds = 59;
            }
            if (seconds == 0 && minutes == 0 && hours >= 1) {
                hours = hours - 1;
                minutes = 59;
                seconds = 59;
            }

            return { 'hours': hours, 'minutes': minutes, 'seconds': seconds  };
        }

        // stop timer-counter event
        function stopTimer() {
            clearInterval(counterEvent);
        }


        // ******************************************************************************************************
        //                                      Event Activities Handling
        // ******************************************************************************************************
        //    append new activity in event-activity section
        function appendNewEventActivity(eventActivityHtml) {
            $('#event_activities_list').append(eventActivityHtml);
        }

        // talent-present button event handling
        $('#talentPresentBtn').click(function (e) {
            e.preventDefault();
            blockUi( true );
            let talentPresentData = $('#bookingInfoForm').serializeArray();

            talentPresentAjax(talentPresentData); // stop time, ajax call
        });

        // talent-present button event handling
        $('#talentMarkTimeCompletedBtn').click(function (e) {
            e.preventDefault();
            blockUi( true );
            let markTimeCompletedData = $('#bookingInfoForm').serializeArray();

            talentMarkTimeCompletedAjax(markTimeCompletedData);
        });

        // talent-present ajax
        function talentPresentAjax(talentPresentData) {
            $.ajax({
                type: 'GET',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.booking-management.talent-present') }}",
                data: talentPresentData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        // appendNewEventActivity(response.data.eventActivityHtml);
                        window.location.reload();
                        toasterSuccess( true, response.message);
                    } else if (!response.status) {
                        blockUi( false );
                        toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, value);
                    });
                }
            });
        }

        // talent mark-time-completed ajax
        function talentMarkTimeCompletedAjax(markTimeCompletedData) {
            $.ajax({
                type: 'GET',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.booking.mark-completed') }}",
                data: markTimeCompletedData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                    } else if (!response.status) {
                        blockUi( false );
                        toasterSuccess( false, response.error);
                        // window.location.reload();
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, value);
                    });
                }
            });
        }

        {{-- dispute query ajax --}}
        $('#disputeFormSubmitBtn').on('click', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            blockUi( true );
            let disputeFormData =  $('#disputeForm').serializeArray();
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('dispute.query.submit') }}",
                data: disputeFormData,
                success: function (response) {
                    if ( Boolean(response.status) ) {
                        blockUi( false );
                        hidePopup();
                        toasterSuccess( true, response.message);
                    } else if ( !Boolean(response.status) ) {
                        blockUi( false );
                        toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    blockUi( false );
                    $.each(data.responseJSON.errors, function (key, value) {
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if (value.includes(key)) {
                            $(element).val(value);
                        }
                    });
                }
            });
        });

        // open a pusher connection
        function openPusherConnection() {
            let pusherConnection;
            let appEnvironment = '{{ env('APP_ENV') }}';
            if ( appEnvironment !== 'production' || appEnvironment !== 'Production' ) {
                // Pusher.logToConsole = true;
            }
            pusherConnection =  new Pusher('{{ config('services.pusher.key') }}', {
                cluster: '{{ config('services.pusher.cluster') }}',
                forceTLS: true
            });
            return pusherConnection;
        }

        // subscribe to a channel and return the channel object
        function subscribeToChannel(channelName) {
            let pusherConnection = openPusherConnection();
            return pusherConnection.subscribe(channelName);
        }
    </script>

    <script type="text/javascript">
        // ready document scripts sections
        $(document).ready(function() {
            // lists scroll relocates
            listScrollRelocate('talentEventActivityTopList', 100); // relocate activity section scroll
            listScrollRelocate('talentEventActivityBottomList', 100); // relocate event-chat section scroll

            // ******************************************************************************************************
            //                              Booking-Timer events real-time handling, start
            // ******************************************************************************************************
            let timerStoppedChannelName = '{{ auth()->id() }}'+ "-" +currentBookingData.id+ "-" +currentBookingData.moderator_id;
            let bookingTimerChannel = subscribeToChannel(timerStoppedChannelName); // subscribe to timer elated channels

            bookingTimerChannel.bind('eventTimerStarted', function (data) {
                appendNewEventActivity(data.timerStartedData.eventActivityHtml);
                listScrollRelocate('talentEventActivityTopList');
                let timerRemainingTime = data.timerStartedData.remainingTime;
                setTimeCounterTime(timerRemainingTime.hours, timerRemainingTime.minutes, timerRemainingTime.seconds);
                startTimer();
                toasterSuccess(true, 'Moderator started the timer');
            });
            bookingTimerChannel.bind('eventTimerStopped', function (data) {
                appendNewEventActivity(data.timerStoppedData.eventActivityHtml);
                listScrollRelocate('talentEventActivityTopList');
                stopTimer();
                toasterSuccess(true, 'Moderator stopped the timer');
            });
            bookingTimerChannel.bind('eventTimeCompleted', function (data) {
                appendNewEventActivity(data.timeCompletedData.eventActivityHtml);
                listScrollRelocate('talentEventActivityTopList');
                stopTimer();
                toasterSuccess(true, 'Event time completed');
            });

            // ******************************************************************************************************
            //                              Event based chat real-time handling, start
            // ******************************************************************************************************
            let eventConversationId = $('#active_conversation_input').val();
            let eventChatChannelName = '{{ auth()->id() }}'+ "-" +eventConversationId;
            let eventChatChannel = subscribeToChannel(eventChatChannelName); // subscribe to timer elated channels

            eventChatChannel.bind('newMessage', function (data) {
                $.each(data.newMessageData.messagesHtml, function (key, value) {
                    appendMessage(value);
                    messageScrollRelocate();
                });
            });


            // ******************************************************************************************************
            //                                  Time-counter related scripts start
            // ******************************************************************************************************
            let days;
            let hours;
            let minutes;
            let seconds;
            let bookingData = @json($booking); // parse the booking object passed to current view
            let bookingActivityData = @json($booking['timerActivity']); // parse the booking-activity object passed to current view

            // check for the current booking activity and handling
            if (bookingActivityData) {
                if ( $('#timer_in_progress').val() == 1 ) {
                    let expectedEndTime = $('#expected_end_time').val(); // format is same as of JS Date object
                    let nowDateTime = new Date();
                    let nowUtcString = nowDateTime.toUTCString();
                    let nowTimestamp = (Date.parse(nowUtcString) / 1000);
                    let endTimeTimestamp = (Date.parse(expectedEndTime) / 1000);
                    let leftTimestamp;
                    if (endTimeTimestamp > nowTimestamp) {
                        leftTimestamp = endTimeTimestamp - nowTimestamp;
                    } else {
                        leftTimestamp = 0;
                    }

                    days = Math.floor(leftTimestamp / 86400);
                    hours = Math.floor((leftTimestamp - (days * 86400)) / 3600);
                    minutes = Math.floor((leftTimestamp - (days * 86400) - (hours * 3600)) / 60);
                    seconds = Math.floor((leftTimestamp - (days * 86400) - (hours * 3600) - (minutes * 60)));

                    // set time-counter time
                    setTimeCounterTime(hours, minutes, seconds);
                    startTimer();
                } else {
                    hours = $('#remaining_hours').val();
                    minutes = $('#remaining_minutes').val();
                    seconds = $('#remaining_seconds').val();
                    // set time-counter time
                    setTimeCounterTime(hours, minutes, seconds);
                }
            } else {
                setTimeCounterTime(bookingData.remaining_hours, bookingData.remaining_minutes, bookingData.remaining_seconds);
            }


            // ******************************************************************************************************
            //                                  Popups hide/show handling
            // ******************************************************************************************************
            // Approve popup
            $('#aprove_Btn').click(function(){
                $("body").addClass("hiddden");
                $(".aprovePopup").show();
            });
            $('.cancel_bttn ').click(function(){
                $("body").removeClass("hiddden");
                $(".custom_popup").hide();
            });
            // confirm popup
            $('#confirm_Btn').click(function(){
                $("body").addClass("hiddden");
                $(".confirmPopup").show();
            });
            // dispute popup
            $('#dispute_btn').click(function(){
                $("body").addClass("hiddden");
                $(".disputePopup").show();
            });
        });
    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection

