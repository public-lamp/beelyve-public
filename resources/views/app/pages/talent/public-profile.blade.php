{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Talent-Profile')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/datepicker.css') }}" />
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer></script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">
            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">

                        {{-- profile basic section --}}
                        <div class="orderSteps_section">
                            <div class="db_profile">
                                <div class="featureWeek_info">
                                    <div class="feartureProflie_info">
                                        <div class="featureProfile_avatar">
                                            <span>
                                                <img src="{{ isset($talent['image']) ? $talent['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="logo">
                                                {{-- <small class="online"><i></i>online</small>--}}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featureProfile_avatar_inner">
                                        <div class="featureProfile_avatar_text">
                                            <strong>{{ $talent['full_name'] }}</strong>
                                            <small>{{ $talent['talentProfessionalInfo']['talentCategory']['name'] }}</small>
                                        </div>

                                        <div class="profile_rating">
                                            <div class="profile_rating_info">
                                                <ul>
                                                    @if( $talent['rating'] > 0.0 && $talent['rating'] < 0.5 )
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                    @elseif( $talent['rating'] >= 0.5 && $talent['rating'] < 1.5 )
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                    @elseif( $talent['rating'] >= 1.5 && $talent['rating'] < 2.5 )
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                    @elseif( $talent['rating'] >= 2.5 && $talent['rating'] < 3.5 )
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                    @elseif( $talent['rating'] >= 3.5 && $talent['rating'] < 4.5 )
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                    @elseif( $talent['rating'] >= 4.5 )
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                    @endif
                                                </ul>
                                                <strong>({{ count($talent['reviews']) }})</strong>
                                            </div>
                                        </div>

                                        <div class="profileDiscription">
                                            <p><strong>About: </strong>{{ $talent['about'] }}</p>
                                        </div>

                                        @if( auth()->check() && !auth()->user()->isAdmin() )
                                            <div class="profileBttns">
                                                @if( isset($user) && $user['favoriteTalents']->isNotEmpty() && in_array($talent['id'], $user->favoriteTalents()->pluck('favorable_id')->toArray()) )
                                                    <a href="{{ route('favourite.user.remove', ['favorable_id' => $talent['id']]) }}" class="fvrtBtn default_bttn">
                                                        <i class="fa fa-heart red_fvrt" aria-hidden="true"></i> Un-Favorite
                                                    </a>
                                                @else
                                                    <a href="{{ route('favourite.user.add', ['favorable_id' => $talent['id']]) }}" class="fvrtBtn default_bttn">
                                                        <i class="fa fa-heart-o" aria-hidden="true"></i> Add to Favorite
                                                    </a>
                                                @endif
                                                <a href="{{ route('chat.conversation.start', ['other_user_id' => $talent['id'], 'conversation_type_id' => 1]) }}" class="msgBtn default_bttn">Message</a>
                                            </div>
                                        @endif



                                        @if(count($talent['socialAccounts']) > 0)
                                            <div class="profilr_social">
                                                <label>Verify by Social</label>
                                                <ul>
                                                    <li>
                                                        @foreach($talent['socialAccounts'] as $socialAccount)
                                                            @if($socialAccount['social_provider_name_id'] == 1)
                                                                <a href="{{ $talent['talentBasicInfo']['fb_profile_link'] }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                                <strong>{{ $socialAccount['name'] }}</strong>
                                                            @endif
                                                        @endforeach
                                                    </li>
                                                    <li>
                                                        @foreach($talent['socialAccounts'] as $socialAccount)
                                                            @if($socialAccount['social_provider_name_id'] == 2)
                                                                <a href="{{ $talent['talentBasicInfo']['insta_profile_link'] }}"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                                                <strong>{{ $socialAccount['name'] }}</strong>
                                                            @endif
                                                        @endforeach
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="order_bookSection">
                                <div class="order_bookSection_info">
                                    <div class="order_booktabs">
                                        <ul>
                                            @foreach( $talent['talentServicesPricing'] as $talentServicesPricingKey => $talentServicesPricing )
                                                <li><a href="#servicesPricingOrder_{{ $talentServicesPricingKey }}" @if($talentServicesPricing['servicesPricingType']['id'] == 1)class="orderTab_active" @endif>{{ $talentServicesPricing['servicesPricingType']['name'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    @foreach($talent['talentServicesPricing'] as $servicesPricingKey => $servicesPricing)
                                        <div class="order_booktabs_data" id="servicesPricingOrder_{{ $servicesPricingKey }}" style="display: block;">
                                            <div class="order_tabs_infoHead">
                                                <div class="db_tittle">
                                                    <div class="db_tittle_heading">
                                                        <h4>Booking Options</h4>
                                                    </div>
                                                    <div class="our_earning">
                                                        <strong><b>$<i>{{ $servicesPricing['price'] }}</i></b></strong>
                                                    </div>
                                                </div>

                                                <div class="clubHouse_info">
                                                    <h4>
                                                        @foreach($servicesPricing['servicesPricingType']['talentServicesMediums'] as $talentServicesMedium)
                                                            <span>{{ $talentServicesMedium['servicesMedium']['name'] }} </span>
                                                        @endforeach
                                                    </h4>
                                                    <p>{{ $servicesPricing['description'] }}</p>
                                                    <strong>{{ $servicesPricing['servicesPricingSlot']['name'] }} session  </strong>

                                                    <div class="session_list">
                                                        <ul>
                                                            @foreach( $servicesPricing['main_features'] as $main_feature )
                                                                <li>
                                                                    <span><img src="{{ asset('project-assets/images/list_check.png') }}" alt="image" /></span>
                                                                    <small>{{ $main_feature }}</small>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="session_bttns">
                                                <ul>
                                                    <li><a href="{{ route('booking-request.start', ['talent_id' => request()->get('talent_id'), 'talent_services_pricing_id' => $servicesPricing->id]) }}" class="default_bttn addDate">Book Now<b></b>(${{ $servicesPricing['price'] }})</a></li>
                                                    {{-- <li><a href="#" class="compareBtn">Compare</a></li> --}}
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        {{-- work section --}}
                        <div class="myWork_section">
                            <div class="db_tittle">
                                <div class="db_tittle_heading">
                                    <h4>MY Work</h4>
                                </div>
                                {{-- <div class="our_earning">--}}
                                {{-- <strong><a href="#">View All</a></strong>--}}
                                {{-- </div>--}}
                            </div>

                            <div class="myWork_slider">
                                <div class="slider-for_video main_preview_img">
                                    @if( isset($talent['talentWorkFiles']) )
                                        @foreach($talent['talentWorkFiles'] as $workFile)
                                            @if($workFile->type === 'video')
                                                <div>
                                                    <div class="work_banner_slide">
                                                         <span>
                                                             <iframe width="560" height="315" src="{{ $workFile->file_url }}" frameborder="0" allowfullscreen></iframe>
                                                         </span>
                                                    </div>
                                                </div>
                                            @else
                                                <div>
                                                    <div class="work_banner_slide">
                                                        <span><img src="{{ asset($workFile->file_url) }}" alt="work-image" /><i class="vider_overlay"></i></span>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="slider-nav_video">
                                    @if( isset($talent['talentWorkFiles']) )
                                        @foreach($talent['talentWorkFiles'] as $workFile)
                                            @if($workFile->type === 'video')
                                                <div>
                                                    <div class="work_banner_slide">
                                                        {{--                                                        <span>--}}
                                                        {{--                                                            <video class="embed-responsive-item" width="250" height="150" controls autoplay=false>--}}
                                                        {{--                                                                <source src="{{ $workFile->file_url }}" type="video/mp4">--}}
                                                        {{--                                                            </video>--}}
                                                        {{--                                                            <i class="vider_overlay"></i>--}}
                                                        {{--                                                        </span>--}}
                                                         <span><iframe width="560" height="315" src="{{ $workFile->file_url }}" controls="0" autoplay="false" frameborder="0" allowfullscreen></iframe><i class="vider_overlay"></i></span>
                                                    </div>
                                                </div>
                                            @else
                                                <div>
                                                    <div class="work_banner_slide">
                                                        <span><img src="{{ asset($workFile->file_url) }}" alt="work-image" /><i class="vider_overlay"></i></span>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{-- services section --}}
                        <div class="services_section">
                            <div class="services_tabs">
                                <ul>
                                    <li><a href="#service" class="tab_highlight">My Service</a></li>
                                    <li><a href="#experience">Experience</a></li>
                                    <li><a href="#availability">Availability</a></li>
                                    <li><a href="#reviews">Reviews</a></li>
                                    {{-- <li><a href="#socials">Socials</a></li> --}}
                                </ul>
                            </div>

                            <div class="services_tabs_data_outer">
                                <div class="services_tabs_data" id="service" style="display: block;">
                                    <h4>About my service</h4>
                                    <p>{{ $talent['talentProfessionalInfo']['service'] }}</p>
                                </div>
                                <div class="services_tabs_data" id="experience">
                                    <h4>Experience</h4>
                                    <p>{{ $talent['talentProfessionalInfo']['experience'] }}</p>
                                </div>
                                <div class="services_tabs_data" id="availability">
                                    <div class="availability_info">
                                        @if( !empty($talent['userAvailabilityDays']) && $talent['availabilityCategory']['availability_category_id'] == 2 )
                                            <div class="avaiable_days" style="display: block;">
                                                <div class="avaiable_daysInner">
                                                    <div class="d-flex days_tab">
                                                        <div class="btn_days">
                                                            @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                    {{ substr($weekDay->name, 0, 3) }}
                                                                    <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                           name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                           data-child="{{ $weekDay->slug }}_parent" readonly />
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    @foreach($talent['userAvailabilityDays'] as $userAvailDayKey => $availabilityDay)
                                                        <div class="{{ $availabilityDay['weekDay']['slug'] }}_parent main_parents" id="{{ $availabilityDay['weekDay']['slug'] }}_parent" style="display: none;">
                                                            <div class="time_sec">
                                                                @if( !$availabilityDay['availabilitySlots']->isEmpty() )
                                                                    @foreach($availabilityDay['availabilitySlots'] as $slotKey => $availabilitySlot)
                                                                        <div class="price_from">
                                                                            <span>Time From</span>
                                                                            <input type="text" class="input_price_from"
                                                                                   id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                   data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                   value="{{ $availabilitySlot['time_from'] }}"
                                                                                   readonly />
                                                                        </div>
                                                                        <div class="price_from time_to">
                                                                            <span> Time To</span>
                                                                            <input type="text" class="input_price_from"
                                                                                   id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                   value="{{ $availabilitySlot['time_to'] }}"
                                                                                   readonly />
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <div class="price_from">
                                                                        <span>Time From</span>
                                                                        <input type="text" class="input_price_from"
                                                                               id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                               data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                               placeholder="Not Available"
                                                                               readonly />
                                                                    </div>
                                                                    <div class="price_from time_to">
                                                                        <span> Time To</span>
                                                                        <input type="text" class="input_price_from"
                                                                               id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                               placeholder="Not Available"
                                                                               readonly />
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @elseif( $talent['availabilityCategory']['availability_category_id'] == 1 )
                                            <p>Available Everyday & Any time</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="services_tabs_data" id="reviews">
                                    <h4>Reviews</h4>
                                    <div class="reviews_info_list">
                                        @if(count($talent['reviews']) > 0)
                                            <ul>
                                                @foreach($talent['reviews'] as $review)
                                                    <li>
                                                        <div class="reviews_info_head">
                                                            <div class="reviews_profile">
                                                                <div class="reviews_profile_avatar">
                                                                    <a href="javascript:void(0);"><img src="{{ isset($review['reviewer']['image']) ? $review['reviewer']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="#" /></a>
                                                                </div>
                                                                <div class="reviews_profile_info">
                                                                    <div class="reviews_profile_info_rating">
                                                                        <a href="javascript:void(0);">{{ $review['reviewer']['full_name'] }}</a>
                                                                        <div class="reviews_rate">
                                                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                                            <b>{{ $review['rating'] }}</b>
                                                                        </div>
                                                                    </div>
                                                                    {{-- <div class="reviews_profile_country">--}}
                                                                    {{-- <span><img src="{{ isset($review['reviewer']['image']) ? $review['reviewer']['image'] : asset('project-assets/images/flag_icon.png') }}" alt="Reveiwer_img" /></span>--}}
                                                                    {{-- <strong>{{ $review['reviewer']['full_name'] }}</strong>--}}
                                                                    {{-- </div>--}}
                                                                </div>
                                                            </div>
                                                            {{-- <div class="reviews_profile_link">--}}
                                                            {{-- <a href="#"><img src="{{ asset('project-assets/images/profile_page_img.png') }}" alt="#" /></a>--}}
                                                            {{-- </div>--}}
                                                        </div>
                                                        <div class="reviews_profile_detail">
                                                            <p>{{ $review['comments'] }}</p>
                                                            <div class="published_info">
                                                                <strong>Published at: {{ parseDateTimeWithTimeZone($review['created_at'], session()->get('browserTimeZone')) }}</strong>
                                                                {{-- <div class="published_info_links">--}}
                                                                {{-- <a href="javascript:void(0);"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Helpful</a>--}}
                                                                {{-- <a href="#"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Not Helpful</a>--}}
                                                                {{-- </div>--}}
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @else
                                            <div class="w-100 text-center">
                                                <p>No Reviews</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });

    </script>

@endsection

@section('ajax')
@endsection




