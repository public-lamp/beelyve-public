@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Availability')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>
@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Availabilities</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="talent_profile_tabs">
                                    <ul>
                                        <li><a href="{{ route('talent.profile.edit') }}">Basic Profile</a></li>
                                        <li><a href="javascript:void(0);" class="active_tab">Availability</a></li>
                                        <li><a href="{{ route('talent.services-pricing') }}">Services Pricing</a></li>
                                        <li><a href="{{ route('talent.profile.work-gallery') }}">Gallery</a></li>
                                        <li><a href="{{ route('talent.social-connectivity') }}">Social Connectivity</a></li>
                                    </ul>
                                </div>

                                {{-- Availabbility section starts --}}
                                <div class="update_profile_data d-block" id="updateTab_2">
                                    <form id="talentAvailabilityForm">
                                        <div class="availability_detail">
                                            <div class="availability_options">
                                                <ul>
                                                    @foreach( \App\Models\Generic\AvailabilityCategory::all() as $availabilityCategory )
                                                        <li>
                                                            <div class="chooseOptionDetail">
                                                                <label>
                                                                    <input type="radio" class="option-input radio" value="{{ $availabilityCategory->id }}"
                                                                           name="availability_category"
                                                                           data-avail-name = '{{ $availabilityCategory->name }}'
                                                                           @if( isset($talentProfile['availabilityCategory']) && $availabilityCategory->id == $talentProfile['availabilityCategory']['availability_category_id'] ) checked @endif />
                                                                    {{ $availabilityCategory->name }}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>

                                            <div class="availability_info">
                                                @if( isset($talentProfile['availabilityCategory']) && (!empty($talentProfile['userAvailabilityDays']) && $talentProfile['availabilityCategory']['availability_category_id'] == 2) )
                                                    <div class="avaiable_days" style="display: block;">
                                                        <div class="avaiable_daysInner">

                                                            <div class="d-flex days_tab">
                                                                <div class="btn_days">
                                                                    @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                        <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                            {{ substr($weekDay->name, 0, 3) }}
                                                                            <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                                   name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                                   data-child="{{ $weekDay->slug }}_parent" />
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            @foreach($talentProfile['userAvailabilityDays'] as $userAvailDayKey => $availabilityDay)
                                                                <div class="{{ $availabilityDay['weekDay']['slug'] }}_parent main_parents" id="{{ $availabilityDay['weekDay']['slug'] }}_parent" style="display: none;">
                                                                    <div class="sound-signal">
                                                                        <strong class="reset_time"><a href="javascript:void(0);">Reset</a></strong>
                                                                    </div>
                                                                    <div class="time_sec">
                                                                        @if( !$availabilityDay['availabilitySlots']->isEmpty() )
                                                                            @foreach($availabilityDay['availabilitySlots'] as $slotKey => $availabilitySlot)
                                                                                <div class="price_from">
                                                                                    <span>Time From</span>
                                                                                    <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                           name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_from][]"
                                                                                           id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                           data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                           value="{{ $availabilitySlot['time_from'] }}"
                                                                                            {{--  value="{{ parseTimeWithTimeZone($availabilitySlot['time_from'], session('browserTimeZone'), 'UTC', 'h:i A' ) }}"--}}
                                                                                           readonly="" />
                                                                                </div>
                                                                                <div class="price_from time_to">
                                                                                    <span> Time To</span>
                                                                                    <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                           name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_to][]"
                                                                                           id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                           value="{{ $availabilitySlot['time_to'] }}"
                                                                                            {{-- value="{{ parseTimeWithTimeZone($availabilitySlot['time_to'], session('browserTimeZone'), 'UTC', 'h:i A' ) }}"--}}
                                                                                           readonly="" />
                                                                                </div>
                                                                            @endforeach
                                                                        @else
                                                                            <div class="price_from">
                                                                                <span>Time From</span>
                                                                                <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                       name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_from][]"
                                                                                       id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_from_0"
                                                                                       data-time-to-id="days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0"
                                                                                       placeholder="00:00" readonly="" />
                                                                            </div>
                                                                            <div class="price_from time_to">
                                                                                <span> Time To</span>
                                                                                <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                       name="days[{{ $availabilityDay['weekDay']['slug'] }}][time][time_to][]"
                                                                                       id='days_{{ $availabilityDay['weekDay']['slug'] }}_time_time_to_0'
                                                                                       placeholder="00:00" readonly="" />
                                                                            </div>
                                                                        @endif
                                                                        <div class="add_hours_btn">
                                                                            <div class="cssCircle add_hours" data-name="{{ $availabilityDay['weekDay']['slug'] }}">
                                                                                Add
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avaiable_days" style="/* display: block; */">
                                                        <div class="avaiable_daysInner">
                                                            <div class="d-flex days_tab">
                                                                <div class="btn_days">
                                                                    @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                        <label class="labelo" for="days_{{ $weekDay->slug }}">
                                                                            {{ substr($weekDay->name, 0, 3) }}
                                                                            <input class="days_radio" type="radio" value="{{ $weekDay->slug }}"
                                                                                   name="days[{{ $weekDay->slug }}]" id="days_{{ $weekDay->slug }}"
                                                                                   data-child="{{ $weekDay->slug }}_parent" />
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @foreach(\App\Models\Generic\WeekDay::all() as $weekDay)
                                                                <div class="{{ $weekDay->slug }}_parent main_parents" id="{{ $weekDay->slug }}_parent" style="display: none;">
                                                                    <div class="sound-signal">
                                                                        <strong class="reset_time"><a href="javascript:void(0);">Reset</a></strong>
                                                                    </div>
                                                                    <div class="time_sec">
                                                                        <div class="price_from">
                                                                            <span>Time From</span>
                                                                            <input type="text" class="input_price_from plugin_date  uiFromTimePicker"
                                                                                   name="days[{{ $weekDay->slug }}][time][time_from][]"
                                                                                   id="days_{{ $weekDay->slug }}_time_time_from_0"
                                                                                   data-time-to-id="days_{{ $weekDay->slug }}_time_time_to_0"
                                                                                   placeholder="00:00" readonly="" />
                                                                        </div>
                                                                        <div class="price_from time_to">
                                                                            <span> Time To</span>
                                                                            <input type="text" class="input_price_from plugin_date uiToTimePicker"
                                                                                   name="days[{{ $weekDay->slug }}][time][time_to][]"
                                                                                   id='days_{{ $weekDay->slug }}_time_time_to_0'
                                                                                   placeholder="00:00" readonly="" />
                                                                        </div>
                                                                        <div class="add_hours_btn">
                                                                            <div class="cssCircle add_hours" data-name="{{ $weekDay->slug }}">
                                                                                Add
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Submit" id="talentAvailabilityFormBtn" />
                                        </div>
                                    </form>
                                </div>
                                {{-- Avaiability Section ends --}}

                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('body').addClass('chat_body');
        });
    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">
        {{-- availability ajax, stars --}}
        $('#talentAvailabilityForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let availabilityData =  $('#talentAvailabilityForm').serialize();
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('talent.availability.set') }}",
                data: availabilityData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.reload();
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        let keyId = key.replaceAll('.', '_');
                        console.log(keyId+": "+value);
                        blockUi( false );
                        let element = document.getElementById(keyId);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- availability ajax, Ends --}}
    </script>
@endsection








