@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Profile Basic')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/select2/select2.css') }}" />

    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/select2/select2.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>
@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Update Profile</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="talent_profile_tabs">
                                    <ul>
                                        <li><a href="javascript:void(0);" class="active_tab">Basic Profile</a></li>
                                        <li><a href="{{ route('talent.availability') }}">Availability</a></li>
                                        <li><a href="{{ route('talent.services-pricing') }}">Services Pricing</a></li>
                                        <li><a href="{{ route('talent.profile.work-gallery') }}">Gallery</a></li>
                                        <li><a href="{{ route('talent.social-connectivity') }}">Social Connectivity</a></li>
                                    </ul>
                                </div>

                                {{--Talent basic info section starts--}}
                                <div class="update_profile_data d-block">
                                    <form id="talentBasicInfoForm">

                                        <div class="edit_profile_head">
                                            <div class="choose_profile">
                                                <div class="profile_img">
                                                    <span>
                                                        <b class="my_file">
                                                            <input class="file_input" type='file' onchange="readURL(this);" name="user_image" />
                                                            <small><i class="fa fa-camera fa-3" aria-hidden="true"></i>Change</small>
                                                        </b>
                                                        <img id="blah" src="{{ isset( $talentProfile['image'] ) ? $talentProfile['image'] : asset('project-assets/images/avatar_white.png') }}" alt="your image" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="popup_form">
                                            <ul>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>First Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ auth()->user()->first_name ? auth()->user()->first_name : '' }}" name="first_name" id="first_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Last Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ auth()->user()->last_name ? auth()->user()->last_name : '' }}" name="last_name" id="last_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>User Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{  auth()->check() ? auth()->user()->user_name : '' }}" name="user_name" id="user_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Select talent category</strong>
                                                        <div class="custom_dropdown">
                                                            <select name="talent_category_id" id="talent_category_id" >
                                                                <option selected disabled>Select talent category</option>
                                                                @foreach( \App\Models\Talent\TalentCategory::all() as $talentCat )
                                                                    <option value="{{ $talentCat->id  }}" @if( isset( $talentProfile['talentProfessionalInfo'], $talentProfile['talentProfessionalInfo']['talent_category_id'] ) && $talentProfile['talentProfessionalInfo']['talent_category_id'] == $talentCat->id ) selected @endif >
                                                                        {{ $talentCat->name  }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Country</strong>
                                                        <div class="custom_dropdown">
                                                            <select name="country" id="country" >
                                                                <option selected disabled>Select your country</option>
                                                                @foreach( \App\Models\Generic\Country::all() as $country )
                                                                    <option value="{{ $country->name }}" @if( isset($talentProfile['userAddress'], $talentProfile['userAddress']['country']) && $talentProfile['userAddress']['country'] === $country->name ) selected @endif >
                                                                        {{ $country->name  }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Starting rate</strong>
                                                        <div class="custom_field_input unitPrice">
                                                            <i class="fa fa-usd" aria-hidden="true"></i>
                                                            <input type="text" value="{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['start_rate'] : '' }}" name="talent_start_rate" id="talent_start_rate" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Tagline</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['tagline'] : '' }}"
                                                                   list="existingTaglinesList" name="tagline" id="tagline" />
                                                            <datalist id="existingTaglinesList">
                                                                @foreach($existingTaglines as $tagline)
                                                                    <option value="{{ $tagline }}">
                                                                @endforeach
                                                            </datalist>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Facebook Profile Link</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentBasicInfo']) ? $talentProfile['talentBasicInfo']['fb_profile_link'] : '' }}" name="fb_profile_link" id="fb_profile_link" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Instagram Profile Link</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentBasicInfo']) ? $talentProfile['talentBasicInfo']['insta_profile_link'] : '' }}" name="insta_profile_link" id="insta_profile_link" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>My Service</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['service'] : '' }}" name="talent_service" id="talent_service" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>Experience</strong>
                                                        <div class="custom_field_textarea">
                                                            <textarea name="experience" id="experience">{{ isset($talentProfile['talentProfessionalInfo']) ? $talentProfile['talentProfessionalInfo']['experience'] : '' }}</textarea>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="">
                                                    <div class="custom_field">
                                                        <strong>About</strong>
                                                        <div class="custom_field_textarea">
                                                            <textarea name="talent_about" id="talent_about">{{ isset($talentProfile['about']) ? $talentProfile['about'] : '' }}</textarea>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Save" id="updateTalentProfileFormBtn" />
                                        </div>
                                    </form>
                                </div>
                                {{--Talent basic info section ends--}}

                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">
        // select2 multi-tag select
        $(document).ready(function() {
            $('.selectMultiple').select2();
        })
    </script>

@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">

        {{-- Basic profile-info ajax, stars --}}
        $('#talentBasicInfoForm').on('submit', function (e) {
            $('.errorAlertText').html('');
            e.preventDefault();
            let basicInfoData =  new FormData( this );
            blockUi( true );

            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.basic.update') }}",
                data: basicInfoData,
                success: function (response) {
                    if (response.status) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        return window.location.reload();
                        {{--window.location.href = "{{ route('talent-dashboard') }}"--}}
                    } else if (!response.status) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if ( value.includes(key) ) {
                            $(element).val(value);
                        }
                    });
                }
            });
        });
        {{-- Basic profile-info ajax, Ends --}}
    </script>


    <script type="text/javascript">
        var formdata = new FormData();

        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function(){
                    $(".preview_img").addClass('my_img');
                    $("#previewImg").attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
        $(document).ready(function(){
            $(".preview_img i").click(function(){
                $('.preview_img').removeClass('my_img');
            });
        });
    </script>


    <script type="text/javascript">
        var countFile=0;
        $(".file_input").on("change", function(event) {
            let files = event.target.files[0]; // also use like    var files = $('#imgInp')[0].files[0];
            let fileName = files.name;
            let fsize = files.size;
            let fileSizeInMbs = 5;
            let fileSizeLimit = fileSizeInMbs * 1024;
            let ext = fileName.split('.').pop().toLowerCase();
            const allowedExtension = ['jpg', 'png', 'jpeg'];

            if (!allowedExtension.includes(ext)) { // check image extension
                errorMsg('Warning! only .PNG, .JPEG or .JPG image allowed.');
            }
            if ((fsize/1024) / fileSizeLimit > 1.0) { // validate image size
                errorMsg('File size can not be more than ' +fileSizeInMbs+ ' MBs');
            }
        });

        $(document).ready(function() {
            $('body').addClass('chat_body');
        });
    </script>
@endsection








