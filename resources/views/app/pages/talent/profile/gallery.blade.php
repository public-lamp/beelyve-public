@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', ' Gallery')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/select2/select2.css') }}" />

    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/select2/select2.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>
@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Work Gallery</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="talent_profile_tabs">
                                    <ul>
                                        <li><a href="{{ route('talent.profile.edit') }}">Basic Profile</a></li>
                                        <li><a href="{{ route('talent.availability') }}">Availability</a></li>
                                        <li><a href="{{ route('talent.services-pricing') }}">Services Pricing</a></li>
                                        <li><a href="javascript:void(0);" class="active_tab">Gallery</a></li>
                                        <li><a href="{{ route('talent.social-connectivity') }}">Social Connectivity</a></li>
                                    </ul>
                                </div>

                                <!-- Gallery tab, starts -->
                                <div class="update_profile_data d-block" id="updateTab_5">
                                    <div class="updated_gallery_img">
                                        <h4>Uploaded Images</h4>
                                        <div class="updated_gallery_img_list">
                                            @if( $workImagesCount > 0 )
                                                <ul>
                                                    @foreach( $talentProfile['talentWorkFiles'] as $workFile)
                                                        @if($workFile['type'] === 'image')
                                                            <li>
                                                                <div class="gallery_img_info">
                                                                    <span><img src="{{ asset($workFile['file_url']) }}" alt="image" /></span>
                                                                    {{-- <a href="#" class="edit_icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>--}}
                                                                    <a href="{{ route('talent.work-file.delete', ['id' => $workFile['id']]) }}" class="trash_icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                                    <strong>Lorem Ipsum</strong>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <div class="no_data_here">
                                                    <p>No uploaded images found</p>
                                                     <div class="custom_submit ">
                                                        <a href="{{ route('talent.profile.work-files') }}">Upload Images</a>
                                                     </div>
                                                </div>
                                            @endif
                                        </div>
                                        <br>

                                        <h4>Uploaded Video</h4>
                                        <div class="updated_gallery_img_list update_myVideo">
                                            <ul>
                                                @if( $workVideosCount > 0 )
                                                    @foreach( $talentProfile['talentWorkFiles'] as $workFile)
                                                        @if($workFile['type'] === 'video')
                                                            <li>
                                                                <div class="gallery_img_info">
                                                                    <span>
                                                                        <iframe width="560" height="315" src="{{ asset($workFile['file_url']) }}" frameborder="0" allowfullscreen></iframe></iframe>
                                                                    </span>
                                                                    <a href="{{ route('talent.work-file.delete', ['id' => $workFile['id']]) }}" class="trash_icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                                    <strong>Lorem Ipsum</strong>
                                                                </div>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div class="no_data_here">
                                                        <p>No Data Uploaded yet.</p>
                                                         <div class="custom_submit ">
                                                            <a href="{{ route('talent.profile.work-files') }}">Upload Video</a>
                                                         </div>
                                                    </div>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="main-div"></div>
                                </div>
                                <!-- Gallery tab, ends -->

                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- work images script --}}
        {{--        <script type="text/javascript">--}}
        {{--            // media gallery image--}}
        {{--            // $('#work_images').ssi_uploader();--}}
        {{--        </script>--}}

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')

    <script type="text/javascript">
        // select2 multi-tag select
        $(document).ready(function() {
            $('.selectMultiple').select2();
        })
    </script>

@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">

        {{-- Work history stars --}}
        $('#talentWorkFilesForm').on('submit', function (e) {
            e.preventDefault();
            let workHistoryData =  new FormData( $('#talentWorkFilesForm')[0] );
            blockUi( true );
            $.ajax({
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('talent.profile.work-files.save') }}",
                data: workHistoryData,
                success: function (response) {
                    if ( Boolean(response.status) ) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.reload();
                    } else if ( !Boolean(response.status) ) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, key+": "+value);
                    });
                }
            });
        });
        {{-- Work history, Ends --}}
    </script>


    <script type="text/javascript">
        var formdata = new FormData();

        function previewFile(input) {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function(){
                    $(".preview_img").addClass('my_img');
                    $("#previewImg").attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
        $(document).ready(function(){
            $(".preview_img i").click(function(){
                $('.preview_img').removeClass('my_img');
            });
        });
    </script>


    <script type="text/javascript">
        var countFile=0;
        $(".file_input").on("change", function(event) {
            var files = event.target.files[0]; // also use like    var files = $('#imgInp')[0].files[0];
            var fileName = files.name;
            var fsize = files.size;
            var tmppath = URL.createObjectURL(files);
            var ext = fileName.split('.').pop().toLowerCase();
            if ((fsize / 1024) / 1024 <= 1) {
                if (countFile <= 5) {
                    if (ext == 'jpg' || ext == 'png' || ext == 'jpeg') {

                        $('.main-div').show();
                        var html = '<span class="close-spn-close"><span class="card_img remove-file-icon"  data-id="' + countFile + '">x</span><img class="preview-imgs" src="' + tmppath + '"></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    } else {
                        chatBox1.find('.main-div').show();
                        var html = '<span class="downloaded-items1234 clearfix file_download close-f-attch close-spn-close" href="#"><span class="card_file remove-file-icon"   data-id="' + countFile + '">x</span><span class="left1">' + fileName + '</span></span>';
                        formdata.append('attachment[]', files);
                        $('.chat-main-div-new').find('.main-div').append(html);
                    }
                    countFile++;
                    //   $(this).parents('div.chat-main-div-new').find('textarea.emojionearea').focus();
                } else {
                    errorMsg('Only five files allow at a time');
                }
            } else {
                errorMsg('Select file less than 1 MB');
            }
        });

        $(document).on("click", ".remove-file-icon", function(e) {
            var div_length = $(this).closest('.main-div').children().length;
            $(this).closest('.close-spn-close').remove();
            countFile--;
            div_length--;
            if (div_length == 0) {
                // $('.chat-main-div-new').find('.main-div').html('');
                // $('.chat-main-div-new').find('.main-div').hide();
                // $('.chat-main-div-new').find(".file_input").val('');
            }
        });

        $(function() {
            $('#sort_more').change(function(){
                $('.sorting').hide();
                $('#' + $(this).val()).show();
            });
        });
        $(document).ready(function(){
            $('body').addClass('chat_body');
        });
    </script>
@endsection








