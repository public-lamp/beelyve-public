@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Social Connectivity')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/select2/select2.css') }}" />

    <link rel="stylesheet" href="{{ asset('project-assets/ssi-uploader/styles/ssi-uploader.css') }}"/>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script type="text/javascript" src="{{ asset('project-assets/js/select2/select2.js') }}" defer ></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('project-assets/ssi-uploader/js/ssi-uploader.js') }}"></script>
@endsection


{{-- page header section --}}
@section('page-header')
    @include('app.headers.talent-dashboard-header')
@endsection

@section('main-content')
    <main>

        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">
                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Social Connectivity</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="talent_profile_tabs">
                                    <ul>
                                        <li><a href="{{ route('talent.profile.edit') }}">Basic Profile</a></li>
                                        <li><a href="{{ route('talent.availability') }}">Availability</a></li>
                                        <li><a href="{{ route('talent.services-pricing') }}">Services Pricing</a></li>
                                        <li><a href="{{ route('talent.profile.work-gallery') }}">Gallery</a></li>
                                        <li><a href="javascript:void(0);" class="active_tab">Social Connectivity</a></li>
                                    </ul>
                                </div>

                                <!-- social connectivity tab, starts -->
                                <div class="update_profile_data d-block" id="updateTab_6">
                                    <div class="social_activity_info">
                                        <h4><b class="fb_bg"><i class="fa fa-facebook" aria-hidden="true"></i></b>Facebook Connectivity</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et scelerisque enim. Pellentesque sit amet ultrices augue.
                                            Nam consectetur erat ex, nec porttitor dolor tempus sit amet. Duis molestie bibendum consectetur. In convallis imperdiet efficitur.
                                            Maecenas a dolor at turpis consequat interdum. Nam faucibus enim ex, et eleifend felis aliquet quis.</p>
                                        <div class="custom_submit">
                                            {{-- <input type="submit" value="Save" id=""> --}}
                                            <a href="{{ route('social.login', ['provider' => 'facebook']) }}">Connect With Facebook</a>
                                        </div>
                                    </div>
                                    <div class="social_activity_info">
                                        <h4><b class="instagram_bg"><i class="fa fa-instagram" aria-hidden="true"></i></b>Instagram Connectivity</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et scelerisque enim. Pellentesque sit amet ultrices augue.
                                            Nam consectetur erat ex, nec porttitor dolor tempus sit amet. Duis molestie bibendum consectetur. In convallis imperdiet efficitur.
                                            Maecenas a dolor at turpis consequat interdum. Nam faucibus enim ex, et eleifend felis aliquet quis.</p>
                                        <div class="custom_submit">
                                            <!-- <input type="submit" value="Save" id=""> -->
                                            <a href="{{ route('instagram.login') }}">Connect With Instagram</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- social connectivity tab, ends -->
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection








