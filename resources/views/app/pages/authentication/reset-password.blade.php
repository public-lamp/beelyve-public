@extends('app.layouts.main-layout')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.simple-header')
@endsection

{{-- page title --}}
@section('title', 'Home')

@section('main-content')
    <main>
        <div class="main_container">

            <div class="main_container_inner">
                <div class="auto_container">
                    <div class="main_container_innerDetail">

                        <div class="reset_passw_info">
                            <div class="custom_popup_tittle">
                                <h2>Reset Password</h2>
                                <p>Please enter your new password</p>
                            </div>
                            <form id="resetPasswordForm">
                                @csrf
                                <div class="popup_form">
                                    <ul>
                                        <li>
                                            <div class="custom_field">
                                                <strong>New Password</strong>
                                                <div class="custom_field_input">
                                                    <input type="password" value="" name="reset_password" id="reset_password" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom_field">
                                                <strong>Confirm Password</strong>
                                                <div class="custom_field_input">
                                                    <input type="password" value="" name="reset_password_confirmation" id="reset_password_confirmation" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <br>

                                {{-- hidden reset torken --}}
                                <input type="hidden" value="{{ session('password_reset_token') }}" name="password_reset_token" />

                                <div class="custom_submit">
                                    <input type="submit" value="Submit" id="resetPasswordBtn" />
                                </div>
                            </form>

                            <div class="do_you_agree">
                                <strong>Back to <a href="{{ route('login') }}">Sign in</a></strong>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')

@endsection

@section('ajax')
    <script>
        // page ajax start
        $(document).ready(function() {

            $('#resetPasswordBtn').on('click', function (e) {
                // set form input fields to normal
                $('.errorAlertText').html('');
                e.preventDefault();
                let resetFormData = $('#resetPasswordForm').serialize();
                blockUi( false );
                $.ajax({
                    type: 'POST',
                    url: "{{ route('reset.password.action') }}",
                    data: resetFormData,
                    success: function (response) {
                        if ( response.status ) {
                            blockUi( false );
                            toasterSuccess( true, response.message);
                            return window.location.href = "{{ route('login') }}";
                        }
                        else if ( !response.status ) {
                            blockUi( false );
                            return toasterSuccess( false, response.error);
                        }
                        else {
                            blockUi( false );
                            return toasterSuccess( false, 'Something went wrong, please try again');
                        }
                    },
                    error: function (data) {
                        $.each(data.responseJSON.errors, function (key, value) {
                            blockUi( false );
                            let element = document.getElementById(key);
                            let errorAlertHtml = validationText(value, 'error');
                            $(element).after(errorAlertHtml);
                            if (value.includes(key)) {
                                $(element).val(value);
                            }
                        });
                    }
                });
            });
        });

    </script>
@endsection
