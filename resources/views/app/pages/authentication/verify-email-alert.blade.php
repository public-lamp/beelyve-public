@extends('app.layouts.main-layout')

@section('title', 'Email Verification Alert')

@section('main-content')
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p>Verify Your Email Address</p>
                    </div>
                    <div class="card-body">
                        @if ( session()->remove('resent') )
                            <div class="alert alert-success" role="alert">
                                <p>A fresh verification link has been sent to your email address.</p>
                            </div>
                        @else
                            <p>Before proceeding, please check your email for a verification link.</p>
                        @endif
                        <span>If you did not receive the email</span>
                        <a href="{{ route('verification.resend') }}">click here to request another</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
