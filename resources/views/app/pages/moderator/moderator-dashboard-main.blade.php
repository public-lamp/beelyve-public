@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Dashboard')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    {{--    @include('app.headers.simple-header')--}}
    @include('app.headers.moderator-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">

                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h4>Profile</h4>
                            </div>
                            <div class="our_earning">
                                <strong>Spending<b>
                                    $<i>{{ $moderatorSpending }}</i></b>
                                </strong>
                            </div>
                        </div>
                        @if(!isset(auth()->user()->stripeAccountDetail) || auth()->user()->stripeAccountDetail->stripe_account_id == null)
                            <div class="our_todo_section">
                                {{--todos text and horizonatl line--}}
                                {{--<div class="our_todo_head">--}}
                                {{--<strong>to-dos <i>1</i></strong>--}}
                                {{--</div>--}}

                                <div class="our_todo_info">
                                    <div class="profile_incomplete">
                                        <span><img src="{{ asset('project-assets/images/stripe.png') }}" alt="Setting-icon" /></span>
                                        <p>Stripe Account connectivity is required for payouts. Connect your profile with stripe account</p>
                                    </div>
                                    <a href="{{$btnUrl}}" class="default_bttn">Connect Now</a>

                                    <button type="button" class="close btnClose" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        @endif

                        <div class="dashboard_info">
                            {{-- profile card starts --}}
                            <div class="db_profile ">
                                <div class="featureWeek_info">
                                    <div class="feartureProflie_info">
                                        <div class="featureProfile_avatar">
                                            <a href="{{ route('moderator.profile') }}">
                                                <em class="editBttn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></em>
                                            </a>
                                            <span class="profile_dp">
                                                <img src="{{ isset( $moderator['image'] ) ? $moderator['image'] : asset('project-assets/images/avatar_black.png') }}" alt="#">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featureProfile_avatar_inner">
                                        <div class="featureProfile_avatar_text">
                                            <strong>{{ $moderator['user_name'] }}</strong>
                                            <small>Moderator</small>
                                        </div>

                                        <div class="profile_rating">
                                            <div class="profile_rating_info">
                                                <ul>
                                                    <li>
                                                        @php $rating = $moderator['rating']; @endphp
                                                        @foreach( range(1,5) as $i)
                                                            <a href="javascript:void(0)" class="fill_rate fa-stack" style="width:1em">
                                                                @if($rating > 0.0)
                                                                    @if($rating > 0.5)
                                                                        <i class="fa fa-star fa-stack-1x"></i>
                                                                    @else
                                                                        <i class="fa fa-star-half fa-stack-1x"></i>
                                                                    @endif
                                                                @endif
                                                                @php $rating--; @endphp
                                                            </a>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                                <strong>({{ $moderator['rating'] }})</strong>
                                            </div>
                                        </div>

                                        <div class="achieved_task_info">
                                            <ul>
                                                <li>
                                                    <div class="achieved_task_info_list">
                                                        <span>Completed Projects</span>
                                                        <strong>({{ count($completedBookings) }})</strong>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="achieved_task_info_list">
                                                        <span>Member Since</span>
                                                        <strong>{{ parseDateWithTimeZone($moderator['created_at'], session()->get('browserTimeZone')) }}</strong>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="achieved_task_info_list">
                                                        <span>Amount Spent</span>
                                                        <strong>${{ $moderatorSpending }}</strong>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Projects listings starts --}}
                            <div class="dashboard_infoDetail">
                                <div class="dashboard_inner_detail">
                                    <div class="dashboard_tabs">
                                        <ul>
                                            <li><a href="#activeProject" class="activeTab">Upcoming Bookings<strong>{{ $assignedBookings->total() }}</strong></a></li>
                                            <li><a href="#completeProject">Completed Projects<strong>{{ $completedBookings->total() }}</strong></a></li>
                                        </ul>
                                    </div>

                                    <div class="dashboard_tabs_outer">
                                        <div class="dashboard_tabs_data" style="display: none;" id="activeProject">
                                            <div class="dashboard_dataTable_head">
                                                <div class="data_col w-35">
                                                    {{--                                                    <div class="formSorted">--}}
                                                    {{--                                                        <strong>Sort by:</strong>--}}
                                                    {{--                                                        <div class="custom_dropdown">--}}
                                                    {{--                                                            <select>--}}
                                                    {{--                                                                <option>Date</option>--}}
                                                    {{--                                                                <option>Time</option>--}}
                                                    {{--                                                            </select>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                    </div>--}}
                                                </div>

                                                <div class="data_col w-25">
                                                    <h5>Start date & time</h5>
                                                </div>
                                                <div class="data_col w-10">
                                                    <h5>Total</h5>
                                                </div>
                                                <div class="data_col w-12">
                                                    <h5>Action</h5>
                                                </div>
                                                <div class="data_col w-18">
                                                    <h5>Status</h5>
                                                </div>
                                            </div>

                                            {{-- Project listing section --}}
                                            <div class="dashboard_tabs_dataList">
                                                <ul>
                                                    @if( $assignedBookings->isNotEmpty() )
                                                        @foreach($assignedBookings as $assignedBooking)
                                                            <li>
                                                                <div class="dashboard_tabs_dataInfo">
                                                                    <div class="data_col w-35">
                                                                        <div class="data_profile_info">
                                                                            <span><img src="{{ isset($assignedBooking['talent']['image']) ? $assignedBooking['talent']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                            <div class="data_profile_infoText">
                                                                                <h4>{{ $assignedBooking['talent']['name'] }}</h4>
                                                                                <p>{{ $assignedBooking['title'] }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="data_col w-25">
                                                                        <small>{{ parseDateTimeWithTimeZone($assignedBooking['time_from'], session()->get('browserTimeZone')) }}</small>
                                                                    </div>
                                                                    <div class="data_col w-10">
                                                                        <small>${{ $assignedBooking['servicePricing']['price'] }}</small>
                                                                    </div>
                                                                    <div class="data_col w-12">
                                                                        <div class="action_view">
                                                                            <a href="{{ route('booking.detail', ['booking_id' => $assignedBooking->id]) }}">
                                                                                <button type="button" class="btn btn-info">Details</button>
                                                                            </a>
                                                                            <a href="{{ route('moderator.booking.management', ['booking_id' => $assignedBooking->id]) }}">
                                                                                <button type="button" class="btn btn-primary">Manage</button>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="data_col w-18">
                                                                        <div class="activeBttn">
                                                                            <a href="javascript:void(0)" class="default_bttn">{{ $assignedBooking['bookingStatus']['name'] }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <!-- No projects section -->
                                                         <div class="no_projecty_info">
                                                            <strong>No projects to show, <a href="{{ route('search-talent') }}">Find Talent</a></strong>
                                                        </div>
                                                    @endif
                                                </ul>
                                            </div>
                                            <div class="viewMore">
                                                <a href="{{ route('moderator.bookings.search', ['booking_status' => 'upcoming']) }}">View More
                                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="dashboard_tabs_data" id="completeProject">
                                            <div class="dashboard_dataTable_head">
                                                <div class="data_col w-30">
                                                    {{-- <div class="formSorted">--}}
                                                    {{-- <strong>Sort by:</strong>--}}
                                                    {{-- <div class="custom_dropdown">--}}
                                                    {{-- <select>--}}
                                                    {{-- <option>Date</option>--}}
                                                    {{-- <option>Time</option>--}}
                                                    {{-- </select>--}}
                                                    {{-- </div>--}}
                                                    {{-- </div>--}}
                                                </div>

                                                {{-- <div class="data_col w-15">--}}
                                                {{-- <h5>Start date & time</h5>--}}
                                                {{-- </div>--}}
                                                <div class="data_col w-15">
                                                    <h5>Completed</h5>
                                                </div>
                                                <div class="data_col w-7">
                                                    <h5>Total</h5>
                                                </div>
                                                <div class="data_col w-12">
                                                    <h5>Action</h5>
                                                </div>
                                                <div class="data_col w-12">
                                                    <h5>Status</h5>
                                                </div>
                                            </div>

                                            <div class="dashboard_tabs_dataList">
                                                <ul>
                                                    @if( $completedBookings->isNotEmpty() )
                                                        @foreach($completedBookings as $completedBooking)
                                                            <li>
                                                                <div class="dashboard_tabs_dataInfo">
                                                                    <div class="data_col w-30">
                                                                        <div class="data_profile_info">
                                                                            <span><img src="{{ isset($completedBooking['talent']['image']) ? $completedBooking['talent']['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="#" /></span>
                                                                            <div class="data_profile_infoText">
                                                                                <h4>{{ $completedBooking['talent']['name'] }}</h4>
                                                                                <p>{{ $completedBooking['title'] }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    {{-- <div class="data_col w-15">--}}
                                                                    {{-- <small>{{ date('d M, Y h:i a', strtotime($completedBooking['time_from'])) }}</small>--}}
                                                                    {{-- </div>--}}
                                                                    <div class="data_col w-15">
                                                                        <small>{{ parseDateTimeWithTimeZone($completedBooking['time_to'], session()->get('browserTimeZone')) }}</small>
                                                                    </div>
                                                                    <div class="data_col w-7">
                                                                        <small>${{ $completedBooking['servicePricing']['price'] }}</small>
                                                                    </div>
                                                                    <div class="data_col w-12">
                                                                        <div class="action_view">
                                                                            <a href="{{ route('booking.detail', ['booking_id' => $completedBooking->id]) }}">
                                                                                <button type="button" class="btn btn-info">Details</button>
                                                                            </a>
                                                                            @if( in_array($completedBooking['booking_status_id'], [4, 6, 7, 8])  )
                                                                                <a href="{{ route('moderator.booking.management', ['booking_id' => $completedBooking->id]) }}">
                                                                                    <button type="button" class="btn btn-primary">Manage</button>
                                                                                </a>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="data_col w-12">
                                                                        <div class="activeBttn">
                                                                            <a href="javascript:void(0)" class="default_bttn">{{ $completedBooking['bookingStatus']['name'] }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <!-- No projects section -->
                                                        <div class="no_projecty_info">
                                                            <strong>No projects to show</strong>
                                                        </div>
                                                    @endif
                                                </ul>
                                            </div>
                                            <div class="viewMore">
                                                <a href="{{ route('moderator.bookings.search', ['booking_status' => 'completed']) }}">View More
                                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection



