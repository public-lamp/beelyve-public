@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Conversations')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    {{--    @include('app.headers.simple-header')--}}
    @include('app.headers.moderator-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            @include('app.sections.chat.conversations_section')

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--    @include('app.sections.talent.edit-profile-modal')--}}
@endsection

@section('ajax')
@endsection



