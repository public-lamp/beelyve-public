@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Favorites')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    {{--    @include('app.headers.simple-header')--}}
    @include('app.headers.moderator-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <div class="event_pages db_messageChat bookingTab_sett">
                <div class="auto_container">

                    <div class="dasbord_container_detail">
                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h4>Favorite Talents</h4>
                            </div>
                        </div>
                        <div class="dashboard_info">
                            <div class="dashboard_infoDetail">
                                <div class="dashboard_inner_detail">

                                    <div class="dashboard_tabs_outer">
                                        <div class="dashboard_tabs_data">
                                            <div class="dashboard_dataTable_head">
                                                <div class="data_col w-25">
                                                    <h5>Talent Details</h5>
                                                </div>

                                                <div class="data_col w-15">
                                                    <h5>Rating</h5>
                                                </div>
                                                <div class="data_col w-25">
                                                    <h5>Working Since</h5>
                                                </div>
                                                <div class="data_col w-35">
                                                    <h5>Actions</h5>
                                                </div>
                                            </div>

                                            <div class="dashboard_tabs_dataList">
                                                <ul>
                                                    @if( count($favouriteUsers) > 0 )
                                                        @foreach($favouriteUsers as $favorite)
                                                            <li>
                                                                <div class="dashboard_tabs_dataInfo">
                                                                    <div class="data_col w-25">
                                                                        <div class="data_profile_info">
                                                                            <span><img src="{{ isset($talent['image']) ? $talent['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="img"></span>
                                                                            <div class="data_profile_infoText">
                                                                                <h4>{{ $favorite['talent']['user_name'] }}</h4>
                                                                                <p>{{ $favorite['talent']['talentProfessionalInfo']['talentCategory']['name'] }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="data_col w-15">
                                                                        <small>{{ $favorite['talent']['rating'] }}</small>
                                                                    </div>
                                                                    <div class="data_col w-25">
                                                                        <small>{{ parseDateWithTimeZone($favorite['talent']['created_at'], session('browserTimeZone')) }}</small>
                                                                    </div>
                                                                    <div class="data_col w-35 text-center">
                                                                        <a href="{{ route('favourite.user.remove', ['favorable_id' => $favorite['favorable_id']]) }}">
                                                                            <button type="button" class="btn btn-danger">Un-Favorite</button>
                                                                        </a>
                                                                        <a href="{{ route('talent.public-profile', ['talent_id' => encryptor('encrypt', $favorite['favorable_id'])]) }}">
                                                                            <button type="button" class="btn btn-primary">Profile</button>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <!-- favorite users -->
                                                        <div class="no_projecty_info">
                                                            <strong>No Favorite Users to show</strong>
                                                        </div>
                                                    @endif
                                                </ul>
                                            </div>
                                            <div class="">
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination">{{ $favouriteUsers->appends(request()->except('page'))->links() }}</ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection



