@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Profile')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    {{--    @include('app.headers.simple-header')--}}
    @include('app.headers.moderator-dashboard-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <div class="dasbord_container">
                <div class="auto_container">
                    <div class="dasbord_container_detail">

                        <div class="update_profile editPopup">
                            <div class="custom_popup_tittle">
                                <h3>Update Profile</h3>
                            </div>
                            <div class="update_profile_detail">
                                <div class="update_profile_data d-block">
                                    <form id="moderatorProfileForm">
                                        <div class="edit_profile_head">
                                            <div class="choose_profile">
                                                <div class="profile_img">
                                                <span>
                                                    <strong class="my_file">
                                                        <input type='file' name="profile_image" id="profile_image" onchange="readURL(this);" />
                                                        <small><i class="fa fa-camera fa-3" aria-hidden="true"></i>Change</small>
                                                    </strong>
                                                    <img id="blah" src="{{ isset($moderator['image']) ? $moderator['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="your image" />
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="popup_form">
                                            <ul>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>First Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ $moderator['first_name'] }}" name="first_name" id="first_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-50">
                                                    <div class="custom_field">
                                                        <strong>Last Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ $moderator['last_name'] }}" name="last_name" id="last_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-100">
                                                    <div class="custom_field">
                                                        <strong>User Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ $moderator['user_name'] }}" name="user_name" id="user_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Save">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
    <script type="text/javascript">
        //    save info ajax
        $('#moderatorProfileForm').submit(function (e) {
            // set input fields to normal
            $('.errorAlertText').html('');
            e.preventDefault();
            blockUi( true );
            let moderatorProfileData = new FormData( this );

            $.ajax({
                method: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                processData: false,
                contentType: false,
                cache: false,
                enctype: 'multipart/form-data',
                url: "{{ route('moderator.profile.save') }}",
                data: moderatorProfileData,
                success: function (response) {
                    blockUi( false );
                    removeAlertElementsByClass('errorAlertText');
                    if ( response.status ) {
                        toasterSuccess( true,  response.message);
                    } else if( !response.status ) {
                        toasterSuccess( false, response.error);
                    } else {
                        toasterSuccess( false, 'Something went wrong, please try again');
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        toasterSuccess( false, value);
                    });
                },
                statusCode: {
                    429: function() {
                        blockUi( false );
                        toasterSuccess( false, 'Too many attempts, Please try after some time');
                    }
                }
            });
        });
    </script>
@endsection



