{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Admin Login')

@section('main-content')
    <main>
        <div class="main_container">
            <div class="containerDetail">
                <div class="auto_container">

                    <div class="custom_popup_inner p-0">
                        <div class="custom_popup_detail">
                            {{-- login alert message --}}
                            @if(session()->has('loginAlertMessage'))
                                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                                    <strong>Login Alert!</strong>{{ session()->get('loginAlertMessage') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="custom_popup_tittle">
                                <h2>Admin Login</h2>
                            </div>
                            <form id="adminLoginForm">
                                @csrf
                                <div class="popup_form">
                                    <ul>
                                        <li>
                                            <div class="custom_field">
                                                <strong>Email</strong>
                                                <div class="custom_field_input">
                                                    <input type="text" value="" name="admin_login_email" id="admin_login_email" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom_field">
                                                <strong>Your Password</strong>
                                                <div class="custom_field_input">
                                                    <input type="password" value="" name="admin_login_password" id="admin_login_password" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="custom_submit">
                                    <input type="submit" value="Sign In" id="adminLoginBtn" />
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
@endsection

@section('ajax')
    {{-- Admin login scripts --}}
    <script type="text/javascript">
        // page ajax start
        $('#adminLoginBtn').on('click', function (e) {
            // set input fields to normal
            $('.errorAlertText').html('');
            e.preventDefault();
            blockUi( true );
            let adminLoginData = $('#adminLoginForm').serializeArray();
            let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            adminLoginData.push({ name: 'time_zone', value: timeZone });

            $.ajax({
                method: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('admin.login.post') }}",
                data: adminLoginData,
                success: function (response) {
                    blockUi( false );
                    if ( !response.status ) {
                        return toasterSuccess( false, response.error);
                    }
                    toasterSuccess( true,  response.message);
                    window.location.href = response.data.next_url;
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        toasterSuccess( false, value);
                    });
                },
                statusCode: {
                    429: function() {
                        blockUi( false );
                        toasterSuccess( false, 'Too many attempts, Please try after some time');
                    }
                }
            });
        });
    </script>
@endsection
