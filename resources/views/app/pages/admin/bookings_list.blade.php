@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Bookings')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">

        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">
                <div class="d_board_detail">
                    <div class="custom_tittle_section text-left">
                        <h3>Bookings Listing</h3>
                    </div>
                    <div class="login_cotainerDetail">
                        <div class="table_wrapper">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table  table-bordered" id="bookings_table" style="width:100%">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Title</th>
                                                <th scope="col">Detail Note (Moderator)</th>
                                                <th scope="col">Moderator Name</th>
                                                <th scope="col">Talent Name</th>
                                                <th scope="col">Time From</th>
                                                <th scope="col">Time To</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Payment Status</th>
                                                <th scope="col">Payout Status</th>
                                                <th scope="col">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($bookings as $bookingKey => $booking)
                                                <tr>
                                                    <th scope="row">{{ $bookingKey+1 }}</th>
                                                    <td>{{ $booking['title'] }}</td>
                                                    <td>{{ $booking['detail_note'] }}</td>
                                                    <td>{{ $booking['moderator']['full_name'] }}</td>
                                                    <td>{{ $booking['talent']['full_name'] }}</td>
                                                    <td>{{ parseDateTimeWithTimeZone($booking['time_from'], auth()->user()->time_zone) }}</td>
                                                    <td>{{ parseDateTimeWithTimeZone($booking['time_to'], auth()->user()->time_zone) }}</td>
                                                    <td>{{ $booking['bookingStatus']['name'] }}</td>
                                                    <td>{{ $booking['bookingPaymentStatus']['name'] }}</td>
                                                    <td>
                                                        @if( isset($booking['bookingPayout']) )
                                                            {{ $booking['bookingPayout']['payoutStatus']['status'] }}
                                                        @else
                                                            Not Initiated Yet
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if( isset($booking['bookingPayout']) && in_array($booking['bookingPayout']['payout_status_id'], [2, 4, 5]) )
                                                            @if(in_array($booking['booking_status_id'], [5]))
                                                                <a href="{{ route('admin.payouts.moderator.process', ['booking_id' => $booking->id]) }}" id="manualPayModeratorBtn">
                                                                    <button type="button" class="btn btn-info">Pay back to Moderator</button>
                                                                </a>
                                                            @else
                                                                <a href="{{ route('admin.payouts.process', ['booking_id' => $booking->id]) }}" id="manualPayTalentBtn">
                                                                    <button type="button" class="btn btn-info">Pay Talent Now</button>
                                                                </a>
                                                            @endif
                                                        @else
                                                            No Action Required
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });

            //    datatables
            $('#bookings_table').DataTable({
                "scrollX": true
            });


            //   Talent manual payout btn click handling
            $('#manualPayTalentBtn').click(function () {
                blockUi();
            })

            //   Moderator manual payout btn click handling
            $('#manualPayModeratorBtn').click(function () {
                blockUi();
            })
        });
    </script>
@endsection
