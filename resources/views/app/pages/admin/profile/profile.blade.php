@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Profile')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">

        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">
                <div class="d_board_detail">

                    <div class="update_profile editPopup">
                        <div class="db_contact">
                            <div class="custom_tittle_section ">
                                <h3 class="p-0">Admin Profile</h3>
                            </div>

                            <div class="contactUs_detail">
                                <div class="update_profile_data d-block">
                                    <form id="adminProfileForm">
                                        <div class="edit_profile_head">
                                            <div class="choose_profile">
                                                <div class="profile_img">
                                                    <span>
                                                        <strong class="my_file">
                                                            <input type='file' name="profile_image" id="profile_image" onchange="readURL(this);" />
                                                            <small><i class="fa fa-camera fa-3" aria-hidden="true"></i>Change</small>
                                                        </strong>
                                                        <img id="blah" src="{{ isset($admin['image']) ? $admin['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="your image" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="popup_form">
                                            <ul>
                                                <li class="w-100">
                                                    <div class="custom_field">
                                                        <strong>First Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ $admin['first_name'] }}" name="first_name" id="first_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-100">
                                                    <div class="custom_field">
                                                        <strong>Last Name</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ $admin['last_name'] }}" name="last_name" id="last_name" />
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="w-100">
                                                    <div class="custom_field">
                                                        <strong>Email Address</strong>
                                                        <div class="custom_field_input">
                                                            <input type="text" value="{{ $admin['email'] }}" name="email_address" id="email_address" disabled readonly />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="custom_submit show_submit">
                                            <input type="submit" value="Save">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });


            //    save info ajax
            $('#adminProfileForm').submit(function (e) {
                // set input fields to normal
                $('.errorAlertText').html('');
                e.preventDefault();
                blockUi( true );
                let adminProfileData = new FormData( this );

                $.ajax({
                    method: 'POST',
                    headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                    processData: false,
                    contentType: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    url: "{{ route('admin.profile.save') }}",
                    data: adminProfileData,
                    success: function (response) {
                        blockUi( false );
                        removeAlertElementsByClass('errorAlertText');
                        if ( response.status ) {
                            toasterSuccess( true,  response.message);
                        } else if( !response.status ) {
                            toasterSuccess( false, response.error);
                        } else {
                            toasterSuccess( false, 'Something went wrong, please try again');
                        }
                    },
                    error: function (data) {
                        $.each(data.responseJSON.errors, function (key, value) {
                            blockUi( false );
                            let element = document.getElementById(key);
                            let errorAlertHtml = validationText(value, 'error');
                            $(element).after(errorAlertHtml);
                            toasterSuccess( false, value);
                        });
                    },
                    statusCode: {
                        429: function() {
                            blockUi( false );
                            toasterSuccess( false, 'Too many attempts, Please try after some time');
                        }
                    }
                });
            });
        });
    </script>
@endsection
