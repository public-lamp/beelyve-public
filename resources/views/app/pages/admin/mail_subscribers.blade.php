@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Users')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">

        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">

                <div class="d_board_detail">
                    <div class="custom_tittle_section text-left">
                        <h3>Users Listing</h3>
                    </div>
                    <div class="login_cotainerDetail">
                        <div class="table_wrapper">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-bordered display" id="mailSubscribersTable" style="width:100%">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Subscriber Type</th>
                                                <th scope="col">Subscribed Email</th>
                                                <th scope="col">Subscribed At</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($mailSubscribers as $subKey => $subscriber)
                                            <tr>
                                                <th scope="row">{{ ++$subKey }}</th>
                                                <td>{{ isset($subscriber['user']) ? 'Registered User' : 'Guest' }}</td>
                                                <td>{{ $subscriber['email'] }}</td>
                                                <td>{{ parseDateTimeWithTimeZone($subscriber['created_at'], auth()->user()->time_zone) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });

            //    datatables
            $('#mailSubscribersTable').DataTable({
                "scrollX": true
            });
        });
    </script>
@endsection

@section('ajax')
@endsection
