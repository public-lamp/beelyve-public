@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Send Subscribers Email')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">
        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">
                <div class="d_board_detail">

                    <div class="db_contact">
                        <div class="custom_tittle_section ">
                            <h3 class="p-0">Service Charges Settings</h3>
                        </div>
                        <div class="contactUs_detail changeServices">
                            <div class="contactUs_detail_info">
                                <form id="saveServiceChargesForm">
                                    <div class="popup_form">
                                        <ul>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Service type</strong>
                                                    <div class="custom_dropdown">
                                                        <select name="service_type_id" id="service_type_id" >
                                                            <option selected disabled>Select service type</option>
                                                            @foreach(\App\Models\Generic\ServiceType::all() as $serviceType)
                                                                <option value="{{ $serviceType['id'] }}" @if($serviceType['id'] == $bookingServiceCharge['service_type_id']) selected @endif>{{ $serviceType['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Service Charge Type</strong>
                                                    <div class="custom_dropdown">
                                                        <select name="service_charge_type_id" id="service_charge_type_id" >
                                                            <option selected disabled>Select service charge type</option>
                                                            @foreach(\App\Models\Generic\ServiceChargeType::all() as $serviceChargeType)
                                                                <option value="{{ $serviceChargeType['id'] }}" @if($serviceChargeType['id'] == $bookingServiceCharge['service_charge_type_id']) selected @endif>{{ $serviceChargeType['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Value</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="{{ $bookingServiceCharge['value'] }}" name="charge_value" id="charge_value" />
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <br>
                                    <div class="custom_submit">
                                        <input type="submit" value="Save">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
@endsection

@section('ajax')
    {{-- Admin send subscription email --}}
    <script type="text/javascript">
        // page ajax start
        $('#saveServiceChargesForm').submit(function (e) {
            // set input fields to normal
            $('.errorAlertText').html('');
            e.preventDefault();
            blockUi( true );
            let subscriptionEmailData = $('#saveServiceChargesForm').serializeArray();

            $.ajax({
                method: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('admin.service-charges.post') }}",
                data: subscriptionEmailData,
                success: function (response) {
                    blockUi( false );
                    if ( response.status ) {
                        toasterSuccess( true,  response.message);
                    } else  {
                        toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                    });
                },
                statusCode: {
                    429: function() {
                        blockUi( false );
                        toasterSuccess( false, 'Too many attempts, Please try after some time');
                    }
                }
            });
        });
    </script>
@endsection
