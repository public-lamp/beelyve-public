@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Payouts')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">

        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">
                <div class="d_board_detail">
                    <div class="custom_tittle_section text-left">
                        <h3>Payouts Listing</h3>
                    </div>
                    <div class="login_cotainerDetail">
                        <div class="table_wrapper">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table  table-bordered" id="bookings_table" style="width:100%">
                                        <thead class="thead-dark">
                                            <tr>

                                                <th scope="col">#</th>
                                                <th scope="col">Stripe Payout Id</th>
                                                <th scope="col">Booking Id</th>
                                                <th scope="col">Booking Title</th>
                                                <th scope="col">Booking Total Amount</th>
                                                <th scope="col">Transferable Amount</th>
                                                <th scope="col">Service Charges</th>
                                                <th scope="col">Payout status</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payouts as $payoutKey => $payoutRecord)
                                                <tr>
                                                    <th scope="row">{{ $payoutKey+1 }}</th>
                                                    <td>{{ isset($payoutRecord['stripe_payout_id'])?$payoutRecord['stripe_payout_id']:"" }}</td>
                                                    <td>{{ isset($payoutRecord['booking']['id'])?$payoutRecord['booking']['id']:"" }}</td>
                                                    <td>{{ isset($payoutRecord['booking']['title'])?$payoutRecord['booking']['title']:"" }}</td>
                                                    <td>{{ "USD".$payoutRecord['booking']['amount'] }}</td>
                                                    <td>{{ "USD".$payoutRecord['amount'] }}</td>
                                                    <td>{{ "USD".($payoutRecord['booking']['amount'] - $payoutRecord['amount']) }}</td>
                                                    <td>{{ isset($payoutRecord['payoutStatus']['slug'])?$payoutRecord['payoutStatus']['slug']:""}}</td>
                                                    <td><a class="btn btn-success" target="_blank" href="{{route('booking.detail',['booking_id'=>$payoutRecord->booking_id])}}">View Booking</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });

            //    datatables
            $('#bookings_table').DataTable({
                "scrollX": true
            });
        });
    </script>
@endsection
