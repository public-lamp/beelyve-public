@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Admin Dashboard Main')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">

        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">
                <div class="d_board_detail welcomPageSetting p-0">

                    <div class="db_welcomeSection">
                        <div class="custom_tittle_section text-left">
                            <h3 class="p-0 m-0">Dashboard</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="welcomeCol_info">
                                    <a href="javascript:void(0);">
                                        <span><i class="fa fa-user-circle-o" aria-hidden="true"></i></span>
                                        <strong>Registered Users<b>{{ count($users) }}</b></strong>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="welcomeCol_info">
                                    <a href="javascript:void(0);">
                                        <span><i class="fa fa-bookmark" aria-hidden="true"></i></span>
                                        <strong>Today's Bookings<b>{{ count($todayBookings) }}</b></strong>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="welcomeCol_info">
                                    <a href="javascript:void(0);">
                                        <span><i class="fa fa-bookmark" aria-hidden="true"></i></span>
                                        <strong>Upcoming Bookings<b>{{ count($upcomingBookings) }}</b></strong>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="welcomeCol_info">
                                    <a href="javascript:void(0);">
                                        <span><i class="fa fa-bookmark" aria-hidden="true"></i></span>
                                        <strong>Completed Bookings<b>{{ count($completedBookings) }}</b></strong>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="welcomeCol_info">
                                    <a href="javascript:void(0);">
                                        <span><i class="fa fa-usd" aria-hidden="true"></i></span>
                                        <strong>Funds Transferred<b>${{ $fundsTransferred }}</b></strong>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="welcomeCol_info">
                                    <a href="javascript:void(0);">
                                        <span><i class="fa fa-usd" aria-hidden="true"></i></span>
                                        <strong>Funds in Escrow<b>${{ $fundsReceived }}</b></strong>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });
        });
    </script>
@endsection

@section('ajax')
@endsection
