@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Send Subscribers Email')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">
        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">
                <div class="d_board_detail">

                    <div class="db_contact">
                        <div class="custom_tittle_section ">
                            <h3 class="p-0">Send Email to Subscribers</h3>
                        </div>
                        <div class="contactUs_detail">
                            <div class="contactUs_detail_info">
                                <form id="sendMailListEmailForm">
                                    <div class="popup_form">
                                        <ul>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Email Title</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="" name="email_title" id="email_title" />
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Email Subject</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="" name="email_subject" id="email_subject" />
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Message</strong>
                                                    <div class="custom_field_textarea">
                                                        <textarea name="email_message" id="email_message"></textarea>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <br>
                                    <div class="custom_submit">
                                        <input type="submit" value="Dispatch">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });
        });
    </script>
@endsection

@section('ajax')
    {{-- Admin send subscription email --}}
    <script type="text/javascript">
        // page ajax start
        $('#sendMailListEmailForm').submit(function (e) {
            // set input fields to normal
            $('.errorAlertText').html('');
            e.preventDefault();
            blockUi( true );
            let subscriptionEmailData = $('#sendMailListEmailForm').serializeArray();

            $.ajax({
                method: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('admin.subscribers.send-email.post') }}",
                data: subscriptionEmailData,
                success: function (response) {
                    blockUi( false );
                    if ( response.status ) {
                        toasterSuccess( true,  response.message);
                    } else  {
                        toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        toasterSuccess( false, value);
                    });
                },
                statusCode: {
                    429: function() {
                        blockUi( false );
                        toasterSuccess( false, 'Too many attempts, Please try after some time');
                    }
                }
            });
        });
    </script>
@endsection
