@extends('app.layouts.admin.admin_layout')

{{-- page title --}}
@section('title', 'Users')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('main-content')
    {{-- admin main header --}}
    @include('app.layouts.admin.main_header')

    <div class="db_container">

        {{-- sidebar --}}
        @include('app.layouts.admin.main_sidebar')

        <div class="db_containerDetail">
            <div class="db_containerDetail_inner">

                <div class="d_board_detail">
                    <div class="custom_tittle_section text-left">
                        <h3>Users Listing</h3>
                    </div>
                    <div class="login_cotainerDetail">
                        <div class="table_wrapper">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-bordered display" id="users_table" style="width:100%">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Registered Email</th>
                                                <th scope="col">User Name</th>
                                                <th scope="col">First Name</th>
                                                <th scope="col">Last Name</th>
                                                <th scope="col">Role</th>
                                                <th scope="col">Registered Since</th>
                                                <th scope="col">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $userKey => $user)
                                                <tr>
                                                    <th scope="row">{{ $userKey+1 }}</th>
                                                    <td>{{ $user['email'] }}</td>
                                                    <td>{{ $user['user_name'] }}</td>
                                                    <td>{{ $user['first_name'] }}</td>
                                                    <td>{{ $user['first_name'] }}</td>
                                                    <td>
                                                        @foreach($user['roles'] as $role)
                                                            {{ $role['name'] }} <br>
                                                        @endforeach
                                                    </td>
                                                    <td>{{ parseDateTimeWithTimeZone($user['created_at'], auth()->user()->time_zone) }}</td>
                                                    <td>
                                                        @if($user->istalent())
                                                            @if(isset($user['featuredStatus']))
                                                                <a href="{{ route('admin.user.featured.undo', ['user_id' => $user->id]) }}" id="undoUserFeatured">
                                                                    <button type="button" class="btn btn-danger">Undo Featured Status</button>
                                                                </a>
                                                            @else
                                                                <a href="{{ route('admin.user.featured.make', ['user_id' => $user->id, 'role_id' => 2]) }}" id="makeUserFeatured">
                                                                    <button type="button" class="btn btn-info">Make as Featured</button>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--    admin dashboard main static-footer --}}
    @include('app.layouts.admin.main_footer')
@endsection

@section('page-misc-scripts')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.has_dp').click(function() {
                $(this).parent("li").toggleClass("active_nav");
                $(this).parent(".db_nav_menu ul li").find('ul').stop().slideToggle();
            });

            //    datatables
            $('#users_table').DataTable({
                "scrollX": true
            });

        });

        //   make user featured click handling
        $('#makeUserFeatured').click(function () {
            blockUi();
        });
        //   undo user featured click handling
        $('#undoUserFeatured').click(function () {
            blockUi();
        });
    </script>
@endsection

@section('ajax')
@endsection
