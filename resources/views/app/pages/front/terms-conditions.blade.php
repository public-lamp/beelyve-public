{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Terms & Conditions')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section aboutUs_banner tabBanner">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img">
                        <img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="#" />
                    </span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Terms & Conditions<p><a href="{{ route('home') }}">www.{{ strtolower(config('app.name')) }}.com</a></p></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">
                        <div class="terms_condition_detail">
                            <h4>Effective date: <b>1st September 2021</b></h4>
                                <h4>1. Introduction</h4>
                                <p>Welcome to <a href="{{ route('home') }}">www.{{ strtolower(config('app.name')) }}.com</a> (“Site” or “Website”).</p>
                                <p>This website is owned and operated by Moderator Live, Inc. from the United States. Throughout the Site, the terms “we”, “us”, “platform”, “Moderator Live” and “our” refer to Moderator Live, Inc. We offer this website, including all information, tools, and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies, and notices stated here.</p>
                                <p>By visiting our site and/or when using any of our site features, you (“User”) engage in our “Service” and agree to be bound by the following terms and conditions (“Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms and Conditions apply to all users of the site, including without limitation users who are browsers, users, and/ or contributors of content.</p>
                                <h4>PLEASE READ THE FOLLOWING TERMS AND DISCLAIMERS CAREFULLY BEFORE USING THE SERVICES. IF YOU DO NOT AGREE WITH THESE TERMS, OUR PRIVACY POLICY, OR ANY OTHER OF OUR POLICIES, YOU SHOULD NOT USE THE SERVICES.</h4>
                                <h4>2. Moderator Live – General Information</h4>
                                <ul class="list_info listSetting">
                                    <li><b>About.</b> Moderator Live is an online marketplace that helps to connect experts (“Sellers”)
                                            to be booked (“Booking”) for live digital events, zoom calls, Instagram live, or any other
                                            live streaming platform with buyers (“Buyers”) who are purchasing their services. The
                                            Buyers and Sellers are collectively referred to as the “Users”. To find out more
                                            information about what we do, please refer to our website.</li>
                                    <li><b>Sole discretion.</b> We reserve the right to add/discontinue any service anytime at our sole
                                        discretion.</li>
                                </ul>

                                <h4>3. Eligibility</h4>
                                <p>Moderator Live is strictly limited to parties who can lawfully enter into and form contracts on the
                                Internet. If you are under age 18, you may only use the Services with the consent of your parent
                                or legal guardian. Please be sure your parent or legal guardian has reviewed and discussed
                                these Terms with you.</p>
                                <p>Only registered users may buy and sell services on Moderator Live. Registration is free. In
                                registering for an account, you agree to provide us with accurate, complete, and updated
                                information and must not create an account for fraudulent or misleading purposes. You are
                                solely responsible for any activity on your account and for maintaining the confidentiality and
                                security of your password. We are not liable for any acts or omissions by you in connection with
                                your account.</p>

                                <h4>4. Hiring Sellers</h4>
                                <p>You specifically acknowledge and agree that Moderator Live is only a conduit that allows buyers
                                to browse and hire experts for their events and online streaming. Moderator Live takes no
                                responsibility and assumes no liability for any actions, non-actions, speeches, or content
                                attributable to the seller.</p>
                                <h4>AS A SELLER, YOU AGREE THAT YOU ARE SOLELY RESPONSIBLE FOR THE CONTENT
                                OF AND PERFORMANCE OF YOUR SPEECH. AS A SELLER OR AS A USER HIRING A
                                SELLER, YOU AGREE THAT MODERATOR LIVE HAS NO LIABILITY OR OBLIGATION TO
                                YOU REGARDING THE QUALITY OF A SELLERS SERVICES OR THE PERFORMANCE OR
                                NON-PERFORMANCE OF A SERVICE.</h4>
                                <p>Buyers and sellers will work directly together regarding any disputes, issues, or disagreements
                                related to the Services within 10 days as described in detail in the dispute section
                                below. Moderator Live is not responsible and has no obligation, to mediate or otherwise
                                intercede in any such disagreement. Moderator Live may, but is not obligated to, forward to you
                                any complaints or concerns it receives.</p>

                                <h4>5. Permitted use</h4>
                                <p>You agree to use the Site and the Services only for purposes that are permitted by these Terms
                                and in compliance with all applicable laws, regulations, and generally accepted practices or
                                guidelines in the relevant jurisdictions. You may only use the Site and Services for your noncommercial,
                                non-exclusive, non-assignable, non-transferable, and limited personal use, and no
                                other purposes.</p>
                                <p>You will not (and will not attempt to):</p>

                                <ul class="list_info listSetting">
                                    <li>Access any of the Services by any means other than through the interface that is provided by Moderator Live;</li>
                                    <li>Gain unauthorized access to Moderator Live’s computer system or engage in any activity that interferes with the performance of, or impairs the functionality or security of the Site, the Services, Moderator Live’s networks, and computer systems;</li>
                                    <li>Access any of the Site or the Services through any automated means or with any automated features or devices (including use of scripts or web crawlers);</li>
                                    <li>Access or collect any personally identifiable information, including any names, email addresses, or other such information for any purpose, including, without limitation, commercial purposes;</li>
                                    <li>Reproduce, duplicate, copy, sell, trade, or resell any aspect of the Site or the Services for any purpose; and</li>
                                    <li>Reproduce, duplicate, copy, sell, trade, or resell any products or services bearing any trademark, service mark, trade name, logo, or service mark owned by Moderator Live in a way that is likely or intended to confuse the owner or authorized user of such marks, names or logos.</li>
                                </ul>

                                <h4>6. Violations</h4>
                                <p>Bookings and/or users may be removed by Moderator Live from the Site for violations of these Terms of Service, which may include (but are not limited to) the following violations and/or materials:</p>

                                <ul class="list_info listSetting_circle">
                                    <li>Illegal or Fraudulent services</li>
                                    <li>Copyright Infringement, Trademark Infringement, and violation of a third party's terms of service reported.</li>
                                    <li>Adult oriented services, Pornographic, Inappropriate/Obscene</li>
                                    <li>Intentional copies of Bookings</li>
                                    <li>Spam, nonsense, or violent or deceptive Bookings</li>
                                    <li>Bookings misleading to Buyers or others</li>
                                    <li>Exceedingly low-quality Services</li>
                                    <li>Promoting Moderator Live and/or Moderator Live Bookings through activities that are prohibited by any laws, regulations, and/or third parties' terms of service, as well as through any marketing activity that negatively affects our relationships with our users or partners.</li>
                                </ul>
                                <h4>7. Seller Terms</h4>

                                <ul class="list_info listSetting">
                                    <li>Sellers can accept or reject a booking. The buyer will be refunded in the event of a rejection of a booking.</li>
                                    <li>Sellers must fulfill their bookings, and may not cancel bookings regularly or without cause. Canceling bookings will affect Sellers’ reputation and status.</li>
                                    <li>Sellers will receive a user rating between 1 to 5 stars. High ratings allow sellers to obtain advanced Seller levels from level 1 to level 2 and to top-rated seller. In certain cases, exceedingly low ratings may lead to the suspension of the Seller’s account.</li>
                                    <li>Users may not offer or accept payments using any method other than placing an booking through Moderator Live.</li>
                                    <li>Services and Bookings that are removed for violations mentioned above, may result in the suspension of the Seller’s account.</li>
                                    <li>For security concerns, Beelyve may temporarily disable a Seller’s ability to withdraw revenue to prevent fraudulent or illicit activity. This may come as a result of security issues, improper behavior reported by other users, or associating multiple Beelyve accounts to a single withdrawal provider.</li>
                                    <li>Services and Bookings that are removed for violations are not eligible to be restored or edited.</li>
                                    <li>Each Service you sell and successfully complete, accredits your account with a revenue equal to 90% of the purchase amount and 10% goes to Moderator Live.</li>
                                    <li>Sellers are responsible for paying any direct or indirect taxes, including any GST, VAT or income tax, which may apply to them depending on residency, location or otherwise, under provisions of their jurisdiction. Sellers represent and warrant that they comply, and will comply at all times, with their obligations under income tax provisions in their jurisdiction. The price shown on the website is inclusive of all such taxes and charges that may apply to the Sellers.</li>
                                    <li>Sellers acknowledge that when they miss an event, and/or leave before 25% of the time is completed, the Buyer is fully refunded and the Seller will be subject to negative reviews that will adversely affect their profile.</li>
                                </ul>

                                <h4>8. Buyer Terms</h4>
                                <ul class="list_info listSetting">
                                    <li>You may not offer direct payments to Sellers using payment systems outside of the Beelyve platform.</li>
                                    <li>Moderator Live retains the right to use all publicly published delivered works for Moderator Live marketing and promotional purposes.</li>
                                </ul>

                                <h4>9. General Terms</h4>
                                <ul class="list_info listSetting">
                                    <li>If a Buyer books a Seller, the Seller can set their service as “No Refund” or “Refunds” up until 24 hours before the commencement of the event.</li>
                                    <li>In the event of a “No Refund”, the seller will enable buyer to reschedule without charge at sole discretion.</li>
                                    <li>If a Seller does not check-in to the event that they booked, they are not entitled to leave a review.</li>
                                    <li>Both Buyer and Seller have to check-in with our event management system to show whether they are present during the event or not. This is what <a href="{{ route('home') }}">www.{{ strtolower(config('app.name')) }}.com</a> will go by to determine whether or not Buyers and Sellers are present during an event.</li>
                                </ul>

                                <h4>10. Payment Withdrawal</h4>
                                <ul class="list_info listSetting">
                                    <li>To withdraw your revenue, you must have an account with at least one of Moderator Live's Payment Service Providers for the withdrawal. All funds eligible for Withdrawal will be held on your behalf at an account with Moderator Live’s Payment Services Provider. All payment services, including withdrawal services, will be provided by Moderator Live’s Payment Services Provider.</li>
                                    <li>Revenues are only made available for withdrawal from the Revenue page following a safety clearance period of 14 days after the order is marked as complete.</li>
                                    <li>For security concerns, Moderator Live may temporarily disable a Seller’s ability to withdraw revenue to prevent fraudulent or illicit activity. This may come as a result of security issues, improper behavior reported by other users, or associating multiple Moderator Live accounts to a single withdrawal provider.</li>
                                    <li>Withdrawals can only be made in the amount available to you.</li>
                                    <li>Withdrawal fees vary depending on the withdrawal method.</li>
                                </ul>

                                <h4>11. Limited License and Site Access; Acceptable Use</h4>
                            <p>You may not: (a) resell or make any commercial use of this Site or any of the contents of this
Site; (b) modify, adapt, translate, reverse engineer, decompile, disassemble or convert any of
the contents of this Site not intended to be so read; (c) copy, imitate, mirror, reproduce,
distribute, publish, download, display, perform, post or transmit any of the contents of this Site in
any form or by any means; or (d) use any data mining, bots, spiders, automated tools or similar
data gathering and extraction methods on the contents of the Site or to collect any information
from the Site or any other user of the Site.</p>
<p>You use this Site at your own risk. You agree that you will be personally responsible for your use
of this Site and all of your communication and activity on this Site. If we determine, in our sole
discretion, that you engaged in prohibited activities, were not respectful of other users, or
otherwise violated the Terms and Conditions, we may deny you access to this Site on a
temporary or permanent basis and any decision to do so is final.</p>

<h4>12. Accounts, Registrations, and Passwords</h4>
<p>You agree and understand that you are responsible for maintaining the confidentiality of your
login ID and any passwords associated with any account you use to access the
Services. Accordingly, you agree that you will be solely responsible for all activities that occur
under your account. You must receive permission from the owner of the account to log in to the
Services using another person’s account. You can change your password at any time.</p>
<p>If you become aware of any unauthorized use of your password or of your account, you agree to
change your password to prevent further unauthorized access, and notify us immediately.</p>

<h4>THE UNAUTHORIZED USE OF YOUR ACCOUNT COULD CAUSE YOU TO INCUR
LIABILITY TO US AND/OR THIRD PARTIES. NEITHER US NOR ANY OF OUR OWNERS,
EMPLOYEES, AGENTS OR AFFILIATES WILL HAVE ANY LIABILITY TO YOU FOR ANY
UNAUTHORIZED TRANSACTION MADE USING YOUR PASSWORD THAT OCCURS
BEFORE YOU HAVE NOTIFIED US OF THE UNAUTHORIZED USE OF YOUR PASSWORD
AND WE HAVE HAD A REASONABLE OPPORTUNITY TO ACT ON THAT NOTICE.</h4>

<p>We may, without notice, suspend or cancel your account at any time and for any reason
including without limitation, if we believe, in our sole discretion, that your password is being
used without your authorization or otherwise in a fraudulent manner.</p>

<h4>13. Intellectual Property Rights</h4>
<p>Your use of the Site and its contents grants no rights to you concerning any copyright, designs,
and trademarks and all other intellectual property and material rights mentioned, displayed, or
relating to the Content (defined below) on the Site. All Content, including third party trademarks,
designs, and related intellectual property rights mentioned or displayed on this Site, are
protected by national intellectual property and other laws. Any unauthorized reproduction,
redistribution or other use of the Content is prohibited and may result in civil and criminal
penalties. You may use the Content only with our prior written and express authorization. To
inquire about obtaining authorization to use the Content, please contact us at
<a href="javascript:void(0);">Support@beelyve.com</a></p>

<p>In addition to the intellectual property rights mentioned above, "Content" is defined as any
graphics, photographs, including all image rights, sounds, music, video, audio, or text on the
Site.</p>

<h4>14. Disputes</h4>
<p>We encourage our Buyers and Sellers to try and settle conflicts amongst themselves. If for any
reason this fails after using the Resolution Center or if you encounter non-permitted usage on
the Site, users can contact Moderator Live's Customer Support.</p>
<p>In the event a Buyer initiates a dispute against the Seller, the Buyer has a 10 days timeframe to
resolve it upon submitting legitimate reasoning on valid grounds. Failure to do so without
reasoning and lack of information may result in non-eligibility for a refund.</p>

<h4>15. Monitoring Activity</h4>

<p>Moderator Live has no obligation to monitor this Site or any portion thereof. However, we
reserve the right to review any posted content and remove, delete, redact or otherwise modify
such content, in our sole discretion, at any time and from time to time, without notice or further
obligation to you. Moderator Live has no obligation to display or post any content. Moderator
Live, subject to the Privacy Policy reserves the right to disclose, at any time and from time to
time, any information or posted the content that it deems necessary or appropriate, including
without limitation to satisfy any applicable, law, regulation, contractual obligation, legal, dispute
process, or governmental request. </p>

<h4>16. Digital Millennium Copyright Act</h4>

<p>Moderator Live expects all users to respect the intellectual property rights of others. Moderator
Live may remove material that appears in its sole discretion to infringe upon the intellectual
property rights of others and we will terminate the access rights of any repeat infringer. If you
are a copyright owner or an agent thereof and believe that any Content infringes upon your
copyrights, you may notify us at Support@Moderatorlive.com. The notification must include the
following information: physical or electronic signature of the owner or authorized agent of the
owner of the allegedly infringed work; identification of the allegedly infringed work; identification
of the material that is claimed to be infringing and reasonably sufficient information for
Moderator Live to locate the material; contact information of the notifying party, such as an
address, telephone number, and email; a statement that the notifying party has a good faith
belief that the use of the material in the manner complained of is not authorized by the owner of
the allegedly infringed work, its agent or the law; and a statement, under penalty of perjury that
the information in the notification is accurate and the notifying party is the owner or authorized
agent of the allegedly infringed work.</p>

<h4>17. Disclaimer</h4>
<p>TO THE FULLEST EXTENT PERMISSIBLE UNDER APPLICABLE LAW, MODERATOR LIVE
EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS,
EXPRESS OR IMPLIED, INCLUDING ANY (A) WARRANTIES OF MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE OR USE AS TO THE SITE AND ITS CONTENT,
INCLUDING THE INFORMATION, DATA, SOFTWARE, OR PRODUCTS CONTAINED
THEREIN, OR THE RESULTS OBTAINED BY THEIR USE OR AS TO THE PERFORMANCE
THEREOF, (B) WARRANTIES OR CONDITIONS ARISING THROUGH COURSE OF
DEALING, AND (C) WARRANTIES OR CONDITIONS OF UNINTERRUPTED OR ERROR FREE
ACCESS OR USE. THE SITE AND ALL CONTENTS THEREIN AND COMPONENTS
THEREOF ARE PROVIDED ON AN “AS IS” BASIS AND YOUR USE OF THE SITE IS AT
YOUR OWN RISK.</p>

<h4>18. Limitation of Liability</h4>
<p>You agree that in no event shall Moderator Live be liable to you, or any third party, for any lost
profits, incidental, consequential, punitive, special, or indirect damages arising out of or in
connection with the Site or the Terms and Conditions, even if advised as to the possibility of
such damages, regardless of whether the claim for such damages is based in contract, tort,
strict liability or otherwise.</p>
<p>This limitation on liability includes, but is not limited to, any (i) errors, mistakes, or inaccuracies
in any Content or for any loss or damage of any kind incurred by you as a result of your use of
or reliance on the Content; (ii) the transmission of any bugs, viruses, Trojan horses or the like
which may infect your equipment, failure of mechanical or electronic equipment; (iii)
unauthorized access to or use of the Site or Moderator Live' secure servers and/or any personal
information and/or financial information stored therein; or (iv) theft, operator errors, strikes or
other labor problems or any force majeure.</p>

<h4>19. Indemnification</h4>
<p>You agree to indemnify and hold Moderator Live and its subsidiaries, affiliates, officers,
directors, agents, and employees, harmless from and against any suit, action, claim, demand,
penalty or loss, including reasonable attorneys’ fees, made by or resulting from any third party
due to or arising out of your use of the Site, breach of the Terms and Conditions or the materials it incorporates by reference, or your violation of any law, regulation, booking or other legal
mandates, or the rights of a third party.</p>



<h4>20. Dispute Resolution & Governing Laws</h4>
<p>In the event of a dispute arising out of or in connection with these terms or any contract between
you and us, then you agree to attempt to settle the dispute by engaging in good faith with us in a
process of mediation before commencing arbitration or litigation.</p>
<p>These Terms and Conditions shall be governed by and construed in accordance with the laws of
the United States and you hereby submit to the exclusive jurisdiction of the US courts in the
State where Moderator Live is based in.</p>

<h4>21. Children</h4>
<p>If you use or engage with the website and are under 18 years of age, you must have your
parent’s or legal guardian’s permission to do so. By using or engaging with the website, you
also acknowledge and agree that you are permitted by your jurisdiction’s applicable law to use
and/or engage with the website.</p>

<h4>22. Privacy & Cookies</h4>
<p>For more information on how we collect your information and cookies, please refer to our
Privacy Policy and Cookie Policy.</p>

<h4>23. Changes</h4>
<P>We reserve the right to update and revise these Terms and Conditions at any time. You will
know if these Terms and Conditions have been revised since your last visit to the website by
referring to the "Effective Date of Current Policy" date at the top of this page. Your use of our
Site constitutes your acceptance of these Terms and Conditions as amended or revised by us
from time to time, and you should, therefore, review these Terms and Conditions regularly. </P>

<h4>24. Electronic Communications</h4>
<P>When you visit the Site or send us e-mails, you are communicating with us electronically. In so
doing, you consent to receive communications from us electronically. You agree that all
agreements, notices, disclosures, and other communications that we provide to you
electronically satisfy any legal requirement that such communication is in writing.</P>


<h4>25. Severability</h4>
<p>If any of these Terms and Conditions shall be deemed invalid, void, or for any reason
unenforceable, that term shall be deemed severable and shall not affect the validity and
enforceability of any remaining terms or conditions.</p>

<h4>26. Assignment </h4>
<p>We shall be permitted to assign, transfer, or subcontract our rights and obligations under these
terms without your consent or any notice to you. You shall not be permitted to assign, transfer,
or subcontract any of your rights and obligations under this agreement.</p>

<h4>27. Force Majeure</h4>
<p>Moderator Live is not liable for any delays caused by circumstances beyond Moderator Live’s
control, e.g. general labor dispute, extreme weather, acts of war, fire, lightning, terrorist attacks,
changed governmental bookings, technical problems, defects in power- /tele-/computer
communications or other communication and defects or delays in the service by sub-suppliers
due to circumstances set forth above.</p>


<h4>28. Entire Agreement</h4>
<p>These Terms and Conditions set forth the entire understanding and agreement between you
and Moderator Live concerning the subject matter herein and supersede all prior or
contemporaneous communications and proposals, whether electronic, oral or written concerning
the Site. A printed version of these Terms and Conditions and any notice given in electronic form
shall be admissible in judicial or administrative proceedings based upon or relating to these
Terms and Conditions to the same extent and subject to the same conditions as other business
documents and records originally generated and maintained in printed form. Any rights not
expressly granted herein are reserved. You may not assign the Terms and Conditions, or
assign, transfer or sublicense your rights therein. A failure to act concerning a breach by you or
others does not waive Moderator Live's right to act concerning subsequent or similar breaches. </p>

<h4>others does not waive Moderator Live's right to act concerning subsequent or similar breaches.
29. Term and Termination</h4>
<p>This agreement becomes effective the date that you first access the Site and remains effective
until it is terminated consistent with its terms. Violations of this agreement may result in the
immediate termination of this agreement and denials or terminations of your access to the Site.
Such restrictions may be temporary or permanent. Upon termination, your right to use this Site
shall be revoked. All disclaimers, limitations of liability, indemnities, and rights of ownership and
licenses to Moderator Live shall survive any termination.</p>

<h4>30. Relationship of the Parties</h4>

<p>The relationship of the parties is that of an independent contractor. This Agreement does not
create a joint venture or partnership between sellers or users and Moderator Live, and each will
act independently of the other. Neither you nor Moderator Live is empowered to bind or commit
the other to any other contract or other obligation. You and Moderator Live agree that there are
no third party beneficiaries to this Agreement.</p>

<h4>31. Contact Us</h4>
<p>For any questions, complaints, and queries or to report any violations, kindly send an email on
<a href="javascript:void(0);">support@beelyve.com</a></p>

 </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
{{--    <script type="text/javascript" src="https://js/jquery.mCustomScrollbar.concat.min.js"></script>--}}
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
@endsection



