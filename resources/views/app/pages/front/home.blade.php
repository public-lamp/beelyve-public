@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Home')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/owl.carousel.min.css') }}" />
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')
<main>
    <div class="main_container">

        {{-- home page main banner-slider --}}
        @include('app.sections.front.home-banner')

        {{-- Landing page main sections --}}
        <div class="main_container_inner">
            <div class="auto_container">
                <div class="main_container_innerDetail">

                    <!-- featured profile section -->
                    <div class="features_this_week">
                        <h3>Featured this week</h3>
                        <div class="features_this_week_list">
                            <div class="owl-carousel featureWeek_slider owl-theme">
                                @if(count($featuredTalents) > 0)
                                    @foreach($featuredTalents as $featuredTalent)
                                    <div class="item">
                                        <div class="featureWeek_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ isset($featuredTalent['image']) ? $featuredTalent['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="img" />
                                                    <!-- <b class="sattus_online"></b> -->
                                                </span>
                                                </div>
                                                <div class="featureProfile_avatar_text">
                                                    <strong>{{ $featuredTalent['full_name'] }}</strong>
                                                    <small>{{ $featuredTalent['talentProfessionalInfo']['talentCategory']['name'] }}</small>
                                                </div>
                                            </div>
                                            <div class="profile_rating">
                                                <div class="profile_rating_info">
                                                    <ul>
                                                        @if( $featuredTalent['rating'] > 0.0 && $featuredTalent['rating'] < 0.5 )
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        @elseif( $featuredTalent['rating'] >= 0.5 && $featuredTalent['rating'] < 1.5 )
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        @elseif( $featuredTalent['rating'] >= 1.5 && $featuredTalent['rating'] < 2.5 )
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        @elseif( $featuredTalent['rating'] >= 2.5 && $featuredTalent['rating'] < 3.5 )
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        @elseif( $featuredTalent['rating'] >= 3.5 && $featuredTalent['rating'] < 4.5 )
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        @elseif( $featuredTalent['rating'] >= 4.5 )
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                        @endif
                                                    </ul>
                                                    <strong>({{ count($featuredTalent['reviews']) }})</strong>
                                                </div>
                                                    <a href="{{ route( 'talent.public-profile', ['talent_id' => encryptor('encrypt', $featuredTalent['id'])] ) }}" class="hour_price">Book Now</a>
                                            </div>
                                            <div class="priceinfo_starting">
                                                <strong>Starting price<b>${{ $featuredTalent['talentProfessionalInfo']['start_rate'] }}</b></strong>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="text-center"> No Featured Profile Found</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- services categories section -->
                    <div class="features_this_week">
                        {{-- <h3>{{ \App\Models\Talent\TalentCategory::all()->count() }}+ different services categories</h3>--}}
                        <h3>Categories</h3>
                        <div class="features_this_week_list">
                            <div class="owl-carousel cattegories owl-theme">
                                @foreach(\App\Models\Talent\TalentCategory::all() as $serviceCategory)
                                    <div class="item">
                                        <div class="cattegory_item">
                                            <span>
                                                <img src="{{ $serviceCategory['image'] }}" alt="#" />
                                            </span>
                                            <strong>{{ $serviceCategory['name'] }}</strong>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <!-- search talent with few talent profiles section -->
                    <div class="available_feature_section onHomePge">
                        <h3>Find the best talent</h3>
                        <div class="available_feature_sorted">
                            <div class="sorted_nav tanelt_sort">
                                @include('app.sections.front.find-talent-search-section')
                                <div class="icon_list_grid">
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-th iconGrid" aria-hidden="true"></i>
                                        <i class="fa fa-th-list iconList" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        {{-- profiles list --}}
                        <div class="available_feature_list">
                            <ul>
                                @foreach($homePageTalents as $talent)
                                    <li>
                                        <div class="featureWeek_info">
                                            <div class="feartureProflie_info"><a href="{{ route( 'talent.public-profile', ['talent_id' => encryptor('encrypt', $talent['id'])] ) }}" class="goto_profile"></a>
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{ isset($talent['image']) ? $talent['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="profile image">
                                                        {{-- <small class="online"><i></i>online</small> --}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="featureProfile_avatar_inner">
                                                <div class="featureProfile_avatar_text">
                                                    <strong>{{ $talent['user_name'] }}</strong>
                                                    <small>{{ $talent['talentProfessionalInfo']['talentCategory']['name'] }}</small>
                                                </div>

                                                <div class="profile_rating">
                                                    <div class="profile_rating_info">
                                                        <ul>
                                                            @if( $talent['rating'] > 0.0 && $talent['rating'] < 0.5 )
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $talent['rating'] >= 0.5 && $talent['rating'] < 1.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $talent['rating'] >= 1.5 && $talent['rating'] < 2.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $talent['rating'] >= 2.5 && $talent['rating'] < 3.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $talent['rating'] >= 3.5 && $talent['rating'] < 4.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $talent['rating'] >= 4.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @endif
                                                        </ul>
                                                        <strong>({{ count($talent['reviews']) }})</strong>
                                                    </div>
                                                </div>

                                                <div class="profileDiscription">
                                                    <p>{{ $talent['about'] }}</p>
                                                </div>

                                                <div class="favrt_price">
                                                    @if( auth()->check() )
                                                        <div class="favrt_icon">
                                                            @if( isset($user) && $user['favoriteTalents']->isNotEmpty() && in_array($talent['id'], $user->favoriteTalents()->pluck('favorable_id')->toArray()) )
                                                                <a href="{{ route('favourite.user.remove', ['favorable_id' => $talent['id']]) }}">
                                                                    <span><i class="fa fa-heart red_fvrt" aria-hidden="true"></i></span>
                                                                </a>
                                                            @else
                                                                <a href="{{ route('favourite.user.add', ['favorable_id' => $talent['id']]) }}">
                                                                    <span>
                                                                        <i class="fa fa-heart-o un_fvrt" aria-hidden="true"></i>
                                                                        <i class="fa fa-heart fvrt" aria-hidden="true"></i>
                                                                    </span>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    @endif

                                                    <div class="priceinfo_starting">
                                                        <strong>Starting at<b>${{ $talent['talentProfessionalInfo']['start_rate'] }}</b></strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @if( count($homePageTalents) > 6 )
                            <div class="viewMore">
                                <a href="{{ route('search-talent') }}">View More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                            </div>
                        @endif
                    </div>

                    <!-- how we work section -->
                    <div class="how_work_section">
                        <div class="how_work_section_detail">
                            <div class="how_work_info">
                                <div class="how_work_tittle">
                                    <h3>How it works</h3>
                                    <label>Easily browse our growing catalogue of Talent using our filters so that you're guaranteed to find the perfect fit for your Youtube Channel, Podcast, Show, Instagram Live or any other virtual event.</label>
                                </div>
                                <div class="how_work_steps">
                                    <ul>
                                        <li>
                                            <div class="how_work_steps_info">
                                                <b class="toggleAerow"></b>
                                                <div class="step_count">
                                                    <span class="step1">1</span>
                                                </div>
                                                <div class="step_count_info">
                                                    <strong>Complete Profile</strong>
                                                    <p>Provide as much information as possible to describe who you are, and what you do.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="how_work_steps_info">
                                                <b class="toggleAerow"></b>
                                                <div class="step_count">
                                                    <span class="step2">2</span>
                                                </div>
                                                <div class="step_count_info">
                                                    <strong>Search Talent</strong>
                                                    <p>Find the best talent for your needs & budget.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="how_work_steps_info">
                                                <b class="toggleAerow"></b>
                                                <div class="step_count">
                                                    <span class="step3">3</span>
                                                </div>
                                                <div class="step_count_info">
                                                    <strong>Book Now</strong>
                                                    <p>Book immediately with peace in mind, knowing your payment is held securely by us until you tell us that the Talent has arrived at your digital event.</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="d_agency_video">
                                <!-- partial:index.partial.html -->
                                <div class="blackOut">
                                    <div class="popupAlignCenter">
                                        <!-- popup content populates here -->
                                    </div>
                                </div>
                                <div class="videoPopup">
                                    <a href="javascript:void(0)" onclick="videoPopupItem.launchPopUp();">
                                        <img src="{{ asset('project-assets/images/work_video_banner.webp') }}" alt="video popup" />
                                        <i class="fa fa-play playBttn"  onclick="videoPopupItem.launchPopUp();" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- why choose us section -->
                    <div class="whyChoose_section">
                        <div class="custom_tittle_section">
                            <h2 class="p-0">A World of Talent At Your Fingertips</h2>
                            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>--}}
                        </div>
                        <div class="whyChoose_columns">
                            <ul>
                                <li>
                                    <div class="whyChoose_columns_info">
                                        <span><img src="{{ asset('project-assets/images/trusted_icon.png') }}" alt="img" /></span>
                                        <h4>The best for every budget</h4>
                                        <p>Find high quality services at every price point. No hourly rates, just event based pricing</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="whyChoose_columns_info">
                                        <span><img src="{{ asset('project-assets/images/easy_use_icon.png') }}" alt="img" /></span>
                                        <h4>Quality Talent booked quickly</h4>
                                        {{-- <p>Find the right talent to join your live digital event within minutes</p>--}}
                                        <p>Find the perfect talent to join your virtual event within minutes. No more searching through multiple social media platforms to find Influencers that may or may not have contact information on their profile.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="whyChoose_columns_info">
                                        <span><img src="{{ asset('project-assets/images/faster_icon.png') }}" alt="img" /></span>
                                        <h4>Protected payments, every time</h4>
                                        <p>Always know what you’ll pay upfront. You'll get to verify the talent is present at your event and your payment isn't released until the time you booked is completed.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="whyChoose_columns_info">
                                        <span><img src="{{ asset('project-assets/images/faster_icon_4.png') }}" alt="img" /></span>
                                        <h4>Any digital live platform </h4>
                                        <p>Talent can provide services through any live audio and/or video platform. Whether that's zoom, clubhouse, instagram or facebook live, etc. the choice is yours.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- @include('app.sections.front.trusted-by-section')--}}

    </div>
</main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    <!-- weekly-update section -->
    @include('app.sections.footers.weekly-update-section')

    {{-- main footer --}}
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
@endsection



