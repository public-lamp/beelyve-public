{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'About')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/owl.carousel.min.css') }}" />
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section   sett_home_banner aboutTxt">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img"><img src="{{ asset('project-assets/images/about_banner.jpg') }}" alt="About_banner" /></span>

                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <label>beelyve for business</label>
                                <h1>Beelyve is where you connect with your favorite stars</h1>
                                <p>Unlock access to stars and convert customers by leveraging the power of Beelyve</p>
                                <a href="{{ route('search-talent') }}" class="default_bttn">Browse Talent</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">

                        <div class="aboutUs_detail d-none">
                            <div class="aboutUs_info">
                                <div class="custom_tittle_section">
                                    <h3>About Company</h3>
                                    <b></b>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,</p>
                                </div>

                                <div class="about_info_steps">
                                    <ul>
                                        <li>
                                            <div class="about_info_steps_detail">
                                                <div class="stepText">
                                                    <span>01</span>
                                                </div>
                                                <div class="stepText_detail">
                                                    <strong>Loram Ipsum</strong>
                                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="about_info_steps_detail">
                                                <div class="stepText">
                                                    <span>02</span>
                                                </div>
                                                <div class="stepText_detail">
                                                    <strong>Loram Ipsum</strong>
                                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="about_info_steps_detail">
                                                <div class="stepText">
                                                    <span>03</span>
                                                </div>
                                                <div class="stepText_detail">
                                                    <strong>Loram Ipsum</strong>
                                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="aboutAvatar">
                                <span><img src="{{ asset('project-assets/images/aboutDetail_avatar.png') }}" alt="#" /></span>
                            </div>
                        </div>


                        <!-- how we work section -->
                    <div class="how_work_section p-0">
                        <div class="how_work_section_detail">
                            <div class="how_work_info">
                                <div class="how_work_tittle">
                                    <h3>How it works</h3>
                                    <label>Start finding a talent for your event easily for you</label>
                                </div>
                                <div class="how_work_steps">
                                    <ul>
                                        <li>
                                            <div class="how_work_steps_info">
                                                <b class="toggleAerow"></b>
                                                <div class="step_count">
                                                    <span class="step1">1</span>
                                                </div>
                                                <div class="step_count_info">
                                                    <strong>Find the right Talent for any occasion</strong>
                                                    <p>Meet & greets, play with your favorite gamer, collaborate with talent in front of your own audience, get live coaching, or book someone for a performance.
                                                         There’s no limit on the types of services provided by Talent as long as it’s provided on a live virtual platform.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="how_work_steps_info">
                                                <b class="toggleAerow"></b>
                                                <div class="step_count">
                                                    <span class="step2">2</span>
                                                </div>
                                                <div class="step_count_info">
                                                    <strong>Book an available time slot </strong>
                                                    <p>Include all the important details about your event request. After it’s
                                                        submitted the Talent will accept and be prepared to go live on the platform chosen by the Talent.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="how_work_steps_info">
                                                <b class="toggleAerow"></b>
                                                <div class="step_count">
                                                    <span class="step3">3</span>
                                                </div>
                                                <div class="step_count_info">
                                                    <strong>Enjoy the moment</strong>
                                                    <p>Access to Stars has never been this easy, so be sure to enjoy it and capture the moment with a recording. (If allowed/provided by the Talent)</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="d_agency_video">
                                <!-- partial:index.partial.html -->
                                <div class="blackOut">
                                    <div class="popupAlignCenter">
                                        <!-- popup content populates here -->
                                    </div>
                                </div>
                                <div class="videoPopup">
                                    <a href="javascript:void(0)" onclick="videoPopupItem.launchPopUp();">
                                        <img src="{{ asset('project-assets/images/work_video_banner.webp') }}" alt="video popup" />
                                        <i class="fa fa-play playBttn"  onclick="videoPopupItem.launchPopUp();" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="custom_submit browseTalent">
                        <a href="{{ route('search-talent') }}">Browse Talent</a>
                    </div>

                    </div>
                </div>
            </div>

            <div class="about_whyChoose_columns">
                <div class="auto_container">
                    <div class="whyChoose_columns">
                        <ul>
                            <li>
                                <div class="whyChoose_columns_info">
                                    <span><img src="{{ asset('project-assets/images/trusted_icon.png') }}" alt="#"></span>
                                    <h4>Trusted</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </li>
                            <li>
                                <div class="whyChoose_columns_info">
                                    <span><img src="{{ asset('project-assets/images/easy_use_icon.png') }}" alt="#"></span>
                                    <h4>Easy to Use</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </li>
                            <li>
                                <div class="whyChoose_columns_info">
                                    <span><img src="{{ asset('project-assets/images/faster_icon.png') }}" alt="#"></span>
                                    <h4>Faster</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
     <!-- weekly-update section -->
     @include('app.sections.footers.weekly-update-section')
    {{-- footer --}}
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });

    </script>
@endsection

@section('ajax')
@endsection



