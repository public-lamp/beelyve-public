{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Faq')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section aboutUs_banner tabBanner d-none">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img">
                        <img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="#" />
                    </span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Frequently Asked Questions (FAQ)</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">
                        <div class="faq_detail">
                            <h2 class="pb-4 text-center text-uppercase">Faqs</h2>
                            <div class="faq_detail_list">
                                <ul>
                                    <li>
                                        <div class="faq_info">
                                            <h5>How do I know if my favorite celebrity joined Beelyve?</h5>
                                            <div class="faq_info_show">
                                                <p>If you subscribe to our newsletter, we’ll shoot you an email letting you know when your favorite celebrities have joined.</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>I don’t live in the US, can I still book a celebrity from Beelyve?</h5>
                                            <div class="faq_info_show">
                                                <p>You sure can</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>What percentage do I earn from each completed booking?</h5>
                                            <div class="faq_info_show">
                                                <p>For bookings completed through <a href="#">www.beelyve.com</a>, you’ll receive 80% of the booking price. The remaining 20% and any service, transaction, or order processing fee goes to Beelyve (see our Terms of Service).</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>When do I get paid?</h5>
                                            <div class="faq_info_show">
                                                <p>Once your banking is set up, you can transfer your earnings from your wallet. To transfer your earnings, select the amount you’d like to receive and select “transfer funds”. You will receive your funds within 7 days after initiating transfer. *Most transfer come in sooner than 7 days*</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>How do I know if the talent I’m booking is the real person?</h5>
                                            <div class="faq_info_show">
                                                <p> Always check the “verify by social” links on the talent profile. Social links will not be shown on a profile unless the talent has signed into the social media accounts displayed on the profile. This is our way of identifying someone is who they say they are.</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>Can you help me choose the perfect talent for my event?</h5>
                                            <div class="faq_info_show">
                                                <p>That depends on what you’re looking for. If you are looking to book a specific talent, you can search for them on <a href="#">www.beelyve.com</a>. Otherwise you can browse our website and use our filters to find the perfect talent for your event.</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>How do I sign up to become a talent?</h5>
                                            <div class="faq_info_show">
                                                <p>You can sign up as a talent on <a href="#">www.beelyve.com</a> by signing up for an account and select the talent circle instead of client. However your profile and service will not be visible on our site if you have not verified who you are by signing into one of your other social media accounts using our “social connectivity” section in the talent profile section.</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>How will I go live with my talent?</h5>
                                            <div class="faq_info_show">
                                                <p>You will go live on whichever platform the talent provides their service through, which is visible on the ordering screen when you book. During the booking process, you will provide the link to your event so that the talent can easily join when it is time. </p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="faq_info">
                                            <h5>What happens if my talent does not show up?</h5>
                                            <div class="faq_info_show">
                                                <p>Events are managed through our event management module. Your event time will not begin until both you and the talent have confirmed that you’re both present and inside of the live event. If your talent does not show up, your event will never begin and you will be refunded. See our terms of service page.</p>
                                            </div>
                                            <b class="faq_btn"><img src="{{ asset('project-assets/images/chevron_down_blue.png') }}" alt="#" /></b>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
{{--    <script type="text/javascript" src="https://js/jquery.mCustomScrollbar.concat.min.js"></script>--}}
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });

    </script>

@endsection

@section('ajax')
@endsection



