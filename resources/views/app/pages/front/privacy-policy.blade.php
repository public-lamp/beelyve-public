{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Privacy Policy')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section aboutUs_banner tabBanner">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img">
                        <img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="#" />
                    </span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Privacy Policy<p><a href="{{ route('home') }}">www.{{ strtolower(config('app.name')) }}.com</a></p></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">
                        <div class="terms_condition_detail">




<h4>Effective date: <b>1st September 2021</b></h4>
                            <p>Welcome to www.Moderatorlive.com (the “Site”) which is owned by Moderator Live Inc. (“we”,
“us” and “our”) and operated from the United States. This privacy policy has been compiled to
better serve those who are concerned with how their ‘Personally Identifiable Information’ (PII) is
being used online. PII, as described in US privacy law and information security, is information
that can be used on its own or with other information to identify, contact, or locate a single
person, or to identify an individual in context. Please read our privacy policy carefully to get a
clear understanding of how we collect, use, protect, or otherwise handle your PII in accordance
with our website.</p>
<h4>Collection of Personal and Non-Personal Information (PII and Non-PII)</h4>

                            <p>We collect and store certain personally identifiable information about you (i.e., information which
may identify you in some way; such as your name, billing address, telephone number, email
address, screen name and/or financial information) (“PII”) only when you voluntarily submit it.
We may request that you submit PII in certain instances, such as when you purchase or sell
services on our platform, submit comments or questions, request information, participate in a
promotion, contest or sweepstakes or utilize other features or functions of a Site.</p>
                            <p>When you connect to our Site, there is certain technical, non-personally identifiable information
which does not identify any individual (“Non-PII”) that may be collected and stored through the
use of “cookie” technology and IP addresses. PII is not extracted in this process. Non-PII may
include, but is not limited to, your IP host address, internet browser type, personal computer
settings, and the number, duration, and character of page visits. This information assists us to
update a Site so that it remains interesting to our visitors and contains content that is tailored to
visitors’ interests.</p>

<p><b>Use of Your PII.</b> When you provide your PII at a Site, we will limit the use of the PII for the
purpose for which it was collected in accordance with the terms of this Privacy Policy. Other
limited uses of your PII may include:</p>





                            <ul class="list_info">
                                <li>To respond to your questions, comments, and requests;</li>
                                <li>To ensure marketplace integrity, prevent fraud and maintain a safe and secure
marketplace. For example, we use your information to track and prevent fraudulent
activities and other inappropriate activities, monitor content integrity, conduct security
investigations and risk assessments, verify or authenticate information provided by you,
enforce our Terms of Service and comply with applicable laws. We conduct certain
behavioral analytics to achieve the above objectives and in limited cases, if we detect
activity that we think poses a risk to the Moderator Live marketplace, other users, our
community, or third parties, automated processes may restrict or limit your ability to use
Moderator Live.</li>
                                <li>To provide you with access to certain areas and features on a Site; and to communicate
with you about your activities on a Site.</li>

                                <li>To investigate suspected fraud, harassment, physical threats, or other violations of any
law, rule or regulation, the rules or policies of a Site, or the rights of third parties; or to
investigate any suspected conduct which we deem improper.</li>
                                <li>To the extent that you provide us with financial information in connection with shopping
or commercial services offered on a Site, we may use the financial information that you
provide to fulfill the services.</li>
                                <li>To help us develop, deliver and improve the services, content and advertising.</li>
                                <li>To share with our parent, subsidiary, and affiliated companies, and promotional partners
involved in creating, producing, delivering, or maintaining a Site, as required to perform
functions on our behalf in connection with the Site (such as administration of the Site,
administration of promotions or other features on the Site, marketing, data analysis, and
customer services). In order to share such information, it may be necessary for us to
transmit your PII outside the jurisdiction set forth below and you agree to this transfer.
Further use or disclosure of PII by these parties for other purposes is not permitted.</li>
                                <li>To share with third party service providers whom we employ to perform functions on our
behalf in connection with the Site (such functions may include, but are not limited to,
sending postal mail and e-mail, removing repetitive information from customer lists,
analyzing data and providing marketing assistance, and providing customer service).
These third party service providers have access to PII that is needed to perform their
functions, and may collect or store PII as part of their performance of these functions,
but are not authorized to use it for other purposes.</li>
                                <li>For our internal purposes such as auditing, data analysis, and research to improve our
services and customer communications.</li>
                                <li>For other purposes as disclosed when your PII is collected or in any additional terms and
conditions applicable to a particular feature of a Site.</li>
                                <li>For disclosures required by law, regulation, or court order.</li>
                                <li>For the purpose of or in connection with legal proceedings or necessary for establishing,
defending, or exercising legal rights.</li>
                                <li>In an emergency to protect the health or safety of users of a Site or the general public, or
in the interests of national security.</li>
                            </ul>


                            <p>Except as provided for herein, we will not provide any of your PII to any third parties without
your specific consent.</p>
                            <p><b>Managing Your Personal Information.</b> You have the ultimate control over the PII that we
collect and use. You can always chose not to provide certain PII, but please keep in mind that
you may not be able to use some of the features offered by a Site unless you provide us with
your PII. If you wish to verify, update, or correct any of your PII collected through a Site, contact
us on the details provided in the contact section of this policy.</p>

    <h4>We may also use your PII for the following purposes:</h4>
                            <p><b>Direct Marketing.</b> We may use your personal information to send you Moderator Live-related
marketing communications as permitted by law. You will have the ability to opt-out of our
marketing and promotional communications.</p>
                            <p><b>For research and development.</b> We may use your personal information for research and
development purposes, including to analyze and improve the Service and our business.</p>
                            <p><b>To create anonymous data.</b> We may create aggregated, de-identified or other anonymous
data records from your personal information and other individuals whose personal information we collect. We make personal information into anonymous data by excluding information (such
as your name) that makes the data personally identifiable to you. We may use this anonymous
data and share it with third parties for our lawful business purposes, including to analyze and
improve the Service and promote our business.</p>
                            <p><b>To comply with laws and regulations.</b> We use your personal information as we believe
necessary or appropriate to comply with applicable laws, lawful requests, and legal process,
such as to respond to subpoenas or requests from government authorities.</p>
                            <p><b>For compliance, fraud prevention and safety.</b> We may use your personal information and
disclose it to law enforcement, government authorities, and private parties as we believe
necessary or appropriate to: (a) protect our, your or others’ rights, privacy, safety or property
(including by making and defending legal claims); (b) audit our internal processes for
compliance with legal and contractual requirements; (c) enforce the terms and conditions that
govern the Service; and (d) protect, investigate and deter against fraudulent, harmful,
unauthorized, unethical or illegal activity, including cyber-attacks and identity theft.</p>
                            <p><b>With your consent.</b> In some cases we may specifically ask for your consent to collect, use or
share your personal information, such as when required by law.</p>
<h4>Cookies</h4>
                            <p>Please refer to our cookie policy to know more about how we handle cookies.</p>
                            <h4>Security</h4>
                            <p>We adopt appropriate data collection, storage and processing practices and security measures
to protect against unauthorized access, alteration, disclosure or destruction of your personal
information, username, password, transaction information and data stored on our Site. The
security on the internet is not 100% guaranteed, however we use best efforts to keep it secure
for the users. We do not accept liability for unintentional disclosure. We encourage you to use
caution when using the Internet.</p>
<h4>NOTICE TO CALIFORNIA RESIDENTS</h4>
<p>We are required by the California Consumer Privacy Act (“CCPA”) to provide to California
residents an explanation of how we collect, use and share their Personal Information, and of the
rights and choices, we offer to California residents concerning that Personal Information.</p>
<p><b>Personal information that we collect, use, and share.</b> We do not sell personal information.
As we explain in this Privacy Policy, we use cookies and other tracking tools to analyze website
traffic and facilitate advertising.</p>

<p><b>Your California privacy rights.</b> The CCPA grants California residents the following rights.
However, these rights are not absolute, and in certain cases we may decline your request as
permitted by law.</p>

                              <ul class="list_info">
                                <li><b>Information.</b> You can request information about how we have collected, used and
shared and used your Personal Information during the past 12 months.

                                <ul class="list_info">
                                    <li>The categories of Personal Information that we have collected.</li>
                                    <li>The categories of sources from which we collected Personal Information.</li>
                                    <li>The business or commercial purpose for collecting and/or selling Personal
Information.</li>
                                    <li>The categories of third parties with whom we share Personal Information.</li>
                                    <li>Whether we have disclosed your Personal Information for a business purpose,
and if so, the categories of Personal Information received by each category of
third party recipient.</li>
                                    <li>Whether we’ve sold your Personal Information, and if so, the categories of
Personal Information received by each category of third party recipient.</li>
                                </ul>
                                </li>

                                <li><b>Access.</b> You can request a copy of the Personal Information that we have collected
about you during the past 12 months.</li>
                                <li><b>Deletion.</b> You can ask us to delete the Personal Information that we have collected from
you.</li>
                            </ul>
                            <p>You are entitled to exercise the rights described above free from discrimination in the form of
legally prohibited increases in the price or decreases in the quality of our Service.</p>

                            <p><b>How to exercise your California rights.</b> You may exercise your California privacy rights
described above as follows:</p>
                            <ul class="list_info">
                                <li><b>Right to information, access and deletion.</b> You can request to exercise your
information, access and deletion rights, by emailing support@Moderatorlive.com. We
reserve the right to confirm your California residence to process your requests and will
need to confirm your identity to process your requests to exercise your information,
access or deletion rights. As part of this process, government identification may be
required. Consistent with California law, you may designate an authorized agent to
make a request on your behalf. In order to designate an authorized agent to make a
request on your behalf, you must provide a valid power of attorney, the requester’s valid
government-issued identification, and the authorized agent’s valid government issued
identification. We cannot process your request if you do not provide us with sufficient
detail to allow us to understand and respond to it.</li>
                                <li><b>Request a list of third party marketers.</b> California’s “Shine the Light” law (California
Civil Code § 1798.83) allows California residents to ask companies with whom they have
formed a business relationship primarily for personal, family or household purposes to
provide certain information about the companies’ sharing of certain personal information
with third parties for their direct marketing purposes during the preceding year (if any).
You can submit such a request by sending an email to support@Moderatorlive.com with
“Shine the Light” in the subject line. The request must include your current name, street
address, city, state, and zip code and attest to the fact that you are a California resident.</li>
</ul>

<p>We cannot process your request if you do not provide us with sufficient detail to allow us to
understand and respond to it. </p>
<h4>Sharing your personal information</h4>
<p>We do not sell, trade, or rent Users personal identification information to others in accordance
with the CCPA. We may share generic aggregated demographic information not linked to any
personal identification information regarding visitors and users with our business partners,
trusted affiliates and advertisers for the purposes outlined above.</p>

<h4>Children</h4>
<p>Our Site is not intended for children under 13 years of age and we do not knowingly collect
personal information from children under 13.</p>
<h4>Rights of EU Users</h4>

<p>Under applicable EU regulation, you have the following rights in respect of your personal
information:</p>


                            <ul class="list_info">
                                    <li>to obtain a copy of your personal information together with information about how and on
what basis that personal information is processed;</li>
                                    <li>to rectify inaccurate personal information;</li>
                                    <li>to erase your personal information in limited circumstances where (a) you believe that it
is no longer necessary for us to hold your personal information; (b) we are processing
your personal information on the basis of legitimate interests and you object to such
processing, and we cannot demonstrate an overriding legitimate ground for the
processing; (c) where you have provided your personal information to us with your
consent and you wish to withdraw your consent and there is no other ground under
which we can process your personal information; and (d) where you believe the personal
information we hold about you is being unlawfully processed by us;</li>
                                    <li>to restrict processing of your personal information where: (a) the accuracy of the
personal information is contested; (b) the processing is unlawful but you object to the
erasure of the personal information; (c) we no longer require the personal information for
the purposes for which it was collected, but it is required for the establishment, exercise
or defense of a legal claim or (d) you have objected to us processing your personal
information based on our legitimate interests and we are considering your objection;</li>
<li>to object to decisions which are based solely on automated processing or profiling;</li>
<li>where you have provided your personal information to us with your consent, to ask us for
a copy of this data in a structured, machine-readable format and to ask us to share (port)
this data to another data controller; or</li>
<li>to obtain a copy of or access to safeguards under which your personal information is
transferred outside of the EEA.</li>
                                </ul>

                                <p>In addition to the above, you have the right to lodge a complaint with a supervisory authority for
data protection.</p>
<p>We will ask you for additional data to confirm your identity and for security purposes, before
disclosing data requested by you. We reserve the right to charge a fee where permitted by law.
We will decline to process requests that jeopardize the privacy of others, are extremely
impractical, or would cause us to take any action that is not permissible under applicable laws.
Additionally, as permitted by applicable laws, we will retain where necessary certain personal
information for a limited period of time for record-keeping, accounting and fraud prevention
purposes.</p>

<h4>Third party websites</h4>

<p>Users may find advertising or other content on our Site that link to the sites and services of our
partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control
the content or links that appear on these sites and are not responsible for the practices
employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their
own privacy policies and customer service policies. Browsing and interaction on any other
website, including websites which have a link to our Site, is subject to that website's own terms
and policies.</p>
<h4>Data Retention</h4>
<p>We will retain your information for as long as needed to provide you the Site services, for our
valid business purposes, and as necessary to comply with our legal obligations, resolve
disputes, defend our legal rights, and enforce our agreements.</p>

<h4>Changes</h4>
<p>Moderator Live has the discretion to update this privacy policy at any time. When we do, we will
revise the updated date at the bottom of this page. We encourage Users to frequently check this
page for any changes to stay informed about how we are helping to protect the personal
information we collect. You acknowledge and agree that it is your responsibility to review this
privacy policy periodically and become aware of modifications.</p>
<h4>Acceptance</h4>
<p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy,
please do not use our Site. Your continued use of the Site following the posting of changes to
this policy will be deemed your acceptance of those changes.</p>
<h4>Contacting us</h4>
<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings
with this site, please contact us at:</p>
<h4>Moderator Live</h4>
<p>Email:<a href="mailto:support@moderatorlive.com">support@Moderatorlive.com</a></p>






                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
@endsection



