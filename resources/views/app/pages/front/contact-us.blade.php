{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Contact')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section aboutUs_banner tabBanner">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img">
                        <img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="#" />
                    </span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Contact Us</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="containerDetail">
                <div class="auto_container">
                    <div class="containerDetail_info">
                        <div class="contactUs_detail">
                            <div class="contactUs_detail_info">
                                <div class="custom_popup_tittle">
                                    <h2>Contact Us</h2>
                                    <p>We’re here to help and answer any question you might have. We look forward to hear from you</p>
                                </div>
                                <form id="contactUsForm">
                                    <div class="popup_form">
                                        <ul>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Your Email</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="" name="contact_email" id="contact_email" />
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Inquiry Title</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="" name="inquiry_title" id="inquiry_title" />
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Message</strong>
                                                    <div class="custom_field_textarea">
                                                        <textarea name="message" id="message"></textarea>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <br>
                                    <div class="custom_submit">
                                        <input type="submit" value="Submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script type="text/javascript">
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
    <script type="text/javascript">
        $('#contactUsForm').on('submit', function (e) {
            // set form input fields to normal
            $('.errorAlertText').html('');
            e.preventDefault();
            let contactFormData = $('#contactUsForm').serializeArray();
            blockUi( true );
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('contact.post') }}",
                data: contactFormData,
                success: function (response) {
                    if ( response.status ) {
                        blockUi( false );
                        toasterSuccess( true, response.message);
                        window.location.reload();
                    }
                    else if ( !response.status ) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                    else {
                        blockUi( false );
                        return toasterSuccess( false, 'Something went wrong, please try again');
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );

                        const contactFormKeys = ['email', 'inquiry_title', 'message'];
                        if (contactFormKeys.includes(key)) {
                            let element = document.getElementById(key);
                            let errorAlertHtml = validationText(value, 'error');
                            $(element).after(errorAlertHtml);
                        } else {
                            toasterSuccess( false, value);
                        }
                    });
                },
                statusCode: {
                    429: function() {
                        blockUi( false );
                        toasterSuccess( false, 'Too many attempts, Please try after a minute');
                    }
                }
            });
        });
    </script>
@endsection



