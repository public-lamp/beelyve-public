@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Details')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @if( auth()->user()->isModerator() )
        @include('app.headers.moderator-dashboard-header')
    @elseif( auth()->user()->isTalent() )
        @include('app.headers.talent-dashboard-header')
    @else
        @include('app.headers.simple-header')
    @endif
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <div class="event_pages db_messageChat">
                <div class="auto_container">
                    <div class="dasbord_container_detail">

                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h3>Booking Details</h3>
                            </div>
                        </div>

                        {{-- Details container section --}}
                        <div class="bookingDetail_section">
                            <div class="bookingDetail_section_fl">
                                <div class="bookingDetail_fl_info">
                                    <div class="booking_description">
                                        <strong><b>Title: </b>{{ $booking['title'] }}</strong>
                                        <h4>Booking Note (By Moderator)</h4>
                                        <p>{{ $booking['detail_note'] }}</p>
                                    </div>

                                    <div class="slots_info">
                                        <h4>Booking Slot Details</h4>
                                        <ul>
                                            <li>
                                                <strong>Date</strong>
                                                <small>{{ parseDateWithTimeZone($booking['time_from'], auth()->user()->time_zone) }}</small>
                                            </li>
                                            <li>
                                                <strong>Time from</strong>
                                                <small>{{ parseTimeWithTimeZone($booking['time_from'], auth()->user()->time_zone) }}</small>
                                            </li>
                                            <li>
                                                <strong>Time To</strong>
                                                <small>{{ parseTimeWithTimeZone($booking['time_to'], auth()->user()->time_zone) }}</small>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="slots_info">
                                        <h4>Booking Services Details</h4>
                                        <ul>
                                            <li>
                                                <strong>Services Plan</strong>
                                                <small>{{ $booking['bookingServicesDetail']['talent_services_pricing']['services_pricing_type']['name'] }}</small>
                                            </li>
                                            <li>
                                                <strong>Services Charges</strong>
                                                <small>${{ $booking['bookingServicesDetail']['talent_services_pricing']['price'] }}</small>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="mainFeatures">
                                        <h6>Main Features</h6>
                                        <ul>
                                            @foreach($booking['bookingServicesDetail']['talent_services_pricing']['main_features'] as $feature)
                                                <li>{{ $feature }}</li>
                                            @endforeach
                                        </ul>
                                    </div>


                                    <div class="booking_description descrp">
                                        <h6>Services descriptive note (By Talent)</h6>
                                        <p>{{ $booking['bookingServicesDetail']['talent_services_pricing']['description'] }}</p>
                                    </div>
                                </div>
                            </div>

                            {{-- talent short profile section --}}
                            @if(auth()->user()->isModerator())
                                <div class="bookingDetail_section_fr">
                                    <div class="db_profile ">
                                        <div class="featureWeek_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{ isset($booking['talent']['image']) ? $booking['talent']['image'] : asset('project-assets/images/profile_avatar.png')}}" alt="#">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="featureProfile_avatar_inner">
                                                <div class="featureProfile_avatar_text text-center w-100">
                                                    <strong>{{ $booking['talent']['user_name'] }}</strong>
                                                    <small>{{ $booking['talent']['talentProfessionalInfo']['talentCategory']['name'] }}</small>
                                                </div>

                                                <div class="profile_rating">
                                                    <div class="profile_rating_info">
                                                        <ul>
                                                            @if( $booking['talent']['rating'] > 0.0 && $booking['talent']['rating'] < 0.5 )
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['talent']['rating'] >= 0.5 && $booking['talent']['rating'] < 1.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['talent']['rating'] >= 1.5 && $booking['talent']['rating'] < 2.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['talent']['rating'] >= 2.5 && $booking['talent']['rating'] < 3.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['talent']['rating'] >= 3.5 && $booking['talent']['rating'] < 4.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['talent']['rating'] >= 4.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @endif
                                                        </ul>
                                                        <strong>({{ $booking['talent']['rating'] }})</strong>
                                                    </div>
                                                </div>

                                                <div class="achieved_task_info">
                                                    <ul>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Completed Projects</span>
                                                                <strong>({{ $talentCompletedBookings }})</strong>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Member Since</span>
                                                                <strong>{{ parseDateWithTimeZone($booking['talent']['created_at'], auth()->user()->time_zone) }}</strong>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Amount Earned</span>
                                                                <strong>${{ $talentEarning }}</strong>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="viewProfile">
                                        <a href="{{ route( 'talent.public-profile', ['talent_id' => encryptor('encrypt', $booking['talent']['id'])] ) }}">View Profile</a>
                                    </div>
                                </div>
                            @endif

                            {{-- Moderator profile section --}}
                            @if(auth()->user()->isTalent())
                                <div class="bookingDetail_section_fr">
                                    <div class="db_profile ">
                                        <div class="featureWeek_info">
                                            <div class="feartureProflie_info">
                                                <div class="featureProfile_avatar">
                                                    <span>
                                                        <img src="{{ isset($booking['moderator']['image']) ? $booking['moderator']['image'] : asset('project-assets/images/profile_avatar.png')}}" alt="image">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="featureProfile_avatar_inner">
                                                <div class="featureProfile_avatar_text text-center w-100">
                                                    <strong>{{ $booking['moderator']['user_name'] }}</strong>
                                                </div>

                                                <div class="profile_rating">
                                                    <div class="profile_rating_info">
                                                        <ul>
                                                            @if( $booking['moderator']['rating'] > 0.0 && $booking['moderator']['rating'] < 0.5 )
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['moderator']['rating'] >= 0.5 && $booking['moderator']['rating'] < 1.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['moderator']['rating'] >= 1.5 && $booking['moderator']['rating'] < 2.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['moderator']['rating'] >= 2.5 && $booking['moderator']['rating'] < 3.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['moderator']['rating'] >= 3.5 && $booking['moderator']['rating'] < 4.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @elseif( $booking['moderator']['rating'] >= 4.5 )
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                            @endif
                                                        </ul>
                                                        <strong>{{ $booking['moderator']['rating'] }}</strong>
                                                    </div>
                                                </div>

                                                <div class="achieved_task_info">
                                                    <ul>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Completed Projects</span>
                                                                <strong>({{ $moderatorCompletedBookings }})</strong>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Member Since</span>
                                                                <strong>{{ parseDateWithTimeZone($booking['moderator']['created_at'], auth()->user()->time_zone) }}</strong>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="achieved_task_info_list">
                                                                <span>Amount Spent</span>
                                                                <strong>${{ $moderatorEarning }}</strong>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                        {{-- Details section Ends --}}

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection
