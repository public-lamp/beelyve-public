@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Management')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')

    @if( auth()->user()->isModerator() )
        @include('app.headers.moderator-dashboard-header')
    @elseif( auth()->user()->isTalent() )
        @include('app.headers.talent-dashboard-header')
    @else
        @include('app.headers.simple-header')
    @endif

@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <!-- banner slider -->
            <div class="banner_section ">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img"><img src="{{asset('project-assets/images/about_us_banner.png') }}" alt="#" /></span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Event Management</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="event_page">
                <div class="auto_container">
                    <div class="dasbord_container_detail ">
                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h5>Event #1234/S</h5>
                            </div>
                            <div class="our_earning">
                                <strong>April 6, 9:00 pm</strong>
                            </div>
                        </div>

                        <div class="event_section_head">
                            <div class="db_profile ">
                                <div class="featureWeek_info">
                                    <div class="feartureProflie_info">
                                        <div class="featureProfile_avatar">
                                            <span>
                                                <img src="{{asset('project-assets/images/profile_avatar.png') }}" alt="#">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featureProfile_avatar_inner">
                                        <div class="featureProfile_avatar_text">
                                            <strong>Angela Moss</strong>
                                            <small>Photographer</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="event_section_head_right">
                                <div class="duration_timer">
                                    <label>Duration 30 mints</label>
                                    <ul>
                                        <li>
                                            <span>00</span>
                                            <strong>Hours</strong>
                                        </li>
                                        <li>
                                            <span>30</span>
                                            <strong>Minutes</strong>
                                        </li>
                                        <li>
                                            <span>00</span>
                                            <strong>Seconds</strong>
                                        </li>
                                    </ul>
                                </div>
                                <div class="event_bttns">
                                    <a href="javascript:void(0)" class="default_bttn " id="confirm_Btn">Confirm </a>
                                    <a href="javascript:void(0)" class="default_bttn approve_bttn" id="aprove_Btn">Approve  </a>
                                </div>
                                <div class="dispute_alert">
                                    <span id="dispute_btn"> 
                                        <img src="{{asset('project-assets/images/alert_icon.png') }}" alt="#" />
                                        Dispute
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="event_activity_section">
                            <div class="event_activity_fl">
                                <div class="event_activity_tabs">
                                    <ul>
                                        <li><a href="#" class="activity_active">Activity</a></li>
                                        <li><a href="#">Service Details</a></li>
                                        <li><a href="#">Moderator Rules</a></li>
                                    </ul>
                                </div>

                                <div class="activity_chat_box">
                                    <div class="activity_chat_box_info">
                                        <label class="activity_date">
                                            April 6
                                        </label>

                                        <div class="activity_info_list">
                                            <ul>
                                                <li>
                                                    <div class="activity_info_status">
                                                        <div class="status_info">
                                                            <span class="status_active"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                        </div>

                                                        <div class="activity_info_perform_outer">
                                                            <div class="activity_info_perform">
                                                                <strong>Talent is Present</strong>
                                                                <span>9:00  <i>am</i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="activity_info_status">
                                                        <div class="status_info">
                                                            <span class="status_inactive"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                        </div>

                                                        <div class="activity_info_perform_outer">
                                                            <div class="activity_info_perform">
                                                                <strong>Moderator Conform </strong>
                                                                <span>9:01  <i>am</i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="activity_info_status">
                                                        <div class="status_info">
                                                            <span class="status_inactive"><i class="fa fa-check fa-3" aria-hidden="true"></i></span>
                                                        </div>

                                                        <div class="activity_info_perform_outer">
                                                            <div class="activity_info_perform">
                                                                <strong>Time Started</strong>
                                                                <span>9:01  <i>am</i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="chat_list">
                                            <ul>
                                                <li>
                                                    <div class="chat_list_info">
                                                        <div class="chatAvatar">
                                                            <div class="featureProfile_avatar">
                                                                    <span>
                                                                        <img src="{{asset('project-assets/images/profile_avatar.png') }}" alt="#">
                                                                        <b class="sattus_online"></b>
                                                                    </span>
                                                                <strong>Bobby</strong>
                                                            </div>
                                                        </div>
                                                        <div class="chat_conversation">
                                                            <div class="chat_conversation_info error_msg">
                                                                <strong>Sorry lost connection, please wait me 1 mint</strong>
                                                            </div>
                                                            <div class="chat_seen">
                                                                <strong>Today, 10:30 am</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="chat_list_info user1">
                                                        <div class="chatAvatar">
                                                            <div class="featureProfile_avatar">
                                                                    <span>
                                                                        <img src="{{asset('project-assets/images/profile_avatar.png') }}" alt="#">
                                                                        <b class="sattus_online"></b>
                                                                    </span>
                                                                <strong>You</strong>
                                                            </div>
                                                        </div>
                                                        <div class="chat_conversation">
                                                            <div class="chat_conversation_info ">
                                                                <strong>Ok i will wait</strong>
                                                            </div>
                                                            <div class="chat_seen">
                                                                <strong>Today, 10:30 am</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="chat_list_info">
                                                        <div class="chatAvatar">
                                                            <div class="featureProfile_avatar">
                                                                    <span>
                                                                        <img src="{{asset('project-assets/images/profile_avatar.png') }}" alt="#">
                                                                        <b class="sattus_online"></b>
                                                                    </span>
                                                                <strong>Bobby</strong>
                                                            </div>
                                                        </div>
                                                        <div class="chat_conversation">
                                                            <div class="chat_conversation_info ">
                                                                <strong>Sorry lost connection</strong>
                                                            </div>
                                                            <div class="chat_seen">
                                                                <strong>Today, 10:30 am</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="chat_form">
                                        <div class="chat_form_message">
                                            <textarea placeholder="Message..."rows="2"></textarea>
                                        </div>
                                        <div class="chat_form_bttn">
                                            <button type="submit" class="default_bttn">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="event_activity_fr">
                                <div class="event_order_detail">
                                    <div class="event_order_head">
                                        <strong>Order Details</strong>
                                        <div class="order_menu_bttn">
                                            <small><img src="{{asset('project-assets/images/menu_elipsis.png') }}" alt="#" /></small>
                                        </div>

                                    </div>

                                    <div class="event_order_info">
                                        <div class="event_order_img">
                                            <span><img src="{{asset('project-assets/images/order_img.png') }}" alt="#" /></span>
                                        </div>
                                        <div class="event_order_text">
                                            <p>I will need help to create more
                                                meaning + Energy in my career </p>
                                        </div>
                                    </div>

                                    <div class="orderDetail_summary">
                                        <ul>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Ordered from</strong>
                                                    <b>Bobby/23</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Date</strong>
                                                    <b>April 6, 9:00 am</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Total Price</strong>
                                                    <b>$25.75</b>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="summary_text">
                                                    <strong>Order number</strong>
                                                    <b>#FOBq21213</b>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        
    <!-- Approve popup -->


    <div class="custom_popup sett_approveConfirm_popup aprovePopup">
        <div class="custom_popup_inner">
            <div class="custom_popup_detail">
                <div class="custom_popup_info">
                    <div class="custom_popup_tittle">
                        <h3>Are you sure want to<br>
                            Approve?</h3> 
                    </div>  
    
                    

                    <div class="custom_submit">
                        <!-- <input type="submit" value="Yes, Approve"  /> -->
                        <a href="javascript:void(0)" class=" ">Yes, Approve</a>
                        <a href="javascript:void(0)" class="approve_bttn cancel_bttn">Cancel</a>
                    </div> 

                </div>
            </div>
        </div>
    </div>


    <!-- confirm popup -->


    <div class="custom_popup sett_approveConfirm_popup confirmPopup">
        <div class="custom_popup_inner">
            <div class="custom_popup_detail">
                <div class="custom_popup_info">
                    <div class="custom_popup_tittle">
                        <h3>Are you sure want to<br>
                            Confirm?</h3> 
                    </div>  
    
                    

                    <div class="custom_submit">
                        <!-- <input type="submit" value="Confirm" /> -->
                        <a href="javascript:void(0)" class=" ">Confirm</a>
                        <a href="javascript:void(0)" class="approve_bttn cancel_bttn">Cancel</a>
                    </div> 

                </div>
            </div>
        </div>
    </div>


     <!-- dispute popup -->


 <div class="custom_popup rateReview_popup disputePopup">
    <div class="custom_popup_inner">
        <div class="custom_popup_detail">
            <div class="custom_popup_info">
                <div class="custom_popup_tittle">
                    <h3>Dispute</h3> 
                </div>  
                

                <div class="popup_form">
                    <ul>  

                        <li>
                            <div class="custom_field">
                                <strong>Query</strong>
                                <div class="custom_field_textarea">
                                    <textarea></textarea>
                                </div>
                            </div>
                        </li> 
                    </ul>
                </div>
  
                 

                <div class="custom_submit">
                <!-- <a href="javascript:void(0)" class=" ">Submit</a> -->
                    <input type="submit" value="Submit" class="cancel_bttn" />
                </div> 

            </div>
        </div>
    </div>
</div>





    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
<script type="text/javascript">

    $(document).ready(function() {

        // Approve popup

        $('#aprove_Btn').click(function(){
            $("body").addClass("hiddden");
            $(".aprovePopup").show();
        });
        $('.cancel_bttn ').click(function(){
            $("body").removeClass("hiddden");
            $(".custom_popup").hide();
        });

        // confirm popup 

        $('#confirm_Btn').click(function(){
            $("body").addClass("hiddden");
            $(".confirmPopup").show();
        });

        // dispute popup 

        $('#dispute_btn').click(function(){
            $("body").addClass("hiddden");
            $(".disputePopup").show();
        });
    });

</script>
    
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection

