{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Payment')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/datepicker.css') }}" />
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
    <script src="https://js.stripe.com/v3/"></script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">
            <div class="dasbord_container bookingStepss">
                <div class="auto_container">

                    <div class="dasbord_container_detail">
                        <div class="update_profile update_request_page">
                            <div class="custom_popup_tittle">
                                <h3>Booking Request</h3>
                            </div>
                            <div class="container">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-3 bs-wizard-step complete">
                                        <div class="text-center bs-wizard-stepnum">Step 1</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Date</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 2</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Time Slots</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 3</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Details</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step active"><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 4</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"> Booking Payment</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="update_profile_detail">

                            {{-- Payments section start --}}
                            <div class="update_profile_data" id="updateTab_4">
                                <form id="paymentForm">
                                   @csrf

                                    <div class="addPayment_form">
                                        <ul>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Card Number</strong>
                                                    <div class="custom_field_input stripeElementCustom" id="card_number"></div>
                                                    <div id="cardNumberErrors" class="cardError" role="alert"></div>
                                                </div>
                                            </li>
                                            <li class="w_33">
                                                <div class="custom_field">
                                                    <strong>Expiration Date</strong>
                                                    <div class="custom_field_input stripeElementCustom" id="card_expiry"></div>
                                                    <div id="cardExpiryErrors" class="cardError" role="alert"></div>
                                                </div>
                                            </li>
                                            <li class="w_33">
                                                <div class="custom_field">
                                                    <strong>CVC</strong>
                                                    <div class="custom_field_input stripeElementCustom" id="card_cvc"></div>
                                                    <div id="cardCvcErrors" class="cardError" role="alert"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="custom_submit steps_bttons">
                                        <a href="{{ route('booking-request.detail') }}">Back</a>
                                        <input type="submit" value="Submit (${{ session()->get('bookingProcessResponse')['currentBooking']['servicePricing']['price'] }})" id="paymentFormSubmitBtn" />
                                    </div>
                                </form>

                                <div id="cardErrors" class="cardError" role="alert"></div>

                            </div>
                            {{-- Payments section ends --}}

                        </div>
                        <br>
                    </div>

                </div>
            </div>
        </div>

    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">

        // show_date_calender_popup
        $( function() { // date picker
            $( "#datepicker_date" ).datepicker({ minDate: -0, maxDate: "+1M +10D", dateFormat: "yy-mm-dd" });
        });

        $('.addDate').click(function() {
            $('#selectDate').show();
        });

        // show_time_popup
        $('.showTime_popup').click(function() {
            $('#selectDate').hide();
            $('#showTime_popup').show();
        });

        // bookingDetail_popup
        $('.bookingDetail_popup').click(function() {
            $('#selectDate, #showTime_popup').hide();
            $('#bookingDetail_popup').show();
        });

        // show_time_popup
        $('.show_payment_popup').click(function() {
            $('#selectDate , #bookingDetail_popup').hide();
            $('#show_payment_popup').show();
        });

    </script>

    {{-- stripe checkout --}}
    <script type="text/javascript">
        // Create a Stripe client.
        let stripe = Stripe('{{ config('services.stripe.publishable') }}');

        // Create an instance of Elements.
        let elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        let style = {
            base: {
                color: '#636363',
                fontSize: '15px',
                textAlign: 'left',
                lineHeight: '1',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        let cardNumber = elements.create('cardNumber', {
            style: style
        });
        cardNumber.mount('#card_number');

        let expiry = elements.create('cardExpiry', {
            'style': style
        });
        expiry.mount('#card_expiry');

        let cvc = elements.create('cardCvc', {
            'style': style
        });
        cvc.mount('#card_cvc');

        // Card number real-time validation
        cardNumber.addEventListener('change', function(event) {
            let displayError = document.getElementById('cardNumberErrors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Expiry real-time validation
        expiry.addEventListener('change', function(event) {
            let displayError = document.getElementById('cardExpiryErrors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Expiry real-time validation
        cvc.addEventListener('change', function(event) {
            let displayError = document.getElementById('cardCvcErrors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        let form = document.getElementById('paymentForm');
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            blockUi( true );
            stripe.createToken(cardNumber).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    let errorElement = document.getElementById('cardErrors');
                    errorElement.textContent = result.error.message;
                    blockUi( false );
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // select booking date
            const data = { 'stripeTokenId': token.id };
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('booking.payment.charge') }}",
                data: data,
                success: function (response) {
                    blockUi( false );
                    if ( response.status ) {
                        toasterSuccess( true,  response.message );
                        let nextUrl;
                        if ( Boolean( response.data.next_url.trim() ) ) {
                            nextUrl = response.data.next_url;
                        } else {
                            nextUrl = '{{ route('home') }}';
                        }
                        window.location.href = nextUrl;
                    } else {
                        toasterSuccess( false, response.error);
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, value);
                    });
                }
            });
        }
    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
@endsection




