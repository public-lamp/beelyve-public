{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Time')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/datepicker.css') }}" />
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

<main>
    <div class="main_container">
        <div class="dasbord_container bookingStepss">
            <div class="auto_container">

                <div class="dasbord_container_detail">
                    <div class="update_profile update_request_page">
                        <div class="custom_popup_tittle">
                            <h3>Booking Request</h3>
                        </div>
                        <div class="container">
                            <div class="row bs-wizard" style="border-bottom:0;">
                                <div class="col-xs-3 bs-wizard-step active complete">
                                  <div class="text-center bs-wizard-stepnum">Step 1</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">Booking Date</div>
                                </div>

                                <div class="col-xs-3 bs-wizard-step  "><!-- complete -->
                                  <div class="text-center bs-wizard-stepnum">Step 2</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">Time Slots</div>
                                </div>

                                <div class="col-xs-3 bs-wizard-step  "><!-- complete -->
                                  <div class="text-center bs-wizard-stepnum">Step 3</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center">Booking Details</div>
                                </div>

                                <div class="col-xs-3 bs-wizard-step "><!-- active -->
                                  <div class="text-center bs-wizard-stepnum">Step 4</div>
                                  <div class="progress"><div class="progress-bar"></div></div>
                                  <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                  <div class="bs-wizard-info text-center"> Booking Payment</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="update_profile_detail">

                        {{--Booking date section starts--}}
                        <div class="update_profile_data" id="updateTab_1" style="display: block;">
                            <div class="custom_popup_info">
                                <div class="custom_popup_tittle">
                                    <h3>Select Date</h3>
                                </div>
                                <div class="select_date_info">
                                    <div id="datepicker_date"></div>
                                </div>
                                <div class="custom_submit steps_bttons">
                                    <a href="javascript:void(0)" id="bookingDateSelect">Next</a>
                                </div>
                            </div>
                        </div>
                        {{--Booking date section ends--}}

                    </div>
                    <br>
                </div>

            </div>
        </div>
    </div>

</main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">

        // show_date_calender_popup
        $( function() { // date picker
            $( "#datepicker_date" ).datepicker({ minDate: -0, maxDate: "+1M +10D", dateFormat: "yy-mm-dd" });
        });

{{--        @if( session('bookingProcessResponse.bookableDate') )--}}
{{--        $( function() { // date picker--}}
{{--            --}}{{--$("#datepicker_date").datepicker("setDate", {{ date('Y-m-d', strtotime(session('bookingProcessResponse.bookableDate'))) }});--}}
{{--            $( "#datepicker_date" ).datepicker( "setDate", "2012-03-25" );--}}
{{--        });--}}
{{--        @endif--}}

        $('.addDate').click(function() {
            $('#selectDate').show();
        });

        // show_time_popup
        $('.showTime_popup').click(function() {
            $('#selectDate').hide();
            $('#showTime_popup').show();
        });

        // bookingDetail_popup
        $('.bookingDetail_popup').click(function() {
            $('#selectDate, #showTime_popup').hide();
            $('#bookingDetail_popup').show();
        });

        // show_time_popup
        $('.show_payment_popup').click(function() {
            $('#selectDate , #bookingDetail_popup').hide();
            $('#show_payment_popup').show();
        });

    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
    <script type="text/javascript">

        // select booking date
        $('#bookingDateSelect').on('click', function (e) {
            // ajax call
            $('.errorAlertText').html('');   // set alert input field to normal
            e.preventDefault();
            const bookingDate = $("#datepicker_date").datepicker().val();
            const current_booking_id = $('#current_booking_id').val();
            const data = { 'date': bookingDate, 'current_booking_id': current_booking_id };
            blockUi( true );
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('booking-request.date-selection.post') }}",
                data: data,
                success: function (response) {
                    blockUi( false );
                    if ( !response.status ) {
                        return toasterSuccess( false, response.error);
                    }
                    let nextUrl;
                    if ( response.status ) {
                        toasterSuccess( true,  response.message );
                        nextUrl = response.data.next_url;
                    } else {
                        toasterSuccess( false,  response.error );
                        nextUrl = response.data.next_url;
                    }

                    if ( Boolean(nextUrl.trim()) ) {
                        window.location.href = nextUrl;
                    } else {
                        window.location.reload();
                    }

                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if (value.includes(key)) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, value);
                    });
                }
            });
        });

    </script>
@endsection




