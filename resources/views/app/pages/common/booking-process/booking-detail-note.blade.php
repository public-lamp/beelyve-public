{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Booking Details')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/datepicker.css') }}" />
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">
            <div class="dasbord_container bookingStepss">
                <div class="auto_container">

                    <div class="dasbord_container_detail">
                        <div class="update_profile update_request_page">
                            <div class="custom_popup_tittle">
                                <h3>Booking Request</h3>
                            </div>
                            <div class="container">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-3 bs-wizard-step  complete">
                                        <div class="text-center bs-wizard-stepnum">Step 1</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Date</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 2</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Time Slots</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step active complete"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 3</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Details</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step "><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 4</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"> Booking Payment</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="update_profile_detail">

                            {{-- Booking detail section start --}}
                            <div class="update_profile_data" id="updateTab_3">
                                <div class="custom_popup_info">
                                    <div class="custom_popup_tittle">
                                        <h3>Booking Details</h3>
                                    </div>
                                    <div class="">
                                        <div class="addPayment_form">
                                            <form id="bookingDetailsForm">
                                                <ul>
                                                    <li>
                                                        <div class="custom_field">
                                                            <strong>Event Title</strong>
                                                            <div class="custom_field_input">
                                                                <input name="booking_title" value="@if ( session('bookingProcessResponse') ){{  session('bookingProcessResponse.currentBooking.title') }}@endif"
                                                                    placeholder="Event title here" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom_field">
                                                            <strong>Event Description</strong>
                                                            <div class="custom_field_textarea">
                                                                <textarea name="booking_detail_note">@if ( session('bookingProcessResponse') ) {{  session('bookingProcessResponse.currentBooking.detail_note') }} @endif</textarea>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <br>
                                                </ul>
                                            </form>
                                        </div>

                                        <div class="custom_submit steps_bttons">
                                            @if( auth()->check() && session()->has('bookingProcessResponse') )
                                                <a href="{{ route('booking-request.available-slots', [ 'time_slot' => ['start_time' => session()->get('bookingProcessResponse.currentBooking.time_from'), 'end_time' => session()->get('bookingProcessResponse.currentBooking.time_to')] ]) }}">Back</a>
                                            @else
                                                <a href="{{ route('home') }}">Back</a>
                                            @endif
                                            <a href="javascript:void(0)" id="bookingDetailsFormBtn">Next</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            {{-- Booking detail section ends --}}

                        </div>
                        <br>
                    </div>

                </div>
            </div>
        </div>

    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">

        // show_date_calender_popup
        $( function() { // date picker
            $( "#datepicker_date" ).datepicker({ minDate: -0, maxDate: "+1M +10D", dateFormat: "yy-mm-dd" });
        });

        $('.addDate').click(function() {
            $('#selectDate').show();
        });

        // show_time_popup
        $('.showTime_popup').click(function() {
            $('#selectDate').hide();
            $('#showTime_popup').show();
        });

        // bookingDetail_popup
        $('.bookingDetail_popup').click(function() {
            $('#selectDate, #showTime_popup').hide();
            $('#bookingDetail_popup').show();
        });

        // show_time_popup
        $('.show_payment_popup').click(function() {
            $('#selectDate , #bookingDetail_popup').hide();
            $('#show_payment_popup').show();
        });

    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
    <script type="text/javascript">

        // being-created booking details
        $('#bookingDetailsFormBtn').on('click', function (e) {
            // ajax call
            e.preventDefault();
            blockUi( true );
            const data = $('#bookingDetailsForm').serialize();
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('booking-request.detail.post') }}",
                data: data,
                success: function (response) {
                    if ( !response.status ) {
                        blockUi( false );
                        return toasterSuccess( false, response.error);
                    }
                    let nextUrl;
                    if ( response.status ) {
                        blockUi( false );
                        toasterSuccess( true,  response.message );
                        nextUrl = response.data.next_url;
                    } else {
                        blockUi( false );
                        toasterSuccess( false,  response.error );
                        if ( response.data.next_url ) {
                            nextUrl = response.data.next_url;
                        }
                    }

                    return window.location.href = nextUrl;
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, value);
                    });
                }
            });
        });

    </script>
@endsection




