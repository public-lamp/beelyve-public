{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Available Slots')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/datepicker.css') }}" />
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer></script>

    {{-- slot selection handling --}}
    <script type="text/javascript">
        function slotSelected( slotPostFix ) {
            $('#selectTimeSlot').attr('data-slot-postfix', slotPostFix);
        }
    </script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">
            <div class="dasbord_container bookingStepss">
                <div class="auto_container">

                    <div class="dasbord_container_detail">
                        <div class="update_profile update_request_page">
                            <div class="custom_popup_tittle">
                                <h3>Booking Request</h3>
                            </div>
                            <div class="container">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-3 bs-wizard-step  complete ">
                                        <div class="text-center bs-wizard-stepnum">Step 1</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Date</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step active complete"><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 2</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Time Slots</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step  "><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 3</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Details</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step "><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 4</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="javascript:void(0)" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"> Booking Payment</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="update_profile_detail">

                            {{-- Time-slots section starts --}}
                            <div class="update_profile_data" id="updateTab_2">
                                <div class="custom_popup_info">
                                    <div class="custom_popup_tittle">
                                        <h3>Select Time</h3>
                                    </div>

                                    <div class="selectTime_info">
                                        <ul>
                                            @if( !empty($daySlots) )
                                                @foreach($daySlots as $slotKey => $item)
                                                    <li>
                                                        <form id="time_slot_form_{{ $slotKey }}">
                                                            <div id="slot_item_div_{{ $slotKey }}" class="chooseOptionDetail @if( (!session()->has('bookingProcessResponse.currentBooking.time_from') && $slotKey == 0) || (session('bookingProcessResponse.currentBooking.time_from') && explode(' ', session('bookingProcessResponse.currentBooking.time_from'))[1] == $item['slot_start_time'].':00') ) checked_item @endif">
                                                                <label>
                                                                    <input type="radio" class="option-input radio" name="time_slot_radio_{{ $slotKey }}"
                                                                           id="time_slot_radio_{{ $slotKey }}" onclick="slotSelected( {{ $slotKey }} )"
                                                                           @if( (!session('bookingProcessResponse.currentBooking.time_from') && $slotKey == 0) || (session()->has('bookingProcessResponse.currentBooking.time_from') && explode(' ', session('bookingProcessResponse.currentBooking.time_from'))[1] == $item['slot_start_time'].':00') ) checked @endif
                                                                    />
                                                                </label>
                                                                <div class="timezone_input">
                                                                    <label>
                                                                        {{-- from talent timezone to moderator time zone --}}
                                                                        {{ parseTimeWithTimeZone($item['slot_start_time'], auth()->user()->time_zone, $currentBooking['talent']['time_zone'], 'h:i a') }}
                                                                        <input type="hidden" value="{{ parseTimeWithTimeZone($item['slot_start_time'], auth()->user()->time_zone, $currentBooking['talent']['time_zone']) }}" {{-- from Talent timezone to UTC time zone --}}
                                                                            name="time_slot[start_time]" id="time_slot_start_time_{{ $slotKey }}" readonly="" />
                                                                    </label>
                                                                </div>
                                                                <b>To</b>
                                                                {{-- <i>-</i> --}}
                                                                <div class="timezone_input frTime">
                                                                    <label>
                                                                        {{-- from talent timezone to moderator time zone --}}
                                                                        {{ parseTimeWithTimeZone($item['slot_end_time'], auth()->user()->time_zone, $currentBooking['talent']['time_zone'], 'h:i a') }}
                                                                        <input type="hidden" value="{{ parseTimeWithTimeZone($item['slot_end_time'], auth()->user()->time_zone, $currentBooking['talent']['time_zone']) }}"
                                                                               name="time_slot[end_time]" id="slot_end_time_{{ $slotKey }}" readonly="" />
                                                                    </label>
                                                                </div>
                                                                {{-- <b>am</b>--}}
                                                            </div>
                                                        </form>
                                                    </li>
                                                    {{-- select already set time's value from session --}}
                                                    @if( session()->has('bookingProcessResponse') && session('bookingProcessResponse.currentBooking.time_from') && explode(' ', session('bookingProcessResponse.currentBooking.time_from'))[1] == $item['slot_start_time'].':00' )
                                                        <script>
                                                           slotSelected( {{ $slotKey }} )
                                                        </script>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="no_slot">
                                                    <p>No slots found for the selected date please <a href="{{ url()->previous() }}">Try another Date</a> </p>
                                                </div>
                                            @endif
                                        </ul>
                                    </div>

                                    <div class="custom_submit steps_bttons">
                                        @if( auth()->check() && session()->has('bookingProcessResponse') )
                                            <a href="{{ route('booking-request.start', ['talent_id' => encryptor('encrypt', session()->get('bookingProcessResponse.currentBooking.talent_id')), 'talent_services_pricing_id' => session()->get('bookingProcessResponse.currentBooking.talent_services_pricing_id')]) }}">Back</a>
                                        @else
                                            <a href="{{ route('home') }}">Back</a>
                                        @endif

                                        @if( !empty($daySlots) && session('bookingProcessResponse') ) {{-- select first form to serialise --}}
                                            <a href="javascript:void(0)" id="selectTimeSlot" data-slot-postfix="0">Next</a>
                                        @else {{-- select already selected form to serialise --}}
                                            <a href="javascript:void(0)" id="selectTimeSlot" data-slot-postfix="">Next</a>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            {{-- Time-slots Section ends --}}

                        </div>
                        <br>
                    </div>

                </div>
            </div>
        </div>

    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">

        // show_date_calender_popup
        $( function() { // date picker
            $( "#datepicker_date" ).datepicker({ minDate: -0, maxDate: "+1M +10D", dateFormat: "yy-mm-dd" });
        });
        $('.addDate').click(function() {
            $('#selectDate').show();
        });
        // show_time_popup
        $('.showTime_popup').click(function() {
            $('#selectDate').hide();
            $('#showTime_popup').show();
        });
        // bookingDetail_popup
        $('.bookingDetail_popup').click(function() {
            $('#selectDate, #showTime_popup').hide();
            $('#bookingDetail_popup').show();
        });
        // show_time_popup
        $('.show_payment_popup').click(function() {
            $('#selectDate , #bookingDetail_popup').hide();
            $('#show_payment_popup').show();
        });


        //    slot selection handling
        // function slotSelected( slotPostFix ) {
        //     $('#selectTimeSlot').attr('data-slot-postfix', slotPostFix);
        // }
    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
    <script type="text/javascript">

        // select time slot for booking
        $('#selectTimeSlot').on('click', function (e) {
            // ajax call
            e.preventDefault();
            const formId = 'time_slot_form_' + $('#selectTimeSlot').attr('data-slot-postfix');
            const data = $('#'+formId).serialize();
            blockUi( true );
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('booking-request.slot-selection.post') }}",
                data: data,
                success: function (response) {
                    blockUi( false );
                    if ( !response.status ) {
                        return toasterSuccess( false, response.error);
                    }
                    let nextUrl;
                    if ( response.status ) {
                        if ( response.data.next_url ) {
                            toasterSuccess( true,  response.message );
                            nextUrl = response.data.next_url;
                            window.location.href = nextUrl;
                        } else {
                            toasterSuccess( true,  response.message );
                        }
                    } else {
                        toasterSuccess( false,  response.error );
                    }
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        toasterSuccess( false, value);
                    });
                }
            });
        });

    </script>
@endsection




