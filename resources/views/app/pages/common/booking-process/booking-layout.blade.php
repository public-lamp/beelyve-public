{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Talent-Profile')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('project-assets/css/datepicker.css') }}" />
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('project-assets/js/slick.js') }}" defer ></script>
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')

    <main>
        <div class="main_container">
            <div class="dasbord_container bookingStepss">
                <div class="auto_container">

                    <div class="dasbord_container_detail">
                        <div class="update_profile update_request_page">
                            <div class="custom_popup_tittle">
                                <h3>Booking Request</h3>
                            </div>
                            <div class="container">
                                <div class="row bs-wizard" style="border-bottom:0;">
                                    <div class="col-xs-3 bs-wizard-step active complete ">
                                        <div class="text-center bs-wizard-stepnum">Step 1</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Date</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step  "><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 2</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Time Slots</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step  "><!-- complete -->
                                        <div class="text-center bs-wizard-stepnum">Step 3</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center">Booking Details</div>
                                    </div>

                                    <div class="col-xs-3 bs-wizard-step "><!-- active -->
                                        <div class="text-center bs-wizard-stepnum">Step 4</div>
                                        <div class="progress"><div class="progress-bar"></div></div>
                                        <a href="#" class="bs-wizard-dot"></a>
                                        <div class="bs-wizard-info text-center"> Booking Payment</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="update_profile_detail">
                            <div class="update_profile_tabs">
                                <ul>
                                    <li><a href="#updateTab_1" class="updateTab_active">Booking Date</a></li>
                                    <li><a href="#updateTab_2">Time Slots</a></li>
                                    <li><a href="#updateTab_3">Booking Detail</a></li>
                                    <li><a href="#updateTab_4">Booking Payment</a></li>
                                </ul>
                            </div>


                            {{--Booking date section starts--}}
                            <div class="update_profile_data" id="updateTab_1" style="display: block;">
                                <div class="custom_popup_info">
                                    <div class="custom_popup_tittle">
                                        <h3>Select Date</h3>
                                    </div>
                                    <div class="select_date_info">
                                        @if(session()->has('bookingProcessResponse'))
                                            <input type="hidden" value="{{ session()->get('bookingProcessResponse')['currentBooking']['id'] }}" id="current_booking_id" />
                                        @endif
                                        <div id="datepicker_date"></div>
                                    </div>
                                    <div class="custom_submit steps_bttons">
                                        <a href="javascript:void(0)" id="bookingDateSelect">Next</a>
                                    </div>
                                </div>
                            </div>
                            {{--Booking date section ends--}}

                            {{-- Time-slots section starts --}}
                            <div class="update_profile_data" id="updateTab_2">
                                <div class="custom_popup_info">
                                    <div class="custom_popup_tittle">
                                        <h3>Select Time</h3>
                                    </div>

                                    <div class="selectTime_detail">
                                        <div class="selectTime_zone">
                                            <ul>
                                                <li>
                                                    <div class="chooseOptionDetail">
                                                        <label>
                                                            <input type="radio" class="option-input radio" name="est" checked="">
                                                            est
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="chooseOptionDetail">
                                                        <label>
                                                            <input type="radio" class="option-input radio" name="est">
                                                            cst
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="chooseOptionDetail">
                                                        <label>
                                                            <input type="radio" class="option-input radio" name="est">
                                                            fst
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="selectTime_info">
                                            <ul>
                                                @if( session()->has('bookingProcessResponse') )
                                                    @foreach(session()->get('bookingProcessResponse')['searchableDayProcessedSlots'] as $item)
                                                        <li>
                                                            <div class="chooseOptionDetail checked_item">
                                                                <label> <input type="radio" class="option-input radio" name="time" checked="">  </label>
                                                                <div class="timezone_input">
                                                                    <input type="text" value="{{ date('H:i a', strtotime($item['slot_start_time'])) }}" placeholder="9:30" />
                                                                </div>
                                                                <b>am</b>
                                                                <i>-</i>
                                                                <div class="timezone_input">
                                                                    <input type="text" value="{{ date('H:i a', strtotime($item['slot_end_time'])) }}" placeholder="9:30" />
                                                                </div>
                                                                <b>am</b>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @else
                                                    <li>Session expired, please start over</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="custom_submit steps_bttons">
                                        <a href="#">Back</a>
                                        <a href="#">Next</a>
                                    </div>

                                </div>
                            </div>
                            {{-- Time-slots Section ends --}}

                            {{-- Booking detail section start --}}
                            <div class="update_profile_data" id="updateTab_3">
                                <div class="custom_popup_info">
                                    <div class="custom_popup_tittle">
                                        <h3>Booking Detail</h3>
                                    </div>
                                    <div class="">

                                        <div class="addPayment_form">
                                            <ul>
                                                <li>
                                                    <div class="custom_field">
                                                        <strong>Booking Description</strong>
                                                        <div class="custom_field_textarea">
                                                            <textarea></textarea>
                                                        </div>
                                                    </div>
                                                </li>
                                                <br>
                                            </ul>
                                        </div>

                                        <div class="custom_submit steps_bttons">
                                            <a href="#">Back</a>
                                            <a href="#">Next</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            {{-- Booking detail section ends --}}

                            {{-- Payments section start --}}
                            <div class="update_profile_data" id="updateTab_4">
                                <div class="custom_popup_info">
                                    <div class="custom_popup_tittle">
                                        <h3>Add Payment</h3>
                                    </div>

                                    <div class="addPayment_form">
                                        <ul>
                                            <li class="w_100">
                                                <div class="custom_field">
                                                    <strong>How Would you like to pay?</strong>
                                                    <div class="custom_dropdown">
                                                        <select>
                                                            <option>Select here</option>
                                                            <option>Budget 1</option>
                                                            <option>Budget 2</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Card Number</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="">
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="w_33">
                                                <div class="custom_field">
                                                    <strong>Expiry date</strong>
                                                    <div class="custom_dropdown">
                                                        <select>
                                                            <option>Select Month</option>
                                                            <option>Select Month 1</option>
                                                            <option>Select Month 2</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="w_33">
                                                <div class="custom_field">
                                                    <strong>Expiry year</strong>
                                                    <div class="custom_dropdown">
                                                        <select>
                                                            <option>Select Year</option>
                                                            <option>Select Year 1</option>
                                                            <option>Select Year 2</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="w_33">
                                                <div class="custom_field">
                                                    <strong>CVC</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="">
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="custom_field">
                                                    <strong>Card holder name</strong>
                                                    <div class="custom_field_input">
                                                        <input type="text" value="">
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="custom_submit steps_bttons">
                                        <a href="#">Back</a>
                                        <input type="submit" value="Save" />
                                    </div>
                                </div>
                            </div>
                            {{-- Payments section ends --}}

                        </div>
                        <br>
                    </div>

                </div>
            </div>
        </div>

    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
    <script type="text/javascript">

        // show_date_calender_popup
        $( function() { // date picker
            $( "#datepicker_date" ).datepicker({ minDate: -0, maxDate: "+1M +10D", dateFormat: "yy-mm-dd" });
        });

        $('.addDate').click(function() {
            $('#selectDate').show();
        });

        // show_time_popup
        $('.showTime_popup').click(function() {
            $('#selectDate').hide();
            $('#showTime_popup').show();
        });

        // bookingDetail_popup
        $('.bookingDetail_popup').click(function() {
            $('#selectDate, #showTime_popup').hide();
            $('#bookingDetail_popup').show();
        });

        // show_time_popup
        $('.show_payment_popup').click(function() {
            $('#selectDate , #bookingDetail_popup').hide();
            $('#show_payment_popup').show();
        });

    </script>
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });
    </script>

@endsection

@section('ajax')
    <script type="text/javascript">

        // select booking date
        $('#bookingDateSelect').on('click', function (e) {
            // ajax call
            $('.errorAlertText').html('');   // set alert input field to normal
            e.preventDefault();
            const bookingDate = $("#datepicker_date").datepicker().val();
            const current_booking_id = $('#current_booking_id').val();
            const data = { 'date': bookingDate, 'current_booking_id': current_booking_id };
            blockUi( true );
            $.ajax({
                type: 'POST',
                headers: { 'X-CSRF-TOKEN': $('meta[name = "_token"]').attr('content') },
                url: "{{ route('booking-request.date-selection.post') }}",
                data: data,
                success: function (response) {
                    blockUi( false );
                    if ( !response.status ) {
                        return toasterSuccess( false, response.error);
                    }
                    toasterSuccess( true,  response.message );
                    let nextUrl;
                    if ( response.data.next_url ) {
                        nextUrl = response.data.next_url;
                    } else {
                        nextUrl = "{{ route('booking-request.available-slots') }}";
                    }

                    return window.location.href = nextUrl;

                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        blockUi( false );
                        let element = document.getElementById(key);
                        let errorAlertHtml = validationText(value, 'error');
                        $(element).after(errorAlertHtml);
                        if (value.includes(key)) {
                            $(element).val(value);
                        }
                        toasterSuccess( false, value);
                    });
                }
            });
        });

    </script>
@endsection




