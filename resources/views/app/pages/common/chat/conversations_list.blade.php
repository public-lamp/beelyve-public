@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Conversations')

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')

    @if( auth()->user()->isModerator() )
        @include('app.headers.moderator-dashboard-header')
    @elseif( auth()->user()->isTalent() )
        @include('app.headers.talent-dashboard-header')
    @elseif( auth()->user()->isAdmin() )
        @include('app.headers.simple-header')
    @else
        @include('app.headers.simple-header')
    @endif
@endsection

@section('main-content')
    <main>
        <div class="main_container">

            <div class="banner_section">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img"><img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="img" /></span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Messages</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="auto_container">
                    <div class="dasbord_container_detail messagePage">
                        <div class="db_tittle">
                            <div class="db_tittle_heading">
                                <h4>Message List</h4>
                            </div>
                            <div class="sorted_dropdown dropdown_sort">
                                <div class="custom_dropdown">
                                    <select>
                                        <option>Sort by: Date</option>
                                        <option>Sort by: Date 1</option>
                                        <option>Sort by: Date 2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="message_section">
                            <ul>
                                <li>
                                    <div class="message_section_info">
                                        <div class="feartureProflie_info">
                                            <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                    <b class="sattus_online"></b>
                                                </span>
                                            </div>
                                            <div class="featureProfile_avatar_text">
                                                <strong>Jack Black</strong>
                                                <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="message_section_info">
                                        <div class="feartureProflie_info">
                                            <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                    <b class="sattus_online"></b>
                                                </span>
                                            </div>
                                            <div class="featureProfile_avatar_text">
                                                <strong>Jack Black</strong>
                                                <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="message_section_info">
                                        <div class="feartureProflie_info">
                                            <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                    <b class="sattus_online"></b>
                                                </span>
                                            </div>
                                            <div class="featureProfile_avatar_text">
                                                <strong>Jack Black</strong>
                                                <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="message_section_info">
                                        <div class="feartureProflie_info">
                                            <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                    <b class="sattus_online"></b>
                                                </span>
                                            </div>
                                            <div class="featureProfile_avatar_text">
                                                <strong>Jack Black</strong>
                                                <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="message_section_info">
                                        <div class="feartureProflie_info">
                                            <div class="featureProfile_avatar">
                                                <span>
                                                    <img src="{{ asset('project-assets/images/profile_avatar.png') }}" alt="img">
                                                    <b class="sattus_online"></b>
                                                </span>
                                            </div>
                                            <div class="featureProfile_avatar_text">
                                                <strong>Jack Black</strong>
                                                <small>You sent an attachment <b>Today, 10:30 am</b></small>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

{{-- page footer section --}}
@section('page-footer')
    @include('app.footers.simple-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
@endsection

@section('ajax')
@endsection



