{{--main layout--}}
@extends('app.layouts.main-layout')

{{-- page title --}}
@section('title', 'Find Talent')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('project-assets/css/owl.carousel.min.css') }}" />
@endsection

@section('page-scripts')
@endsection

{{-- page header section --}}
@section('page-header')
    @include('app.headers.main-header')
@endsection

@section('main-content')
    <main>
        <div class="main_container">
            <!-- banner slider -->
            <div class="banner_section searchTalented tabBanner">
                <div class="banner_slider_section bannerOverlay">
                    <span class="banner_img"><img src="{{ asset('project-assets/images/about_us_banner.png') }}" alt="about_img" /></span>
                    <div class="banner_info">
                        <div class="auto_container">
                            <div class="banner_info_detail">
                                <h1>Find Best Talent</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main_container_inner">
                <div class="auto_container">
                    <div class="main_container_innerDetail">
                        <!-- searched talents results -->
                        <div class="available_feature_section onHomePge " style="padding: 0;">
                            <div class="available_feature_sorted">
                                <div class="sorted_nav tanelt_sort">
                                    @include('app.sections.front.find-talent-search-section')
                                    <div class="icon_list_grid">
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-th iconGrid" aria-hidden="true"></i>
                                            <i class="fa fa-th-list iconList" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="search_container">
                                <h3>{{ \App\Models\Talent\TalentCategory::all()->count() }}+ different services categories</h3>
                            </div>

                            @if( $talents->isEmpty() )
                                {{-- No search record found --}}
                                <div class="empty_data">
                                    <p>No Data Available</p>
                                </div>
                            @else
                                <div class="available_feature_list">
                                    <ul>
                                        @foreach($talents as $talent)
                                            <li>
                                                <div class="featureWeek_info">
                                                    <div class="feartureProflie_info"><a href="{{ route( 'talent.public-profile', ['talent_id' => encryptor('encrypt', $talent['id'])] ) }}" class="goto_profile"></a>
                                                        <div class="featureProfile_avatar">
                                                            <span>
                                                                <img src="{{ isset($talent['image']) ? $talent['image'] : asset('project-assets/images/profile_avatar.png') }}" alt="image">
                                                                {{-- <small class="online"><i></i>online</small> --}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="featureProfile_avatar_inner">
                                                        <div class="featureProfile_avatar_text">
                                                            <strong>{{ $talent['first_name']. ' ' .$talent['last_name'] }}</strong>
                                                            <small>{{ $talent['talentProfessionalInfo']['talentCategory']['name'] }}</small>
                                                        </div>

                                                        <div class="profile_rating">
                                                            <div class="profile_rating_info">
                                                                <ul>
                                                                    @if( $talent['rating'] > 0.0 && $talent['rating'] < 0.5 )
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                    @elseif( $talent['rating'] >= 0.5 && $talent['rating'] < 1.5 )
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                    @elseif( $talent['rating'] >= 1.5 && $talent['rating'] < 2.5 )
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                    @elseif( $talent['rating'] >= 2.5 && $talent['rating'] < 3.5 )
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                    @elseif( $talent['rating'] >= 3.5 && $talent['rating'] < 4.5 )
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                    @elseif( $talent['rating'] >= 4.5 )
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                        <li><a href="javascript:void(0)" class="fill_rate"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                                    @endif
                                                                </ul>
                                                                <strong>({{ count($talent['reviews']) }})</strong>
                                                                {{-- <b>(403)</b> --}}
                                                            </div>
                                                        </div>

                                                        <div class="profileDiscription">
                                                            <p>{{ $talent['about'] }}</p>
                                                        </div>

                                                        <div class="favrt_price">
                                                            @if( auth()->check() )
                                                                <div class="favrt_icon">
                                                                    @if( isset($user) && $user['favoriteTalents']->isNotEmpty() && in_array($talent['id'], $user->favoriteTalents()->pluck('favorable_id')->toArray()) )
                                                                        <a href="{{ route('favourite.user.remove', ['favorable_id' => $talent['id']]) }}">
                                                                            <span><i class="fa fa-heart red_fvrt" aria-hidden="true"></i></span>
                                                                        </a>
                                                                    @else
                                                                        <a href="{{ route('favourite.user.add', ['favorable_id' => $talent['id']]) }}">
                                                                    <span>
                                                                        <i class="fa fa-heart-o un_fvrt" aria-hidden="true"></i>
                                                                        <i class="fa fa-heart fvrt" aria-hidden="true"></i>
                                                                    </span>
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                            @endif

                                                            <div class="priceinfo_starting">
                                                                <strong>Starting at<b>${{ $talent['talentProfessionalInfo']['start_rate'] }}</b></strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                {{-- pagination --}}
                                <div class="">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">{{ $talents->links() }}
                                        </ul>
                                    </nav>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

{{-- page footer section --}}
@section('page-footer')
    {{-- footer --}}
    @include('app.footers.main-footer')
@endsection

{{--  page misc scripts  --}}
@section('page-misc-scripts')
@endsection

{{-- included sections and componets --}}
@section('rendered-sections')
    {{--     signup model/popup section --}}
    @include('app.sections.authentication.html.signup-modal')

    {{--     signin model/popup section --}}
    @include('app.sections.authentication.html.login-modal')

    {{--     forgot model/popup section --}}
    @include('app.sections.authentication.html.forgot-password-modal')

    {{-- auth modals hide/show script --}}
    <script>
        {{-- login modal show/hide script --}}
        $('.custom_popup').on('click', function() {
            $('.custom_popup').hide();
        });
        $('body .signup_modal_link ').click(function() {
            $('.custom_popup').hide();
            $('#signup_modal ').fadeIn();
        });
        $('body .login_modal_link ').click(function() {
            $('.custom_popup').hide();
            $(' #login_modal ').fadeIn();
        });
        $('body .forgot_modal_link').click(function() {
            $('.custom_popup').hide();
            $('#forgot_modal').fadeIn();
        });
        $('.custom_popup_info').on('click', function(e) {
            e.stopPropagation();
        });

    </script>
@endsection

@section('ajax')
@endsection
