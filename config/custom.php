<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin related configurations
    |--------------------------------------------------------------------------
    |
    | Admin related MISC configurations
    |
    */

    'admin' => [
        'domain' => env('ADMIN_SUBDOMAIN', 'admin'),
    ],



];
