<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTalentSearchFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_search_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parent_filter_name')->nullable();
            $table->string('parent_filter_key')->nullable();
            $table->string('name')->nullable();
            $table->string('key')->nullable();
            $table->string('min_value')->nullable();
            $table->string('max_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talent_search_filters');
    }
}
