<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemainingTimeAddBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            if ( !Schema::hasColumns('bookings', ['total_minutes_duration', 'remaining_hours', 'remaining_minutes', 'remaining_seconds']) ) {
                $table->string('remaining_hours')->nullable()->after('time_to');
                $table->string('remaining_minutes')->nullable()->after('remaining_hours');
                $table->string('remaining_seconds')->nullable()->after('remaining_minutes');
                $table->string('total_minutes_duration')->nullable()->after('remaining_seconds');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        if ( Schema::hasColumns('bookings', ['total_minutes_duration', 'remaining_hours', 'remaining_minutes', 'remaining_seconds']) ) {
            Schema::dropColumns('bookings', ['total_minutes_duration', 'remaining_hours', 'remaining_minutes', 'remaining_seconds']);
        }

        Schema::enableForeignKeyConstraints();
    }
}
