<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingIdAddConversations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumns('conversations', ['booking_id']) ) {
            Schema::table('conversations', function (Blueprint $table) {
                $table->unsignedBigInteger('booking_id')->nullable()->after('conversation_type_id');

                $table->foreign('booking_id')->references('id')->on('bookings')->cascadeOnDelete();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        if ( Schema::hasColumns('conversations', ['booking_id']) ) {
            Schema::table('conversations', function (Blueprint $table) {
                $table->dropForeign(['booking_id']);
                $table->dropColumn(['booking_id']);
            });
        }

        Schema::enableForeignKeyConstraints();
    }
}
