<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('talent_id')->nullable();
            $table->unsignedBigInteger('moderator_id')->nullable();
            $table->unsignedBigInteger('talent_services_pricing_id')->nullable();
            $table->unsignedBigInteger('booking_status_id')->nullable();
            $table->unsignedBigInteger('booking_payment_status_id')->nullable();
            $table->string('title')->nullable();
            $table->float('amount')->nullable();
            $table->timestamp('time_from')->nullable();
            $table->timestamp('time_to')->nullable();
            $table->mediumText('detail_note')->nullable();
            $table->boolean('reminder_sent')->default(false);
            $table->timestamps();

            $table->foreign('talent_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('moderator_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('talent_services_pricing_id')->references('id')->on('talent_services_pricing')->cascadeOnDelete();
            $table->foreign('booking_status_id')->references('id')->on('booking_status')->cascadeOnDelete();
            $table->foreign('booking_payment_status_id')->references('id')->on('booking_payment_status')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
