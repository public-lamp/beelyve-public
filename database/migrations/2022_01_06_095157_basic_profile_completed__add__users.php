<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BasicProfileCompletedAddUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumns('users', ['basic_profile_completed']) ) {
            Schema::table('users', function (Blueprint $table) {
                $table->boolean('basic_profile_completed')->default(0)->after('profile_completed');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        if ( Schema::hasColumns('users', ['basic_profile_completed']) ) {
            Schema::dropColumns('users', ['basic_profile_completed']);
        }

        Schema::enableForeignKeyConstraints();
    }
}
