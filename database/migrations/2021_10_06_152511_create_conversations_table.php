<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('initiated_from')->nullable();
            $table->unsignedBigInteger('initiated_to')->nullable();
            $table->unsignedBigInteger('conversation_type_id')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();

            $table->foreign('initiated_from')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('initiated_to')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('conversation_type_id')->references('id')->on('conversation_types')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
