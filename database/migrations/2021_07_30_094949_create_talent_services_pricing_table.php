<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTalentServicesPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_services_pricing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('services_pricing_type_id')->nullable();
            $table->unsignedBigInteger('services_pricing_slot_id')->nullable();
            $table->float('price')->nullable();
            $table->json('main_features')->nullable();
            $table->mediumText('description')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('services_pricing_type_id')->references('id')->on('services_pricing_types')->cascadeOnDelete();
            $table->foreign('services_pricing_slot_id')->references('id')->on('services_pricing_slots')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talent_services_pricing');
    }
}
