<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTimerActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_timer_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('booking_id')->nullable();
            $table->string('last_started_at')->nullable();
            $table->string('last_stopped_at')->nullable();
            $table->string('expected_end_time')->nullable();
            $table->float('total_minutes_time')->nullable();
            $table->float('remaining_minutes_time')->nullable();
            $table->float('served_minutes_time')->nullable();
            $table->integer('remaining_hours')->nullable();
            $table->integer('remaining_minutes')->nullable();
            $table->integer('remaining_seconds')->nullable();
            $table->boolean('in_progress')->nullable()->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('booking_id')->references('id')->on('bookings')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_timer_activities');
    }
}
