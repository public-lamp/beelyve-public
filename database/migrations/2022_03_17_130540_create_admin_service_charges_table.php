<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminServiceChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_service_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_type_id')->nullable();
            $table->unsignedBigInteger('service_charge_type_id')->nullable();
            $table->string('value')->nullable();
            $table->timestamps();

            $table->foreign('service_type_id')->references('id')->on('service_types')->cascadeOnDelete();
            $table->foreign('service_charge_type_id')->references('id')->on('service_charge_types')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_service_charges');
    }
}
