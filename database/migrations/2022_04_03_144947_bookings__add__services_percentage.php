<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingsAddServicesPercentage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumns('bookings', ['service_amount']) ) {
            Schema::table('bookings', function (Blueprint $table) {
                $table->string('service_amount')->nullable()->after('amount');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        if ( Schema::hasColumns('bookings', ['service_amount']) ) {
            Schema::dropColumns('bookings', ['service_amount']);
        }

        Schema::enableForeignKeyConstraints();
    }
}
