<?php

namespace Database\Seeders;

use App\Models\Chat\ConversationType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ConversationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ConversationType::truncate();

        $conversationTypes = [
            [
                'id' => 1,
                'name' => 'Individual',
                'slug' => 'individual'
            ],
            [
                'id' => 2,
                'name' => 'Project Based',
                'slug' => 'projectBased'
            ]
        ];

        foreach ($conversationTypes as $type) {
            ConversationType::create([
                'id' => $type['id'],
                'name' => $type['name'],
                'slug' => $type['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
