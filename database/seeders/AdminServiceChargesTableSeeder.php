<?php

namespace Database\Seeders;

use App\Models\Generic\AdminServiceCharge;
use App\Models\Generic\ServiceChargeType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class AdminServiceChargesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $adminServiceCharges = AdminServiceCharge::all();
        if (count($adminServiceCharges) == 0) {
            Schema::disableForeignKeyConstraints();
            AdminServiceCharge::truncate();

            $conversationTypes = [
                [
                    'id' => 1,
                    'service_type_id' => 1,
                    'service_charge_type_id' => 1,
                    'value' => 20
                ]
            ];

            foreach ($conversationTypes as $type) {
                AdminServiceCharge::create([
                    'id' => $type['id'],
                    'service_type_id' => $type['service_type_id'],
                    'service_charge_type_id' => $type['service_charge_type_id'],
                    'value' => $type['value']
                ]);
            }

            Schema::enableForeignKeyConstraints();
        }
    }
}
