<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Generic\PaymentCategory;

class PaymentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        PaymentCategory::truncate();

        $items = [
            [
                'id' => 1,
                'name' => 'booking',
                'slug' => 'booking'
            ]
        ];

        foreach ($items as $item) {
            PaymentCategory::create([
                'id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
