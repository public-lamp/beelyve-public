<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Talent\ServicesPricingSlot;

class ServicesPricingSlotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ServicesPricingSlot::truncate();

        $servicesSlots = [
            [
                'id' => 1,
                'name' => '15-minutes',
                'slug' => '15_minutes',
                'duration' => 15,
            ],
            [
                'id' => 2,
                'name' => '30-minutes',
                'slug' => '30_minutes',
                'duration' => 30,
            ],
            [
                'id' => 3,
                'name' => '45-minutes',
                'slug' => '45_minutes',
                'duration' => 45,
            ],
            [
                'id' => 4,
                'name' => '60-minutes',
                'slug' => '60_minutes',
                'duration' => 60,
            ],
            [
                'id' => 5,
                'name' => '90-minutes',
                'slug' => '90_minutes',
                'duration' => 90,
            ],
        ];

        foreach ($servicesSlots as $item) {
            ServicesPricingSlot::create([
                'id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug'],
                'duration' => $item['duration']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
