<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Generic\AvailabilityCategory;

class AvailabilityCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        AvailabilityCategory::truncate();

        $availCats = ['Everyday & Any time', 'Custom'];

        foreach ($availCats as $availCat) {
            AvailabilityCategory::create(['name' => $availCat]);
        }

        Schema::enableForeignKeyConstraints();

    }
}
