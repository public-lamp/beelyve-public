<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\Generic\Experience;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('experiences')->truncate();

        $experiences = ['Under 2 years', '2-5 years','5-10 years','10+ years'];

        foreach ($experiences as $experience) {
            Experience::create(['name' => $experience]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
