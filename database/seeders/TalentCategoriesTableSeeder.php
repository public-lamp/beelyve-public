<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Talent\TalentCategory;

class TalentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        TalentCategory::truncate();

        $talentCategories = [
            [
                'id' => 1,
                'name' => 'Actor',
                'key' => 'actor',
                'image' => asset('project-assets/images/service_categories/actor.jpg'),
            ],
            [
                'id' => 2,
                'name' => 'Artist',
                'key' => 'artist',
                'image' => asset('project-assets/images/service_categories/artist.jpg'),
            ],
            [
                'id' => 3,
                'name' => 'Athlete',
                'key' => 'athlete',
                'image' => asset('project-assets/images/service_categories/athlete.jpg'),
            ],
            [
                'id' => 4,
                'name' => 'Beauty Specialist',
                'key' => 'beautySpecialist',
                'image' => asset('project-assets/images/service_categories/beauty.jpg'),
            ],
            [
                'id' => 5,
                'name' => 'Chef',
                'key' => 'chef',
                'image' => asset('project-assets/images/service_categories/chef.jpg'),
            ],
            [
                'id' => 6,
                'name' => 'Comedian',
                'key' => 'comedian',
                'image' => asset('project-assets/images/service_categories/comedian.jpg'),
            ],
            [
                'id' => 7,
                'name' => 'Commentator',
                'key' => 'commentator',
                'image' => asset('project-assets/images/service_categories/commentator.jpg'),
            ],
            [
                'id' => 8,
                'name' => 'Creator/Influencer',
                'key' => 'creator/Influencer',
                'image' => asset('project-assets/images/service_categories/influencer.jpg'),
            ],
            [
                'id' => 9,
                'name' => 'Editor',
                'key' => 'editor',
                'image' => asset('project-assets/images/service_categories/editor.jpg'),
            ],
            [
                'id' => 10,
                'name' => 'Educator',
                'key' => 'educator',
                'image' => asset('project-assets/images/service_categories/educator.jpg'),
            ],
            [
                'id' => 11,
                'name' => 'Entrepreneur',
                'key' => 'entrepreneur',
                'image' => asset('project-assets/images/service_categories/enterpreneur.jpg'),
            ],
            [
                'id' => 12,
                'name' => 'Gamer',
                'key' => 'gamer',
                'image' => asset('project-assets/images/service_categories/gamer.jpg'),
            ],
            [
                'id' => 13,
                'name' => 'Health/Wellness Specialist',
                'key' => 'health/WellnessSpecialist',
                'image' => asset('project-assets/images/service_categories/health.jpg'),
            ],
            [
                'id' => 14,
                'name' => 'Investor',
                'key' => 'investor',
                'image' => asset('project-assets/images/service_categories/investor.jpg'),
            ],
            [
                'id' => 15,
                'name' => 'Model',
                'key' => 'model',
                'image' => asset('project-assets/images/service_categories/model.jpg'),
            ],
            [
                'id' => 16,
                'name' => 'Musician',
                'key' => 'musician',
                'image' => asset('project-assets/images/service_categories/musician.jpg'),
            ],
            [
                'id' => 17,
                'name' => 'Photographer',
                'key' => 'photographer',
                'image' => asset('project-assets/images/service_categories/photographer.jpg'),
            ],
            [
                'id' => 18,
                'name' => 'Public Speaker',
                'key' => 'publicSpeaker',
                'image' => asset('project-assets/images/service_categories/public_speaker.jpg'),
            ],
            [
                'id' => 19,
                'name' => 'Reality TV Star',
                'key' => 'realityTVStar',
                'image' => asset('project-assets/images/service_categories/reality_star.jpg'),
            ],
            [
                'id' => 20,
                'name' => 'Technology',
                'key' => 'technology',
                'image' => asset('project-assets/images/service_categories/technology.jpg'),
            ],
            [
                'id' => 21,
                'name' => 'Writer',
                'key' => 'writer',
                'image' => asset('project-assets/images/service_categories/writer.jpg'),
            ],
            [
                'id' => 22,
                'name' => 'Other',
                'key' => 'other',
                'image' => asset('project-assets/images/service_categories/other.jpg'),
            ],
        ];

        TalentCategory::insert($talentCategories);

    }

}
