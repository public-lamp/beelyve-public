<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Generic\WeekDay;

class WeekDaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        WeekDay::truncate();

        $users = [
            [
                'id' => 1,
                'name' => 'Monday',
                'slug' => 'monday',
                "order" => 1
            ],
            [
                'id' => 2,
                'name' => 'Tuesday',
                'slug' => 'tuesday',
                "order" => 2
            ],
            [
                'id' => 3,
                'name' => 'Wednesday',
                'slug' => 'wednesday',
                "order" => 3
            ],
            [
                'id' => 4,
                'name' => 'Thursday',
                'slug' => 'thursday',
                "order" => 4
            ],
            [
                'id' => 5,
                'name' => 'Friday',
                'slug' => 'friday',
                "order" => 5
            ],
            [
                'id' => 6,
                'name' => 'Saturday',
                'slug' => 'saturday',
                "order" => 6
            ],
            [
                'id' => 7,
                'name' => 'Sunday',
                'slug' => 'sunday',
                "order" => 7
            ]
        ];

        foreach ($users as $user) {
            WeekDay::create([
                'id' => $user['id'],
                "name" => $user['name'],
                "slug" => $user['slug'],
                'order' => $user['order']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
