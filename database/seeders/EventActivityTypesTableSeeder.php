<?php

namespace Database\Seeders;

use App\Models\Generic\Event\EventActivityType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class EventActivityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        EventActivityType::truncate();

        $items = [
            [
                'id' => 1,
                'name' => 'Talent Present',
                'slug' => 'talent_present',
                'about' => 'Talent is present',
            ],
            [
                'id' => 2,
                'name' => 'Talent completed the event',
                'slug' => 'talent_completedEvent',
                'about' => 'Talent completed the event',
            ],
            [
                'id' => 3,
                'name' => 'Time started',
                'slug' => 'time_started',
                'about' => 'Time started',
            ],
            [
                'id' => 4,
                'name' => 'Time paused',
                'slug' => 'time_paused',
                'about' => 'Time paused',
            ],
            [
                'id' => 5,
                'name' => 'Time resumed',
                'slug' => 'time_resumed',
                'about' => 'Time resumed',
            ],
            [
                'id' => 6,
                'name' => 'Time completed',
                'slug' => 'time_completed',
                'about' => 'Time completed',
            ],
            [
                'id' => 7,
                'name' => 'Moderator confirm the talent presence',
                'slug' => 'moderator_confirmTalentPresence',
                'about' => 'Moderator confirms the presence',
            ],
            [
                'id' => 8,
                'name' => 'Moderator approved the event completion',
                'slug' => 'moderator_approvedEventCompletion',
                'about' => 'Moderator approved the event completion',
            ],
        ];

        foreach ($items as $item) {
            EventActivityType::create([
                'id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug'],
                'about' => $item['about'],
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
