<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Talent\ServicesPricingType;

class ServicesPricingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ServicesPricingType::truncate();

        $servicesPricingTypes = [
            [
                'id' => 1,
                'name' => 'Basic',
                'slug' => 'basic',
            ],
            [
                'id' => 2,
                'name' => 'Standard',
                'slug' => 'standard',
            ],
            [
                'id' => 3,
                'name' => 'Premium',
                'slug' => 'premium',
            ],
        ];

        foreach ($servicesPricingTypes as $item) {
            ServicesPricingType::create([
                'id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
