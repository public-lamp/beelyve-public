<?php

namespace Database\Seeders;

use App\Models\Generic\BookingPaymentStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class BookingPaymentStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        BookingPaymentStatus::truncate();

        $status = [
            [
                'id' => 1,
                'name' => 'Not initiated',
                'slug' => 'notInitiated',
                'about' => 'Payment not initiated yet by moderator',
            ],
            [
                'id' => 2,
                'name' => 'Buyer initiated',
                'slug' => 'buyerInitiated',
                'about' => 'Payment initiated by moderator',
            ],
            [
                'id' => 3,
                'name' => 'Received in escrow',
                'slug' => 'receivedInEscrow',
                'about' => 'Payment is received in escrow',
            ],
            [
                'id' => 4,
                'name' => 'Initiated to seller',
                'slug' => 'initiatedToSeller',
                'about' => 'Payment is initiated to seller',
            ],
            [
                'id' => 5,
                'name' => 'Received by seller',
                'slug' => 'initiatedToSeller',
                'about' => 'Payment is initiated to seller',
            ],
            [
                'id' => 6,
                'name' => 'completed',
                'slug' => 'completed',
                'about' => 'Payment process is completed',
            ],
            [
                'id' => 7,
                'name' => 'Initiated back to Client',
                'slug' => 'initiatedBackToClient',
                'about' => 'Charge back payout attempted to Client Stripe connect-account',
            ]
        ];

        foreach ($status as $item) {
            BookingPaymentStatus::create([
                'id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug'],
                'about' => $item['about'],
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
