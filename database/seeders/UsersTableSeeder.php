<?php

namespace Database\Seeders;

use App\Models\Auth\UserRole;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        Schema::disableForeignKeyConstraints();
        User::whereIn('id', [1, 2, 3])->forceDelete();
        UserRole::whereIn('user_id', [1, 2, 3])->forceDelete();

        $users = [
            [
                'id' => 1,
                'role_ids' => [1],
                "user_name" => "TestAdmin",
                "first_name" => "Test",
                "last_name" => "Admin",
                "email" => "admin@example.com",
                'mobile' => '+18581234123',
                "password" => bcrypt("123456"),
                'image' => asset('project-assets/images/profile_avatar.png'),
                "email_verified_at" => date('Y-m-d H:i:s'),
                "profile_completed" => false,
                'time_zone' => 'UTC'
            ],
            [
                'id' => 2,
                'role_ids' => [2],
                "user_name" => "TestTalent",
                "first_name" => "Test",
                "last_name" => "Talent",
                "email" => "talent@example.com",
                'mobile' => '+18581234124',
                "password" => bcrypt("123456"),
                'image' => asset('project-assets/images/profile_avatar.png'),
                "email_verified_at" => date('Y-m-d H:i:s'),
                "profile_completed" => false,
                'time_zone' => 'UTC'
            ],
            [
                'id' => 3,
                'role_ids' => [3],
                "user_name" => "TestModerator",
                "first_name" => "Test",
                "last_name" => "Moderator",
                "email" => "moderator@example.com",
                'mobile' => '+18581234125',
                "password" => bcrypt("123456"),
                'image' => asset('project-assets/images/profile_avatar.png'),
                "email_verified_at" => date('Y-m-d H:i:s'),
                "profile_completed" => false,
                'time_zone' => 'UTC'
            ],
        ];
        foreach ($users as $user) {
            User::create([
                'id' => $user['id'],
                "user_name" => $user['user_name'],
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'email' => $user['email'],
                'mobile' => $user['mobile'],
                'password' => $user['password'],
                'image' => $user['image'],
                'email_verified_at' => $user['email_verified_at'],
                'profile_completed' => $user['profile_completed'],
                'time_zone' => $user['time_zone'],
            ]);
            foreach ($user['role_ids'] as $role) {
                UserRole::create([
                    'role_id' => $role,
                    'user_id' => $user['id'],
                ]);
            }
        }

        Schema::enableForeignKeyConstraints();
    }

}
