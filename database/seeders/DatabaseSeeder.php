<?php

namespace Database\Seeders;

use App\Models\Generic\BookingPaymentStatus;
use App\Models\Generic\Event\BookingEventActivity;
use App\Models\Generic\ServiceType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(ExperiencesTableSeeder::class);
        $this->call(TalentCategoriesTableSeeder::class);
        $this->call(AvailabilityCategoriesTableSeeder::class);
        $this->call(WeekDaysTableSeeder::class);
        $this->call(TalentSearchFiltersTableSeeder::class);
        $this->call(ServicesPricingSlotsTableSeeder::class);
        $this->call(ServicesMediumsTableSeeder::class);
        $this->call(ServicesPricingTypesTableSeeder::class);
        $this->call(BookingStatusTableSeeder::class);
        $this->call(BookingPaymentStatusTableSeeder::class);
        $this->call(PaymentCategoriesTableSeeder::class);
        $this->call(MessageTypesTableSeeder::class);
        $this->call(ConversationTypesTableSeeder::class);
        $this->call(SocialProviderNamesTableSeeder::class);
        $this->call(EventActivityTypesTableSeeder::class);
        $this->call(PayoutStatusTableSeeder::class);
        $this->call(ServiceTypesTableSeeder::class);
        $this->call(ServiceChargeTypesTableSeeder::class);
        $this->call(AdminServiceChargesTableSeeder::class);
    }

}
