<?php

namespace Database\Seeders;

use App\Models\Auth\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Role::truncate();

        $roles = [
            [
                'id' => 1,
                'name' => 'Admin',
                'key' => 'admin'
            ],
            [
                'id' => 2,
                'name' => 'Talent',
                'key' => 'talent'
            ],
            [
                'id' => 3,
                'name' => 'Client',
                'key' => 'client'
            ],

        ];

        foreach ($roles as $role) {
            Role::create([
                'id' => $role['id'],
                'name' => $role['name'],
                'key' => $role['key']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
