<?php

namespace Database\Seeders;

use App\Models\Generic\ServiceChargeType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ServiceChargeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ServiceChargeType::truncate();

        $conversationTypes = [
            [
                'id' => 1,
                'name' => 'Percentage',
                'slug' => 'percentage'
            ]
        ];

        foreach ($conversationTypes as $type) {
            ServiceChargeType::create([
                'id' => $type['id'],
                'name' => $type['name'],
                'slug' => $type['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
