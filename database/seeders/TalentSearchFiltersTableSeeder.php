<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Generic\TalentSearchFilter;

class TalentSearchFiltersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        TalentSearchFilter::truncate();

        $searchFilters = [
            [
                'id' => 1,
                'parent_filter_name' => null,
                'parent_filter_key' => null,
                'name' => 'Best Rated',
                'key' => 'bestRated',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 2,
                'parent_filter_name' => null,
                'parent_filter_key' => null,
                'name' => 'Most Popular',
                'key' => 'mostPopular',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 3,
                'parent_filter_name' => 'Prices',
                'parent_filter_key' => 'prices',
                'name' => 'High to low',
                'key' => 'highToLow',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 4,
                'parent_filter_name' => 'Prices',
                'parent_filter_key' => 'prices',
                'name' => 'Low to High',
                'key' => 'lowToHigh',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 5,
                'parent_filter_name' => 'Price',
                'parent_filter_key' => 'price',
                'name' => 'Free',
                'key' => 'free',
                'min_value' => 0,
                'max_value' => 0,
            ],
            [
                'id' => 6,
                'parent_filter_name' => 'Price',
                'parent_filter_key' => 'price',
                'name' => '$1-$50',
                'key' => '$1-$50',
                'min_value' => 1,
                'max_value' => 50,
            ],
            [
                'id' => 7,
                'parent_filter_name' => 'Price',
                'parent_filter_key' => 'price',
                'name' => '$51-$100',
                'key' => '$51-$100',
                'min_value' => 51,
                'max_value' => 100,
            ],
            [
                'id' => 8,
                'parent_filter_name' => 'Price',
                'parent_filter_key' => 'price',
                'name' => '$100-$200',
                'key' => '$100-$200',
                'min_value' => 100,
                'max_value' => 200,
            ],
            [
                'id' => 9,
                'parent_filter_name' => 'Price',
                'parent_filter_key' => 'price',
                'name' => '$201-$400',
                'key' => '$201-$400',
                'min_value' => 201,
                'max_value' => 400,
            ],
            [
                'id' => 10,
                'parent_filter_name' => 'Price',
                'parent_filter_key' => 'price',
                'name' => '$401+',
                'key' => '$401+',
                'min_value' => 400,
                'max_value' => null,
            ],
            [
                'id' => 11,
                'parent_filter_name' => 'Rating',
                'parent_filter_key' => 'rating',
                'name' => '4 star and up',
                'key' => '4StarAndUp',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 12,
                'parent_filter_name' => 'Rating',
                'parent_filter_key' => 'rating',
                'name' => '3 star and up',
                'key' => '3StarAndUp',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 13,
                'parent_filter_name' => 'Rating',
                'parent_filter_key' => 'rating',
                'name' => '2 star and up',
                'key' => '2StarAndUp',
                'min_value' => null,
                'max_value' => null,
            ],
            [
                'id' => 14,
                'parent_filter_name' => 'Rating',
                'parent_filter_key' => 'rating',
                'name' => '1 star and up',
                'key' => '1StarAndUp',
                'min_value' => null,
                'max_value' => null,
            ],
        ];

        foreach ($searchFilters as $item) {
            TalentSearchFilter::create([
                'id' => $item['id'],
                'parent_filter_name' => $item['parent_filter_name'],
                'parent_filter_key' => $item['parent_filter_key'],
                'name' => $item['name'],
                'key' => $item['key']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
