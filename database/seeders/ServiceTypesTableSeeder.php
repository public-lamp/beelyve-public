<?php

namespace Database\Seeders;

use App\Models\Generic\ServiceType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ServiceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ServiceType::truncate();

        $conversationTypes = [
            [
                'id' => 1,
                'name' => 'Booking Event',
                'slug' => 'bookingEvent'
            ]
        ];

        foreach ($conversationTypes as $type) {
            ServiceType::create([
                'id' => $type['id'],
                'name' => $type['name'],
                'slug' => $type['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
