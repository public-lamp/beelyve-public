<?php

namespace Database\Seeders;

use App\Models\Chat\MessageType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class MessageTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        MessageType::truncate();

        $messageTypes = [
            [
                'id' => 1,
                'name' => 'Text',
                'slug' => 'text'
            ],
            [
                'id' => 2,
                'name' => 'Image',
                'slug' => 'image'
            ],
            [
                'id' => 3,
                'name' => 'File',
                'slug' => 'file'
            ],
            [
                'id' => 4,
                'name' => 'Emoji',
                'slug' => 'emoji'
            ],

        ];

        foreach ($messageTypes as $type) {
            MessageType::create([
                'id' => $type['id'],
                'name' => $type['name'],
                'slug' => $type['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
