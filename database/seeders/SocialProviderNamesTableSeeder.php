<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Generic\SocialProviderName;

class SocialProviderNamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        SocialProviderName::truncate();

        $messageTypes = [
            [
                'id' => 1,
                'name' => 'Facebook',
                'slug' => 'facebook'
            ],
            [
                'id' => 2,
                'name' => 'Instagram',
                'slug' => 'instagram'
            ]
        ];

        foreach ($messageTypes as $type) {
            SocialProviderName::create([
                'id' => $type['id'],
                'name' => $type['name'],
                'slug' => $type['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
