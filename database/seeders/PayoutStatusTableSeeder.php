<?php

namespace Database\Seeders;

use App\Models\Auth\Role;
use App\Models\PayoutStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class PayoutStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        PayoutStatus::truncate();

        $payoutStatus = [
            [
                'id' => 1,
                'status' => 'pending',
                'slug'=>'Payout is pending'
            ],
            [
                'id' => 2,
                'status' => 'low_balance',
                'slug' => 'InSufficient balance in account'
            ],
            [
                'id' => 3,
                'status' => 'completed',
                'slug' => 'Payout completed successfully'
            ],
            [
                'id' => 4,
                'status' => 'stripe_account_not_created',
                'slug' => 'User Stripe Account Is Not Created'
            ],
            [
                'id' => 5,
                'status' => 'failed',
                'slug' => 'Unable to process the payout'
            ],

        ];

        foreach ($payoutStatus as $status) {
            PayoutStatus::create([
                'id' => $status['id'],
                'status' => $status['status'],
                'slug' => $status['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
