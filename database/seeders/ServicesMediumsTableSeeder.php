<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\Talent\ServicesMedium;

class ServicesMediumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        ServicesMedium::truncate();

        $servicesSlots = [
            [
                'id' => 1,
                'name' => '1 on 1',
                'slug' => '1on1',
            ],
            [
                'id' => 2,
                'name' => 'Social Media Live',
                'slug' => 'SocialMediaLive',
            ],
            [
                'id' => 3,
                'name' => 'Virtual Event',
                'slug' => 'VirtualEvent',
            ],

//            [
//                'id' => 1,
//                'name' => 'Zoom',
//                'slug' => 'zoom',
//            ],
//            [
//                'id' => 2,
//                'name' => 'Skype',
//                'slug' => 'skype',
//            ],
//            [
//                'id' => 3,
//                'name' => 'ClubHouse',
//                'slug' => 'clubHouse',
//            ],
//            [
//                'id' => 4,
//                'name' => 'Instagram Live',
//                'slug' => 'instagramLive',
//            ],
//            [
//                'id' => 5,
//                'name' => 'Google Meets',
//                'slug' => 'googleMeets',
//            ],
//            [
//                'id' => 6,
//                'name' => 'Discord',
//                'slug' => 'discord',
//            ],
//            [
//                'id' => 7,
//                'name' => 'Twitter spaces',
//                'slug' => 'twitterSpaces',
//            ],
//            [
//                'id' => 8,
//                'name' => 'Twitch',
//                'slug' => 'twitch',
//            ],
        ];

        foreach ($servicesSlots as $item) {
            ServicesMedium::create([
                'id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug']
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
