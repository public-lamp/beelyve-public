<?php

namespace Database\Seeders;

use App\Models\Generic\BookingStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class BookingStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        BookingStatus::truncate();

        $roles = [
            [
                'id' => 1,
                'name' => 'Being Created',
                'slug' => 'beingCreated',
                'about' => 'Task is being created by moderator',
            ],
            [
                'id' => 2,
                'name' => 'Created',
                'slug' => 'created',
                'about' => 'Task is created by moderator',
            ],
            [
                'id' => 3,
                'name' => 'Booking Requested',
                'slug' => 'bookingRequested',
                'about' => 'Task visible to talent after creation by moderator',
            ],
            [
                'id' => 4,
                'name' => 'Accepted',
                'slug' => 'accepted',
                'about' => 'Talent accepted the booking',
            ],
            [
                'id' => 5,
                'name' => 'Rejected',
                'slug' => 'rejected',
                'about' => 'Talent rejected the booking',
            ],
            [
                'id' => 6,
                'name' => 'In-progress',
                'slug' => 'inProgress',
                'about' => 'Booking event started and in progress',
            ],
            [
                'id' => 7,
                'name' => 'Pending Approval',
                'slug' => 'pendingApproval',
                'about' => 'Talent marked the booking event completed and approval pending from moderator side',
            ],
            [
                'id' => 8,
                'name' => 'Completed & Review Pending',
                'slug' => 'completedReviewPending',
                'about' => 'Moderator approved the event closing and Review is pending',
            ],
            [
                'id' => 9,
                'name' => 'disputed',
                'slug' => 'disputed',
                'about' => 'Moderator did not accepted the event closing',
            ],
            [
                'id' => 10,
                'name' => 'Closed',
                'slug' => 'closed',
                'about' => 'Booking event is completely closed',
            ]
        ];

        foreach ($roles as $role) {
            BookingStatus::create([
                'id' => $role['id'],
                'name' => $role['name'],
                'slug' => $role['slug'],
                'about' => $role['about'],
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
