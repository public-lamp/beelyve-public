$(document).ready(function() {

    // <!-- home slider -->
    $('.banner_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,

            }
        }
    });

    // <!-- feature week slider -->
    $('.featureWeek_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout: 5100,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 4,

            }
        }
    });

    // <!-- testimonial slider slider -->
    $('.testimonial_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5100,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,

            }
        }
    });

    // <!-- latest news slider -->
    $('.latest_news_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,

            }
        }
    });
});
