// select2 multi-tag select



$(document).ready(function() {


    $("#pricing_basic_main_features").focus(function() {
        if( this.value === '' ) {
            this.value +='• ';
        }
    });
    $("#pricing_standard_main_features").focus(function() {
        if( this.value === '' ) {
            this.value +='• ';
        }
    });
    $("#pricing_premium_main_features").focus(function() {
        if( this.value === '' ) {
            this.value +='• ';
        }
    });

    $("#pricing_basic_main_features").keyup(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            this.value +='• ';
        }
        var txtval = this.value;
        if(txtval.substr(txtval.length - 1) == '\n'){
            this.value = txtval.substring(0,txtval.length - 1);
        }
    });
    $("#pricing_standard_main_features").keyup(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            this.value +='• ';
        }
        var txtval = this.value;
        if(txtval.substr(txtval.length - 1) == '\n'){
            this.value = txtval.substring(0,txtval.length - 1);
        }
    });
    $("#pricing_premium_main_features").keyup(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            this.value +='• ';
        }
        var txtval = this.value;
        if(txtval.substr(txtval.length - 1) == '\n'){
            this.value = txtval.substring(0,txtval.length - 1);
        }
    });



    // search bar shw

    $('.searchIcon').click(function() {
        $('body').addClass('hiddden');
        $('.search_popout').fadeIn();
    });

    $('.close_search').click(function() {
        $('body').removeClass('hiddden');
        $('.search_popout').hide();
    });





	$('.menuIcon').click(function() {
        $('.menu_nav').slideDown();
    });

   $('.closeNav').click(function() {
        $('.menu_nav').slideUp();
    });


    // dashboard menu funtion 

    $('.menuIcon').click(function() {
        $('body').addClass("showNav");
    });

   $('.closeNav').click(function() {
        $('body').removeClass("showNav");
    });




    $('.upload_logo label input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $('.upload_logo b').text(fileName);
    });
    $('.upload_icon label input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $('.upload_icon b').text(fileName);
    });


    //select date time
    $('.main_parents').hide();
    $('.days_tab input[type="radio"][class="days_radio"]').on('click', function () {
        $('.labelo').removeClass('active');
        $('.main_parents').hide();
        $(this).parent('label').addClass('active');
        $("#"+$(this).data('child')).show();
    });


    $('.avaiable_days input[type="radio"][class="time_radio"]').on('click', function () {
        if($(this).val() == 'enter_time')
        {
            $(this).parent('div.sound-signal').siblings('div.time_hide').removeClass('hide_div');
        } else {
            $(this).parent('div.sound-signal').siblings('div.time_hide').addClass('hide_div');
        }
    });


    // talent dashboard, custom availability's time section append
    let counter = 0;
    $('.add_hours').click(function() {
        counter++;

        var data = $(this).data('name');
        let from_time_id = 'days_'+data+'_time_time_from_'+counter;
        let to_time_id = 'days_'+data+'_time_time_to_'+counter;

        var name_from= `days[${data}][time][time_from][]`;
        var name_to= `days[${data}][time][time_to][]`;

        var html = '<div class="time_sec time_hide">';
        html +='<div class="price_from">';
        html += ' <span> Time From</span>';
        html += '<input type="text" class="input_price_from plugin_date uiFromTimePicker" autocomplete="off" placeholder="00:00" name="'+name_from+'" id="'+from_time_id+'" >';
        html += '</div>';
        html += '<div class="price_from time_to">';
        html += '<span> Time To</span>';
        html += '<input type="text" class="input_price_from plugin_date uiToTimePicker" autocomplete="off" placeholder="00:00" name="'+name_to+'" id="'+to_time_id+'">';
        html += '</div>';
        html += '<div class="add_hours_btn">';
        html += '<div class="cssCircle subtract_hours">';
        html += 'Remove';
        html += '</div>';
        html += '</div></div>';

        $(this).parents('div.main_parents').append(html);

        // initialise timepicker
        $(`#${from_time_id}`).timepicker({
            timeFormat: 'hh:mm p',
            interval: 15,
            minTime: '12:00 AM',
            maxTime: '11:59 PM',
            startTime: '12:00 AM',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
        $(`#${to_time_id}`).timepicker({
            timeFormat: 'hh:mm p',
            interval: 15,
            minTime: '12:00 AM',
            maxTime: '11:59 PM',
            startTime: '12:00 AM',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

    });


    $(document).on('click','.subtract_hours',function() {
        $(this).parents('div.time_hide').remove();
    });


    $('.availability_options label input[type="radio"]').change(function() {
        let availCategoryId = $(this).val();
        if( availCategoryId == 1 ) {
            $('.avaiable_days').hide();
        }
        if( availCategoryId == 2 ){
            $('.avaiable_days').slideDown();
        }
    });


    if ($(".selectTime_info .chooseOptionDetail input").is(":checked")) {
        $('label.checked').removeClass('checked');
        $(this).parents(".chooseOptionDetail").addClass("checked");
    }

    // instantiate timepicker
    $('.uiFromTimePicker').timepicker({
        timeFormat: 'hh:mm p',
        interval: 15,
        minTime: '12:00 AM',
        maxTime: '11:59 PM',
        startTime: '12:00 AM',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $('.uiToTimePicker').timepicker({
        timeFormat: 'hh:mm p',
        interval: 15,
        minTime: '12:00 AM',
        maxTime: '11:59 PM',
        startTime: '12:00 AM',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });


    $('.selectTime_info .option-input').click(function() {
        $(".chooseOptionDetail").removeClass('checked_item');
        $(this).parents(".chooseOptionDetail").addClass('checked_item');

        $('.option-input').removeAttr('checked','checked' );
        $(this).parents('.chooseOptionDetail').find('form .option-input').attr('checked','checked' );

    });


    $('.faq_btn').click(function(){
        $(this).parent('.faq_info').toggleClass('show_faq');
        $(this).parent('.faq_info').find('.faq_info_show').stop().slideToggle();
    });


    // <!-- home slider -->
    $('.banner_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 0,
        autoplay: false,
        autoplaySpeed: 1000,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,

            }
        }
    });


    <!-- feature week slider -->
    $('.featureWeek_slider').owlCarousel({
        nav: false,
        loop: false,
        margin: 10,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5100,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
                margin: 10,
            },
            1000: {
                items: 4,

            },
            1400: {
                items: 4,

            }
        }
    });




    <!-- feature week slider -->
    $('.cattegories').owlCarousel({
        nav: true,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 4100,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 4,
                margin: 10,
            },
            1000: {
                items: 7,

            },
            1400: {
                items: 7,

            }
        }
    });





    <!-- testimonial slider slider -->
    $('.testimonial_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5100,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,

            }
        }
    });


    <!-- latest news slider -->
    $('.latest_news_slider').owlCarousel({
        nav: true,
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,

            }
        }
    });


    <!-- my work  slider -->
    $('.slider-for_video').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav_video'
    });
    $('.slider-nav_video').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for_video',
        dots: true,
        //centerMode: true,
        focusOnSelect: true,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            // centerMode: true,

          }

        }, {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            infinite: true,

          }
        },  {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
          }
        }]
    });






    $('.icon_list_grid a').click(function(){
        $('.available_feature_section').toggleClass('showList');
    });


    $('.toggleAerow').click(function(){
        $(this).parent('.how_work_steps_info').toggleClass('show_accordian');
    });

});

// video content popup
function videoPopup(objectName, videoUrl) {
    this.videoUrl = videoUrl;
    this.screenSize = $(window).width();
    this.heightOfVideo = this.screenSize * .4;
    this.videoPop = '<div class="popUpWrapper">' +
        '<div id="videoWrap"">' +
        '<button type="button" class="videoClose" onclick="' + objectName + '.closeVideo()">X</button>' +
        '<iframe width="100%" height="' + this.heightOfVideo + '" src="' + this.videoUrl + '" frameborder="0" allowfullscreen></iframe></div>' +
        '</div>',
        this.closeVideo = function() {
            $('.blackOut').fadeOut('slow');
            $('.popupAlignCenter').html("");
        },
        this.launchPopUp = function() {
            $(window).scroll(function() { return false; });
            if ($('.blackOut').css('display') == "none") {
                $('.blackOut').fadeIn('slow');
            }
            $('.blackOut').css('z-index', '1000');
            $('.popupAlignCenter').html(this.videoPop);
            $('.popUpWrapper').fadeIn('slow');
        }
}


$(document).mouseup(function(e) {
    var container = $(".popUpWrapper");
    if ($('.blackOut').css('z-index') != "0") {
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.blackOut').fadeOut('slow');
        }
    }
});


// var videoPopupItem = new videoPopup('videoPopupItem', 'https://www.youtube.com/embed/BA-uYgkrhO0');
var videoPopupItem = new videoPopup('videoPopupItem', 'https://www.youtube.com/embed/dtvKG2K19iY');
// video content popup


// tabs
$('.event_activity_tabs ul li a').on('click', function(event) {
    event.preventDefault();

    $('.activity_active').removeClass('activity_active');
    $(this).addClass('activity_active');
    $('.chatBox_tabs').hide();
    $($(this).attr('href')).show();
});
$('.event_activity_tabs  ul li a:first').trigger('click');



// tabs chats
$('.dashboard_tabs ul li a').on('click', function(event) {
    event.preventDefault();

    $('.activeTab').removeClass('activeTab');
    $(this).addClass('activeTab');
    $('.dashboard_tabs_data').hide();
    $($(this).attr('href')).show();
});
$('.dashboard_tabs ul li a:first').trigger('click');




 // tabs Order
$('.order_booktabs ul li a').on('click', function(event) {
    event.preventDefault();

    $('.orderTab_active').removeClass('orderTab_active');
    $(this).addClass('orderTab_active');
    $('.order_booktabs_data').hide();
    $($(this).attr('href')).show();
});
$('.order_booktabs ul li a:first').trigger('click');


// tabs services
$('.services_tabs ul li a').on('click', function(event) {
    event.preventDefault();
    $('.tab_highlight').removeClass('tab_highlight');
    $(this).addClass('tab_highlight');
    $('.services_tabs_data').hide();
    $($(this).attr('href')).show();
});
$('.services_tabs ul li a:first').trigger('click');


//tabs update profile
$('.update_profile_tabs ul li a').on('click', function(event) {
    event.preventDefault();

    $('.updateTab_active').removeClass('updateTab_active');
        $(this).addClass('updateTab_active');
        $('.update_profile_data').hide();
        $($(this).attr('href')).show();
    });
$('.update_profile_tabs ul li a:first').trigger('click');


$('.show_next').click(function(){
    $(this).parent('.viewMore').hide();
    $('.update_profile_tabs ul li a').removeClass('updateTab_active');
    $('.update_profile_tabs ul li:nth-child(2) a').addClass('updateTab_active');
    $('#updateTab_1').hide();
    $('#updateTab_2').show();
});





// read file, file selection handling (talent profile)
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


// talent dashboard main page, todos button close action
$('#talentDashTodosClose').click( function () {
    $('#talentDashTodosSection').hide();
})
