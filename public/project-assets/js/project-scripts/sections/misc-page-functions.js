
// return blockUi or UnBlockUi method
function blockUi( block = true) {

    if ( Boolean(block) ) {
        $('#custom_loader').show();
    }
    if ( !Boolean(block) ) {
        $('#custom_loader').hide();
    }

}

// return blockUi or UnBlockUi method
function toasterSuccess( successToaster = true, message) {

    if ( Boolean(successToaster) ) {
        window.toastr.success( message );
    } else {
        window.toastr.error( message );
    }

}

// old block ui method, not in use now
function blockUiOld( block = true) {
    // blockUi section
    const blockUiSection = $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    if ( Boolean(block) ) {
        return blockUiSection;
    }
    $.unblockUI();
}
