// return blockUi or UnBlockUi method
function validationText( valueText, type ) {
    let error = `<span class="danger errorAlertText text-danger" style="font-size: 12px;">${valueText}</span>`;
    let warning = `<span class="danger errorAlertText text-warning" style="font-size: 12px;">${valueText}</span>`;

    if ( type === 'error' ) {
        return error;
    }
    else if (type === 'warning') {
        return warning;
    }
}
