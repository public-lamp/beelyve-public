<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;

class GenericController extends Controller
{
    // todo, not being used in project for now, may remove it
    public function calculateTime(Request $request) {
        $timeStr = $request->time_string;
        $timeFormat = $request->current_format;
        $requiredFormat = $request->required_format;
        $additionTime = $request->addition_minutes;

        $addition = '+' .$additionTime. ' minutes';
        $responseTime = date( $requiredFormat, strtotime( $addition, strtotime($timeStr) ) );

        return Helper::jsonResponse(1, 'Time converted', ['time' => str_replace(' ', '', $responseTime)], '');
    }
}
