<?php

namespace App\Http\Controllers\Common\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Common\Chat\ChatServices;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{

    // start conversation
    public function startChatConversation(Request $request, ChatServices $chatServ) {
        $errorMessages = [
            'other_user_id.required' => 'Valid user selection is required to start conversation',
            'conversation_type_id.required' => 'Invalid conversation type selection',
        ];
        $schema = [
            'other_user_id' => 'required|integer',
            'conversation_type_id' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $schema, $errorMessages);
        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first());
        }

        return $chatServ->chatConversationStart($request);
    }

    // render Conversation Messages
    public function renderConversationMessages(Request $request, ChatServices $chatServ) {
        $errorMessages = [
            'conversation_id.required' => 'Please select a conversation',
        ];
        $schema = [
            'conversation_id' => 'required|integer',
        ]; 
        $this->validate($request, $schema, $errorMessages);

        return $chatServ->conversationMessagesRender($request);
    }


    // render talent chat conversations page
    public function renderChatConversations(Request $request, ChatServices $chatServ) {
        return $chatServ->chatConversationsRender();
    }

    // render talent chat messages page
    public function renderChatMessages(Request $request, ChatServices $chatServ) {
        return $chatServ->chatMessagesRender();
    }

    // get auth user conversations
    public function conversationsGet(Request $request, ChatServices $chatServ) {
        return $chatServ->getConversations();
    }

    // save and send message
    public function saveMessage(Request $request, ChatServices $chatServ) {
        return $chatServ->messageSave($request);
    }

}
