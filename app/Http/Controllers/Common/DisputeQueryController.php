<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Services\Common\BookingRequestServices;
use App\Services\Common\DisputeQueryServices;
use Illuminate\Http\Request;

class DisputeQueryController extends Controller
{

    // submit dispute query
    public function disputeQuerySubmit( Request $request, DisputeQueryServices $bookingSer) {
        $schema = [
            'booking_id' => 'required|integer',
            'dispute_title' => 'required|max:200',
            'dispute_query' => 'required|max:900',
        ];
        $errorMessages = [
            'booking_id.required' => 'Booking selection is required to submit dispute query',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $data = $bookingSer->submitDisputeQuery($request);
    }

}
