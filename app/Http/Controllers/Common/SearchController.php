<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Services\Common\SearchServices;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    //   return talent dashboard main
    public function searchTalent( Request $request, SearchServices $searchSer) {
        $schema = [
            'search_text' => 'nullable',
            'search_filters' => 'array',
            'search_filters.talent_service_category' => 'nullable',
            'search_filters.sort_by' => 'nullable',
        ];
        $this->validate($request, $schema);

        return $data = $searchSer->talentSearch($request);
    }

}
