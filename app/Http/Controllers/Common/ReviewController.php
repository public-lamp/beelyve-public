<?php

namespace App\Http\Controllers\Common;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Generic\Booking;
use App\Services\Common\ReviewServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{

    // render talent dashboard main
    public function moderatorAddEventReview(Request $request, ReviewServices $dashboardServ) {
        $schema = [
            'booking_id' => 'required|integer',
            'rating' => 'required|integer|max:5',
            'comments' => 'required|max:900',
        ];
        $errorMessages = [
            'booking_id.required' => 'Valid booking selection required',
            'booking_id.integer' => 'Valid booking selection required',
        ];
        $this->validate($request, $schema, $errorMessages);

        $user = Auth::user();
        $booking = Booking::where(['id' => $request->booking_id, 'moderator_id' => $user->id])
            ->whereIn('booking_status_id', [8]) // approved by moderator
            ->first();
        if (!$booking) {
            return Helper::jsonResponse(0, 'Can not perform operation', [], 'Booking not found');
        }

        return $dashboardServ->moderatorSubmitEventReview($request, $booking);
    }

}
