<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Common\BookingRequestServices;

class BookingRequestController extends Controller
{

    //   render talent booking request page
    public function bookingRequestPageRender( Request $request, BookingRequestServices $bookingSer) {
        $schema = [
            'talent_id' => 'required',
            'talent_services_pricing_id' => 'required|integer',
        ];
        $errorMessages = [
            'talent_id.required' => 'Valid talent selection required',
            'talent_services_pricing_id.required' => 'Valid services selection required',
            'talent_services_pricing_id.integer' => 'Valid services selection required',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $data = $bookingSer->renderBookingRequestPage($request);
    }


    //    Booking-date selection
    public function bookingRequestSelectDate( Request $request, BookingRequestServices $bookingSer) {
        $schema = [
            'date' => 'required|date_format:Y-m-d',
        ];
        $errorMessages = [
            'date.required' => 'Please select booking date',
            'date.date_format' => 'Valid formatted date selection is required',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $data = $bookingSer->selectBookingRequestDate($request);
    }


    //    Booking-date selection
    public function bookingRequestAvailableSlotsRender( Request $request, BookingRequestServices $bookingSer) {
        return $data = $bookingSer->renderBookingRequestAvailableSlots($request);
    }


    //    Booking time-slot selection
    public function bookingSlotSelection( Request $request, BookingRequestServices $bookingSer) {
        $schema = [
            'time_slot' => 'required|array',
            'time_slot.start_time' => 'required|date_format:h:i a',
            'time_slot.end_time' => 'required|date_format:h:i a',
        ];
        $errorMessages = [
            'time_slot.required' => 'Valid time slot selection is required',
            'time_slot.array' => 'Valid time slot selection is required',
            'time_slot.start_time.required' => 'Valid time slot selection is required',
            'time_slot.start_time.date_format' => 'Valid time slot selection is required',
            'time_slot.end_time.required' => 'Valid time slot selection is required',
            'time_slot.end_time.date_format' => 'Valid time slot selection is required',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $data = $bookingSer->selectBookingSlot($request);
    }


    //    booking request details page render
    public function bookingRequestDetailRender( Request $request, BookingRequestServices $bookingSer) {
        return $data = $bookingSer->renderBookingRequestDetail($request);
    }


    //    booking request details page render
    public function bookingRequestDetailAdd( Request $request, BookingRequestServices $bookingSer) {
        $schema = [
            'booking_title' => 'required|max:100',
            'booking_detail_note' => 'required|max:900',
        ];
        $errorMessages = [
            'booking_title.required' => 'Booking title required',
            'booking_title.max' => 'Booking title must note be greater than 100 characters',
            'booking_detail_note.required' => 'Details note is required',
            'booking_detail_note.max' => 'Details note must note be greater than 900 characters'

        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $data = $bookingSer->addBookingRequestDetail($request);
    }


    //    booking payments
    public function bookingPaymentRender( Request $request, BookingRequestServices $bookingSer) {
        return $data = $bookingSer->renderBookingPayment($request);
    }


    //    booking payments
    public function bookingPaymentCharge( Request $request, BookingRequestServices $bookingSer) {
        return $data = $bookingSer->chargeBookingPayment($request);
    }


    //    booking request details page render
    public function bookingDetailRender( Request $request, BookingRequestServices $bookingSer) {
        return $data = $bookingSer->renderBookingDetail($request);
    }

}
