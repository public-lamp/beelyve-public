<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\Generic\SocialAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Http;

class SocialLoginController extends Controller
{

    public function redirect($provider) {
        return Socialite::driver($provider)->redirect();
    }


    // social callback handling
    public function socialCallback($provider) {
        try {
            $userId = Auth::id();
            $socialUser =   Socialite::driver($provider)->stateless()->user();
            $now = date('Y-m-d H:i:s');
            if ($provider == 'facebook') {
                $socialAccountData = [
                    'user_id' => $userId,
                    'social_provider_name_id' => 1, // for facebook
                    'token' => $socialUser->token,
                    'email' => isset($socialUser->user['email']) ? $socialUser->user['email'] : null,
                    'name' => $socialUser->user['name'],
                    'provider_id' => $socialUser->user['id'],
                    'avatar' => isset($socialUser->avatar) ? $socialUser->avatar : null,
                    'expires_in' => $socialUser->expiresIn,
                    'last_updated_at' => $now,
                ];
                SocialAccount::updateOrCreate(['user_id' => $userId, 'social_provider_name_id' => 1], $socialAccountData);
            } else {
                return redirect()->route('home')->with('error', 'Invalid social connectivity provider selection, please try again');
            }

            return redirect()->route('home')->with('success', Str::ucfirst($provider).' social connectivity successful');
        } catch (\Exception $e) {
            return redirect()->route('home')->with('error', $e->getMessage());
        }

    }


    // facebook data deletion
    public function facebookDataDeletion(Request $request)
    {
        return Redirect::route('terms-conditions.get');
    }


    // instagram provider redirect
    public function redirectToInstagramProvider() {
        $appId = config('services.instagram.client_id');
        $redirectUri = urlencode(config('services.instagram.redirect'));
        return redirect()->to("https://api.instagram.com/oauth/authorize?app_id={$appId}&redirect_uri={$redirectUri}&scope=user_profile,user_media&response_type=code");
    }

    public function instagramProviderCallback(Request $request) {
        try {
            $code = $request->code;
            if (empty($code)) {
                return redirect()->route('home')->with('error', 'Failed to login with Instagram.');
            }

            $appId = config('services.instagram.client_id');
            $secret = config('services.instagram.client_secret');
            $redirectUri = config('services.instagram.redirect');

            // Get access token
            $instaAccessTokenResponse = Http::asForm()->post('https://api.instagram.com/oauth/access_token', [
                'app_id' => $appId,
                'app_secret' => $secret,
                'grant_type' => 'authorization_code',
                'redirect_uri' => $redirectUri,
                'code' => $code,
            ]);
            if ($instaAccessTokenResponse->getStatusCode() != 200) {
                return redirect()->route('home')->with('error', 'Unauthorized login to Instagram.');
            }
            $content = $instaAccessTokenResponse->getBody()->getContents();
            $content = json_decode($content);
            $accessToken = $content->access_token;

            // Get user info
            $instaUserResponse = Http::get("https://graph.instagram.com/me", [
                'fields' => 'id,username,account_type',
                'access_token' => $accessToken
            ]);
            $content = $instaUserResponse->getBody()->getContents();
            $oAuth = json_decode($content);

            $instagramAccountData = [
                'user_id' => Auth::id(),
                'social_provider_name_id' => 2, // for instagram
                'token' => $accessToken,
                'name' => $oAuth->username,
                'provider_id' => $oAuth->id,
                'last_updated_at' => date('Y-m-d H:i:s'),
            ];
            SocialAccount::updateOrCreate(['user_id' => Auth::id(), 'social_provider_name_id' => 2], $instagramAccountData);

            return redirect()->route('home')->with('success', 'Instagram social connectivity successful');
        } catch (\Exception $e) {
            return redirect()->route('home')->with('error', 'Something went wrong, please try again');
        }
    }

    // instagram data deletion
    public function instagramDataDeletion(Request $request)
    {
        return Redirect::route('terms-conditions.get');
    }

}
