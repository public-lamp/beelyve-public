<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Services\Common\FavouriteUserServices;
use Illuminate\Http\Request;

class FavouriteUsersController extends Controller
{

    // submit dispute query
    public function addFavouriteUser( Request $request, FavouriteUserServices $favouriteService) {
        $schema = [
            'favorable_id' => 'required|integer',
        ];
        $errorMessages = [
            'favorable_id.required' => 'User selection is required to add in favorite list',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $favouriteService->favouriteUserAdd($request);
    }

    // submit dispute query
    public function removeFavouriteUser( Request $request, FavouriteUserServices $favouriteService) {
        $schema = [
            'favorable_id' => 'required|integer',
        ];
        $errorMessages = [
            'favorable_id.required' => 'User selection is required to add in favorite list',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $favouriteService->favouriteUserRemove($request);
    }

}
