<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Services\Stripe\StripeServices;
use Illuminate\Http\Request;

class StripeController extends Controller
{
    public function verifyStripeToken(Request $request,StripeServices $stripeServices){
        return $stripeServices->stripeRegistrationCallback($request);
    }
}
