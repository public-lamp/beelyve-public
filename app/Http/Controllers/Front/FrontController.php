<?php

namespace App\Http\Controllers\Front;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Admin\PayoutGeneratedEmail;
use App\Mail\Talent\TalentPayoutGeneratedEmail;
use App\Models\Generic\Booking;
use App\Models\Payout;
use App\Services\Stripe\StripeServices;
use Illuminate\Http\Request;
use App\Services\Front\FrontServices;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FrontController extends Controller
{


    // returns the home view
    public function homeViewRender(Request $request, FrontServices $frontServices)
    {
        return $frontServices->renderHomeView($request);
    }

    // returns about-us view
    public function aboutViewRender(Request $request, FrontServices $frontServices)
    {
        return $frontServices->renderAboutView($request);
    }

    // returns contact-us view
    public function contactViewRender(Request $request, FrontServices $frontServices)
    {
        return $frontServices->renderContactView($request);
    }

    // return faq view
    public function faqViewRender(Request $request, FrontServices $frontServices)
    {
        return $frontServices->renderFaqView($request);
    }

    // return terms n conditions view
    public function termsConditionsViewRender(Request $request, FrontServices $frontServices)
    {
        return $frontServices->renderTermsConditionsView($request);
    }

    // return privacy view
    public function privacyPolicyViewRender(Request $request, FrontServices $frontServices)
    {
        return $frontServices->renderPrivacyPolicyView($request);
    }

    public function transferAmount(Request $request,StripeServices  $stripeServices){
        dd($stripeServices->transferAmount(500000,"acct_1KNzWsRS2qywI7Ea"));
        return redirect()->back()->with('success','Your Payouts has been processed successfully');
    }

    // contact query submit to admin
    public function contactQuerySubmit(Request $request, FrontServices $frontServices)
    {
        $schema = [
            'contact_email' => 'required|email',
            'inquiry_title' => 'required|max:100',
            'message' => 'required|max:900',
        ];
        //        validation
        $this->validate($request, $schema);

        return $frontServices->submitContactQuery($request);
    }

    // subscribe to mailing list
    public function subscribeMailingList(Request $request, FrontServices $frontServices)
    {
        $schema = [
            'subscriber_email' => 'required|email',
        ];

        //        validation
        $this->validate($request, $schema);

        return $frontServices->mailingListSubscribe($request);
    }


    // session set browser timezone
    public function sessionSetTimezone(Request $request) {
        Session::put('browserTimeZone', $request->browserTimeZone);
        return Helper::jsonResponse(1, 'Browser time zone set');
    }


//    // send payout email
//    public function payoutEmail() {
//        $booking = Booking::where('id', 1)->first();
//        $payoutData = [
//            'booking' => $booking,
//            'payout' => Payout::where('booking_id', $booking->id)
//                ->with('payoutStatus')
//                ->first(),
//            'adminPayoutNote' => 'Payout generated to talent account successfully',
//            'talentPayoutNote' => 'Payout generated to your Stripe connect-account successfully, the amount will reflect in your Stripe account within seven working days',
//        ];
//        // send payout email to admin
//        Mail::to(config('mail.admin.address'))->send(new TalentPayoutGeneratedEmail($payoutData));
//
//        return redirect()->back()->with('success', 'Email sent');
//    }

}
