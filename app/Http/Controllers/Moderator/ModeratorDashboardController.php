<?php

namespace App\Http\Controllers\Moderator;

use App\Helpers\FileSystemHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Moderator\ModeratorDashboardServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class ModeratorDashboardController extends Controller
{

    // render talent dashboard main
    public function renderModeratorDashboard(Request $request, ModeratorDashboardServices $dashboardServ)
    {
        return $dashboardServ->moderatorDashboardRender($request);
    }


    // render talent dashboard main
    public function renderModeratorProfile() {
        $moderator = Auth::user();
        return view('app.pages.moderator.moderator_profile')->with(['moderator' => $moderator]);
    }


    // moderator profile data update
    public function moderatorProfileSave(Request $request, ModeratorDashboardServices $dashboardServ) {
        $schema = [
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5,120',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'user_name' => 'required|max:100',
        ];
        //        validation
        $this->validate($request, $schema);

        try {
            $user = Auth::user();
            $fileInput = $request->profile_image;
            $imageUploadResult = [];
            if (  filled($fileInput) ) {
                $imageUploadResult = FileSystemHelper::publicStorageImageUpload($fileInput);
                if ( !$imageUploadResult['success'] ) {
                    return Helper::jsonResponse(0, 'Image upload unsuccessful', [], $imageUploadResult['error']);
                }

                $previousFileBaseName = FileSystemHelper::getBaseName($user->image);
                $existsPreviousStorageFile = Storage::disk('publicStorageImages')->exists($previousFileBaseName);
                if ($existsPreviousStorageFile) {
                    FileSystemHelper::deleteStorageImage('publicStorageImages', $user->image, $previousFileBaseName);
                }
            }

            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'user_name' => $request->last_name,
                'image' => !empty($imageUploadResult) ? $imageUploadResult['data']['url'] : $user->image,
            ]);

            return Helper::jsonResponse(1, 'Profile updated successfully');
        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }
    }


    // render talent dashboard main
    public function moderatorFavouriteUsers(ModeratorDashboardServices $dashboardServ)
    {
        return $dashboardServ->favouriteUsersModerator();
    }

}
