<?php

namespace App\Http\Controllers\Moderator\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Moderator\Chat\ModeratorChatServices;

class ModeratorChatController extends Controller
{

    /*    =================================  Common Chat Routes requests ======================================== */
    // render talent chat conversations page
    public function renderModeratorChatConversations(Request $request, ModeratorChatServices $modChatServ) {
        return $data = $modChatServ->chatConversationsRender($request);
    }

    // render talent chat messages page
    public function renderModeratorChatMessages(Request $request, ModeratorChatServices $modChatServ) {
        return $data = $modChatServ->chatMessagesRender($request);
    }

}
