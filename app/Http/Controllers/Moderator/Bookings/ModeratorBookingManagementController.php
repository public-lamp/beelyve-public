<?php

namespace App\Http\Controllers\Moderator\Bookings;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Moderator\Bookings\ModeratorBookingManagementServices;

class ModeratorBookingManagementController extends Controller
{

    // render talent booking management main page
    public function moderatorBookingManagement(Request $request, ModeratorBookingManagementServices $bookingManageServ)
    {
        if ( !isset($request->booking_id) ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'can not perform action', [], 'Booking selection is required');
            }
            return redirect()->back()->with('error', 'Booking selection is required');
        }

        $data = $bookingManageServ->renderModeratorBookingManagement($request);
        return $data;
    }


    // start booking timer-counter
    public function startBookingTimer(Request $request, ModeratorBookingManagementServices $bookingManageServ)
    {
        if ( !isset($request->booking_id) || !isset($request->booking_duration) ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'can not perform action', [], 'Valid booking data is required to perform operation');
            }
            return redirect()->back()->with('error', 'Valid booking data is required to perform operation');
        }

        return $bookingManageServ->bookingTimerStart($request);
    }

    // stop booking timer-counter
    public function stopBookingTimer(Request $request, ModeratorBookingManagementServices $bookingManageServ)
    {
        // validation on required data
        if ( !isset($request->booking_id) || !isset($request->booking_duration) || !isset($request->stopped_at) ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'can not perform action', [], 'Valid booking data is required to perform operation');
            }
            return redirect()->back()->with('error', 'Valid booking data is required to perform operation');
        }

        return $bookingManageServ->bookingTimerStop($request);
    }

    // stop booking timer-counter
    public function approveCompletion(Request $request, ModeratorBookingManagementServices $bookingManageServ)
    {
        // validation on required data
        if ( !isset($request->booking_id) ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'can not perform action', [], 'Valid booking data is required to perform operation');
            }
            return redirect()->back()->with('error', 'Valid booking data is required to perform operation');
        }

        return $bookingManageServ->approveBookingCompletion($request);
    }


}
