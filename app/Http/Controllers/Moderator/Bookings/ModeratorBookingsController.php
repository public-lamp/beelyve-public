<?php

namespace App\Http\Controllers\Moderator\Bookings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Moderator\Bookings\ModeratorBookingServices;

class ModeratorBookingsController extends Controller
{

    // render talent dashboard main
    public function projectsListing(Request $request, ModeratorBookingServices $bookingServ)
    {
        $data = $bookingServ->moderatorProjectsListing($request);
        return $data;
    }


    // Booking requests to Talent
    public function moderatorBookingRequests(Request $request, ModeratorBookingServices $bookingServ)
    {
        $data = $bookingServ->bookingRequestsModerator($request);
        return $data;
    }

}
