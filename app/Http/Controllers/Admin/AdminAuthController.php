<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Admin\AdminAuthServices;
use Illuminate\Support\Facades\Auth;

class AdminAuthController extends Controller
{

    // login view
    public function renderLoginPage()
    {
        return view('app.pages.admin.admin_login');
    }

    //    // attempt login
    public function adminLoginAttempt(Request $request, AdminAuthServices $authServices)
    {
        $schema = [
            'admin_login_email' => 'required',
            'admin_login_password' => 'required',
            'time_zone' => 'required',
        ];
        $errorMessages = [
            'admin_login_email.required' => 'Email required',
            'admin_login_password.required' => 'Password required',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        return $authServices->loginAttemptAdmin($request);
    }


}
