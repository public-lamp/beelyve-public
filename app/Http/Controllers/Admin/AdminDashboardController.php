<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\FileSystemHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Admin\SubscriptionListSendEmail;
use App\Models\Generic\AdminServiceCharge;
use App\Models\Generic\Booking;
use App\Models\Generic\EmailSubscriber;
use App\Models\Generic\FeaturedUser;
use App\Models\Payout;
use App\Models\PayoutException;
use App\Models\User;
use App\Services\Talent\TalentDashboardServices;
use App\Services\Talent\TalentProfileServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AdminDashboardController extends Controller
{

    // render admin dashboard main
    public function renderAdminDashboard() {
        $users = User::whereDoesntHave('roles', function($query) {
            $query->where('roles.id', 1);
        })->get();

        $bookingsQueryBuilder = Booking::whereNotIn('booking_status_id', [1, 2, 3]); // being created, created and booking requested respectively

        $todayBookings = $bookingsQueryBuilder->whereIn('booking_status_id', [4, 6]) // accepted and in-progress
            ->whereDate('time_from', date('Y-m-d'))
            ->get();
        $upcomingBookings = $bookingsQueryBuilder->whereIn('booking_status_id', [4]) // accepted
            ->whereDate('time_from', '>', date('Y-m-d'))
            ->get();
        $completedBookings = Booking::whereIn('booking_status_id', [7, 8, 10]) // pending approval, approved review pending, closed/finished
            ->get();

        $fundsTransferred = Booking::whereIn('booking_payment_status_id', [4])
            ->whereHas('bookingPayout', function($query) {
                $query->whereIn('payouts.payout_status_id', [3]);
            })->sum('amount');
        $fundsReceived = Booking::whereIn('booking_payment_status_id', [3])
            ->sum('amount');

        return view('app.pages.admin.dashboard_main')->with([
            'users' => $users,
            'todayBookings' => $todayBookings,
            'upcomingBookings' => $upcomingBookings,
            'completedBookings' => $completedBookings,
            'fundsTransferred' => $fundsTransferred,
            'fundsReceived' => $fundsReceived,
        ]);
    }


    // render admin dashboard main
    public function renderUsersList() {
        $users = User::whereDoesntHave('roles', function($query) {
            $query->where('roles.id', 1);
        })->with('roles', 'featuredStatus')->get();
        return view('app.pages.admin.users_list')->with(['users' => $users]);
    }


    // render users booking list
    public function renderBookingsList() {
        $bookings = Booking::with(['moderator', 'talent.talentProfessionalInfo.talentCategory', 'bookingStatus', 'bookingPaymentStatus', 'bookingServicesDetail', 'bookingPayout.payoutStatus', 'bookingPayoutException'] )
            ->get();
        return view('app.pages.admin.bookings_list')->with(['bookings' => $bookings]);
    }


    //render all payouts listing
    public function renderPayoutsListing(){
        $payouts = Payout::with('booking','payoutStatus')->get();
        return view('app.pages.admin.payouts-listing')->with(['payouts' => $payouts]);
    }


    //render all payouts exception listing
    public function renderPayoutsExceptionListing(){
        $payoutsExceptions = PayoutException::with('booking')->get();
        return view('app.pages.admin.payouts-exception-listing')->with(['payoutsExceptions' => $payoutsExceptions]);
    }



    // render mail subscribers
    public function renderMailSubscribers() {
        $mailSubscribers = EmailSubscriber::where('active', true)
            ->orderBy('id', 'DESC')
        ->get();
        return view('app.pages.admin.mail_subscribers')->with(['mailSubscribers' => $mailSubscribers]);
    }


    // render send subscribers email
    public function renderSendSubscribersEmail() {
        return view('app.pages.admin.subscribers_send_email');
    }


    // send subscribers email
    public function sendSubscribersEmail(Request $request) {
        $schema = [
            'email_title' => 'required|max:120',
            'email_subject' => 'required|max:100',
            'email_message' => 'required|max:900',
        ];
        //        validation
        $this->validate($request, $schema);

        $subscribersList = EmailSubscriber::where('active', true)
            ->with('user')->get();
        if ($subscribersList->isEmpty()) {
            return Helper::jsonResponse(0, 'Can not perform operation', [], 'Mailing list is empty');
        }

        $subscriberEmailData = [
            'emailTitle' => $request->email_title,
            'emailSubject' => $request->email_subject,
            'emailMessage' => $request->email_message,
            'subscribersList' => $subscribersList,
        ];
        self::sendSubEmail($subscriberEmailData);

        return Helper::jsonResponse(1, 'Email sent to subscribes');
    }


    // render service charges settings page
    public function serviceChargesSettings() {
        $bookingServiceCharge = AdminServiceCharge::where('service_type_id', 1)->first();
        return view('app.pages.admin.service_charges_settings', compact('bookingServiceCharge'));
    }


    // save service charges settings page
    public function serviceChargesSettingsSave(Request $request) {
        $schema = [
            'service_type_id' => 'required|integer|gt:0',
            'service_charge_type_id' => 'required|integer|gt:0',
            'charge_value' => 'required|integer|gt:0.0',
        ];
        //        validation
        $this->validate($request, $schema);

        AdminServiceCharge::updateOrCreate(['service_type_id' => $request->service_type_id], [
            'service_type_id' => $request->service_type_id,
            'service_charge_type_id' => $request->service_charge_type_id,
            'value' => $request->charge_value,
        ])->first();

        return Helper::jsonResponse(1, 'Service charges updates');
    }


    // make Talent as featured
    public function makeUserFeatured(Request $request) {
        $schema = [
            'user_id' => 'required|integer|gt:0',
            'role_id' => 'required|integer|gt:0',
        ];
        //        validation
        $this->validate($request, $schema);

        $user = User::where('id', $request->user_id)
            ->first();
        if (!$user) {
            return redirect()->back()->with('error', 'Talent not found');
        }
        if (!$user->basic_profile_completed) {
            return redirect()->back()->with('error', 'Talent profile not completed yet');
        }

        FeaturedUser::updateOrCreate(['user_id' => $request->user_id], [
           'user_id' => $request->user_id,
           'role_id' => $request->role_id
        ]);

        return redirect()->back()->with('success', 'User marked as featured');
    }

    // make Talent as featured
    public function undoUserFeatured(Request $request) {
        $schema = [
            'user_id' => 'required|integer|gt:0'
        ];
        //        validation
        $this->validate($request, $schema);

        FeaturedUser::where('user_id', $request->user_id)->delete();

        return redirect()->back()->with('success', 'User featured status removed');
    }


    // render profile
    public function renderProfile() {
        $admin = User::where('id', 1)->first();
        return view('app.pages.admin.profile.profile')->with(['admin' => $admin]);
    }


    // save basic profile
    public function profileSave(Request $request) {
        $schema = [
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5,120',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
        ];
        //        validation
        $this->validate($request, $schema);

        try {
            $user = Auth::user();
            $fileInput = $request->profile_image;
            $imageUploadResult = [];
            if (  filled($fileInput) ) {
                $imageUploadResult = FileSystemHelper::publicStorageImageUpload($fileInput);
                if ( !$imageUploadResult['success'] ) {
                    return Helper::jsonResponse(0, 'Image upload unsuccessful', [], $imageUploadResult['error']);
                }

                $previousFileBaseName = FileSystemHelper::getBaseName($user->image);
                $existsPreviousStorageFile = Storage::disk('publicStorageImages')->exists($previousFileBaseName);
                if ($existsPreviousStorageFile) {
                    FileSystemHelper::deleteStorageImage('publicStorageImages', $user->image, $previousFileBaseName);
                }
            }

            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'image' => !empty($imageUploadResult) ? $imageUploadResult['data']['url'] : $user->image,
            ]);

            return Helper::jsonResponse(1, 'Profile updated successfully');
        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }
    }


    //    send email to subscribers list
    public static function sendSubEmail($subscriptionEmailData) {
        foreach ($subscriptionEmailData['subscribersList'] as $subscriber) {
            Mail::to($subscriber['email'])->send(new SubscriptionListSendEmail($subscriptionEmailData, $subscriber));
        }
    }

}
