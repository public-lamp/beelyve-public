<?php

namespace App\Http\Controllers\Talent;

use App\Http\Controllers\Controller;
use App\Services\Talent\TalentProfileServices;
use Illuminate\Http\Request;

class TalentProfileController extends Controller
{

    public function talentPublicProfile(Request $request, TalentProfileServices $talentProfileSer) {
        $schema = [
            'talent_id' => 'required'
        ];
        $errorMessages = [
            'talent_id.required' => 'Valid talent selection is required'
        ];
        $this->validate($request, $schema, $errorMessages);

        return $data = $talentProfileSer->publicTalentProfile($request);
    }

}
