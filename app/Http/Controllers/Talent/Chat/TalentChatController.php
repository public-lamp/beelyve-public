<?php

namespace App\Http\Controllers\Talent\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Talent\Chat\TalentChatServices;

class TalentChatController extends Controller
{

    // render talent chat conversations page
    public function renderTalentChatConversations(Request $request, TalentChatServices $talChatServ) {
        return $data = $talChatServ->chatConversationsRender($request);
    }

    // render talent chat messages page
    public function renderTalentChatMessages(Request $request, TalentChatServices $talChatServ) {
        return $data = $talChatServ->chatMessagesRender($request);
    }

}
