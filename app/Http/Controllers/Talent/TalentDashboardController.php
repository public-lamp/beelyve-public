<?php

namespace App\Http\Controllers\Talent;

use App\Http\Controllers\Controller;
use App\Models\Talent\TalentProfessionalInfo;
use App\Services\Talent\TalentDashboardServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TalentDashboardController extends Controller
{
    // render talent dashboard main
    public function renderTalentDashboard(Request $request, TalentDashboardServices $dashboardServ)
    {
        return $dashboardServ->talentDashboardRender($request);
    }

    // render talent dashboard main
    public function editTalentProfile(Request $request, TalentDashboardServices $dashboardServ)
    {
        return $dashboardServ->talentProfileEdit($request);
    }


    //   return talent social connectivity page
    public function renderSocialConnectivity() {
        return view('app.pages.talent.profile.social_connectivity');
    }


    // talent profile update
    public function talentProfileBasicUpdate(Request $request, TalentDashboardServices $dashboardServ)
    {
        $schema = [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'user_name' => 'required|max:100',
            'talent_category_id' => 'required|integer',
            'country' => 'required',
            'talent_start_rate' => 'required|gt:0.0',
            'experience' => 'required|max:600',
            'tagline' => 'required|max:100',
            'fb_profile_link' => 'required',
            'insta_profile_link' => 'required',
            // 'linkedin_profile_link' => 'required',
            'talent_service' => 'required|max:200',
            'talent_about' => 'required|max:600',
            'talent_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            'profile_completed' => 'boolean'
        ];
        $this->validate($request, $schema);

        return $dashboardServ->updateTalentProfileBasic($request);
    }

}
