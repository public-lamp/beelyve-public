<?php

namespace App\Http\Controllers\Talent\Bookings;

use App\Http\Controllers\Controller;
use App\Models\Generic\Booking;
use App\Services\Moderator\ModeratorDashboardServices;
use App\Services\Talent\Bookings\TalentBookingServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TalentBookingController extends Controller
{

    // render talent dashboard main
    public function talentBookingsListing(Request $request, TalentBookingServices $talBookServ)
    {
        $data = $talBookServ->listingTalentBookings($request);
        return $data;
    }


    // Booking requests to Talent
    public function talentBookingRequests(Request $request, TalentBookingServices $talBookServ)
    {
        $data = $talBookServ->bookingRequestsTalent($request);
        return $data;
    }


    // render talent dashboard main
    public function actionBookingRequest(Request $request, TalentBookingServices $talBookServ)
    {
        $errorMessages = [
            'booking_id.required' => 'Booking selection is required',
            'booking_id.int' => 'Valid booking selection is required',
            'accept.required' => 'Please accept or reject the booking request',
            'accept.boolean' => 'Please accept or reject the booking request',
        ];
        $schema = [
            'booking_id' => 'required|integer',
            'accept' => 'required|boolean',
        ];
        $this->validate($request, $schema, $errorMessages);

        $data = $talBookServ->bookingRequestAction($request);
        return $data;
    }

}
