<?php

namespace App\Http\Controllers\Talent\Bookings;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Talent\Bookings\TalentBookingManagementServices;

class TalentBookingManagementController extends Controller
{

    // render talent booking management main page
    public function renderTalentBookingManagement(Request $request, TalentBookingManagementServices $bookingManageServ)
    {
        $data = $bookingManageServ->talentBookingManagementRender($request);
        return $data;
    }


    // mark talent-present
    public function talentPresent(Request $request, TalentBookingManagementServices $bookingManageServ)
    {
        if ( !isset($request->booking_id) ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'can not perform action', [], 'Booking selection is required');
            }
            return redirect()->back()->with('error', 'Booking selection is required');
        }

        return $bookingManageServ->markTalentPresent($request);
    }


    // mark talent-present
    public function talentMarkTimeCompleted(Request $request, TalentBookingManagementServices $bookingManageServ)
    {
        if ( !isset($request->booking_id) ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'can not perform action', [], 'Booking selection is required');
            }
            return redirect()->back()->with('error', 'Booking selection is required');
        }

        return $bookingManageServ->markTimeCompletedTalent($request);
    }

}
