<?php

namespace App\Http\Controllers\Talent;

use App\Http\Controllers\Controller;
use App\Services\Talent\TalentDashboardServices;
use Illuminate\Http\Request;
use App\Services\Talent\WorkHistoryServices;
use Illuminate\Support\Facades\Auth;
use App\Models\Generic\TalentWorkFile;

class WorkHistoryController extends Controller
{


    //   return talent work gallery page
    public function renderTalentWorkGallery() {
        $user = Auth::user();
        $talentProfile = TalentDashboardServices::talentCompleteProfile($user->id);
        $workImagesCount = TalentWorkFile::where(['talent_id' => $user->id, 'type' => 'image'])->count();
        $workVideosCount = TalentWorkFile::where(['talent_id' => $user->id, 'type' => 'video'])->count();

        return view('app.pages.talent.profile.gallery', compact('talentProfile', 'workImagesCount', 'workVideosCount'));
    }


    //   return upload-work-gallery/files page
    public function renderTalentWorkFilesUpload() {
        $user = Auth::user();
        $talentProfile = TalentDashboardServices::talentCompleteProfile($user->id);
        $workImagesCount = TalentWorkFile::where(['talent_id' => $user->id, 'type' => 'image'])->count();
        $workVideosCount = TalentWorkFile::where(['talent_id' => $user->id, 'type' => 'video'])->count();

        return view('app.pages.talent.profile.upload_files', compact('talentProfile', 'workImagesCount', 'workVideosCount'));
    }


    // talent work files upload
    public function talentWorkFilesSave(Request $request, WorkHistoryServices $talWorkServ) {
        $errorMessages = [
            'work_images.required' => 'Work images required',
            'work_images.array' => 'Work images required',
            // 'work_video' => 'Work introductory video is required',
            'work_video_link' => 'Work introductory video is required',
        ];
        $schema = [
            'work_images' => 'required|array',
            'work_images.*' => 'required|image|mimes:jpeg,JPEG,png,PNG,jpg,JPG,gif,GIF,svg,SVG|max:2048',
            // 'work_video' => 'required|max:10000|mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts'
            'work_video_link' => 'required|max:900'
        ];
        $this->validate($request, $schema, $errorMessages);

        return $talWorkServ->saveTalentWorkFiles($request);
    }


    // talent work files show|get (currently not in use)
    public function talentWorkFilesShow(Request $request, WorkHistoryServices $talWorkServ) {
        return $talWorkServ->showTalentWorkFiles($request);
    }


    // talent work files delete
    public function talentWorkFileDelete(Request $request, WorkHistoryServices $talWorkServ) {
        return $talWorkServ->deleteTalentWorkFile($request);
    }

}
