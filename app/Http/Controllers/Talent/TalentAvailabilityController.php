<?php

namespace App\Http\Controllers\Talent;

use App\Http\Controllers\Controller;
use App\Models\Talent\TalentProfessionalInfo;
use App\Services\Talent\TalentAvailabilityServices;
use App\Services\Talent\TalentDashboardServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TalentAvailabilityController extends Controller
{

    //   return talent availability page
    public function renderAvailability() {
        $user = Auth::user();
        $talentProfile = TalentDashboardServices::talentCompleteProfile($user->id);

        return view('app.pages.talent.profile.availability', compact('talentProfile'));
    }

    //   return talent dashboard main
    public function talentAvailabilitySet( TalentAvailabilityServices $availServ, Request $request ) {
        $errorMessages = [
            'availability_category.required' => 'Valid availability type selection is required',
            'days.required' => 'Please select time slots for every day',
            'days.*.time.required' => 'Time selection is required against every selected day',
            'days.*.time.time_from.required' => 'Time-from selections are required',
            'days.*.time.time_from.*.required_with' => 'Time-from can not be empty',
            'days.*.time.time_from.*.date_format' => 'Time format incorrect',
            'days.*.time.time_to.required' => 'Time-to selections are required',
            'days.*.time.time_to.*.required_with' => 'Time-to can not be empty',
            'days.*.time.time_to.*.date_format' => 'Time format incorrect',
            'days.*.time.time_to.*.after' => 'Time-to must be greater than time-from',
        ];
        $schema = [
            'availability_category' => 'required|integer',
            'days' => 'required|array',
            'days.*.time' => 'required|array',
            'days.*.time.time_from' => 'required|array',
            'days.*.time.time_from.*' => 'nullable|required_with:days.*.time.time_to.*|date_format:h:i A',
            'days.*.time.time_to' => 'required|array',
            'days.*.time.time_to.*' => 'nullable|required_with:days.*.time.time_from.*|date_format:h:i A|after:days.*.time.time_from.*'
        ];
        $this->validate($request, $schema, $errorMessages);

        return $availServ->setTalentAvailability($request);
    }

}
