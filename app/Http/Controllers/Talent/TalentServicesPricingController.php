<?php

namespace App\Http\Controllers\Talent;

use App\Http\Controllers\Controller;
use App\Services\Talent\TalentDashboardServices;
use Illuminate\Http\Request;
use App\Services\Talent\TalentServicesPricingServices;
use Illuminate\Support\Facades\Auth;

class TalentServicesPricingController extends Controller
{

    //   return talent service pricing page
    public function renderTalentServicesPricing() {
        $user = Auth::user();
        $talentProfile = TalentDashboardServices::talentCompleteProfile($user->id);

        return view('app.pages.talent.profile.service_pricing', compact('talentProfile'));
    }

    //    talent services-pricing set
    public function talentServicesPricingSet(Request $request, TalentServicesPricingServices $talentServicesPriSer) {
        $errorMessages = [
            'pricing.required' => 'Pricing details are required',
            'pricing.*.id.required' => 'Invalid pricing type selected',
            'pricing.*.duration.required' => 'Please select duration',
            'pricing.*.duration.integer' => 'Valid duration selection is required',
            'pricing.*.duration.min' => 'Valid duration selection is required',
            'pricing.*.duration.max' => 'Valid duration selection is required',
            'pricing.*.price.required' => 'Pricing is required',
            'pricing.*.mediums.required' => 'Please select mediums',
            'pricing.*.mediums.integer' => 'valid medium selection is required',
            'pricing.*.mediums.min' => 'valid medium selection is required',
            'pricing.*.mediums.max' => 'valid medium selection is required',
            'pricing.*.main_features.required' => 'Main features points are required',
            'pricing.*.main_features.array' => 'Main features all three points are required',
            'pricing.*.main_features.*.required' => 'Field is required',
            'pricing.*.main_features.*.max' => 'Maximum 200 characters are allowed',
            'pricing.*.description.required' => 'Description is required',
        ];
        $schema = [
            'pricing' => 'required|array',
            'pricing.*.id' => 'required|integer|max:3|min:1',
            'pricing.*.duration' => 'required|integer|min:1|max:5',
            'pricing.*.price' => 'required|integer',
            'pricing.*.mediums' => 'required|array',
            'pricing.*.mediums.*' => 'required|integer|min:1|max:3',
            'pricing.*.main_features' => 'required|array',
            'pricing.*.main_features.*' => 'required|max:200',
            'pricing.*.description' => 'required',
        ];
        $this->validate($request, $schema, $errorMessages);

        return $data = $talentServicesPriSer->setTalentServicesPricing($request);
    }

}
