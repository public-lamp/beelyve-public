<?php

namespace App\Http\Controllers\Authentication;

use App\Helpers\AuthHelper;
use App\Http\Controllers\Controller;
use App\Services\Authentication\AuthServices;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

class AuthController extends Controller
{

    // return the signup view
    public function signupFormRender(AuthServices $authServices)
    {
        return $authServices->renderSignupForm();
    }


    // signup request submit
    public function signupAction(Request $request, AuthServices $authServices)
    {
        $schema = [
            'user_name' => 'required|unique:users|string|max:90',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required|integer|min:1|max:3',
            'time_zone' => 'required',
        ];
        $errorMessages = [
            'user_name.required' => 'User name is required',
            'user_name.max' => 'User name may not be more than 90 characters',
            'email.required' => 'Email is required',
            'email.unique' => 'Email is registered already, please take a different one',
            'password.required' => 'Password is required',
            'password.confirmed' => 'Password and confirm password does not same',
            'role_id.required' => 'A valid signup type selection is required',
            'role_id.integer' => 'A valid signup type selection is required',
            'role_id.min' => 'A valid signup type selection is required',
            'role_id.max' => 'A valid signup type selection is required',
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        // process to the services
        return $authServices->actionSignup($request);
    }


    // login view
    public function loginForm(AuthServices $authServices)
    {
        return $authServices->formLogin();
    }


    // login view
    public function login(Request $request, AuthServices $authServices)
    {
        $schema = [
            'login_email' => 'required',
            'login_password' => 'required',
            'remember_me' => 'required|boolean',
            'time_zone' => 'required',
        ];
        $errorMessages = [
            'login_email.required' => 'Email required',
            'login_password.required' => 'Password required',
            'remember_me.required' => 'Invalid selection on remember user',
            'remember_me.boolean' => 'Invalid selection on remember user'
        ];
        //        validation
        $this->validate($request, $schema, $errorMessages);

        if ( url()->previous() == route('facebook.social.callback', ['provider' => 'facebook']) ) {
            AuthHelper::flashConfirmationLoginMessage('To complete your Facebook connectivity, please login again');
        }

        return $authServices->userLogin($request);
    }


    // email verification notice
    public function verificationNotice(AuthServices $authServices)
    {
        return $authServices->noticeVerification();
    }


    // verify email verification
    public function verifyVerification(EmailVerificationRequest $request, AuthServices $authServices)
    {
        return $authServices->verificationVerify($request);
    }


    // verify email send
    public function verificationSend(Request $request, AuthServices $authServices)
    {
        return $authServices->sendVerification($request);
    }


    // email verification notice
    public function verificationResend(Request $request, AuthServices $authServices)
    {
        return $authServices->resendVerification($request);
    }


    // Forgot password form render
    public function forgotPasswordFormRender(AuthServices $authServices)
    {
        return $authServices->renderForgotPasswordForm();
    }


    // Forgot password modal view
    public function forgotPassword(Request $request, AuthServices $authServices)
    {
        $errorMessages = [
            'forgot_email.required' => 'Email is required',
        ];
        $schema = [
            'forgot_email' => 'required',
        ];
        $this->validate($request, $schema, $errorMessages);

        return $authServices->forgotPasswordAction($request);
    }


    // verify-token
    public function verifyForgotToken(Request $request, AuthServices $authServices)
    {
        return $authServices->forgotTokenVerify($request);
    }


    // reset password modal view
    public function resetPasswordFormRender(AuthServices $authServices)
    {
        return $authServices->renderResetPasswordForm();
    }


    // reset password modal view
    public function resetPasswordAction(Request $request, AuthServices $authServices)
    {
        $schema = [
            'reset_password' => 'required|confirmed|min:6|max:100',
        ];
        $errorMessages = [
            'reset_password.required' => 'New password field is required',
            'reset_password.confirmed' => 'New password and confirm new password mismatch',
        ];
        $this->validate($request, $schema, $errorMessages);

        return $authServices->passwordResetAction($request);
    }


    // logout
    public function logout(AuthServices $authServices)
    {
        return $authServices->signOut();
    }

}
