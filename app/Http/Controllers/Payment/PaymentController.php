<?php

namespace App\Http\Controllers\Payment;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Mail\Admin\PayoutGeneratedEmail;
use App\Mail\Bookings\ModeratorPayoutEmail;
use App\Mail\Talent\TalentPayoutGeneratedEmail;
use App\Models\Generic\Booking;
use App\Models\Payout;
use App\Models\PayoutException;
use App\Models\PayoutStatus;
use App\Services\Moderator\Bookings\ModeratorBookingManagementServices;
use App\Services\Stripe\StripeServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{

    //    process payout to talent manually
    public function processTalentManualPayout(Request $request) {
        if ( !isset($request->booking_id) ) {
            return redirect()->back()->with('error', 'Booking selection is required');
        }

        $booking = Booking::where(['id' => $request->booking_id])
            ->with('timerActivity', 'talent', 'talent.stripeAccountDetail')
            ->first();
        if ( !isset($booking) ) {
            return Helper::jsonResponse(0, 'Can not perform action', [], 'No booking found');
        }

        $generatePayout = ModeratorBookingManagementServices::processPayoutToTalent($booking->id, $booking->talent);
        if (!$generatePayout['success']) {
            Session::flash('showErrorAlert', true);
            return redirect()->back()->with('error', $generatePayout['message']);
        }
        Session::flash('showSuccessAlert', true);
        return redirect()->back()->with('success', 'Payout generated to Talent successfully');
    }


    //    process manual-payout to Moderator
    public function processModeratorManualPayout(Request $request) {
        if ( !isset($request->booking_id) ) {
            return redirect()->back()->with('error', 'Booking selection is required');
        }

        $booking = Booking::where(['id' => $request->booking_id])
            ->with('timerActivity', 'moderator', 'moderator.stripeAccountDetail')
            ->first();
        if ( !isset($booking) ) {
            return Helper::jsonResponse(0, 'Can not perform action', [], 'No booking found');
        }

        $generatePayout = self::processPayoutToModerator($booking->id, $booking->moderator);
        if (!$generatePayout['success']) {
            Session::flash('showSuccessAlert', false);
            Session::flash('showErrorAlert', true);
            return redirect()->back()->with('error', $generatePayout['message']);
        }
        Session::flash('showErrorAlert', false);
        Session::flash('showSuccessAlert', true);
        return redirect()->back()->with('success', 'Payout generated to Talent successfully');
    }


    // process payout to moderator
    public static function processPayoutToModerator($bookingId, $moderator) {
        try {
            $responseArray = ['success' => false, 'message' => 'Operation Unsuccessful'];
            $booking = Booking::where('id', $bookingId)
                ->with('talent', 'moderator')
                ->first();
            if( $booking && $booking->amount ) {
                $stripeBalance = new \Stripe\StripeClient(config('services.stripe.secret'));
                $balance = $stripeBalance->balance->retrieve();
                $transferAmount = $booking->amount - ((self::calculatePercentage( config('services.stripe.checkout_percentage'), $booking->amount ))+0.30); // stripe checkout charges percentage plus 30 cents

                // check for account has send-able balance
                if( $balance && isset($balance->available[0]['amount']) && $transferAmount <= $balance->available[0]['amount'] ) {
                    // check for talent has connect account
                    if( isset($moderator->stripeAccountDetail) && $moderator->stripeAccountDetail->stripe_account_id ) {
                        $stripeService = new StripeServices();
                        $stripeResponse = $stripeService->transferAmount($transferAmount, $moderator->stripeAccountDetail->stripe_account_id,"Payouts Process Against Booking Id ".$bookingId);
                        // check payout processed
                        if( $stripeResponse && isset($stripeResponse['id']) ) {
                            Payout::updateOrCreate(['booking_id' => $bookingId], [
                                'booking_id' => $bookingId,
                                'stripe_payout_id' => $stripeResponse['id'],
                                'amount' => $transferAmount,
                                'currency' => 'USD',
                                'payout_status_id' => 3 // completed
                            ]);
                            $booking->update([
                                'booking_payment_status_id' => 4, // initiated to seller
                            ]);

                            // send payout email to admin
                            $payoutData = [
                                'booking' => $booking,
                                'payout' => Payout::where('booking_id', $booking->id)
                                    ->with('payoutStatus')
                                    ->first(),
                                'adminPayoutNote' => 'Payout generated to talent account successfully',
                                'moderatorPayoutNote' => 'Talent rejected your booking request. Payout generated to your Stripe connect-account successfully, the amount will reflect in your Stripe account within seven working days',
                            ];
                            Mail::to(config('mail.admin.address'))->send(new PayoutGeneratedEmail($payoutData)); // to admin
                            Mail::to($booking['moderator']['email'])->send(new ModeratorPayoutEmail($payoutData)); // to moderator

                            $responseArray['success'] = true;
                            $responseArray['message'] = 'Payout generated to Client connect-account successfully';
                        } else { // payout failed
                            Payout::updateOrCreate(['booking_id' => $bookingId], [
                                'booking_id' => $bookingId,
                                'amount' => $transferAmount,
                                'currency' => 'USD',
                                'payout_status_id' => 5 // failed
                            ]);
                            PayoutException::updateOrCreate(['booking_id' => $bookingId], [
                                'booking_id' => $bookingId,
                                'message' => $stripeResponse
                            ]);

                            $responseArray['message'] = 'Payout Failed';
                        }
                    } else { // stripe account not setup yet
                        Payout::updateOrCreate(['booking_id' => $bookingId], [
                            'booking_id' => $bookingId,
                            'amount' => $transferAmount,
                            'currency' => 'USD',
                            'payout_status_id' => 4
                        ]);

                        $responseArray['message'] = 'Client Stripe account not setup';
                    }
                } else { // not enough balance
                    Payout::updateOrCreate(['booking_id' => $bookingId], [
                        'booking_id' => $bookingId,
                        'amount' => $transferAmount,
                        'currency' => 'USD',
                        'payout_status_id' => 2
                    ]);

                    $responseArray['message'] = 'Not enough Stripe balance to generate Payout';
                }
            }

            return $responseArray;
        } catch (\Exception $exception){
            return ['success' => false, 'message' => $exception->getMessage()];
        }

    }


    //    calculates specified percentage amount of total amount
    public static function calculatePercentage($percentage, $totalAmount) {
        return ($percentage / 100) * $totalAmount;
    }

}
