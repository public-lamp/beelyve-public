<?php

namespace App\Http\Middleware\Project;

use App\Helpers\Helper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerifyResetPasswordToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $resetToken = Session::get('password_reset_token');
        if ( !isset($request->password_reset_token) || $request->password_reset_token !== $resetToken ) {
            return Helper::jsonResponse(0, 'Unauthorized', [], 'Authorization error', 403);
        }

        return $next($request);
    }
}
