<?php

namespace App\Http\Middleware\Project;

use App\Models\Generic\UserActivity;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LastActivityAt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ( !auth()->check() ) {
            return $next($request);
        }

        UserActivity::updateOrCreate(['user_id' => Auth::id()], ['last_activity_at' => now()]);
        return $next($request);
    }
}
