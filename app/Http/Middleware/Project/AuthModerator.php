<?php

namespace App\Http\Middleware\Project;

use App\Helpers\Helper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthModerator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $moderator = Auth::user()->isModerator();
        if ($moderator) {
            return $next($request);
        }
        if ( $request->ajax() ) {
            return Helper::jsonResponse(0, 'Authorization error', [], 'To perform the action please login as a Moderator');
        }
        return redirect()->back()->with('error', 'To perform the action please login as a Moderator');
    }

}
