<?php

namespace App\Http\Middleware\Project;

use App\Helpers\Helper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorizeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        if ( !$user || !$user->isAdmin() ) {
            if ($request->ajax()) {
                return Helper::jsonResponse(0, 'Authorization error', [], 'User not authorized');
            }
            return redirect()->back()->with('error', 'User not authorized');
        }

        return $next($request);
    }
}
