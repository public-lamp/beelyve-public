<?php

namespace App\Http\Middleware\Project;

use App\Helpers\Helper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateAdmin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authUser = Auth::user();
        if ($authUser && $authUser->isAdmin()) {
            return $next($request);
        }
        if ( $request->ajax() ) {
            return Helper::jsonResponse(0, 'Authorization error', [], 'To perform the action please login as Admin');
        }
        return redirect()->route('admin.login')->with('error', 'To perform the action please login as Admin');
    }

}
