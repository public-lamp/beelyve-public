<?php


namespace App\Services\Admin;

use App\Helpers\Helper;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminAuthServices
{

    //    login user
    public function loginAttemptAdmin($request) {
        try {
            $loginSucceeded = Auth::attempt([
                'email' => $request->admin_login_email,
                'password' => $request->admin_login_password
            ]);
            if( !$loginSucceeded) {
                return Helper::jsonResponse(0, 'Login unsuccessful', [], 'Incorrect email or password');
            }

            $user = User::where('email', $request->admin_login_email)
                ->first();
            $user->update(['time_zone' => $request->time_zone]);

            $nextUrl = route('admin-dashboard');
            return Helper::jsonResponse(1, 'Login successful', ['next_url' =>  $nextUrl], '');
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], $e->getMessage());
        }
    }

}
