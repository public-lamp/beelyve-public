<?php


namespace App\Services\Moderator\Bookings;

use App\Events\Bookings\EventTimeCompleted;
use App\Events\Bookings\EventTimerStarted;
use App\Events\Bookings\EventTimerStopped;
use App\Helpers\Chat\ChatHelper;
use App\Helpers\Helper;
use App\Mail\Admin\PayoutGeneratedEmail;
use App\Mail\Bookings\ModeratorApprovedEventCompletionEmail;
use App\Mail\Talent\TalentPayoutGeneratedEmail;
use App\Models\Chat\Conversation;
use App\Models\Generic\Booking;
use App\Models\Generic\Event\BookingEventActivity;
use App\Models\Generic\Event\BookingTimerActivity;
use App\Models\Generic\Review;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

use App\Models\Payout;
use App\Models\PayoutException;
use App\Policies\BookingPolicy;
use App\Services\Stripe\StripeServices;

class ModeratorBookingManagementServices
{

     //    Render Talent booking main management
     public  function renderModeratorBookingManagement($request) {
         $booking = Booking::where(['id' => $request->booking_id])
             ->with('moderator', 'talent.talentProfessionalInfo.talentCategory', 'servicePricing', 'bookingStatus', 'bookingServicesDetail', 'timerActivity', 'eventActivities.activityType')
             ->first();
         if ( !isset($booking) ) {
             return redirect()->back()->with('error', 'Valid booking selection is required');
         }

         $conversation = Conversation::where('booking_id', $booking->id)
             ->with('initiatedFrom', 'initiatedTo')
             ->first();
         $processedConversation = ChatHelper::processBookingConversation($conversation);
         $bookingChatMessages = ChatHelper::getBookingConversationChat($processedConversation);
         $review = Review::where(['booking_id' => $booking->id, 'reviewer_id' => Auth::id()])->first();

         return view('app.pages.moderator.bookings.booking_activity_management', compact('booking', 'review'))
             ->with(['bookingChatMessages' => $bookingChatMessages,'conversation' => $processedConversation]);
     }


    // start timer
    public  function bookingTimerStart($request) {
        try {
            $booking = Booking::where(['id' => $request->booking_id])
                ->with('timerActivity')
                ->first();
            // check for the timer activity is already in-progress
            if ( !isset($booking) ) {
                return Helper::returnErrorMessage($request, 'No booking found', [], 'Can not perform action');
            }

            $timeStartChecksPassed = Gate::inspect('moderator-can-start-timer', [BookingPolicy::class, $booking]);
            if ( $timeStartChecksPassed->denied() ) {
                return Helper::returnErrorMessage($request, $timeStartChecksPassed->message());
            }

            $timeNow = date('Y-m-d H:i:s');
            $bookingTimerActivity = BookingTimerActivity::where(['booking_id' => $booking->id, 'user_id' => Auth::id()])
                ->first();
            $remainingTime = ['hours' => getHours($request->booking_duration), 'minutes' => getMinutes($request->booking_duration), 'seconds' => getSeconds($request->booking_duration)];
            if ($bookingTimerActivity) {  // update timer-activity
                $remainingTime = ['hours' => $bookingTimerActivity->remaining_hours, 'minutes' => $bookingTimerActivity->remaining_minutes, 'seconds' => $bookingTimerActivity->remaining_seconds];
                $activityExpectedEndTime = self::addTimestamps( strtotime($timeNow), $bookingTimerActivity->remaining_minutes_time * 60 );
                $bookingTimerActivity->update([
                    'booking_id' => $booking->id,
                    'user_id' => Auth::id(),
                    'last_started_at' => $timeNow,
                    'last_stopped_at' => null,
                    'expected_end_time' => $activityExpectedEndTime,
                    'total_minutes_time' => floatval($request->booking_duration),
                    'in_progress' => true,
                ]);
            } else { // new timer-activity
                $remainingTime = [
                    'hours' => getHours($booking->total_minutes_duration),
                    'minutes' => getMinutes($booking->total_minutes_duration),
                    'seconds' => getSeconds($booking->total_minutes_duration)
                ];
                $activityExpectedEndTime = self::addTimestamps( strtotime($timeNow), floatval($request->booking_duration) * 60 );
                $bookingTimerActivity = BookingTimerActivity::create([
                    'booking_id' => $booking->id,
                    'user_id' => Auth::id(),
                    'last_started_at' => $timeNow,
                    'last_stopped_at' => null,
                    'expected_end_time' => $activityExpectedEndTime,
                    'total_minutes_time' => floatval($request->booking_duration),
                    'remaining_minutes_time' => floatval($request->booking_duration),
                    'served_minutes_time' => 0.0,
                    'in_progress' => true,
                ]);
                $booking->update([ // update booking status
                    'booking_status_id' => 6, // in-progress
                ]);
            }

            $timeNow = date('Y-m-d H:i:s');
            $bookingEventActivityCreated = self::createNewBookingEventActivity($booking, 3, $timeNow);

            $eventActivityHtml = view('app.sections.bookings.event_activity_item')->with(['eventActivity' => $bookingEventActivityCreated['bookingEventActivity']])->render();
            $timerStartedData = ['booking' => $booking, 'bookingTimerActivity' => $bookingTimerActivity, 'eventActivity' => $bookingEventActivityCreated['bookingEventActivity'], 'remainingTime' => $remainingTime, 'eventActivityHtml' => $eventActivityHtml];
            event(new EventTimerStarted($timerStartedData));

            return Helper::returnSuccessMessage($request, 'Timer started', null, ['eventActivityHtml' => $eventActivityHtml]);
        } catch (\Exception $e) {
            return Helper::returnErrorMessage($request, 'Something went wrong, please try again');
        }
    }


    //    Render Talent booking main management
    public  function bookingTimerStop($request) {
        try {
            $booking = Booking::where(['id' => $request->booking_id])
                ->with('timerActivity')
                ->first();

            // check, timer activity stoppable
            if ( !isset($booking->timerActivity->last_started_at) || isset($booking->timerActivity->last_stopped_at) ) {
                return Helper::returnErrorMessage($request, 'Please start timer first to stop it!', [], 'Can not perform action');
            }

            $timerStoppedAt = date('Y-m-d H:i:s', strtotime($request->stopped_at));
            $bookingActivity = $booking->timerActivity;
            $currentDurationServed = self::getTimeDifferenceParams($bookingActivity->last_started_at, $timerStoppedAt);
            $totalServedMinutes = isset($bookingActivity->served_minutes_time)
                ? ($bookingActivity->served_minutes_time + $currentDurationServed['difference_in_minutes'])
                : (0.0 + $currentDurationServed['difference_in_minutes']);
            $remainingTimeInMinutes = $bookingActivity->total_minutes_time - $totalServedMinutes;

            $bookingActivity = BookingTimerActivity::where('id', $booking->timerActivity->id)->first();
            $bookingActivityUpdateData = [
                'booking_id' => $booking->id,
                'user_id' => Auth::id(),
                'last_stopped_at' => $timerStoppedAt,
                'expected_end_time' => null,
                'total_minutes_time' => floatval($request->booking_duration),
                'served_minutes_time' => floatval($totalServedMinutes),
                'remaining_minutes_time' => floatval($remainingTimeInMinutes) > 0 ? floatval($remainingTimeInMinutes) : 0.0,
                'remaining_hours' => getHours($remainingTimeInMinutes),
                'remaining_minutes' => getMinutes($remainingTimeInMinutes),
                'remaining_seconds' => getSeconds($remainingTimeInMinutes),
                'in_progress' => false,
            ];
            $bookingActivity->update($bookingActivityUpdateData);

            // create new event activity on timer
            $eventTimerCompleted = false;
            $timeNow = date('Y-m-d H:i:s');
            if ( floatval($remainingTimeInMinutes) > 0.0 ) {
                $activityTypeId = 4; // timer paused
                $bookingEventActivityCreated = self::createNewBookingEventActivity($booking, $activityTypeId, $timeNow);
                $eventActivityHtml = view('app.sections.bookings.event_activity_item')->with(['eventActivity' => $bookingEventActivityCreated['bookingEventActivity']])->render();
                $timerStartedData = ['booking' => $booking, 'bookingTimerActivity' => $bookingActivity, 'eventActivity' => $bookingEventActivityCreated['bookingEventActivity'], 'eventActivityHtml' => $eventActivityHtml, 'eventTimerCompleted' => $eventTimerCompleted];
                event(new EventTimerStopped($timerStartedData));
            } else { // timer-completed
                $eventTimerCompleted = true;
                $activityTypeId = 6; // timer completed
                $bookingEventActivityCreated = self::createNewBookingEventActivity($booking, $activityTypeId, $timeNow);
                $eventActivityHtml = view('app.sections.bookings.event_activity_item')->with(['eventActivity' => $bookingEventActivityCreated['bookingEventActivity']])->render();
                $timeCompletedData = ['booking' => $booking, 'bookingTimerActivity' => $bookingActivity, 'eventActivity' => $bookingEventActivityCreated['bookingEventActivity'], 'eventActivityHtml' => $eventActivityHtml, 'eventTimerCompleted' => $eventTimerCompleted];
                event(new EventTimeCompleted($timeCompletedData));
            }

            return Helper::jsonResponse(1, 'Timer stopped', ['eventActivityHtml' => $eventActivityHtml, 'eventTimerCompleted' => $eventTimerCompleted]);
        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', 'Something went wrong, please try again');
        }
    }

    // approve booking completion
    public  function approveBookingCompletion($request) {
        try {
            $booking = Booking::where(['id' => $request->booking_id])
                ->with('timerActivity', 'talent','talent.stripeAccountDetail')
                ->first();
            if ( !isset($booking) ) {
                return Helper::jsonResponse(0, 'Can not perform action', [], 'No booking found');
            }

            $timeStartChecksPassed = Gate::inspect('moderator-can-approve-completion', [BookingPolicy::class, $booking]);
            if ( $timeStartChecksPassed->denied() ) {
                return Helper::jsonResponse(0, 'Timer stopped', [], $timeStartChecksPassed->message());
            }

            $timeNow = date('Y-m-d H:i:s');
            $bookingEventActivityCreated = self::createNewBookingEventActivity($booking, 8, $timeNow); // approve event completion
            $booking->update([
                'booking_status_id' => 8,
                'remaining_hours' => 0,
                'remaining_minutes' => 0,
                'remaining_seconds' => 0
            ]);
            if ( isset($booking->timerActivity) ) {
                BookingTimerActivity::where(['user_id' => Auth::id(), 'booking_id' => $booking->id])->update([
                    'remaining_minutes_time' => 0.0,
                    'remaining_hours' => 0,
                    'remaining_minutes' => 0,
                    'remaining_seconds' => 0,
                    'in_progress' => 0,
                ]);
            }

            $eventActivityHtml = view('app.sections.bookings.event_activity_item')->with(['eventActivity' => $bookingEventActivityCreated['bookingEventActivity']])->render();

            $approvalEmailData = [
                'toRoleName' => 'talent',
                'notifiable' => $booking->talent,
                'title' => 'Moderator Approved Booking Completion',
                'bodyText' => 'Hi, ' .$booking->talent['full_name']. '! Moderator approved the booking completion. To check the booking event details click the link below',
                'bookingDetailUrl' => route('booking.detail', ['booking_id' => $booking->id]),
            ];
            self::sendApprovalEmail($approvalEmailData);
            self::processPayoutToTalent($booking->id, $booking->talent);

            return Helper::jsonResponse(1, 'Booking Completion Approved', ['eventActivityHtml' => $eventActivityHtml]);
        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Booking Completion Approved', [], 'Something went wrong, please try again');
        }
    }


    //     process payout to talent stripe-connect account
    public static function processPayoutToTalent($bookingId, $talent) {
        try {
            $responseArray = ['success' => false, 'message' => 'Operation Unsuccessful'];
            $booking = Booking::where('id',$bookingId)
                ->with('talent', 'moderator')
                ->first();
            if( $booking && $booking->amount ) {
                $stripeBalance = new \Stripe\StripeClient(config('services.stripe.secret'));
                $balance = $stripeBalance->balance->retrieve();
                $transferAmount = $booking->amount - $booking->service_amount;

                // check for account has send-able balance
                if( $balance && isset($balance->available[0]['amount']) && $transferAmount <= $balance->available[0]['amount'] ) {
                    // check for talent has connect account
                    if( isset($talent->stripeAccountDetail) && $talent->stripeAccountDetail->stripe_account_id ) {
                        $stripeService = new StripeServices();
                        $stripeResponse = $stripeService->transferAmount($transferAmount, $talent->stripeAccountDetail->stripe_account_id,"Payouts Process Against Booking Id ".$bookingId);
                        // check payout processed
                        if( $stripeResponse && isset($stripeResponse['id']) ) {
                            Payout::updateOrCreate(['booking_id' => $bookingId], [
                                'booking_id' => $bookingId,
                                'stripe_payout_id' => $stripeResponse['id'],
                                'amount' => $transferAmount,
                                'currency' => 'USD',
                                'payout_status_id' => 3 // completed
                            ]);
                            $booking->update([
                                'booking_payment_status_id' => 4, // initiated to seller
                            ]);

                            // send payout email to admin
                            $payoutData = [
                                'booking' => $booking,
                                'payout' => Payout::where('booking_id', $booking->id)
                                    ->with('payoutStatus')
                                    ->first(),
                                'adminPayoutNote' => 'Payout generated to talent account successfully',
                                'talentPayoutNote' => 'Payout generated to your Stripe connect-account successfully, the amount will reflect in your Stripe account within seven working days',
                            ];
                            Mail::to(config('mail.admin.address'))->send(new PayoutGeneratedEmail($payoutData)); // to admin
                            Mail::to($booking['talent']['email'])->send(new TalentPayoutGeneratedEmail($payoutData)); // to talent

                            $responseArray['success'] = true;
                            $responseArray['message'] = 'Payout generated to talent successfully';
                        } else {
                            Payout::updateOrCreate(['booking_id' => $bookingId], [
                                'booking_id' => $bookingId,
                                'amount' => $transferAmount,
                                'currency' => 'USD',
                                'payout_status_id' => 5 // failed
                            ]);
                            PayoutException::updateOrCreate(['booking_id' => $bookingId], ['booking_id' => $bookingId,'message' => $stripeResponse]);

                            $responseArray['message'] = 'Payout Failed';
                        }
                    } else { // stripe account not setup yet
                        Payout::updateOrCreate(['booking_id' => $bookingId], [
                            'booking_id' => $bookingId,
                            'amount' => $transferAmount,
                            'currency' => 'USD',
                            'payout_status_id' => 4 // stripe account not setup
                        ]);
                        $responseArray['message'] = 'Talent Stripe account not setup';
                    }
                } else {
                    Payout::updateOrCreate([
                        'booking_id' => $bookingId],
                        ['booking_id' => $bookingId,
                            'amount' => $transferAmount,
                            'currency' => 'USD',
                            'payout_status_id' => 2 // low balance
                    ]);
                    $responseArray['message'] = 'Not enough Stripe balance to generate Payout';
                }
            }

            return $responseArray;
        } catch (\Exception $exception) {
            return ['success' => false, 'message' => $exception->getMessage()];
        }

    }


    // create new booking-activity
    public static function createNewBookingEventActivity($booking, $activityTypeId, $timeNow = null) {
        $timeNow = isset($timeNow) ? $timeNow : date('Y-m-d H:i:s');
        $bookingEventActivity = BookingEventActivity::create([
            'user_id' => Auth::id(),
            'booking_id' => $booking->id,
            'activity_type_id' => $activityTypeId,
            'performed_at' => $timeNow,
        ]);
        return ['bookingEventActivity' => $bookingEventActivity];
    }


    // calculate time difference from dates, in minutes
    public static function getTimeDifferenceParams($timeFrom, $timeTo = null) {
        $hours = 0;
        $minutes = 0;
        $seconds = 0;
        if ( !isset($timeTo) ) {
            $timeTo = date('Y-m-d H:i:s');
        }

        $timeFrom = strtotime($timeFrom);
        $timeTo = strtotime($timeTo);
        $minutesTime = ($timeTo - $timeFrom) / 60; // difference in seconds and convert to minutes
        if ($minutesTime > 0) {
            $seconds = getSeconds($minutesTime);
            $minutes = getMinutes($minutesTime);
            $hours = getHours($minutesTime);
        }
        return ['hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds, 'difference_in_minutes' => $minutesTime];
    }


    //    calculate sum of two timestamps and return datetime
    public static function addTimestamps($timestampOne, $timestampTwo) {
        $activityExpectedEndTimestamp = $timestampOne + $timestampTwo;
        return date('Y-m-d H:i:s', $activityExpectedEndTimestamp);
    }

    //    mark booking time completed/finished
    public static function updateBookingTimerCompleted($bookingObject) {
        $bookingObject->update([
            'last_stopped_at' => date('Y-m-d H:i:s'),
            'expected_end_time' => null,
            'remaining_minutes_time' => 0.0,
            'served_minutes_time' => $bookingObject->total_minutes_duration,
            'remaining_hours' => 0,
            'remaining_minutes' => 0,
            'remaining_seconds' => 0,
            'in_progress' => false,
        ]);
    }


    //    send approval email to Talent
    public static function sendApprovalEmail($notificationData) {
        Mail::to($notificationData['notifiable']['email'])->send(new ModeratorApprovedEventCompletionEmail($notificationData));
    }

}
