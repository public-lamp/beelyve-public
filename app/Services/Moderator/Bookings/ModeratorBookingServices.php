<?php


namespace App\Services\Moderator\Bookings;


use App\Models\Generic\Booking;
use Illuminate\Support\Facades\Auth;

class ModeratorBookingServices
{

    // assigned booking status array
    protected $bookingRequestsStatus = [3];

    // Bookings rejected by talent status
    protected $rejectedBookingsStatus = [5];

    // accepted or in-progress bookings talent status
    protected $assignedBookingsStatus = [4, 6];

    // completed booking status
    protected $completedBookingsStatus = [7, 8, 10];

    // completed booking status
    protected $allBookings = [3, 4, 5, 6, 7, 8, 9, 10];

    //    pagination records
    private $paginationCount = 5;


    //   render moderator dashboard main-page
    public function moderatorProjectsListing($request) {
        $moderator = Auth::user();
        $orderByKey = isset($request->order_by) ? $request->order_by : 'id';
        $bookingsStatus = isset($request->booking_status) ? $request->booking_status : 'upcoming';

        if ($bookingsStatus === 'upcoming') {
            $bookings = Booking::where('moderator_id', $moderator->id)
                ->whereIn('booking_status_id', $this->assignedBookingsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate(5);
        } elseif ($bookingsStatus === 'completed') {
            $bookings = Booking::where('moderator_id', $moderator->id)
                ->whereIn('booking_status_id', $this->completedBookingsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate(5);
        } else {
            $bookings = Booking::where('moderator_id', $moderator->id)
                ->whereIn('booking_status_id', $this->allBookings)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate(5);
        }

        return view('app.pages.moderator.bookings.bookings-listing-page', compact('moderator', 'bookings', 'bookingsStatus'));
    }


    // Booking-requests to talent
    public function bookingRequestsModerator($request) {
        $moderator = Auth::user();
        $orderByKey = isset($request->order_by) ? $request->order_by : 'id';
        $bookingsStatus = isset($request->booking_status) ? $request->booking_status : 'requested';

        if ($bookingsStatus === 'rejected') {
            $bookings = Booking::where('moderator_id', $moderator->id)
                ->whereIn('booking_status_id', $this->rejectedBookingsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        } else {
            $bookings = Booking::where('moderator_id', $moderator->id)
                ->whereIn('booking_status_id', $this->bookingRequestsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        }

        return view('app.pages.moderator.bookings.moderator_booking_requests', compact('moderator', 'bookings', 'bookingsStatus'));
    }

}
