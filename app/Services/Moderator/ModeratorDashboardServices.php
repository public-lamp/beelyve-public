<?php


namespace App\Services\Moderator;


use App\Models\Generic\Booking;
use App\Models\Generic\BookingPayment;
use App\Models\StripeAccount\UserStripeAccountVerification;
use App\Models\Generic\FavouriteUser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ModeratorDashboardServices
{

    // accepted or in-progress bookings talent status
    protected $assignedBookingsStatus = [4, 6];

    // completed booking status
    protected $completedBookingsStatus = [8, 10];

    //   render moderator dashboard main-page
    public function moderatorDashboardRender($request) {
        $moderator = User::where('id', Auth::id())
            ->with([ 'roles', 'userAddress', 'moderatorBookings'])
            ->first();

        $orderByKey = 'id';

        $assignedBookings = Booking::where('moderator_id', $moderator->id)
            ->whereIn('booking_status_id', $this->assignedBookingsStatus)
            ->orderBy($orderByKey, 'DESC')
            ->with('moderator', 'talent', 'servicePricing', 'bookingStatus')
            ->paginate(3);

        $completedBookings = Booking::where(['moderator_id' => $moderator->id])
            ->whereIn('booking_status_id', $this->completedBookingsStatus)
            ->orderBy($orderByKey, 'DESC')
            ->with('moderator', 'talent', 'servicePricing', 'bookingStatus')
            ->paginate(3);

        $moderatorSpending = BookingPayment::where('user_id', $moderator->id)
            ->whereIn('booking_payment_status_id', [2, 3, 4, 5]) // Booking charges have been paid by moderator
            ->get()->sum('amount');

        $btnUrl = null;
        if (Auth::user()->stripe_account_id == NULL) {
            $uniqueToken = rand(10, 1000) . time();
            UserStripeAccountVerification::updateOrCreate(['user_id'=>Auth::id()],['user_id'=>Auth::id(),'verification_code'=>$uniqueToken,'status'=>1]);
            $btnUrl = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" .config('services.stripe.client_id') . "&scope=read_write&state=" . $uniqueToken . "&redirect_uri=" .config('services.application.url') . "/stripe/connectAccount-callback";
        }

        return view('app.pages.moderator.moderator-dashboard-main', compact('moderator', 'assignedBookings', 'completedBookings', 'moderatorSpending','btnUrl'));
    }


    //   render moderator dashboard main-page
    public function favouriteUsersModerator() {
        $favouriteUsers = FavouriteUser::where('user_id', Auth::id())
            ->with('talent.talentProfessionalInfo.talentCategory')
            ->paginate();

        return view('app.pages.moderator.favourite_users_listing')->with(['favouriteUsers' => $favouriteUsers]);
    }

}
