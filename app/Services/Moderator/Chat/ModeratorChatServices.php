<?php


namespace App\Services\Moderator\Chat;


use App\Services\Common\Chat\ChatServices;

class ModeratorChatServices
{

    //   render moderator chat conversations page
    public function chatConversationsRender($request) {
        $chatService = new ChatServices();
        $conversations = $chatService->getConversations();
        return view('app.pages.moderator.chat.conversations_list', compact('conversations'));
    }

    //   render moderator chat messages page
    public function chatMessagesRender($request) {
        $chatService = new ChatServices(); // instantiate the common chat service
        $conversations = $chatService->getConversations(); // get auth user conversations
        $lastActiveConversation = null;
        $lastActiveConversationMessages = [];
        if ( count($conversations) > 0 ) {
            $lastActiveConversation = $conversations[0]; // last active conversation
            $lastActiveConversationMessages = $chatService->getConversationMessages($lastActiveConversation['id']);
        }

        return view('app.pages.moderator.chat.messages', compact('conversations', 'lastActiveConversation', 'lastActiveConversationMessages'));
    }

}
