<?php


namespace App\Services\Front;

use App\Helpers\Helper;
use App\Mail\Admin\ContactInquiryEmail;
use App\Models\Generic\ContactInquiry;
use App\Models\Generic\EmailSubscriber;
use App\Models\Generic\FeaturedUser;
use App\Models\User;
use App\Services\Talent\TalentDashboardServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class FrontServices
{

    //    render home page
    public function renderHomeView($request) {
        $authUser = Auth::user();
        $user = null;
        if ($authUser) {
            $user = User::where('id', $authUser->id)->with('favoriteTalents')
                ->first();
        }
        $homePageTalents = User::where('basic_profile_completed', true)
            ->whereHas('roles', function ($q) {
                $q->where('roles.id', 2);
            })->take(6)
            ->get();

        foreach($homePageTalents as $key => $talent) {
            $homePageTalents[$key] = TalentDashboardServices::talentCompleteProfile($talent->id);
        }

        // find and process featured Talents
        $featuredTalentsData = FeaturedUser::where('role_id', 2)->with('user')->get();
        $featuredTalents = [];
        foreach($featuredTalentsData as $featureTalent) {
            if (isset($featureTalent->user) && $featureTalent->user->basic_profile_completed) {
                $featuredTalents[] = TalentDashboardServices::talentCompleteProfile($featureTalent->user_id);
            }
        }

        return view('app.pages.front.home', compact('featuredTalents', 'homePageTalents', 'user'));
    }

    //    render about us page
    public function renderAboutView($request) {
        return view('app.pages.front.about-us');
    }

    //    render contact-us page
    public function renderContactView($request) {
        return view('app.pages.front.contact-us');
    }

    //    render contact-us page
    public function renderFaqView($request) {
        return view('app.pages.front.faq');
    }

    //    render contact-us page
    public function renderTermsConditionsView($request) {
        return view('app.pages.front.terms-conditions');
    }

    //    render privacy page
    public function renderPrivacyPolicyView($request) {
        return view('app.pages.front.privacy-policy');
    }


    //   submit user contact-inquiry
    public function submitContactQuery($request) {
        try {
            $contactInquiry = ContactInquiry::create([
                'user_id' => Auth::id(),
                'email' => $request->contact_email,
                'title' => $request->inquiry_title,
                'message' => $request->message,
            ]);

            Mail::to(config('mail.admin.address'))->send(new ContactInquiryEmail($contactInquiry));

            return Helper::jsonResponse(1, 'Your query submitted successfully, we wil contact you shortly');
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }
    }


    //   submit user contact-inquiry
    public function mailingListSubscribe($request) {
        try {
            EmailSubscriber::updateOrCreate(['email' => $request->subscriber_email], [
                'user_id' => Auth::id(),
                'email' => $request->subscriber_email,
            ]);

            return Helper::jsonResponse(1, 'Subscribed to mailing list successfully');
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], $e->getMessage());
        }
    }


    //    generic subscribe user/guest to mailing list
    public static function subscribeEmailToMailingList($email) {
        try {
            EmailSubscriber::updateOrCreate(['email' => $email], [
                'user_id' => Auth::id(),
                'email' => $email,
            ]);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}
