<?php


namespace App\Services\Talent;

use App\Helpers\Helper;
use App\Models\Talent\TalentServicesMedium;
use App\Models\Talent\TalentServicesPricing;
use Illuminate\Support\Facades\Auth;

class TalentServicesPricingServices
{

    //    talent services pricing, set
    public function setTalentServicesPricing($request) {
        try {
            $user = Auth::user();
            TalentServicesPricing::where('user_id', $user->id)->forceDelete();
            foreach ( $request->pricing as $pricingTypeKey => $pricingType ) {
                $data = TalentServicesPricing::create([
                    'user_id' => $user->id,
                    'services_pricing_type_id' => $pricingType['id'],
                    'services_pricing_slot_id' => $pricingType['duration'],
                    'price' => $pricingType['price'],
                    'main_features' => $pricingType['main_features'],
                    'description' => $pricingType['description'],
                ]);
                $this->saveServiceMediums($user, $pricingType['id'], $pricingType['mediums']);
            }
            TalentDashboardServices::updateTalentProfileCompleteStatus($user);
            TalentDashboardServices::updateTalentBasicProfileStatus($user->id);

            return Helper::jsonResponse(1, 'Services pricing saved successfully');
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'something went wrong', $e->getMessage());
        }
    }

    //    save talent service mediums
    public function saveServiceMediums($user, $servicePricingTypeId, $mediums) {
        TalentServicesMedium::where(['user_id' => $user->id, 'services_pricing_type_id' => $servicePricingTypeId])->forceDelete();
        foreach($mediums as $medium) {
            TalentServicesMedium::create([
                'user_id' => $user->id,
                'services_medium_id' => $medium,
                'services_pricing_type_id' => $servicePricingTypeId,
            ]);
        }
    }

}
