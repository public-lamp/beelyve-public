<?php


namespace App\Services\Talent;


use App\Helpers\Helper;
use App\Models\Generic\TalentWorkFile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class WorkHistoryServices
{

        // Talent work history update
        //    public function saveTalentWorkFiles($request) {
        //        $user = Auth::user();
        //
        //        if ( !$request->hasFile('work_video') ) {
        //            return Helper::jsonResponse(0, 'Can not perform action', [], 'Valid video file selection is required');
        //        }
        //
        //        TalentWorkFile::where(['talent_id' => $user->id, 'type' => 'image'])->delete();
        //        foreach($request->work_images as $image) {
        //            if ( !$image->isValid() ) {
        //                return Helper::jsonResponse(0, 'Can not perform action', [], 'Valid images selection is required');
        //            }
        //            $imageName = Helper::uniqueBytes().'_'.time().'_'.str_replace(' ', '', $image->getClientOriginalName());
        //            $imagePath = $image->storeAs('visuals/talent/workImages', $imageName, ['disk' => 'visuals']);
        //            TalentWorkFile::create([
        //                'talent_id' => $user->id,
        //                'type' => 'image',
        //                'file_url' => $imagePath,
        //            ]);
        //        }
        //
        //        $videoName = Helper::uniqueBytes().'_'.time().'_'.str_replace(' ', '', $request->work_video->getClientOriginalName());
        //        $videoPath = $request->file('work_video')
        //            ->storeAs('visuals/talent/workVideos', $videoName, ['disk' => 'visuals']);
        //        TalentWorkFile::updateOrCreate(['talent_id' => $user->id, 'type' => 'video'], [
        //            'talent_id' => $user->id,
        //            'type' => 'video',
        //            'file_url' => $videoPath,
        //        ]);
        //
        //        return Helper::jsonResponse(1, 'Work data saved successfully', [], '');
        //    }


        //    save work images and youtube video link
        public function saveTalentWorkFiles($request) {
            $user = Auth::user();

            TalentWorkFile::where(['talent_id' => $user->id, 'type' => 'image'])->delete();
            foreach($request->work_images as $image) {
                if ( !$image->isValid() ) {
                    return Helper::jsonResponse(0, 'Can not perform action', [], 'Valid images selection is required');
                }
                $imageName = Helper::uniqueBytes().'_'.time().'_'.str_replace(' ', '', $image->getClientOriginalName());
                $imagePath = $image->storeAs('visuals/talent/workImages', $imageName, ['disk' => 'visuals']);
                TalentWorkFile::create([
                    'talent_id' => $user->id,
                    'type' => 'image',
                    'file_url' => $imagePath,
                ]);
            }

            TalentWorkFile::updateOrCreate(['talent_id' => $user->id, 'type' => 'video'], [
                'talent_id' => $user->id,
                'type' => 'video',
                'file_url' => $request->work_video_link,
            ]);

            return Helper::jsonResponse(1, 'Work data saved successfully', [], '');
        }

    // Talent work history get|Show
    public function showTalentWorkFiles($request) {
        $user = Auth::user();

        $talent = User::where('id', $user->id)
            ->with('talentWorkFiles')
            ->first();

        return view('app.pages.talent.bookings.booking_activity_management');
    }


    // Talent work history delete
    public function deleteTalentWorkFile($request) {
        $user = Auth::user();
        TalentWorkFile::where('id', $request->id)->delete();
        return redirect()->back()->with('success', 'Work file deleted successfully');
    }

}
