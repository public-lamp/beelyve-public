<?php


namespace App\Services\Talent;


use App\Helpers\Helper;
use App\Models\Generic\AvailabilitySlot;
use App\Models\Generic\UserAvailabilityDay;
use App\Models\Generic\UsersAvailabilityCategory;
use App\Models\Generic\WeekDay;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TalentAvailabilityServices
{

    //    set talent availability
    public function setTalentAvailability($request) {
        try {
            $user = Auth::user();
            $availabilityDays = $request->days;
            $userAvailabilityCategoryId = $request->availability_category;
            UsersAvailabilityCategory::where('user_id', $user->id)->forceDelete();
            UserAvailabilityDay::where('user_id', $user->id)->forceDelete();
            AvailabilitySlot::where('user_id', $user->id)->forceDelete();
            $userTimeZone = $user->time_zone;

            UsersAvailabilityCategory::create([
                'user_id' => $user->id,
                'availability_category_id' => $userAvailabilityCategoryId
            ]);
            if ($userAvailabilityCategoryId == 2) {
                foreach($availabilityDays as $daySlug => $day) {
                    $weekDay = WeekDay::where('slug', $daySlug)->first();
                    $dayIsActive = isset( $availabilityDays[$daySlug]['time']['time_from'], $availabilityDays[$daySlug]['time']['time_to'] );
                    $userAvailabilityDay = UserAvailabilityDay::create([
                        'user_id' => $user->id,
                        'week_day_id' => $weekDay->id,
                        'is_available' => $dayIsActive,
                    ]);
                    if ($dayIsActive) {
                        for ( $i = 0; $i < count($day['time']['time_from']); $i++) {
                            if ( !empty( $day['time']['time_from'][$i] ) && !empty( $day['time']['time_to'][$i] ) ) {
                                AvailabilitySlot::create([
                                    'user_id' => $user->id,
                                    'user_availability_day_id' => $userAvailabilityDay->id,
                                    'day' => $weekDay->name,
                                    'time_from' => $day['time']['time_from'][$i],
                                    'time_to' => $day['time']['time_to'][$i],
                                    // 'time_from' => parseTimeWithTimeZone($day['time']['time_from'][$i], 'UTC', $userTimeZone, 'h:i A'),
                                    // 'time_to' => parseTimeWithTimeZone($day['time']['time_to'][$i], 'UTC', $userTimeZone, 'h:i A'),
                                ]);
                            }
                        }
                    }
                }
            }

            TalentDashboardServices::updateTalentProfileCompleteStatus($user);
            TalentDashboardServices::updateTalentBasicProfileStatus($user->id);

            return Helper::jsonResponse(1, 'Availability saved successfully', [], '');
        } catch( \Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }

    }


}
