<?php


namespace App\Services\Talent;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class TalentProfileServices
{

    //    talent public profile
    public function publicTalentProfile($request) {
        try {
            $talentId = encryptor('decrypt', $request->talent_id);
            $talent = User::where('id', $talentId)
                ->with([
                    'userAddress', 'talentBasicInfo', 'talentProfessionalInfo', 'talentWorkFiles',
                    'talentProfessionalInfo.talentCategory', 'availabilityCategory', 'userAvailabilityDays',
                    'userAvailabilityDays.weekDay', 'userAvailabilityDays.availabilitySlots', 'reviews.reviewer'
                ])
                ->first();

            $user = null;
            if (Auth::check()) {
                $user = User::where('id', Auth::id())->with('favoriteTalents')
                    ->first();
            }

            return view('app.pages.talent.public-profile', compact('talent', 'user'));
        } catch(\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

}
