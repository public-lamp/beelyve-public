<?php


namespace App\Services\Talent\Bookings;


use App\Http\Controllers\Payment\PaymentController;
use App\Models\Generic\Booking;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class TalentBookingServices
{

    // booking-requests status array
    protected $bookingRequestsStatus = [3];

    // rejected-booking status array
    protected $rejectedBookingsStatus = [5];

    // assigned booking status array
    protected $assignedBookingsStatus = [4, 6];

    // completed booking status array
    protected $completedBookingsStatus = [7, 8, 10];

    // all-bookings status array
    protected $allBookings = [3, 4, 5, 6, 7, 8, 9, 10];

    //    pagination records
    private $paginationCount = 5;

    // talent bookings listings
    public function listingTalentBookings($request) {
        $talent = Auth::user();
        $orderByKey = isset($request->order_by) ? $request->order_by : 'id';
        $bookingsStatus = isset($request->booking_status) ? $request->booking_status : 'upcoming';

        if ($bookingsStatus === 'upcoming') {
            $bookings = Booking::where('talent_id', $talent->id)
                ->whereIn('booking_status_id', $this->assignedBookingsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        } elseif ($bookingsStatus === 'completed') {
            $bookings = Booking::where('talent_id', $talent->id)
                ->whereIn('booking_status_id', $this->completedBookingsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        } elseif ($bookingsStatus === 'overdue') {
            $bookings = Booking::where('talent_id', $talent->id)
                ->whereIn('booking_status_id', $this->assignedBookingsStatus)
                ->where('time_from', '<', date('Y-m-d H:i'))
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        } else {
            $bookings = Booking::where('talent_id', $talent->id)
                ->whereIn('booking_status_id', $this->allBookings)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        }

        return view('app.pages.talent.bookings.bookings_listing_page', compact('talent', 'bookings', 'bookingsStatus'));
    }


    // Booking-requests to talent
    public function bookingRequestsTalent($request) {
        $talent = Auth::user();
        $orderByKey = isset($request->order_by) ? $request->order_by : 'id';
        $bookingsStatus = isset($request->booking_status) ? $request->booking_status : 'requested';

        if ($bookingsStatus === 'rejected') {
            $bookings = Booking::where('talent_id', $talent->id)
                ->whereIn('booking_status_id', $this->rejectedBookingsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        } else {
            $bookings = Booking::where('talent_id', $talent->id)
                ->whereIn('booking_status_id', $this->bookingRequestsStatus)
                ->orderBy($orderByKey, 'DESC')
                ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
                ->paginate($this->paginationCount);
        }

        return view('app.pages.talent.bookings.booking_requests', compact('talent', 'bookings', 'bookingsStatus'));
    }


    // Action booking request (Accept | Reject)
    public function bookingRequestAction($request) {
        $talent = Auth::user();
        $booking = Booking::where(['id' => $request->booking_id, 'talent_id' => $talent->id])
            ->with('timerActivity', 'moderator', 'moderator.stripeAccountDetail')
            ->first();
        if ( !$booking ) {
            return redirect()->back()->with('error', 'Valid booking selection is required');
        }

        $userWithConflictBookings = User::where('id', Auth::id())
            ->with(['talentBookings' => function ($q) use($booking) {
                $q->whereIn('bookings.booking_status_id', [4 , 6, 7]) // accepted, in-progress and supposedly-closed
                    ->where(function ($query) use($booking) {
                        $query->whereRaw('? BETWEEN bookings.time_from AND bookings.time_to',
                            [ date('Y-m-d H:i:s', strtotime($booking['time_from'])) ]
                        )
                        ->orWhereRaw('? BETWEEN bookings.time_from AND bookings.time_to',
                            [ date('Y-m-d', strtotime($booking['time_to'])) ]
                        )
                        ->orWhereBetween('bookings.time_from',
                            [
                                date( 'Y-m-d H:i:s', strtotime($booking['time_from']) ),
                                date( 'Y-m-d', strtotime($booking['time_to']) )
                            ]
                        )
                        ->orWhereBetween('bookings.time_to',
                            [
                                date( 'Y-m-d H:i:s', strtotime($booking['time_from']) ),
                                date( 'Y-m-d', strtotime($booking['time_to']) )
                            ]
                        );
                    });
            }])->first();
        $conflictingBookings = $userWithConflictBookings->talentBookings;
        if ( $conflictingBookings->isNotEmpty() ) {
            Session::flash('error', 'Can not accept, Current booking time slot conflicts with one of the assigned booking');
            return redirect()->back()->with(['conflictingBookings' => $conflictingBookings]);
        }

        if ( $request->accept == true ) {
            $booking->update([
                'booking_status_id' => 4, // accepted
            ]);
        } elseif ( $request->accept == false ) {
            // transfer back the amount to moderator connects-account
            $paymentControllerObject = new PaymentController();
            $paymentControllerObject->processPayoutToModerator($booking->id, $booking->moderator);

            $booking->update([
                'booking_status_id' => 5, // rejected
                'booking_payment_status_id' => 7, // initiated to Moderator's connect-account
            ]);

        } else {
            return redirect()->back()->with('error', 'Invalid action, can only accept or reject a booking invitation');
        }

        return redirect()->back()->with('success', 'Action performed successfully');
    }



}
