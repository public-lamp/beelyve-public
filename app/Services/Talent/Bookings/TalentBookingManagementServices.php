<?php


namespace App\Services\Talent\Bookings;

use App\Events\Bookings\TalentMarkedEventCompleted;
use App\Events\Bookings\TalentMarkedPresence;
use App\Helpers\Chat\ChatHelper;
use App\Helpers\Helper;
use App\Models\Chat\Conversation;
use App\Models\Generic\Booking;
use App\Models\Generic\Event\BookingEventActivity;
use App\Policies\BookingPolicy;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TalentBookingManagementServices
{

     //    Render Talent booking main management
     public  function talentBookingManagementRender($request) {
         if ( !isset($request->booking_id) ) {
             if ($request->ajax()) {
                 return Helper::jsonResponse(0, 'Can not perform action', [], 'Booking selection is required');
             }
             return redirect()->back()->with('error', 'Booking selection is required');
         }

         $booking = Booking::where(['id' => $request->booking_id])
             ->with(['moderator', 'talent.talentProfessionalInfo.talentCategory', 'servicePricing', 'bookingStatus', 'bookingServicesDetail', 'eventActivities.activityType', 'bookingConversation'])
             ->first();

         $conversation = Conversation::where('booking_id', $booking->id)
             ->with('initiatedFrom', 'initiatedTo')
             ->first();
         $processedConversation = ChatHelper::processBookingConversation($conversation);
         $bookingChatMessages = ChatHelper::getBookingConversationChat($processedConversation);

         return view('app.pages.talent.bookings.booking_activity_management', compact('booking', 'bookingChatMessages'))->with(['conversation' => $processedConversation]);
     }

    // mark talent is present for the event
    public  function markTalentPresent($request) {
        $booking = Booking::where(['id' => $request->booking_id, 'talent_id' => Auth::id()])
            ->whereIn('booking_status_id', [4, 6]) // in-progress or accepted/assigned
            ->with('bookingStatus', 'moderator', 'talent', 'timerActivity', 'eventActivities')
            ->first();
        if (!$booking) {
            return Helper::returnErrorMessage($request, 'Booking not in today events list or already completed');
        }

        $markPresentCheckPassed = Gate::inspect('talent-can-mark-presence', [BookingPolicy::class, $booking, $booking->timerActivity, $booking->eventActivities]);
        if ( $markPresentCheckPassed->denied() ) {
            return Helper::returnErrorMessage($request, $markPresentCheckPassed->message());
        }

        $timeNow = date('Y-m-d H:i:s');
        $bookingEventActivityCreated = self::createNewBookingEventActivity($booking, 1, $timeNow);
        $eventActivityHtml = view('app.sections.bookings.event_activity_item')->with(['eventActivity' => $bookingEventActivityCreated['bookingEventActivity']])->render();
        $talentPresentEventData = ['booking' => $booking, 'eventActivity' => $bookingEventActivityCreated['bookingEventActivity'], 'eventActivityHtml' => $eventActivityHtml];
        event(new TalentMarkedPresence($talentPresentEventData)); // fire talent-present event

        $redirect = redirect()->back()->with(['booking' => $booking]);
        return Helper::returnSuccessMessage($request, 'Operation successful', $redirect, ['booking' => $booking]);
    }


    // mark talent is present for the event
    public  function markTimeCompletedTalent($request) {
        try {
            $booking = Booking::where(['id' => $request->booking_id, 'talent_id' => Auth::id()])
                ->whereIn('booking_status_id', [6]) // in-progress or accepted/assigned
                ->with('bookingStatus', 'moderator', 'talent')
                ->first();
            if (!$booking) {
                return Helper::returnErrorMessage($request, 'Booking not in in-progress list or already completed');
            }

            $checksPassed = Gate::inspect('talent-can-mark-booking-complete', [BookingPolicy::class, $booking]);
            if ( $checksPassed->denied() ) {
                return Helper::returnErrorMessage($request, $checksPassed->message());
            }

            $timeNow = date('Y-m-d H:i:s');
            $bookingEventActivityCreated = self::createNewBookingEventActivity($booking, 2, $timeNow); // create completed activity
            $eventActivityHtml = view('app.sections.bookings.event_activity_item')->with(['eventActivity' => $bookingEventActivityCreated['bookingEventActivity']])->render();
            $talentMarkedBookingCompletedData = ['booking' => $booking, 'eventActivity' => $bookingEventActivityCreated['bookingEventActivity'], 'eventActivityHtml' => $eventActivityHtml];
            event(new TalentMarkedEventCompleted($talentMarkedBookingCompletedData)); // fire talent-present event

            $booking->update([
                'booking_status_id' => 7
            ]);

            $redirect = redirect()->back()->with(['booking' => $booking]);
            return Helper::returnSuccessMessage($request, 'Operation successful', $redirect, ['booking' => $booking]);
        } catch (\Exception $e) {
            return Helper::returnErrorMessage($request, 'Something went wrong, please try again');
        }
    }


    // create new booking-activity
    public static function createNewBookingEventActivity($booking, $activityTypeId, $timeNow = null) {
        $timeNow = isset($timeNow) ? $timeNow : date('Y-m-d H:i:s');
        $bookingEventActivity = BookingEventActivity::create([
            'user_id' => Auth::id(),
            'booking_id' => $booking->id,
            'activity_type_id' => $activityTypeId,
            'performed_at' => $timeNow,
        ]);
        return ['bookingEventActivity' => $bookingEventActivity];
    }

}
