<?php


namespace App\Services\Talent;


use App\Helpers\Helper;
use App\Models\Generic\Booking;
use App\Models\Generic\UserAddress;
use App\Models\StripeAccount\UserStripeAccountVerification;
use App\Models\Talent\TalentBasicInfo;
use App\Models\Talent\TalentProfessionalInfo;
use App\Models\User;
use App\Services\Talent\Bookings\TalentBookingServices;
use App\VerificationModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TalentDashboardServices
{

    //   return talent dashboard main
    public function talentDashboardRender($request) {
        $user = Auth::user();
        $talent = self::talentCompleteProfile($user->id);

        $orderByKey = 'id';

        $assignedBookings = Booking::where('talent_id', $user->id)
            ->whereIn('booking_status_id', [4, 6]) // accepted by the talent|Upcoming bookings
            ->orderBy($orderByKey, 'DESC')
            ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
            ->paginate(3);

        $completedBookings = Booking::where(['talent_id' => $user->id])
            ->whereIn('booking_status_id', [8, 10]) // completed status
            ->orderBy($orderByKey, 'DESC')
            ->with('moderator', 'talent', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
            ->paginate(3);

        $talentEarning = Booking::where('talent_id', $user->id)
            ->whereIn('booking_payment_status_id', [4, 5, 6]) // initiated or received amount to talent
            ->get()
            ->sum('amount');

        self::updateTalentProfileCompleteStatus($user); // check and update talent complete profile status
        self::updateTalentBasicProfileStatus($user->id); // check and update talent basic complete profile status

        // stripe connect-account link handling
        $btnUrl = null;
        if (Auth::user()->stripe_account_id == NULL) {
            $uniqueToken = rand(10, 1000) . time();
            UserStripeAccountVerification::updateOrCreate(['user_id'=>Auth::id()],['user_id'=>Auth::id(),'verification_code'=>$uniqueToken,'status'=>1]);
            // $btnUrl = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=".config('services.stripe.client_id') . "&scope=read_write&state=" . $uniqueToken . "&redirect_uri=" .config('services.application.url') . "/stripe/connectAccount-callback";
            $btnUrl = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" .config('services.stripe.client_id') . "&scope=read_write&state=" . $uniqueToken . "&redirect_uri=" .config('services.stripe.redirect_uri');
        }

        return view('app.pages.talent.talent-dashboard-main', compact('talent', 'assignedBookings', 'completedBookings', 'talentEarning','btnUrl') );
    }

    //   return talent profile edit page
    public function talentProfileEdit($request) {
        $user = Auth::user();
        $talentProfile = self::talentCompleteProfile($user->id);
        $existingTaglines = TalentProfessionalInfo::distinct()->pluck('tagline');

        return view('app.pages.talent.profile.basic_profile', compact('talentProfile', 'existingTaglines'));
    }

    //   return talent dashboard main
    public function updateTalentProfileBasic($request) {
        try {
            $user = Auth::user();

            $imageUploadResult = [];
            if (  filled($request->user_image) ) {
                $imageUploadResult = Helper::uploadImage($request->user_image);
                if ( !$imageUploadResult['success'] ) {
                    return Helper::jsonResponse(0, 'Image upload unsuccessful', [], $imageUploadResult['error']);
                }
            }

            // save basic info
            TalentBasicInfo::updateOrCreate(['user_id' => $user->id],
                [
                    'fb_profile_link' => $request->fb_profile_link,
                    'insta_profile_link' => $request->insta_profile_link,
                    // 'linkedin_profile_link' => $request->linkedin_profile_link,
                ]
            );

            // save professional info
            TalentProfessionalInfo::updateOrCreate(['user_id' => $user->id],
                [
                    'talent_category_id' => $request->talent_category_id,
                    'experience' => $request->experience,
                    'tagline' => $request->tagline,
                    'start_rate' => $request->talent_start_rate,
                    'service' => $request->talent_service
                ]
            );

            // save user info
            UserAddress::updateOrCreate(['user_id' => $user->id],
                [
                    'country' => $request->country
                ]
            );

            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'user_name' => $request->user_name,
                'image' => !empty($imageUploadResult) ? $imageUploadResult['data']['url'] : $user->image,
                'about' => $request->talent_about,
                'profile_completed' => isset($request->profile_completed),
            ]);

            self::updateTalentProfileCompleteStatus($user);
            self::updateTalentBasicProfileStatus($user->id);

            return Helper::jsonResponse(1, 'Basic information saved successfully', []);

        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }
    }

    //   return complete profile
    public static function talentCompleteProfile($talentUserId) {
        return $talentProfile = User::where('id', $talentUserId)
            ->with([ 'roles', 'userAddress', 'talentBasicInfo', 'talentProfessionalInfo', 'talentWorkFiles', 'talentProfessionalInfo.talentCategory',
                'availabilityCategory', 'userAvailabilityDays', 'userAvailabilityDays.weekDay', 'userAvailabilityDays.availabilitySlots',
                'talentServicesPricing.servicesPricingType.talentServicesMediums.servicesMedium', 'talentServicesPricing.servicesPricingSlot',
                'talentServicesMediums', 'socialAccounts', 'reviews'])
            ->first();
    }

    //    check for the talent profile completed
    public static function updateTalentProfileCompleteStatus($user) {
        $talentProfile = self::talentCompleteProfile($user['id']);

        $profileIsCompleted = false;
        $profileBasicsAndAvailabilitySet = isset(
            $talentProfile['talentBasicInfo'], // basic info,
            $talentProfile['talentProfessionalInfo'], // professional info
            $talentProfile['availabilityCategory'], // availability
        );
        $pricingIsSet = $talentProfile['talentServicesPricing']->isNotEmpty();
        $workFilesAreSet = $talentProfile['talentWorkFiles']->isNotEmpty();

        if ( $profileBasicsAndAvailabilitySet && $pricingIsSet && $workFilesAreSet ) {
            $profileIsCompleted = true;
        }

        // update talent profile-completed status
        User::where('id', $user['id'])->update([
            'profile_completed' => $profileIsCompleted,
        ]);

        Session::put('userProfileIsComplete', $profileIsCompleted);
        return $profileIsCompleted;

    }


    //    check for the talent basic profile completed
    public static function updateTalentBasicProfileStatus($talentUserId) {
        $talentProfile = self::talentCompleteProfile($talentUserId);

        $basicProfileIsCompleted = false;
        $profileBasicsAndAvailabilitySet = isset(
            $talentProfile['talentBasicInfo'], // basic info,
            $talentProfile['talentProfessionalInfo'], // professional info
            $talentProfile['availabilityCategory'], // availability
        );
        $pricingIsSet = $talentProfile['talentServicesPricing']->isNotEmpty();

        if ( $profileBasicsAndAvailabilitySet && $pricingIsSet ) {
            $basicProfileIsCompleted = true;
        }

        // update talent profile-completed status
        User::where('id', $talentUserId)->update([
            'basic_profile_completed' => $basicProfileIsCompleted,
        ]);

        Session::put('userBasicProfileIsComplete', $basicProfileIsCompleted);
        return $basicProfileIsCompleted;

    }

}
