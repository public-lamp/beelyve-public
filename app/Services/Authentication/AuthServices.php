<?php


namespace App\Services\Authentication;

use App\Helpers\AuthHelper;
use App\Models\Auth\PasswordReset;
use App\Services\Front\FrontServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Auth\UserRole;
use App\Models\User;
use App\Helpers\Helper;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Mail\Authentication\ResetPasswordMail;

class AuthServices
{

    //    returns the signup view
    public function renderSignupForm()
    {
        Session::flash('show_signup_modal', true);
        return redirect()->route('home');
    }


    // signup user
    public function actionSignup($request) {
        try {
            DB::beginTransaction();
            $user = User::create([
                'user_name' => $request->user_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'time_zone' => $request->time_zone,
            ]);
            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $request->role_id
            ]);
            event( new Registered($user) ); // laravel default registered event
            Auth::loginUsingId($user->id);
            FrontServices::subscribeEmailToMailingList($user->email); // add newly created user to mailing subscriber list
            DB::commit();

            return Helper::jsonResponse(1, 'Registration successful! Please check your email to verify your account', $user, '');

        } catch (\Exception $e) {
            DB::rollback(); // rollback logged transactions
            return Helper::jsonResponse(0, 'Something went wrong', [], $e->getMessage());
        }
    }


    //    render the login's form modal
    public function formLogin() {
        Session::flash('show_login_modal', true);
        return redirect()->route('home');
    }


    //    login user
    public function userLogin($request) {
        try {
            $loginSucceeded = Auth::attempt(
                ['email' => $request->login_email, 'password' => $request->login_password],
                $request->remember_me);
            if( !$loginSucceeded) {
                return Helper::jsonResponse(0, 'Login unsuccessful', [], 'Incorrect email or password');
            }

            $user = User::where('email', $request->login_email)
                ->with('roles')
                ->first();
            $user->update(['time_zone' => $request->time_zone]);

            // verify non-admin user
            if ( $user->isAdmin() ) {
                Auth::logout();
                return Helper::jsonResponse(0, 'No user found', [], 'User not found');
            }

            Session::forget('show_login_modal'); // remove login modal session-key
            $nextUrl = AuthHelper::determineNextRoute(route('home'), $user->roles);
            return Helper::jsonResponse(1, 'Login successful', ['next_url' =>  $nextUrl], '');
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], $e->getMessage());
        }
    }


    //    returns the signup view
    public function noticeVerification() {
        return view('app.pages.authentication.verify-email-alert');
    }


    //    verify email
    public function verificationVerify($request) {
        $request->fulfill();

        if ( Session::has('redirects') ) {
            $redirects = Session::pull('redirects');
            Session::remove('redirects');
            return redirect()->route(  $redirects['next_route'] );
        }
        return redirect()->route('home');
    }


    //    send verification email
    public function sendVerification($request) {
        $request->user()->sendEmailVerificationNotification();

        return back()->with('message', 'Verification link sent!');
    }


    //    resend email verification
    public function resendVerification($request) {
        $request->user()->sendEmailVerificationNotification();

        Session::put('resent', true);
        return back();
    }


    //    render the forgot password modal
    public function renderForgotPasswordForm() {
        Session::flash('show_forgot_modal', true);
        return redirect()->route('home');
    }


    //    render the forgot password modal
    public function forgotPasswordAction($request) {
        try {
            $user = User::where('email', $request->forgot_email)->first();
            if(!$user){
                return Helper::jsonResponse(0, 'No user found', [], 'Invalid email given');
            }
            $forgotToken = Helper::uniqueToken();
            PasswordReset::where('email', $user->email)->update([
                'token' => null
            ]);
            PasswordReset::create([
                'email' => $user->email,
                'token' => $forgotToken
            ]);
            Mail::to($user->email)->send( new ResetPasswordMail($forgotToken) );
            return Helper::jsonResponse(1, 'Reset password link is sent to your email', [], '');
        } catch(\Exception $e){
            return Helper::jsonResponse(0, 'Something went wrong', [], $e->getMessage());
        }
    }


    //    render the forgot password modal
    public function forgotTokenVerify($request) {
        try {
            if (filled($request->token)) {
                $passwordReset = PasswordReset::where('token', $request->token)
                    ->with('user')
                    ->first();

                if (!$passwordReset) {
                    abort(403, 'Forbidden');
                }

                Session::put('password_reset_token', $request->token);

                $data = Helper::successArray('Please reset your password', []);
                return view('app.pages.authentication.reset-password', compact('data'));
            }
            abort(403, 'Forbidden');
        } catch(\Exception $e) {
            $data = Helper::errorArray('Something went wrong, please try again', []);
            return back()->with(compact('data'));
        }
    }


    //    render the reset password modal
    public function renderResetPasswordForm() {
        return view('app.pages.authentication.reset-password');
    }


    //    render the reset password modal
    public function passwordResetAction($request) {
        try {
            $passwordResetToken = Session::remove('password_reset_token');
            if (!$passwordResetToken) {
                return Helper::jsonResponse(0, 'Authorization error', [], 'Reset password timeout, please reset password again', 403);
            }
            $passwordReset = PasswordReset::where('token', $passwordResetToken)
            ->with('user')
            ->first();
            $user = $passwordReset->user;
            $user->password =  Hash::make($request->reset_password);
            $user->save();
            PasswordReset::where('email', $user->email)->update([
                'token' => null
            ]);
            Session::forget('password_reset_token');

            return Helper::jsonResponse(1, 'Password reset successful, please login');
        } catch(\Exception $e){
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }
    }


    // logout
    public function signOut() {
        try {
            $user = Auth::user();
            if ($user && $user->isAdmin()) {
                Auth::logout();
                $redirectUrl = redirect()->route('admin.login')->with('success', 'Logged out Successfully');
            } elseif ($user && !$user->isAdmin()) {
                Auth::logout();
                $redirectUrl = redirect()->route('home')->with('success', 'Logged out Successfully');
            } else {
                $redirectUrl = redirect()->route('login')->with('error', 'Please login first');
            }

            return $redirectUrl;
        } catch(\Exception $e) {
            return redirect()->back();
        }
    }

}
