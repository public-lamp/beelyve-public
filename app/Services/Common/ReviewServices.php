<?php


namespace App\Services\Common;


use App\Helpers\Helper;
use App\Models\Generic\Review;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ReviewServices
{

    //    render booking
    public function moderatorSubmitEventReview($request, $booking) {
        try {
            $user = Auth::user();
            Review::updateOrCreate(['booking_id' => $booking->id, 'reviewer_id' => $user->id], [
                'booking_id' => $booking->id,
                'reviewer_id' => $user->id,
                'reviewable_id' => $booking['talent_id'],
                'rating' => intval($request->rating),
                'comments' => $request->comments,
            ]);
            self::updateUserRating($request->rating, $booking['talent_id']);
            $booking->update([
                'booking_status_id' => 10 // closed
            ]);

            return Helper::jsonResponse(1, 'Review submitted successfully');
        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }
    }


    //   update user rating
    public static function updateUserRating($rating, $reviewableId) {
        $reviewable = User::where('id', $reviewableId)
            ->first();
        $existingRatingsArray = Review::where('reviewable_id', $reviewableId)->pluck('rating')->toArray();
        $newRating = (array_sum($existingRatingsArray) + $rating) / (count($existingRatingsArray) + 1);
        $reviewable->update([
            'rating' => $newRating
        ]);

        return $reviewable;
    }

}
