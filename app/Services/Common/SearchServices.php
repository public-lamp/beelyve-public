<?php


namespace App\Services\Common;

use App\Models\User;
use Illuminate\Support\Facades\Auth;


class SearchServices
{

    public function talentSearch($request) {
        try {
            $searchText = isset($request->search_text) ? $request->search_text : null;
            $talentCategory = isset($request['search_filters']['talent_service_category']) ? $request['search_filters']['talent_service_category'] : null;
            $sortByFilterId = isset($request['search_filters']['sort_by']) ? $request['search_filters']['sort_by'] : null;
            $searchByRating = $sortByFilterId ? in_array($sortByFilterId, [11, 12, 13, 14]) : false;
            $searchByPrice = $sortByFilterId ? in_array($sortByFilterId, [5, 6, 7, 8, 9, 10]) : false;

            // auth user with favorite talents
            $authUser = Auth::user();
            $user = null;
            if ($authUser) {
                $user = User::where('id', $authUser->id)->with('favoriteTalents')
                    ->first();
            }

            $talents = User::query()
                ->whereHas('roles', function ($q) {
                    $q->where('roles.id', 2);
                })
                ->where('id', '!=', Auth::id())
                ->where('basic_profile_completed', 1)
                ->with([
                    'userAddress', 'talentBasicInfo', 'talentProfessionalInfo',
                    'talentProfessionalInfo.talentCategory', 'availabilityCategory', 'userAvailabilityDays',
                    'userAvailabilityDays.weekDay', 'userAvailabilityDays.availabilitySlots',
                ]);

            // search text filter
            if ( $searchText ) {
                $talents = $talents->where(function ($q) use($searchText) {
                    $q->where('user_name', 'LIKE', "%{$searchText}%")
                        ->orWhere('first_name', 'LIKE', "%{$searchText}%")
                        ->orWhere('last_name', 'LIKE', "%{$searchText}%")
                        ->orWhereHas('talentProfessionalInfo', function ($q) use($searchText) {
                            $q->where('talent_professional_info.tagline', 'LIKE', "%{$searchText}%");
                        });
                });
            }
            // talent category, filter
            if ( $talentCategory ) {
                if ( in_array($sortByFilterId, [6, 7, 8, 9]) ) {
                    $talents = $talents->orderBy('rating', 'DESC');
                }
                $talents = $talents->whereHas('talentProfessionalInfo.talentCategory', function ($q) use($talentCategory) {
                    $q->where('talent_categories.id', $talentCategory);
                });
            }
            // rating filter
            if ( $searchByRating ) {
                if ($sortByFilterId == 11) { // rating greater than 4
                    $talents = $talents->where('rating', '>=', 4);
                } elseif ($sortByFilterId == 12) { // rating greater than or equals to 3
                    $talents = $talents->where('rating', '>=', 3);
                } elseif ($sortByFilterId == 13) { // rating greater than or equals to 2
                    $talents = $talents->where('rating', '>=', 2);
                } elseif ($sortByFilterId == 14) { // rating greater than or equals to 1
                    $talents = $talents->where('rating', '>=', 1);
                }
            }
            // price range filter
            if ( $searchByPrice ) {
                if ($sortByFilterId == 5) { // starting rate is free
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->whereBetween('talent_professional_info.start_rate', [0, 0]);
                    });
                } elseif ($sortByFilterId == 6) { // starting rate between 1-50
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->whereBetween('talent_professional_info.start_rate', [1, 50]);
                    });
                } elseif ($sortByFilterId == 7) { // starting rate between 51-100
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->whereBetween('talent_professional_info.start_rate', [51, 100]);
                    });
                } elseif ($sortByFilterId == 8) { // starting rate between 100-200
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->whereBetween('talent_professional_info.start_rate', [100, 200]);
                    });
                } elseif ($sortByFilterId == 9) { // starting rate between 201-400
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->whereBetween('talent_professional_info.start_rate', [201, 400]);
                    });
                } elseif ($sortByFilterId == 10) { // starting rate greater than 400
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->where('start_rate', '>', 401);
                    });
                }
            }

            // sort the search by rating
            if ( $sortByFilterId && in_array($sortByFilterId, [1, 2]) ) {
                $talents = $talents->orderBy('rating', 'DESC');
            }
            // sort the search by prices
            if ( $sortByFilterId && in_array($sortByFilterId, [3, 4]) ) {
                if ($sortByFilterId == 3) {
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->orderBY('talent_professional_info.start_rate', 'ASC');
                    });
                } elseif ($sortByFilterId == 6) {
                    $talents = $talents->whereHas('talentProfessionalInfo', function($query) {
                        $query->orderBY('talent_professional_info.start_rate', 'DESC');
                    });
                }
            }
            $talents = $talents->paginate();
            $searchFilters = $request['search_filters'];
            $searchText = $request['search_text'];

            if ( empty($talents) ) {
                return view('app.pages.common.talent-search-page', compact('talents', 'searchFilters', 'searchText'))
                    ->with('error', 'No data found, try different filter');
            }

            return view('app.pages.common.talent-search-page', compact('user', 'talents', 'searchFilters', 'searchText'));
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong, please try again');
        }
    }

}
