<?php


namespace App\Services\Common;

use App\Models\Generic\FavouriteUser;
use Illuminate\Support\Facades\Auth;

class FavouriteUserServices
{

    public function favouriteUserAdd($request) {
        try {
            $user = Auth::user();
            if ( !$user->isModerator() ) {
                return redirect()->back()->with('error', 'Your current role is not authorized to perform this action');
            }

            $favouriteUser = FavouriteUser::where(['user_id' => $user->id, 'favorable_id' => $request->favorable_id])
                ->first();

            if (isset($favouriteUser)) {
                return redirect()->back()->with('error', 'User already in your favorite list');
            }

            FavouriteUser::updateOrCreate(['user_id' => $user->id, 'favorable_id' => $request->favorable_id], [
                'user_id' => $user->id,
                'favorable_id' => $request->favorable_id,
            ]);

            return redirect()->back()->with('success', 'User added in favorite list');
        } catch(\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong, please try again');
        }
    }

    public function favouriteUserRemove($request) {
        try {
            $user = Auth::user();
            FavouriteUser::where(['user_id' => $user->id, 'favorable_id' => $request->favorable_id])
                ->delete();

            return redirect()->back()->with('success', 'User removed from your favorite list');
        } catch(\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong, please try again');
        }
    }

}
