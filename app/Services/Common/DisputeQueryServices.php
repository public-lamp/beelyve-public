<?php


namespace App\Services\Common;

use App\Helpers\Helper;
use App\Mail\Admin\DisputeEmail;
use App\Models\Generic\Booking;
use App\Models\Generic\DisputeQuery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DisputeQueryServices
{

    //    render booking request page
    public function submitDisputeQuery($request) {
        $user = Auth::user();
        $booking = Booking::where('id', $request->booking_id)
            ->where('talent_id', $user->id)
            ->orWhere(function ($query) use($user) {
                $query->where('moderator_id', $user->id);
            })->first();
        if (!$booking) {
            return Helper::jsonResponse(0, 'Can not perform operation', [], 'Booking not found');
        }

        $disputeQuery = DisputeQuery::where(['user_id' => $user->id, 'booking_id' => $booking->id])
            ->first();
        if ($disputeQuery) {
            return Helper::jsonResponse(0, 'Can not perform operation', [], 'Query already been submitted');
        }

        $disputeQuery = DisputeQuery::create([
            'user_id' => $user->id,
            'booking_id' => $booking->id,
            'title' => $request->dispute_title,
            'query' => $request->dispute_query,
        ]);

        $disputeQueryData = [
            'sender' => $user,
            'toEmailAddress' => config('mail.admin.address'),
            'booking' => $booking,
            'disputeQuery' => $disputeQuery
        ];
        self::sendDisputeEmail($disputeQueryData);

        return Helper::jsonResponse(1, 'Dispute query submitted to admin successfully, you will be contacted shortly!');
    }


    // send dispute email to admin
    public static function sendDisputeEmail($notificationData) {
        Mail::to($notificationData['toEmailAddress'])
            ->send(new DisputeEmail($notificationData));
    }

}
