<?php


namespace App\Services\Common\Chat;

use App\Events\Chat\NewMessage;
use App\Helpers\Helper;
use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Chat\ChatHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Arr;

class ChatServices
{

    //    start conversation with a user
    public function chatConversationStart($request) {
        $user = Auth::user();
        $conversation = ChatHelper::findOrCreateConversation($request->other_user_id, $request->conversation_type_id);
        ChatHelper::updateConversationLastActivity($conversation->id);

        if ( $user->isModerator() ) {
            $redirectUrl = route('moderator.chat.messages');
        } elseif( $user->isTalent() ) {
            $redirectUrl = route('talent.chat.messages');
        } elseif( $user->isAdmin() ) {
            $redirectUrl = route('admin.chat.messages');
        } else {
            return redirect()->back()->with('error', 'Trying to do unauthorized attempt');
        }

        return Redirect::to($redirectUrl);
    }


    //    render conversation messages
    public function conversationMessagesRender($request) {
        $conversation = self::findConversation($request->conversation_id);
        if ( !isset($conversation) ) {
            return Helper::jsonResponse(0, 'Can not perform operation', [], 'Valid conversation selection is required');
        }
        $messages = self::getConversationMessages($conversation->id);
        ChatHelper::updateConversationLastActivity($conversation->id);

        $message = Arr::last($messages);
        $lastMessageHtml = view('app.sections.chat.message', compact('message'))->render();
        $conversationMessagesHtml = view('app.sections.chat.conversation_messages_section', compact('messages'))->render();

        return Helper::jsonResponse(1, 'Conversation messages', ['lastMessageHtml' => $lastMessageHtml, 'conversationMessagesHtml' => $conversationMessagesHtml]);
    }


    //   render moderator chat conversations page
    public function chatConversationsRender() {
        $chatService = new ChatServices();
        $conversations = $chatService->getConversations();
        return view('app.pages.common.chat.conversations_list', compact('conversations'));
    }


    //   render moderator chat messages page
    public function chatMessagesRender() {
        $chatService = new ChatServices(); // instantiate the common chat service
        $conversations = $chatService->getConversations(); // get auth user conversations
        $lastActiveConversation = null;
        $lastActiveConversationMessages = [];
        if ( count($conversations) > 0 ) {
            $lastActiveConversation = $conversations[0]; // last active conversation
            $lastActiveConversationMessages = $chatService->getConversationMessages($lastActiveConversation['id']);
        }

        return view('app.pages.common.chat.messages', compact('conversations', 'lastActiveConversation', 'lastActiveConversationMessages'));
    }


    // save and send message
    public function messageSave($request) {
        try {
            $user = Auth::user();
            $conversation = Conversation::where('id', $request->conversation_id)
                ->with('initiatedFrom', 'initiatedTo', 'lastMessage')
                ->first();
            $processedConversation = ChatHelper::processConversation($conversation);
            $messagesData = [];
            if ($request->hasFile('message_file')) { // message is image
                if ( !$request->message_file->isValid() ) {
                    return Helper::jsonResponse(0, 'Can not perform action', [], 'Valid image selection is required');
                }
                $imageName = Helper::uniqueBytes().'_'.time().'_'.str_replace(' ', '', $request->message_file->getClientOriginalName());
                $imageDiskPath = $request->message_file->storeAs('chat-resources/images', $imageName, ['disk' => 'chatResources']);
                $imagePath = asset($imageDiskPath);

                $messagesData[] = [
                    'user_id' => $user->id,
                    'conversation_id' => $processedConversation->id,
                    'message_type_id' => 2,
                    'message' => $imagePath,
                ];
            }
            if ( isset($request->text_message) ) {
                $messagesData[] = [
                    'user_id' => $user->id,
                    'conversation_id' => $processedConversation->id,
                    'message_type_id' => 1,
                    'message' => $request->text_message,
                ];
            }

            if ( empty($messagesData) ) {
                return Helper::jsonResponse(0, 'Empty message', [], 'Empty message');
            }

            $messages = [];
            $receiverMessagesHtml = null;
            $senderMessagesHtml = null;
            foreach ($messagesData as $message) {
                $message = Message::create($message);
                $message = ChatHelper::processMessage($message, $processedConversation['otherUser']);
                $senderMessagesHtml[] = view('app.sections.chat.message', compact('message'))->render();
                $receiverMessagesHtml[] = view('app.sections.chat.receiver_message_section', compact('message'))->render();
                $messages[] = $message;
            }

            // prepare and trigger the new-message event
            $newMessageEventData = [
                'messages' => $messages,
                'messagesHtml' => $receiverMessagesHtml,
            ];
            event(new NewMessage($newMessageEventData));

            return Helper::jsonResponse(1, 'Message sent successfully', ['lastMessagesHtml' => $senderMessagesHtml]);
        } catch (\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', [], 'Something went wrong, please try again');
        }

    }


    //    return auth user conversations
    public function getConversations() {
        $user = Auth::user();
        $conversations = Conversation::where(function ($query) use($user) {
            $query->where(['initiated_from' => $user->id, 'conversation_type_id' => 1])
                ->orWhere(function ($q) use($user) {
                    $q->where(['initiated_to' => $user->id, 'conversation_type_id' => 1]);
                });
        })->orderBy('updated_at', 'DESC')
            ->with('initiatedFrom', 'initiatedTo', 'lastMessage')
            ->get();

        foreach ($conversations as $conversationKey => $conversation) {
            $conversations[$conversationKey] = ChatHelper::processConversation($conversation);
        }
        return $conversations;
    }


    //    return auth user project based conversations
    // Note, not used yet but can be used for project based conversations
    public function getProjectsConversations() {
        $user = Auth::user();
        $conversations = Conversation::where(function ($query) use($user) {
            $query->where(['initiated_from' => $user->id, 'conversation_type_id' => 2]) // 2 for project based conversations
                ->orWhere(function ($q) use($user) {
                    $q->where(['initiated_to' => $user->id, 'conversation_type_id' => 2]); // 2 for project based conversations
                });
        })->orderBy('updated_at', 'DESC')
            ->with('initiatedFrom', 'initiatedTo', 'lastMessage')
            ->get();

        foreach ($conversations as $conversationKey => $conversation) {
            $conversations[$conversationKey] = ChatHelper::processConversation($conversation);
        }
        return $conversations;
    }


    // find conversation
    public function findConversation($conversation_id) {
        return Conversation::where('id', $conversation_id)
            ->with('initiatedFrom', 'initiatedTo')
            ->first();
    }

    //    return conversation messages
    public function getConversationMessages($conversation_id) {
        $processedMessages = []; // processed messages of conversation
        $conversation = Conversation::where('id', $conversation_id)
            ->with('initiatedFrom', 'initiatedTo')
            ->first();

        if (!$conversation) {
            return $processedMessages;
        }
        $processedConversation = ChatHelper::processConversation($conversation);
        $conversationMessages = Message::select(DB::raw('*'))->where(['conversation_id' => $conversation_id])
            ->with('messageType', 'user')
            ->orderBy('created_at', 'ASC')
            ->get();

        foreach($conversationMessages as $message) { // format the fetched conversation messages
            $processedMessages[] = ChatHelper::processMessage($message, $processedConversation['otherUser']);
        }

        return $processedMessages;
    }

}
