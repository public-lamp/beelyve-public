<?php

namespace App\Services\Common;


use App\Helpers\Helper;
use App\Models\Chat\Conversation;
use App\Models\Generic\AdminServiceCharge;
use App\Models\Generic\AvailabilitySlot;
use App\Models\Generic\Booking;
use App\Models\Generic\BookingPayment;
use App\Models\Generic\BookingServicesDetail;
use App\Models\Generic\UsersAvailabilityCategory;
use App\Models\Generic\WeekDay;
use App\Models\Talent\TalentServicesPricing;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Stripe\Stripe;
use Stripe\Charge;

class BookingRequestServices
{

    //    in-progress booking data
    protected $bookingProcessResponse = [
        'currentBooking' => null,
        'bookableDate' => null,
        'searchableDayProcessedSlots' => [],
    ];
    protected $bookingRequestTimedOut; //    request timed-out
    protected $bookingProcessStartUrl; //    redirect url if request timed-out


    //    constructor to update the current booking in session and determines the booking timeout
    public function __construct() {
        if ( Session::has('bookingProcessResponse') ) {
            $this->bookingRequestTimedOut = false;
            $this->bookingProcessResponse = Session::get('bookingProcessResponse');
        } else {
            $this->bookingRequestTimedOut = true;
            $this->bookingProcessStartUrl = route('home');
        }
    }


    //    render booking request page
    public function renderBookingRequestPage($request) {
        $moderator = Auth::user();
        $talentId = encryptor('decrypt', $request->talent_id);

        $talentServicesPricing = TalentServicesPricing::where('id', $request->talent_services_pricing_id)
            ->first();
        if (!$talentId || !$talentServicesPricing) {
            return redirect()->route('home')->with('Invalid pricing selection');
        }

        $bookingServiceAmount = self::getBookingServiceAmount($talentServicesPricing->price);

        // initiate booking request
        Booking::updateOrCreate([
            'moderator_id' => $moderator->id,
            'talent_id' => $talentId,
            'talent_services_pricing_id' => $request->talent_services_pricing_id,
            'booking_status_id' => 1
        ], [
            'moderator_id' => $moderator->id,
            'talent_id' => $talentId,
            'talent_services_pricing_id' => $request->talent_services_pricing_id,
            'booking_status_id' => 1, // being created
            'booking_payment_status_id' => 1, // not initiated by moderator yet
            'amount' => $talentServicesPricing->price, // save the latest services price, offered by talent
            'service_amount' => $bookingServiceAmount > 0.0 ? $bookingServiceAmount : 0.0
        ]);
        $currentBooking = Booking::where([
            'moderator_id' => $moderator->id,
            'talent_id' => $talentId,
            'talent_services_pricing_id' => $request->talent_services_pricing_id,
            'booking_status_id' => 1 // being created
        ])->first();
        self::setOrUpdateBookingServicesDetail($currentBooking);

        $this->bookingProcessResponse['currentBooking'] = $currentBooking;
        Session::put('bookingProcessResponse', $this->bookingProcessResponse);

        return view('app.pages.common.booking-process.select-date');
    }


    //    Booking-date selection
    public function selectBookingRequestDate($request) {
        try {
            // check booking time-out
            if ( $this->bookingRequestTimedOut ) {
                Session::flash('warning', 'Booking request time out, please start over');
                return Helper::jsonResponse(0, 'Timed out', [ 'next_url' => $this->bookingProcessStartUrl ], 'Request timed out, please start over');
            }

            $user = Auth::user();
            $currentBooking = $this->bookingProcessResponse['currentBooking'];
            $talent = User::where('id', $currentBooking->talent_id)->first();
            $talentServicePricing = TalentServicesPricing::where('id', $currentBooking['talent_services_pricing_id'])
                ->with('servicesPricingSlot')->first();

            if ( !$currentBooking || !$talentServicePricing ) {
                return Helper::jsonResponse(0, 'Invalid selection', [], 'Please choose valid service pricing');
            }

            // precess bookable day slot
            $searchableDayProcessedSlots = [];
            $bookableDay = date('l', strtotime($request->date));
            $bookableDate = date('Y-m-d', strtotime($request->date));
            $searchableDuration = $talentServicePricing->servicesPricingSlot->duration;
            $weekDay = WeekDay::where('name', $bookableDay)->first();

            //            $moderatorBookableDateTime = date('Y-m-d H:i:s', strtotime($request->date. ' ' .'00:00:00'));
            //            $talentBookableDateTime = parseDateTimeWithTimeZone($moderatorBookableDateTime, $talent->time_zone, $user->time_zone, 'Y-m-d H:m:s');
            //            $talentBookableDate = date('Y-m-d', strtotime($talentBookableDateTime));
            //            $talentBookableDay = date('l', strtotime($talentBookableDate));
            //            $talentWeekDay = WeekDay::where('name', $talentBookableDay)->first();

            $talentAvailabilityCategory = UsersAvailabilityCategory::where('user_id', $talent->id)
                ->with('availabilityCategory')
                ->first();

            // process slots for whole day (24 Hours)
            if ( $talentAvailabilityCategory->availability_category_id == 1 ) {
                $timeNow = date('Y-m-d H:i:s');
                $talentDateNow = parseDateWithTimeZone($timeNow, $talent->time_zone, 'UTC', 'Y-m-d');
                // check if bookable date is today
                if (strtotime($bookableDate) == strtotime($talentDateNow) ) {
                    $talentTimeNow = parseDateTimeWithTimeZone($timeNow, $talent->time_zone, 'UTC', 'Y-m-d H:i:s');
                    $searchableStartDateTime = date( 'Y-m-d H:i', strtotime($talentTimeNow) );
                } else {
                     $searchableStartDateTime = date( 'Y-m-d H:i', strtotime($talentDateNow .' '. '00:00:00') );
                }

                $processedSlots = [];
                $searchableEndDateTime = date( 'Y-m-d H:i', strtotime($talentDateNow .' '. '23:59:59'));
                $processedSlot = self::getTimeSlot($searchableDuration, $searchableStartDateTime, $searchableEndDateTime, 1);
                $processedSlots = array_merge($processedSlots, $processedSlot);

                foreach ($processedSlots as $slot) {
                    $bookableSlotStartDateTime = date('Y-m-d h:i A', strtotime($talentDateNow. ' '. $slot['slot_start_time']));
                    $bookableSlotEndDateTime = date('Y-m-d h:i A', strtotime($bookableDate. ' '. $slot['slot_end_time']));
                    $bookerSlotStartDateTime = parseDateTimeWithTimeZone($bookableSlotStartDateTime, $user->time_zone, $talent->time_zone, 'Y-m-d h:i A');
                    $bookerSlotEndDateTime = parseDateTimeWithTimeZone($bookableSlotEndDateTime, $user->time_zone, $talent->time_zone, 'Y-m-d h:i A');

                    if ( (date('Y-m-d', strtotime($bookableSlotStartDateTime)) == date('Y-m-d', strtotime($bookerSlotStartDateTime))) && date('Y-m-d', strtotime($bookableSlotEndDateTime)) == date('Y-m-d', strtotime($bookerSlotEndDateTime)) ) {
                        $searchableDayProcessedSlots[] = $slot;
                    }
                }
            }

            // process slots for custom available-day slots
            if ( $talentAvailabilityCategory->availability_category_id == 2 ) {
                $searchableDaySlots = AvailabilitySlot::where([
                    'user_id' => $talent->id,
                    'day' => $weekDay->name
                ])->get();
                foreach( $searchableDaySlots as $key => $searchableDaySlot ) {
                    $bookableSlotStartDateTime = date('Y-m-d h:i A', strtotime($bookableDate. ' '. $searchableDaySlot['time_from']));
                    $bookableSlotEndDateTime = date('Y-m-d h:i A', strtotime($bookableDate. ' '. $searchableDaySlot['time_to']));
                    $bookerSlotStartDateTime = parseDateTimeWithTimeZone($bookableSlotStartDateTime, $user->time_zone, $talent->time_zone, 'Y-m-d h:i A');
                    $bookerSlotEndDateTime = parseDateTimeWithTimeZone($bookableSlotEndDateTime, $user->time_zone, $talent->time_zone, 'Y-m-d h:i A');

                    // todo, may be we can remove this check for maximum slot to show (Not sure)
                    // check if slot-date same in bookable and booker's time zone
                    if ( (date('Y-m-d', strtotime($bookableSlotStartDateTime)) == date('Y-m-d', strtotime($bookerSlotStartDateTime))) && date('Y-m-d', strtotime($bookableSlotEndDateTime)) == date('Y-m-d', strtotime($bookerSlotEndDateTime)) ) {
                        // id bookable day is today
                        if ( $bookableDay == parseTimeWithTimeZone(date('l'), Session('browserTimeZone'), 'UTC', 'l') ) {
                            if ( strtotime($searchableDaySlot['time_from']) > strtotime( parseTimeWithTimeZone(date('h:i A'), Session('browserTimeZone'), 'UTC', 'h:i A')) ) {
                                $processedSlot = self::getTimeSlot($searchableDuration, $searchableDaySlot['time_from'], $searchableDaySlot['time_to']);
                                $searchableDayProcessedSlots = array_merge($searchableDayProcessedSlots, $processedSlot);
                            }
                        } else {
                            $processedSlot = self::getTimeSlot($searchableDuration, $searchableDaySlot['time_from'], $searchableDaySlot['time_to']);
                            $searchableDayProcessedSlots = array_merge($searchableDayProcessedSlots, $processedSlot);
                        }
                    }
                }
            }

            // update current-booking (class-level) object
            $this->bookingProcessResponse['currentBooking'] = $currentBooking;
            $this->bookingProcessResponse['bookableDate'] = date( 'Y-m-d', strtotime($request->date) );
            $this->bookingProcessResponse['searchableDayProcessedSlots'] = $searchableDayProcessedSlots;
            Session::put('bookingProcessResponse', $this->bookingProcessResponse);

            return Helper::jsonResponse(1, 'Time slots found', [ 'next_url' => route('booking-request.available-slots') ]);
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'Something went wrong', ['next_url' => ''], 'Something went wrong, please try again');
        }
    }


    //    render booking
    public function renderBookingRequestAvailableSlots($request) {
        $daySlots = $this->bookingProcessResponse['searchableDayProcessedSlots'];
        $currentBooking = Booking::where('id', $this->bookingProcessResponse['currentBooking']['id'])
        ->with('talent')
            ->first();
        return view('app.pages.common.booking-process.show-available-slots', compact('daySlots', 'currentBooking'));
    }


    //    save booking slot
    public function selectBookingSlot($request) {
        // check booking time-out
        if ( $this->bookingRequestTimedOut ) {
            Session::put('warning', 'Booking request time out, please start over');
            return Helper::jsonResponse(0, 'Timed out', [ 'next_url' => $this->bookingProcessStartUrl ], 'Request timed out, please start over');
        }

        $timeFrom = $this->bookingProcessResponse['bookableDate'].' '.date( 'H:i:s', strtotime( $request->time_slot['start_time'] ) );
        $utcTimeFrom = parseDateTimeWithTimeZone($timeFrom, 'UTC', Auth::user()->time_zone, 'Y-m-d H:i:s');
        $timeTo = $this->bookingProcessResponse['bookableDate'].' '.date( 'H:i:s', strtotime( $request->time_slot['end_time'] ) );
        $utcTimeTo = parseDateTimeWithTimeZone($timeTo, 'UTC', Auth::user()->time_zone, 'Y-m-d H:i:s');

        //        check for the conflicting bookings
        $moderatorWithConflictBookings = User::where('id', Auth::id())
            ->with(['moderatorBookings' => function ($q) use($timeFrom, $timeTo) {
                $q->whereIn('bookings.booking_status_id', [3, 4 , 6, 7]) // accepted, in-progress and supposedly-closed
                ->where(function ($query) use($timeFrom, $timeTo) {
                    $query->whereRaw('? BETWEEN bookings.time_from AND bookings.time_to',
                        [ date('Y-m-d H:i:s', strtotime($timeFrom)) ]
                    )
                    ->orWhereRaw('? BETWEEN bookings.time_from AND bookings.time_to',
                        [ date('Y-m-d', strtotime($timeTo)) ]
                    )
                    ->orWhereBetween('bookings.time_from',
                        [
                            date( 'Y-m-d H:i:s', strtotime($timeFrom) ),
                            date( 'Y-m-d', strtotime($timeTo) )
                        ]
                    )
                    ->orWhereBetween('bookings.time_to',
                        [
                            date( 'Y-m-d H:i:s', strtotime($timeFrom) ),
                            date( 'Y-m-d', strtotime($timeTo) )
                        ]
                    );
                });
            }])->first();
        $conflictingBookings = $moderatorWithConflictBookings->moderatorBookings;
        if ( $conflictingBookings->isNotEmpty() ) {
            return Helper::jsonResponse(0, 'Can not perform action', ['conflictingBookings' => $conflictingBookings], 'Selected time slot conflicts with one of the booked events, please select a different one');
        }

        $booking = Booking::where('id', $this->bookingProcessResponse['currentBooking']['id'])
            ->with('servicePricing.servicesPricingSlot')->first();
        $bookingDurationInMinutes = $booking->servicePricing->servicesPricingSlot->duration;
        $bookingTimeHours = getHours($bookingDurationInMinutes);
        $bookingTimeMinutes = getMinutes($bookingDurationInMinutes);
        $bookingTimeSeconds = getSeconds($bookingDurationInMinutes);
        $booking->update([
                //                'time_from' => $timeFrom,
                //                'time_to' => $timeTo,
                'time_from' => $utcTimeFrom,
                'time_to' => $utcTimeTo,
                'total_minutes_duration' => $bookingDurationInMinutes,
                'remaining_hours' => $bookingTimeHours,
                'remaining_minutes' => $bookingTimeMinutes,
                'remaining_seconds' => $bookingTimeSeconds,
            ]);

        $currentBooking = Booking::where('id', $this->bookingProcessResponse['currentBooking']['id'])->first();
        $this->bookingProcessResponse['currentBooking'] = $currentBooking;
        Session::put('bookingProcessResponse', $this->bookingProcessResponse);

        return Helper::jsonResponse(1, 'Time slot saved', [ 'next_url' => route('booking-request.detail') ]);
    }


    //    render booking
    public function renderBookingRequestDetail($request) {
        if ( $this->bookingRequestTimedOut ) {
            Session::put('warning', 'Booking request time out, please start over');
            return Helper::jsonResponse(0, 'Timed out', [ 'next_url' => $this->bookingProcessStartUrl ], 'Request timed out, please start over');
        }

        return view('app.pages.common.booking-process.booking-detail-note');
    }


    //    render booking
    public function addBookingRequestDetail($request) {
        if ( $this->bookingRequestTimedOut ) {
            Session::put('warning', 'Booking request time out, please start over');
            return Helper::jsonResponse(0, 'Timed out', [ 'next_url' => $this->bookingProcessStartUrl ], 'Request timed out, please start over');
        }

        Booking::where('id', $this->bookingProcessResponse['currentBooking']['id'])
            ->update([
                'title' => $request->booking_title,
                'detail_note' => $request->booking_detail_note,
                'booking_status_id' => 2, // created
            ]);

        $currentBooking = Booking::where('id', $this->bookingProcessResponse['currentBooking']['id'])
            ->with('servicePricing')
            ->first();
        $this->bookingProcessResponse['currentBooking'] = $currentBooking;
        Session::put('bookingProcessResponse', $this->bookingProcessResponse);
        return Helper::jsonResponse(1, 'Booking note saved', [ 'next_url' => route('booking-request.payment') ]);
    }


    //    render booking
    public function renderBookingPayment($request) {
        if ( $this->bookingRequestTimedOut ) {
            Session::put('warning', 'Booking request time out, please start over');
            return Helper::jsonResponse(0, 'Timed out', [ 'next_url' => $this->bookingProcessStartUrl ], 'Request timed out, please start over');
        }

        return view('app.pages.common.booking-process.booking-payment');
    }


    // charge and create payment for booking
    public function chargeBookingPayment($request) {
        if ( $this->bookingRequestTimedOut ) {
            Session::put('warning', 'Booking request time out, please start over');
            return Helper::jsonResponse(0, 'Timed out', [ 'next_url' => $this->bookingProcessStartUrl ], 'Booking timed out, charge unsuccessful please start over');
        }

        try {
            $user = Auth::user();
            $currentBooking = $this->bookingProcessResponse['currentBooking'];

            Stripe::setApiKey( config('services.stripe.secret') );

            $charge = Charge::create([
                'source' => $request->stripeTokenId,
                'currency' => 'USD',
                'amount' => $currentBooking['servicePricing']['price'] * 100
            ]);

            Booking::where('id', $currentBooking['id'])->update([
                'booking_status_id' => 3, // request submitted
                'booking_payment_status_id' => 3, // received in escrow
            ]);

            BookingPayment::updateOrCreate(['user_id' => $currentBooking['talent_id'], 'booking_id' => $currentBooking['id']],
                [
                    'user_id' => $user->id,
                    'booking_id' => $currentBooking['id'],
                    'booking_payment_status_id' => 3, // received in escrow
                    'stripe_charge_id' => $charge['id'],
                    'receipt_url' => $charge['receipt_url'],
                    'amount' => $charge['amount']/100, // convert scents to dollars
                    'currency' => $charge['currency'],
                ]
            );
            // initiate event based conversation
            Conversation::create([
                'initiated_from' => Auth::id(),
                'initiated_to' => $currentBooking['talent_id'],
                'conversation_type_id' => 2, // project-based
                'booking_id' => $currentBooking['id'],
                'is_active' => true,
            ]);

            $nextUrl = route('moderator.bookings.requests');
            return Helper::jsonResponse(1, 'Payment successful', [ 'next_url' =>  $nextUrl]);
        } catch(\Exception $e) {
            return Helper::jsonResponse(0, 'Payment unsuccessful', [ 'next_url' => '' ], $e->getMessage());
        }

    }


    //    render booking
    public function renderBookingDetail($request) {
        if ( !isset($request->booking_id) ) {
            return redirect()->back()->with('error', 'Booking selection is required');
        }

        $booking = Booking::where(['id' => $request->booking_id])
            ->with('moderator', 'talent.talentProfessionalInfo.talentCategory', 'servicePricing', 'bookingStatus', 'bookingServicesDetail')
            ->first();


        $talentCompletedBookings = Booking::where(['talent_id' => Auth::id()])
            ->whereIn('booking_status_id', [7, 8, 10])
            ->count();
        $talentEarning = Booking::where('talent_id', Auth::id())
            ->whereIn('booking_payment_status_id', [5, 6])->sum('amount');

        $moderatorCompletedBookings = Booking::where(['moderator_id' => $booking->moderator->id])
            ->whereIn('booking_status_id', [7, 8, 10])
            ->count();
        $moderatorEarning = Booking::where('moderator_id', $booking->moderator->id)
            ->whereIn('booking_payment_status_id', [5, 6])->sum('amount');
        return view('app.pages.common.bookings.booking-details', compact('booking', 'talentCompletedBookings', 'talentEarning', 'moderatorCompletedBookings', 'moderatorEarning'));
    }


    //    make time slots for the given time
    public static function getTimeSlot($interval, $start_time, $end_time, $addMin = 0) {
        $time = [];
//        $startTime = date('H:i', strtotime($start_time) );
        $startTime = $start_time;
        $startTime = self::divisibleByFiveTime($startTime);
//        $endTime =  date('H:i', strtotime($end_time) );
        $endTime =  $end_time;

        $diffInHours = date('H', strtotime($endTime) - strtotime($startTime) );
        $diffInMin = date('i', strtotime($endTime) - strtotime($startTime) ) + $addMin;
        $possibleIterationsCount = floor(($diffInHours*60 + $diffInMin)/$interval );

        $i = 0;
        while( $i < $possibleIterationsCount ) {
            $start = $startTime;
            $end = date('H:i', strtotime('+'.$interval.' minutes', strtotime($startTime)) );
            $startTime = date('H:i', strtotime('+'.$interval.' minutes', strtotime($startTime)) );
            if( strtotime($startTime) <= strtotime($endTime) ) {
                $time[$i]['slot_start_time'] = $start;
                $time[$i]['slot_end_time'] = $end;
                $i++;
            }
        }
        return $time;
    }


    //    return nearest divisible by 5 time
    public static function divisibleByFiveTime($time, $format = 'H:i') {
        $nextDivisibleByFiveTime = ceil(strtotime($time)/300)*300;
        return date($format, $nextDivisibleByFiveTime);
    }


    //    get current booking services details and save against the current booking
    public static function setOrUpdateBookingServicesDetail($booking) {
        $talentServicesPricing = TalentServicesPricing::where('id', $booking['talent_services_pricing_id'])
            ->with('servicesPricingType.servicesMediums', 'servicesPricingSlot')
            ->first();

        BookingServicesDetail::updateOrCreate(['booking_id' => $booking->id],[
            'talent_services_pricing' => $talentServicesPricing,
            'service_pricing_type' => $talentServicesPricing->servicesPricingType->servicesMediums,
            'service_pricing_type_mediums' => $talentServicesPricing->servicesPricingSlot,
            'service_pricing_slot' => $talentServicesPricing->servicesPricingSlot,
        ]);
    }


    //    return percentage-amount of an amount
    public static function getBookingServiceAmount($bookingAmount) {
        $adminServiceCharge = AdminServiceCharge::where('service_type_id', 1)->first();
        if (!$adminServiceCharge) {
            return 0.0;
        }

        // for now calculating percentage amount only
        if($adminServiceCharge->service_charge_type_id == 1) {
            return ($adminServiceCharge->value / 100) * $bookingAmount;
        } else {
            return 0.0;
        }
    }

}
