<?php


namespace App\Services\Stripe;

use App\Models\StripeAccount\UserStripeAccountDetail;
use App\Models\StripeAccount\UserStripeAccountVerification;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Exception as StripeException;
use Stripe\OAuth as StripeOAuth;
use Stripe\Transfer as StripeTransfer;

class StripeServices
{
    /**
     * @var string
     */
    public $stripeTransferException;

    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }


    // error response
    private function errorResponseJson($e) {
        $body = $e->getJsonBody();
        $err  = $body['error'];
        $errorResponse = new \stdClass();
        $errorResponse->status = $e->getHttpStatus();
        $errorResponse->type = $err['type'];
        $errorResponse->code = $err['code'];
        $errorResponse->message = $err['message'];

        return $errorResponse;
    }


    // transfer amount on stripe connect
    public function transferAmount($amount, $vendorAccount, $description = "Payment transfer") {
        try {
            return StripeTransfer::create([
                "amount" => round($amount * 100),
                "currency" => "usd",
                //"source_transaction" => $transactionId,
                "destination" => $vendorAccount,
                "description" => $description,
            ]);
        } catch (StripeException\RateLimitException $e) {			// Too many requests made to the API too quickly
            $this->stripeTransferException = $e->getMessage();
        } catch (StripeException\InvalidRequestException $e) {		// Invalid parameters were supplied to Stripe's API
            $this->stripeTransferException = $e->getMessage();
        } catch (StripeException\AuthenticationException $e) {		// Authentication with Stripe's API failed
            $this->stripeTransferException = $e->getMessage();
        } catch (StripeException\ApiConnectionException  $e) {		// Network communication with Stripe failed
            $this->stripeTransferException = $e->getMessage();
        } catch (StripeException\ApiErrorException $e) {			// Display a very generic error to the user, and maybe send yourself an email
            $this->stripeTransferException = $e->getMessage();
        } catch (\Exception $e) {						            // Something else happened, completely unrelated to Stripe
            $this->stripeTransferException = $e->getMessage();
        }
        return $this->stripeTransferException;
    }


    // callback for the User connect account success
    public function stripeRegistrationCallback($request) {
        if($request->error) {
            $data = ["error" => true, "msg" => $request->error_description];
            return redirect()->route('home')->with('error', $request->error_description);
        } elseif(!$request->state) {
            return redirect()->route('home')->with('error',"Some parameter is missing.");
        } else {
            $userVeri = UserStripeAccountVerification::where(["verification_code" => $request->state, "status" => 1])->first();
            if(!$userVeri) {
                return redirect()->route('home')->with('error','Your Stripe Verification Code Is Invalid');
            } else {
                $response = $this->oAuthTokenVerify($request->code);
                if(isset($response["error"])) {
                    return redirect()->route('home')->with('error',$response["error_description"]);
                } else {
                    $user = User::with('stripeAccountDetail')->find($userVeri->user_id);
                    if(!$user) {
                        return redirect()->route('home')->with('error','User Not Found');
                    } else {
                        UserStripeAccountDetail::updateOrCreate(['user_id'=>Auth::id()],['user_id'=>$user->id,'stripe_account_id'=>$response["stripe_user_id"]]);
                        $user->save();
                        UserStripeAccountVerification::where(["user_id" => $userVeri->user_id])->update(["status" => "0", 'updated_at' => new \DateTime()]);
                    }
                }
            }
        }

        if(Auth::check() && Auth::user()->isTalent()) {
            return redirect()->route('talent-dashboard')->with('success','Your Stripe Connectivity Is Completed');
        } elseif (Auth::check() && Auth::user()->isModerator()) {
            return redirect()->route('moderator-dashboard')->with('success','Your Stripe Connectivity Is Completed');
        } else {
            return redirect()->route('home')->with('success','Your Stripe Connectivity Is Completed');
        }
    }


    // check Oauth token and get account id
    public function oAuthTokenVerify($authorizationCode) {
        try {
            return StripeOAuth::token(array(
                'grant_type' => 'authorization_code',
                'code' => $authorizationCode,
            ));
        } catch (StripeException\OAuth\InvalidGrantException $e) {
            return $e->getJsonBody();
        }
    }

}
