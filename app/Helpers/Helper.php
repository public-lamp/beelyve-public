<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class Helper
{

    //    json response
    public static function jsonResponse($status, $message, $data = [], $error = "", $statusCode = 200)
    {
        if (!$data) {
            return response()->json(['status' => $status, 'message' => $message, 'data' => $data, 'error' => $error], $statusCode, [], JSON_FORCE_OBJECT);
        }
        return response()->json(['status' => $status, 'message' => $message, 'data' => $data, 'error' => $error], $statusCode);
    }

    //  return error message
    public static function returnErrorMessage($request, $errorMessage, $data = [], $errorTitle = 'Can not perform operation') {
        if ($request->ajax()) {
            return Helper::jsonResponse(0, $errorTitle, $data, $errorMessage);
        }
        return redirect()->back()->with('error', $errorMessage);
    }

    //  return error message
    public static function returnSuccessMessage($request, $successMessage,  $redirect = null, $data = []) {
        if ($request->ajax()) {
            return Helper::jsonResponse(1, $successMessage, $data);
        }
        $redirect = isset($redirect) ? $redirect : redirect()->back()->with('success', $successMessage);
        Session::flash('success', $successMessage);
        return $redirect;
    }

    // generates a success response array
    public static function successArray( $message = 'Operation successful', $data = [] )
    {
        return ['success' => 1, 'message' => $message, 'data' => $data];
    }

    // generates an error response array
    public static function errorArray( $error = "Operation unsuccessful", $data = [] )
    {
        return ['success' => 0, 'error' => $error, 'data' => $data];
    }


    // generates unique string of given length
    public static function uniqueToken($length = 40)
    {
        try {
            $minRandomStrLen = 10;
            $randomBitesLength = 3;
            $randomBites = bin2hex( random_bytes($randomBitesLength) );
            $time = strtotime( date('Y-m-d'));
            $timeLength = strlen($time);
            $localRandomLength = $timeLength + $randomBitesLength;
            $length = $length > $localRandomLength + $minRandomStrLen ? $length : $length == $localRandomLength + $minRandomStrLen;

            return $uniqueToken = $randomBites . Str::random($length - $localRandomLength) . $time;
        } catch(\Exception $e) {
           return Str::random(40);
        }
    }

    // upload image
    public static function uploadImage($file, $directoryPath = "project-assets/users/profile-images") {
        $fileName = $file->getClientOriginalName();
        $fileSize = ( $file->getSize() )/1000;    //Size in kb
        $explodeImage = explode('.', $fileName);
        $fileName = $explodeImage[0];
        $extension = end($explodeImage);
        $fileName = time() . "-" . $fileName . ".".$extension;
        $imageExtensions = ['jpg', 'jpeg', 'png', 'PNG', 'gif', 'svg'];

        if( !in_array($extension, $imageExtensions) ) {
            return self::errorArray('File Extension must be jpg, jpeg , png');
        }
        if( $fileSize > 5000 ) {
            return self::errorArray('Image size should be less than 5 MB');
        }

        $file->move($directoryPath, $fileName);

        $uploadResponse = ["url" => asset($directoryPath.'/'.$fileName), "filename" => $fileName];
        return  self::successArray('Image uploaded',  $uploadResponse);
    }

    //    generates unique bytes of given length or of 4
    public static function uniqueBytes($strLength = 4) {
        // random_bytes returns number of bytes
        // bin2hex converts them into hexadecimal format
        return substr(bin2hex(random_bytes($strLength)), 0, $strLength);
    }

    // convert minutes to hours minutes and seconds
    public static function convertMinutesToTime($durationInMinutes) {
        $hours = 0;
        $minutes = 0;
        $seconds = 0;
        if ($durationInMinutes > 0 ) {
            $hoursTime = $durationInMinutes / 60 ;
            $minutesTime = $hoursTime * 60;
            $secondsTime = $minutesTime * 60;

            $hours = intval( floor($hoursTime) ) ;
            $minutes = intval( floor($minutesTime % 60) );
            $seconds = intval( floor($secondsTime % 60) );
        }

        return ['hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds];
    }

    // convert minutes to seconds
    public static function convertMinutesToSeconds($durationInMinutes) {
        return $seconds = $durationInMinutes * 60;
    }

}
