<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class FileSystemHelper
{

    // upload image
    public static function publicStorageImageUpload($file, $publicPath = "storage/images", $allowedSize = 5120) {
        $fileName = $file->getClientOriginalName();
        $fileSize = ( $file->getSize() )/1000;    //Size in kb
        $explodeImage = explode('.', $fileName);
        $fileName = $explodeImage[0];
        $extension = end($explodeImage);
        $fileName = Helper::uniqueBytes(). '-' .time() . '-' . $fileName . '.' .$extension;
        $imageExtensions = ['jpg', 'jpeg', 'png', 'PNG', 'gif', 'svg'];

        if( !in_array($extension, $imageExtensions) ) {
            return Helper::errorArray('File Extension must be in jpg, jpeg, png, PNG, gif, svg');
        }
        if( $fileSize > $allowedSize ) {
            return Helper::errorArray('Image size should be less than 5 MB');
        }

        $uploadedFile = $file->move($publicPath, $fileName);

        $uploadResponse = ["url" => asset($publicPath.'/'.$fileName), "filename" => $fileName];
        return  Helper::successArray('Image uploaded',  $uploadResponse);
    }


    //    remove/unlink existing image
    public static function unlinkImage($imageUrl) {
        if ( request()->secure() ) {
            unlink($imageUrl);
            return ['success' => true, 'message' => 'File Unlinked successfully'];
        } else {
            return ['success' => false, 'message' => 'Secure connection is required to unlink a file'];
        }
    }


    //    remove/unlink existing image
    public static function deleteStorageImage($storageDisk, $fileUrl, $fileName) {
        return Storage::disk($storageDisk)->delete($fileName);
    }


    // parse url
    public static function parseUrl($url) {
        return parse_url($url);
    }


    // base name of a path
    public static function getBaseName($path) {
        return basename($path);
    }

}
