<?php


namespace App\Helpers\Chat;


use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use App\Models\Chat\MessageType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatHelper
{

    //    return conversation messages
    public static function findOrCreateConversation($otherUserId, $conversationTypeId) {
        $user = Auth::user();

        if ($conversationTypeId == 1) { // one-to-one conversation
            $conversation = Conversation::where(['initiated_from' => $user->id, 'initiated_to' => $otherUserId, 'conversation_type_id' => 1])
                ->orWhere(function ($query) use($user, $otherUserId) {
                    $query->where(['initiated_to' => $user->id, 'initiated_from' => $otherUserId, 'conversation_type_id' => 1]);
                })->first();
        }
        if ($conversationTypeId == 2) { // event based conversation
            $conversation = Conversation::where(['initiated_from' => $user->id, 'initiated_to' => $otherUserId, 'conversation_type_id' => 2])
                ->orWhere(function ($query) use($user, $otherUserId) {
                    $query->where(['initiated_to' => $user->id, 'initiated_from' => $otherUserId, 'conversation_type_id' => 2]);
                })->first();
        }

        if ( !isset($conversation) ) {
            $conversation = Conversation::create([
                'initiated_from' => $user->id,
                'initiated_to' => $otherUserId,
                'conversation_type_id' => $conversationTypeId,
                'is_active' => true
            ]);
            $conversation = Conversation::where('id', $conversation->id)
                ->with('initiatedFrom', 'initiatedTo', 'lastMessage')
                ->first();
        }
        return $conversation;
    }


    //    return conversation messages
    public static function updateConversationLastActivity($conversationId) {
        Conversation::where('id', $conversationId)->update([
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    // check current user is message sender
    public static function checkCurrentUserIsSender($messageUserId) {
        return $messageUserId == Auth::id();
    }


    // validate the requested conversation belongs to requester
    public static function validateCurrentUserInConversation($conversation_id) {
        $conversation = Conversation::where('id', $conversation_id)
            ->where(function ($query) {
                $query->where('initiated_from', Auth::id())
                    ->orWhere(function ($query) {
                        $query->where( 'initiated_to', Auth::id() );
                    });
            })->first();
        return isset($conversation);
    }

    // update the value of updated_at column
    public static function updateConversationLatestActivity($conversation_id) {
        Conversation::where('id', $conversation_id)
            ->update([
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        return true;
    }

    // validate the requested conversation belongs to requester
    public static function processConversation($conversation) {
        $currentUser = Auth::user();
        $otherParticipant = $conversation->initiatedFrom->id == $currentUser->id ? $conversation->initiatedTo : $conversation->initiatedFrom;
        $conversation['currentUser'] = $currentUser;
        $conversation['otherUser'] = $otherParticipant;
        return $conversation;
    }


    // validate the requested conversation belongs to requester
    public static function processMessage($message, $otherUser = null) {
        return $message = [
            'id' => $message->id,
            'current_user_is_sender' => ChatHelper::checkCurrentUserIsSender($message->user_id),
            'user_id' => $message->user_id,
            'message' => $message->message,
            'conversation_id' => $message->conversation_id,
            'message_type_id' => $message->message_type_id,
            'created_at' => $message->created_at,
            'messageType' => $message->messageType,
            'currentUser' => $message->user,
            'otherUser' => $otherUser,
        ];
    }


    // **************************************** Booking/Event Based chat-helpers **************************************
    // validate the requested conversation belongs to requester
    public static function processBookingConversation($conversation) {
        // process booking conversation
        $currentUser = Auth::user();
        $otherParticipant = $conversation->initiatedFrom->id == $currentUser->id ? $conversation->initiatedTo : $conversation->initiatedFrom;
        $conversation['currentUser'] = $currentUser;
        $conversation['otherUser'] = $otherParticipant;
        return $conversation;
    }

    //    return conversation messages
    public static function getBookingConversationChat($processedConversation) {
        $processedMessages = []; // processed messages of conversation
        $conversationMessages = Message::where('conversation_id', $processedConversation['id'])
            ->with('messageType', 'user')
            ->orderBy('created_at', 'ASC')
            ->get();
        foreach($conversationMessages as $message) { // format the fetched conversation messages
            $processedMessages[] = self::processMessage($message, $processedConversation['otherUser']);
        }
        return $processedMessages;
    }

}
