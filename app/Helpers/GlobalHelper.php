<?php

use Illuminate\Support\Facades\Session;
use App\Helpers\Helper;

function encryptor($action, $string) {
    $output = false;

    $encrypt_method = "AES-256-CBC";
    //pls set your unique hashing key
    $secret_key = 'uniqueSecret';
    $secret_iv = 'itIsForEncAndDec';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    //do the encryption given text/string/number
    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ) {
        //decrypt the given text/string/number
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}


// get and return session base redirect
function setSessionRedirect($previousUrl, $nextUrl) {
    $redirect = [
        'redirect_route' => true,
        'previous_route' => $previousUrl,
        'next_route' => $nextUrl
    ];
    if ( Session::has('redirects') ) {
        Session::forget('redirects');
    }
    Session::put('redirects', $redirect);
    return $redirect;
}


// convert and return American format date
function parseDate($timeString, $dateFormat = 'd M, Y') {
    return date( $dateFormat, strtotime($timeString) );
}

// convert and return American format date
function parseTime($timeString, $timeFormat = 'h:i a') {
    return date( $timeFormat, strtotime($timeString) );
}


// convert and return American format date
function formatDateTime($timeString, $timeFormat = 'M m, Y  h:i a') {
    return date( $timeFormat, strtotime($timeString) );
}


// convert time according to time zone
function parseTimeWithTimeZone($time, $toTz, $fromTz = 'UTC', $format = 'h:i a')
{
    $timeObject = new DateTime($time, new DateTimeZone($fromTz));
    $timeObject->setTimezone(new DateTimeZone($toTz));
    return $timeObject->format($format);
}

// convert date according to time zone
function parseDateWithTimeZone($date, $toTz, $fromTz = 'UTC', $format = 'M d, Y')
{
    $timeObject = new DateTime($date, new DateTimeZone($fromTz));
    $timeObject->setTimezone(new DateTimeZone($toTz));
    return $timeObject->format($format);
}

// convert date according to time zone
function parseDateTimeWithTimeZone($dateTime, $toTz, $fromTz = 'UTC', $format = 'd M, Y h:i a')
{
    $timeObject = new DateTime($dateTime, new DateTimeZone($fromTz));
    $timeObject->setTimezone(new DateTimeZone($toTz));
    return $timeObject->format($format);
}


// todo, not being used in project for now, may remove it
function calculateTime($timeData) {
    $timeStr = $timeData['time_string'];
    $timeFormat = $timeData['current_format'];
    $requiredFormat = $timeData['required_format'];
    $additionTime = $timeData['addition_minutes'];

    $addition = '+' .$additionTime. ' minutes';
    $responseTime = date( $requiredFormat, strtotime( $addition, strtotime($timeStr) ) );

    $responseTime = str_replace(' ', '', $responseTime);
    return $responseTime;
}

// convert minutes to hours minutes and seconds
function convertMinutesToTime($durationInMinutes) {
    $hours = intval( floor($durationInMinutes / 60) ) ;
    $minutes = intval( floor($durationInMinutes % 60) );
    $seconds = intval( floor($durationInMinutes / 3600) );

    return ['hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds];
}

// return hours from minutes-duration
function getHours($durationInMinutes) {
   $time = Helper::convertMinutesToTime($durationInMinutes);
   return $time['hours'];
}

// return minutes from minutes-duration
function getMinutes($durationInMinutes) {
    $time = Helper::convertMinutesToTime($durationInMinutes);
    return $time['minutes'];
}

// return seconds from minutes-duration
function getSeconds($durationInMinutes) {
    $time = Helper::convertMinutesToTime($durationInMinutes);
    return $time['seconds'];
}

// check date is today
function checkTodayDate($date) {
    $date = date('Y-m-d', strtotime($date));
    $todayDate = date('Y-m-d');
    return strtotime($date) == strtotime($todayDate);
}

