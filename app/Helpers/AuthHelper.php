<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Session;

class AuthHelper
{

    // prepare next route
    public static function determineNextRoute( $defaultNextUrl = null, $roles = null ) {
        $nextUrl = null;
        if ( !isset($defaultNextUrl) ) {
            $defaultNextUrl = route('home');
        }

        $redirects = self::getSessionRedirect();
        if ( $redirects['redirect_route'] ) {
            $nextUrl = $redirects['next_route'];
        } elseif ( Session::has('url.intended') ) {
            $nextUrl = Session::remove('url.intended');
        } elseif ( isset($roles) && !empty($roles) ) {
            $userRoles = self::checkRole($roles);
            switch($userRoles) {
                case $userRoles['admin'] == true:
                    $nextUrl = route('admin-dashboard');
                    break;
                case $userRoles['talent'] == true:
                    $nextUrl = route('talent-dashboard');
                    break;
                case $userRoles['moderator'] == true:
                    $nextUrl = route('moderator-dashboard');
                    break;
                default:
                    break;
            }
        } else {
            $nextUrl = $defaultNextUrl;
        }

        return $nextUrl;
    }

    // get and return session base redirect
    public static function getSessionRedirect() {
        $redirect = [
            'redirect_route' => false,
            'previous_route' => null,
            'next_route' => null
        ];
        if ( Session::has('redirects') ) {
            $redirects = Session::pull('redirects');
            Session::forget('redirects');
            $redirect['redirect_route'] = true;
            $redirect['previous_route'] = $redirects['previous_route'];
            $redirect['next_route'] = $redirects['next_route'];
        }
        return $redirect;
    }

    // get and return session base redirect
    public static function setSessionRedirect($previousUrl, $nextUrl) {
        if ( !isset($previousUrl) ) {
            $previousUrl = url()->previous();
        }
        if ( !isset($nextUrl) ) {
            $previousUrl = route('home');
        }

        $redirect = [
            'redirect_route' => true,
            'previous_route' => $previousUrl,
            'next_route' => $nextUrl
        ];
        if ( Session::has('redirects') ) {
            Session::forget('redirects');
        }
        Session::put('redirects', $redirect);
        return $redirect;
    }

    // get and return session base redirect
    public static function setIntendedUrl() {
        Session::put('url.intended', url()->previous());
    }

    // check role
    public static function checkRole($roles) {

        $roles = is_array($roles) ? $roles : $roles->toArray();

        $userRoles = ['admin' => 0, 'talent' => 0, 'moderator' => 0];

        $userRoles['admin'] = !empty (array_filter($roles, function($iteration) {
            return $iteration && $iteration['id'] == 1;
        }) );

        $userRoles['talent'] = !empty (array_filter($roles, function($iteration) {
            return $iteration && $iteration['id'] == 2;
        }) );

        $userRoles['moderator'] = !empty (array_filter($roles, function($iteration) {
            return $iteration && $iteration['id'] == 3;
        }) );

        return $userRoles;

    }


    // set login confirmation message
    public static function flashConfirmationLoginMessage($confirmationText) {
        Session::flash('loginAlertMessage', $confirmationText);
    }

}
