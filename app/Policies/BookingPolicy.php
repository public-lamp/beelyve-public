<?php

namespace App\Policies;

use App\Models\Generic\Event\BookingEventActivity;
use App\Models\Generic\Event\BookingTimerActivity;
use App\Services\Moderator\Bookings\ModeratorBookingManagementServices;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // ****************************************** Talent Gates *****************************************
    // talent can mark presence
    public function talentCanMarkPresence($user, $policySlug, $booking, $bookingTimerActivity, $bookingEventActivities) {
        try {
            // check booking can be stated
            $timeNow = Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'))->addMinutes(10);
            if ( strtotime($timeNow) < strtotime($booking->time_from) ) {
                return Response::deny('Can not start event before event start time');
            }

            // check for booking time is completed
            if ( isset($bookingTimerActivity) && $bookingTimerActivity->in_progress == true ) {
                return Response::deny('Booking already in-progress');
            }

            // check for booking time is completed
            if ( isset($bookingTimerActivity) && $bookingTimerActivity->remaining_minutes_time <= 0.0 ) {
                return Response::deny('Booking timer already completed');
            }

            // check present event already submitted
            if( $bookingEventActivities->isNotEmpty() && $bookingEventActivities->last()->activity_type_id == 1 ) {
                return Response::deny('Presence marked already');
            }

            return Response::allow();
        } catch (\Exception $e) {
            return Response::deny('Something went wrong, please try again');
        }
    }

    // check talent can complete booking
    public function talentCanMarkBookingComplete($user, $policySlug, $booking) {
        try {
            // check booking not in past
            $timeNow = date('Y-m-d H:i:s');
            if ( strtotime($timeNow) < strtotime($booking->time_from)  ) {
                return Response::deny('Can not complete booking before Start Time');
            }

            // check for marked completed already
            $bookingMarkedCompleted = BookingEventActivity::where(['booking_id' => $booking->id, 'activity_type_id' => 2]) // activityTypeId 2 for "talent marked booking completed"
                ->first();
            if( $bookingMarkedCompleted ) {
                return Response::deny('Booking marked completed already');
            }

            // check for booking time is completed
            $bookingTimerActivity = BookingTimerActivity::where('booking_id', $booking->id)
                ->first();
            //  if (!$bookingTimerActivity) {
            // return Response::deny('Can not complete booking as booking time is not completed yet');
            // }

            //            $bookingTimerCompleted = false;
            //            if ( $bookingTimerActivity->in_progress == true) {
            //                $timeParams = ModeratorBookingManagementServices::getTimeDifferenceParams($bookingTimerActivity->last_started_at, date('Y-m-d H:i:s'));
            //                if ($timeParams['difference_in_minutes'] > 0.0) {
            //                    $bookingTimerCompleted = false;
            //                } else {
            //                    ModeratorBookingManagementServices::updateBookingTimerCompleted($booking);
            //                    $bookingTimerCompleted = true;
            //                }
            //            } elseif ( $bookingTimerActivity->remaining_minutes_time > 0.0 ) {
            //                $bookingTimerCompleted = false;
            //            } else {
            //                $bookingTimerCompleted = true;
            //            }
            //
            //            if( $bookingTimerCompleted == false ) {
            //                return Response::deny('Can not complete booking as booking is in-progress');
            //            }

            return Response::allow();
        } catch (\Exception $e) {
            return Response::deny('Something went wrong, please try again');
        }
    }


    // ****************************************** Moderator Gates *****************************************
    // check clinic can add new employee
    public function moderatorCanStartTimer($user, $policySlug, $booking) {
        try {
            if ( isset($booking->timerActivity) ) {
                if ( isset($booking->timerActivity) && $booking->timerActivity->in_progress ) {
                    return Response::deny('Please stop timer first to start again!');
                }

                if ( $booking->timerActivity->remaining_minutes_time <= 0.0 ) {
                    return Response::deny('Can not start as Timer is already completed');
                }

                // check for talent present
                $bookingLastActivityEvent = self::bookingLastEventActivity($booking->id);
                if( !isset($bookingLastActivityEvent) || intval($bookingLastActivityEvent->activity_type_id) !=1 ) {
                    return Response::deny('Can not start timer as Talent is not present');
                }

                return Response::allow();
            } else {
                // check for talent present
                $bookingLastActivityEvent = self::bookingLastEventActivity($booking->id);
                if( !isset($bookingLastActivityEvent) || intval($bookingLastActivityEvent->activity_type_id) !=1 ) {
                    return Response::deny('Can not start timer as Talent is not present');
                }

                return Response::allow();
            }
        } catch (\Exception $e) {
            return Response::deny('Something went wrong, please try again');
        }
    }


    // check clinic can add new employee
    public function moderatorCanApproveCompletion($user, $policySlug, $booking) {
        try {
            // check booking not in past
            $timeNow = date('Y-m-d H:i:s');
            if ( strtotime($timeNow) < strtotime($booking->time_from)  ) {
                return Response::deny('Can not complete booking before Start Time');
            }

            //            $timerActivityData = self::checkUpdateBookingTimerComplete($booking);
            //            if ( !isset($timerActivityData['timerActivity']) || (isset($timerActivityData['timerActivity']) && $timerActivityData['timeCompleted'] == false) ) {
            //                return Response::deny('Booking timer not completed yet!');
            //            }

            $approvedEventActivity = self::getBookingEventActivity($booking->id, 8); // eventTypeId 8 for approve event completion
            if ( isset($approvedEventActivity) ) {
                return Response::deny('Booking already approved');
            }

            return Response::allow();
        } catch (\Exception $e) {
            return Response::deny('Something went wrong, please try again');
        }
    }


    //    get last Booking activity
    public static function bookingLastEventActivity($bookingId) {
        return BookingEventActivity::where(['booking_id' => $bookingId])
            ->orderBy('id', 'DESC')
            ->first();
    }

    //    returns the specific eventActivity type for a booking
    public static function getBookingEventActivity($bookingId, $eventTypeId) {
        return BookingEventActivity::where(['booking_id' => $bookingId, 'activity_type_id' => $eventTypeId])
            ->first();
    }

    //    check and update the booking timer completion
    public static function checkUpdateBookingTimerComplete($booking) {
        $timerData = [
            'timeCompleted' => false,
            'timerActivity' => null
        ];
        $bookingTimerActivity = BookingTimerActivity::where('booking_id', $booking->id)->first();
        if ( !isset($bookingTimerActivity) ) {
            return $timerData;
        }

        if ( isset($bookingTimerActivity) && floatval($bookingTimerActivity->remaining_minutes_time) <= 0.0 ) {
            $timerData['timeCompleted'] = true;
            $timerData['timerActivity'] = $bookingTimerActivity;
        }

        if ( isset($bookingTimerActivity) && $bookingTimerActivity->in_progress == true) {
            $timeParams = ModeratorBookingManagementServices::getTimeDifferenceParams($bookingTimerActivity->last_started_at, date('Y-m-d H:i:s'));
            if ($timeParams['difference_in_minutes'] > 0.0) {
                $timerData['timeCompleted'] = false;
                $timerData['timerActivity'] = null;
            } else {
                ModeratorBookingManagementServices::updateBookingTimerCompleted($booking);
                $timerData['timeCompleted'] = true;
                $timerData['timerActivity'] = $bookingTimerActivity;
            }
        }

        return $timerData;
    }

}
