<?php

namespace App\Mail\Authentication;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    private $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function build()
    {
        return $this->markdown('app.emails.auth.forgot-password')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Password Reset Email')
            ->with([
                'forgotToken' => $this->token,
                'title' => 'Forgot Password',
                'text' => "If you’ve lost your password or want to reset it, Press the Button below to get started."

            ]);
    }
}
