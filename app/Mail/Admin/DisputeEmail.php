<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DisputeEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $disputeEmailData, $sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($disputeEmailData)
    {
        $this->disputeEmailData = $disputeEmailData;
        $this->sender = $disputeEmailData['sender'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.admin.dispute_query_email')
            ->from($this->sender->email, $this->sender->user_name)
            ->subject('Dispute Query')
            ->with([
                'title' => 'Event Dispute Query',
                'disputeQuery' => $this->disputeEmailData['disputeQuery'],
                'booking' => $this->disputeEmailData['booking']
            ]);
    }
}
