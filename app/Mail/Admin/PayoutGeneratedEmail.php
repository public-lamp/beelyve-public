<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PayoutGeneratedEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $payoutData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payoutData)
    {
        $this->payoutData = $payoutData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.payouts.admin_payout_generated')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject(config('app.name') .' | '. 'Payout Generated')
            ->with([
                'title' => 'Booking Payout Generated',
                'booking' => $this->payoutData['booking'],
                'payout' => $this->payoutData['payout'],
                'adminPayoutNote' => $this->payoutData['adminPayoutNote'],
            ]);
    }

}
