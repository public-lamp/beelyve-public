<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SubscriptionListSendEmail extends Mailable implements ShouldQueue, ShouldBeUnique
{
    use Queueable, SerializesModels;

    private $subscriptionEmailData;
    private $subscriber;

    public $tries = 3; // Max tries
    public $timeout = 900; // Timeout seconds

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscriptionEmailData, $subscriber)
    {
        $this->subscriptionEmailData = $subscriptionEmailData;
        $this->subscriber = $subscriber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.admin.subscribers_list_email')
            ->from(config('mail.from.address'), config('app.name'))
            ->subject(config('app.name') .' | '. $this->subscriptionEmailData['emailSubject'])
            ->with([
                'title' => config('app.name').' - User Subscription Email',
                'emailTitle' => $this->subscriptionEmailData['emailTitle'],
                'message' => $this->subscriptionEmailData['emailMessage']
            ]);
    }
}
