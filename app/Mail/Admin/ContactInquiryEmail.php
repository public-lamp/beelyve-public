<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactInquiryEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $contactInquiryData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contactInquiryData)
    {
        $this->contactInquiryData = $contactInquiryData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.admin.user_contact_email')
            ->from($this->contactInquiryData['email'], config('app.name').' User')
            ->subject(config('app.name').'|User Query')
            ->with([
                'title' => 'User Contact Query',
                'queryTitle' => $this->contactInquiryData['title'],
                'message' => $this->contactInquiryData['message']
            ]);
    }
}
