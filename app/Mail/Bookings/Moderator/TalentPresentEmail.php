<?php

namespace App\Mail\Bookings\Moderator;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TalentPresentEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $talentPresentEmailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($talentPresentEmailData)
    {
        $this->talentPresentEmailData = $talentPresentEmailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.bookings.moderator.talent_present_email')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Talent present')
            ->with([
                'toRoleName' => $this->talentPresentEmailData['toRoleName'],
                'title' => $this->talentPresentEmailData['title'],
                'bodyText' => $this->talentPresentEmailData['bodyText'],
                'talentBookingManagementLink' => $this->talentPresentEmailData['talentBookingManagementLink'],
                'moderatorBookingManagementLink' => $this->talentPresentEmailData['moderatorBookingManagementLink'],
            ]);
    }
}
