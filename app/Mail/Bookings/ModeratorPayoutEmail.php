<?php

namespace App\Mail\Bookings;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ModeratorPayoutEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $payoutData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payoutData)
    {
        $this->payoutData = $payoutData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.payouts.moderator_payout_generated')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject(config('app.name') .' | '. 'Payout Generated')
            ->with([
                'title' => 'Booking Charge-back Payout Generated',
                'booking' => $this->payoutData['booking'],
                'payout' => $this->payoutData['payout'],
                'moderatorPayoutNote' => $this->payoutData['moderatorPayoutNote'],
            ]);
    }
}
