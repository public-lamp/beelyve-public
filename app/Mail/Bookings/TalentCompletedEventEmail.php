<?php

namespace App\Mail\Bookings;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TalentCompletedEventEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $talentCompletedEventEmailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($talentCompletedEventEmailData)
    {
        $this->talentCompletedEventEmailData = $talentCompletedEventEmailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.bookings.talent_completed_event_email')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Talent Completed Event')
            ->with([
                'toRoleName' => $this->talentCompletedEventEmailData['toRoleName'],
                'title' => $this->talentCompletedEventEmailData['title'],
                'bodyText' => $this->talentCompletedEventEmailData['bodyText'],
                'talentBookingManagementLink' => $this->talentCompletedEventEmailData['talentBookingManagementLink'],
                'moderatorBookingManagementLink' => $this->talentCompletedEventEmailData['moderatorBookingManagementLink'],
            ]);
    }
}
