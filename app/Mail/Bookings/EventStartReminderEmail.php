<?php

namespace App\Mail\Bookings;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventStartReminderEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $eventReminderData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($eventReminderData)
    {
        $this->eventReminderData = $eventReminderData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $notifiable = $this->eventReminderData['notifiable'];
        return $this->markdown('app.emails.bookings.event_start_reminder')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Event Start Reminder')
            ->with([
                'title' => 'Event Start Time Reminder',
                'text' => "Hi ". $notifiable['full_name']. "! This is the Event Start Time reminder email, for more details about the Event/Booking please click the below button",
                'booking' => $this->eventReminderData['booking']
            ]);
    }
}
