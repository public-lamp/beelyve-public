<?php

namespace App\Mail\Bookings;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ModeratorApprovedEventCompletionEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $moderatorApprovedEventEmailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moderatorApprovedEventEmailData)
    {
        $this->moderatorApprovedEventEmailData = $moderatorApprovedEventEmailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('app.emails.bookings.moderator_approved_event_email')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Moderator Approved Event')
            ->with([
                'toRoleName' => $this->moderatorApprovedEventEmailData['toRoleName'],
                'title' => $this->moderatorApprovedEventEmailData['title'],
                'bodyText' => $this->moderatorApprovedEventEmailData['bodyText'],
                'bookingDetailUrl' => $this->moderatorApprovedEventEmailData['bookingDetailUrl'],
            ]);
    }
}
