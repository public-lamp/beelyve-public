<?php

namespace App\Jobs\Bookings;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\Bookings\EventStartReminderEmail;
use App\Models\Generic\Booking;
use Carbon\Carbon;

class EventStartEmailReminderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 3; // Max tries
    public $timeout = 480; // Timeout seconds

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reminderBeforeStartInMinutes = 10;
        $expectedReminderTime = Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'))->subMinutes($reminderBeforeStartInMinutes);
        $bookings = Booking::where(['booking_status_id' => 4, 'reminder_sent' => false])
            ->whereDate('time_from', '<=', $expectedReminderTime)
            ->with('talent', 'moderator')
            ->get();

        if ( $bookings->isNotEmpty() ) {
            foreach ($bookings as $booking) {
                $moderatorNotificationContent = ['notifiable' => $booking->moderator, 'booking' => $booking];
                $talentNotificationContent = ['notifiable' => $booking->talent, 'booking' => $booking];
                Mail::to($moderatorNotificationContent['notifiable']['email'])
                    ->send( new EventStartReminderEmail($moderatorNotificationContent) );
                Mail::to($talentNotificationContent['notifiable']['email'])
                    ->send( new EventStartReminderEmail($moderatorNotificationContent) );
                $booking->update(['reminder_sent' => true]);
            }
        }
    }
}
