<?php

namespace App\Events\Chat;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Event;

class NewMessage extends Event implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $newMessageData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($newMessageData)
    {
        $this->newMessageData = $newMessageData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // channel name by concatenating receiver_id and conversationId with hyphen in middle
        $channel = $this->newMessageData['messages'][0]['otherUser']['id']. '-' .$this->newMessageData['messages'][0]['conversation_id'];
        $channel = strval($channel);

        return new Channel($channel);
    }

    public function broadcastAs()
    { // broadcasting event name
        return 'newMessage';
    }

}
