<?php

namespace App\Events\Bookings;


use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TalentMarkedPresence implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $talentPresentData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($talentPresentData)
    {
        $this->talentPresentData = $talentPresentData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $booking = $this->talentPresentData['booking'];
        $channel = $booking['moderator_id']. '-' .$booking['id']. '-' .$booking['talent_id'];
        $channel = strval($channel);

        return new Channel($channel);
    }

    public function broadcastAs()
    { // broadcasting event name
        return 'talentMarkedPresence';
    }
}
