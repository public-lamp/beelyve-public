<?php

namespace App\Events\Bookings;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EventTimerStarted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $timerStartedData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($timerStartedData)
    {
        $this->timerStartedData = $timerStartedData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $booking = $this->timerStartedData['booking'];
        $channel = $booking['talent_id']. '-' .$booking['id']. '-' .$booking['moderator_id']; // channel name by TalentId, bookingId and moderatorId with hyphen in middle
        $channel = strval($channel);

        return new Channel($channel);
    }

    public function broadcastAs() { // broadcasting event name
        return 'eventTimerStarted';
    }
}
