<?php

namespace App\Events\Bookings;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TalentMarkedEventCompleted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $talentBookingCompletedData, $booking;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($talentBookingCompletedData)
    {
        $this->talentBookingCompletedData = $talentBookingCompletedData;
        $this->booking = $talentBookingCompletedData['booking'];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $channel = $this->booking->moderator_id. '-' .$this->booking->id. '-' .$this->booking->talent_id;
        $channel = strval($channel);

        return new Channel($channel);
    }

    public function broadcastAs() { // broadcasting event name
        return 'talentEventCompleted';
    }


}
