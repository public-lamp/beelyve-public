<?php

namespace App\Events\Bookings;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EventTimerStopped implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $timerStoppedData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($timerStoppedData)
    {
        $this->timerStoppedData = $timerStoppedData;
    }

    /**
     * Get the channels the event should broadcast on.
     *s
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        $booking = $this->timerStoppedData['booking'];
        $channel = $booking['talent_id']. '-' .$booking['id']. '-' .$booking['moderator_id']; // channel name by TalentId, bookingId and moderatorId with hyphen in middle
        $channel = strval($channel);

        return new Channel($channel);
    }

    public function broadcastAs() { // broadcasting event name
        return 'eventTimerStopped';
    }

}
