<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayoutStatus extends Model
{
   protected $table="payout_status";
   protected $guarded=[];
}
