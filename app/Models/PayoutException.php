<?php

namespace App\Models;

use App\Models\Generic\Booking;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayoutException extends Model
{
   protected $table="payout_exceptions";
   protected $guarded=[];

   public function booking() {
       return $this->belongsTo(Booking::class,'booking_id');
   }
}
