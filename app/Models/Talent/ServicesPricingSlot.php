<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesPricingSlot extends Model
{
    use HasFactory;
    protected $table = 'services_pricing_slots';
    protected $guarded = [];

}
