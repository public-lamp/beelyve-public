<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesMedium extends Model
{
    use HasFactory;

    protected $table = 'services_mediums';
    protected $guarded = [];

}
