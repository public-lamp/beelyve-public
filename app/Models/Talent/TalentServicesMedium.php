<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalentServicesMedium extends Model
{
    use HasFactory;

    protected $table = 'talent_services_mediums';
    protected $guarded = [];

    //    services medium
    public function servicesMedium()
    {
        return $this->belongsTo(ServicesMedium::class, 'services_medium_id');
    }
}
