<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalentServicesPricing extends Model
{
    use HasFactory;

    protected $table = 'talent_services_pricing';
    protected $guarded = [];


    //    services-pricing type
    public function servicesPricingType()
    {
        return $this->belongsTo(ServicesPricingType::class, 'services_pricing_type_id');
    }

    //    services-pricing slot
    public function servicesPricingSlot()
    {
        return $this->belongsTo(ServicesPricingSlot::class, 'services_pricing_slot_id');
    }

    //  --------------------------  accessor and mutators ----------------------
    public function setMainFeaturesAttribute($value) // main features mutator
    {
        $this->attributes['main_features'] = json_encode($value);
    }
    public function getMainFeaturesAttribute($value) // main features accessor
    {
        return json_decode($value);
    }

}
