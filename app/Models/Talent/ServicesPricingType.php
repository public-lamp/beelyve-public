<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesPricingType extends Model
{
    use HasFactory;
    protected $table = 'services_pricing_types';
    protected $guarded = [];

    //    services mediums
    public function servicesMediums()
    {
        return $this->belongsToMany(ServicesMedium::class, 'talent_services_mediums', 'services_pricing_type_id', 'services_medium_id');
    }

    //    talent services type medium' pivot
    public function talentServicesMediums()
    {
        return $this->hasMany(TalentServicesMedium::class, 'services_pricing_type_id');
    }

}
