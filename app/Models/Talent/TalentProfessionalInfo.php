<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalentProfessionalInfo extends Model
{
    use HasFactory;

    protected $table = 'talent_professional_info';
    protected $guarded = [];


    //    roles relation
    public function talentCategory()
    {
        return $this->belongsTo(TalentCategory::class, 'talent_category_id');
    }

}
