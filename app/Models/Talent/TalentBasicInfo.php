<?php

namespace App\Models\Talent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalentBasicInfo extends Model
{
    use HasFactory;

    protected $table = 'talent_basic_info';
    protected $guarded = [];

}
