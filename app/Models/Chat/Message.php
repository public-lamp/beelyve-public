<?php

namespace App\Models\Chat;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $table = 'messages';
    protected $guarded = [];

    //    message sender
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    //    message sender
    public function messageType()
    {
        return $this->belongsTo(MessageType::class, 'message_type_id');
    }

}
