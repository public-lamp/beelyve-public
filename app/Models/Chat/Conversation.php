<?php

namespace App\Models\Chat;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use HasFactory;

    protected $table = 'conversations';
    protected $guarded = [];

    //    conversation initiator
    public function initiatedFrom()
    {
        return $this->belongsTo(User::class, 'initiated_from');
    }

    //    conversation initiator
    public function initiatedTo()
    {
        return $this->belongsTo(User::class, 'initiated_to');
    }

    //    conversation messages
    public function messages()
    {
        return $this->hasMany(Message::class, 'conversation_id');
    }

    //    conversation last message
    public function lastMessage()
    {
        return $this->hasOne(Message::class, 'conversation_id')->latest();
    }

}
