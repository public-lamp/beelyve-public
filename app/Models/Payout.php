<?php

namespace App\Models;

use App\Models\Generic\Booking;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
   protected $table="payouts";
   protected $guarded=[];

   public function booking(){
       return $this->belongsTo(Booking::class,'booking_id');
   }

   public function payoutStatus(){
       return $this->belongsTo(PayoutStatus::class,'payout_status_id');
   }
}
