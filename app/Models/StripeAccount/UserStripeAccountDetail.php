<?php

namespace App\Models\StripeAccount;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStripeAccountDetail extends Model
{
    use HasFactory;
    protected $table="user_stripe_account_details";
    protected $guarded=[];
}
