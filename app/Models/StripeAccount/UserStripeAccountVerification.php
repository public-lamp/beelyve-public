<?php

namespace App\Models\StripeAccount;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStripeAccountVerification extends Model
{
    use HasFactory;
    protected $table="stripe_account_verifications";
    protected $guarded=[];
}
