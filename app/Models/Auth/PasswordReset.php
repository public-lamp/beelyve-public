<?php

namespace App\Models\Auth;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    use HasFactory;

    protected $table = 'password_resets';
    protected $guarded = [];

    //    roles relation
    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }

}
