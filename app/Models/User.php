<?php

namespace App\Models;

use App\Models\Auth\Role;
use App\Models\Generic\AvailabilitySlot;
use App\Models\Generic\Booking;
use App\Models\Generic\BookingServicesDetail;
use App\Models\Generic\FavouriteUser;
use App\Models\Generic\FeaturedUser;
use App\Models\Generic\Review;
use App\Models\Generic\SocialAccount;
use App\Models\Generic\TalentWorkFile;
use App\Models\Generic\UserAddress;
use App\Models\Generic\UserAvailabilityDay;
use App\Models\Generic\UsersAvailabilityCategory;
use App\Models\StripeAccount\UserStripeAccountDetail;
use App\Models\StripeAccount\UserStripeAccountVerification;
use App\Models\Talent\TalentBasicInfo;
use App\Models\Talent\TalentCategory;
use App\Models\Talent\TalentProfessionalInfo;
use App\Models\Talent\TalentServicesMedium;
use App\Models\Talent\TalentServicesPricing;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $table = 'users';
    protected $guarded = [];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // check is admin
    public function isAdmin()
    {
        $check = false;
        $check = !$this->roles->filter(function ($item) {
            return $item->id == 1;
        })->isEmpty();
        return $check;
    }

    // check is talent
    public function isTalent()
    {
        $check = false;
        $check = !$this->roles->filter(function ($item) {
            return $item->id == 2;
        })->isEmpty();
        return $check;
    }

    // check is moderator
    public function isModerator()
    {
        $check = false;
        $check = !$this->roles->filter(function ($item) {
            return $item->id == 3;
        })->isEmpty();
        return $check;
    }

    //    roles relation
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    //    user address
    public function userAddress()
    {
        return $this->hasOne(UserAddress::class, 'user_id');
    }

    //    talent basic info
    public function talentBasicInfo()
    {
        return $this->hasOne(TalentBasicInfo::class, 'user_id');
    }

    //    user work history files
    public function talentWorkFiles()
    {
        return $this->hasMany(TalentWorkFile::class, 'talent_id');
    }

    //    roles relation
    public function talentProfessionalInfo()
    {
        return $this->hasOne(TalentProfessionalInfo::class, 'user_id');
    }

    //    availability category
    public function availabilityCategory()
    {
        return $this->hasOne(UsersAvailabilityCategory::class, 'user_id');
    }

    //    availability slots
    public function availabilitySlots()
    {
        return $this->hasMany(AvailabilitySlot::class, 'user_id');
    }

    //    availability slots
    public function userAvailabilityDays()
    {
        return $this->hasMany(UserAvailabilityDay::class, 'user_id');
    }

    //    servicesPricing
    public function talentServicesPricing()
    {
        return $this->hasMany(TalentServicesPricing::class, 'user_id');
    }

    //    servicesPricing
    public function talentServicesMediums()
    {
        return $this->hasMany(TalentServicesMedium::class, 'user_id');
    }

    //    booked by user
    public function moderatorBookings()
    {
        return $this->hasMany(Booking::class, 'moderator_id');
    }

    //    as talent bookings
    public function talentBookings()
    {
        return $this->hasMany(Booking::class, 'talent_id');
    }

    //    as talent bookings
    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class, 'user_id');
    }

    //    user rateable reviews
    public function reviews()
    {
        return $this->hasMany(Review::class, 'reviewable_id');
    }

    //    moderator favorite talents
    public function favoriteTalents()
    {
        return $this->hasMany(FavouriteUser::class, 'user_id');
    }

    public function stripeAccountVerification(){
        return $this->hasOne(UserStripeAccountVerification::class,'user_id');
    }

    public function stripeAccountDetail(){
        return $this->hasOne(UserStripeAccountDetail::class,'user_id');
    }

    //    user featured
    public function featuredStatus()
    {
        return $this->hasOne(FeaturedUser::class, 'user_id');
    }

    // ======================  accessor and mutators ====================
    public function getFullNameAttribute()
    {
        return preg_replace('/\s+/', ' ',$this->first_name). ' ' .preg_replace('/\s+/', ' ',$this->last_name);
    }

}
