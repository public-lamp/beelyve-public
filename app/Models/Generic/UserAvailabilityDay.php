<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAvailabilityDay extends Model
{
    use HasFactory;

    protected $table = 'user_availability_days';
    protected $guarded = [];

    //    availability slots
    public function availabilitySlots()
    {
        return $this->hasMany(AvailabilitySlot::class, 'user_availability_day_id');
    }

    //    availability slots
    public function weekDay()
    {
        return $this->belongsTo(WeekDay::class, 'week_day_id');
    }

}
