<?php

namespace App\Models\Generic;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavouriteUser extends Model
{
    use HasFactory;

    protected $table = 'favourite_users';
    protected $guarded = [];

    //    moderator favorable
    public function talent()
    {
        return $this->belongsTo(User::class, 'favorable_id');
    }

}
