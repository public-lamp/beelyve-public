<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalentSearchFilter extends Model
{
    use HasFactory;

    protected $table = 'talent_search_filters';
    protected $guarded = [];

}
