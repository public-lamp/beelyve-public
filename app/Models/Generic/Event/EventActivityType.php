<?php

namespace App\Models\Generic\Event;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventActivityType extends Model
{
    use HasFactory;

    protected $table = 'event_activity_types';
    protected $guarded = [];
}
