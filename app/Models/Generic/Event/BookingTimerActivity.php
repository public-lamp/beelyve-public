<?php

namespace App\Models\Generic\Event;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingTimerActivity extends Model
{
    use HasFactory;

    protected $table = 'booking_timer_activities';
    protected $guarded = [];

}
