<?php

namespace App\Models\Generic\Event;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingEventActivity extends Model
{
    use HasFactory;

    protected $table = 'booking_event_activities';
    protected $guarded = [];

    // activity type
    public function activityType()
    {
        return $this->belongsTo(EventActivityType::class, 'activity_type_id');
    }

}
