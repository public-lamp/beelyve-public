<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceChargeType extends Model
{
    use HasFactory;

    protected $table = 'service_charge_types';
    protected $guarded = [];
}
