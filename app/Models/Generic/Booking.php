<?php

namespace App\Models\Generic;


use App\Models\Chat\Conversation;
use App\Models\Generic\Event\BookingEventActivity;
use App\Models\Payout;
use App\Models\PayoutException;
use App\Models\Talent\TalentServicesPricing;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generic\Event\BookingTimerActivity;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';
    protected $guarded = [];

    //    availability slots
    public function servicePricing()
    {
        return $this->belongsTo(TalentServicesPricing::class, 'talent_services_pricing_id');
    }

    //    talent
    public function talent()
    {
        return $this->belongsTo(User::class, 'talent_id');
    }

    //    talent
    public function moderator()
    {
        return $this->belongsTo(User::class, 'moderator_id');
    }

    //    booking status
    public function bookingStatus()
    {
        return $this->belongsTo(BookingStatus::class, 'booking_status_id');
    }

    //
    public function bookingPaymentStatus()
    {
        return $this->belongsTo(BookingPaymentStatus::class, 'booking_payment_status_id');
    }

    // booking payout
    public function bookingPayout()
    {
        return $this->hasOne(Payout::class, 'booking_id');
    }

    // booking payout
    public function bookingPayoutException()
    {
        return $this->hasOne(PayoutException::class, 'booking_id');
    }

    //    booking services detail
    public function bookingServicesDetail()
    {
        return $this->hasOne(BookingServicesDetail::class, 'booking_id');
    }

    //    booking timer-activity
    public function timerActivity()
    {
        return $this->hasOne(BookingTimerActivity::class, 'booking_id');
    }

    //    event activities
    public function eventActivities()
    {
        return $this->hasMany(BookingEventActivity::class, 'booking_id');
    }

    //    booking conversation
    public function bookingConversation()
    {
        return $this->hasOne(Conversation::class, 'booking_id');
    }

    //    booking conversation
    public function disputeQuery()
    {
        return $this->hasMany(DisputeQuery::class, 'booking_id');
    }

}
