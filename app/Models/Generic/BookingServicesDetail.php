<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingServicesDetail extends Model
{
    use HasFactory;

    protected $table = 'booking_services_detail';
    protected $guarded = [];

    //  --------------------------  accessor and mutators ----------------------

    public function setTalentServicesPricingAttribute($value) // main features mutator
    {
        $this->attributes['talent_services_pricing'] = json_encode($value);
    }
    public function getTalentServicesPricingAttribute($value) // main features accessor
    {
        return json_decode($value, true);
    }

    public function setServicePricingTypeAttribute($value) // main features mutator
    {
        $this->attributes['service_pricing_type'] = json_encode($value);
    }
    public function getServicePricingTypeAttribute($value) // main features accessor
    {
        return json_decode($value);
    }

    public function setServicePricingTypeMediumsAttribute($value) // main features mutator
    {
        $this->attributes['service_pricing_type_mediums'] = json_encode($value);
    }
    public function getServicePricingTypeMediumsAttribute($value) // main features accessor
    {
        return json_decode($value);
    }

    public function setServicePricingSlotAttribute($value) // main features mutator
    {
        $this->attributes['service_pricing_slot'] = json_encode($value);
    }
    public function getServicePricingSlotAttribute($value) // main features accessor
    {
        return json_decode($value);
    }

}
