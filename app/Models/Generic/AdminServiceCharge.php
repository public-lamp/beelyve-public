<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminServiceCharge extends Model
{
    use HasFactory;

    protected $table = 'admin_service_charges';
    protected $guarded = [];

    //    service charge type
    public function serviceChargeType()
    {
        return $this->belongsTo(ServiceChargeType::class, 'service_charge_type_id');
    }

    //    service type
    public function serviceType()
    {
        return $this->belongsTo(ServiceType::class, 'service_type_id');
    }

}
