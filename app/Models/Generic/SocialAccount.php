<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    use HasFactory;

    protected $table = 'social_accounts';
    protected $guarded = [];

    //   provider name
    public function providerName()
    {
        return $this->belongsTo(SocialProviderName::class, 'social_provider_name_id');
    }

}
