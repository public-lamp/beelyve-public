<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    use HasFactory;

    protected $table = 'user_activities';
    protected $guarded = [];

    protected $casts = [
        'last_activity_at' => 'datetime',
    ];

}
