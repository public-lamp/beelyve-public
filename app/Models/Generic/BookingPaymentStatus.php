<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingPaymentStatus extends Model
{
    use HasFactory;
    protected $table = 'booking_payment_status';
    protected $guarded = [];

}
