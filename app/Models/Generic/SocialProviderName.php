<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialProviderName extends Model
{
    use HasFactory;

    protected $table = 'social_provider_names';
    protected $guarded = [];
}
