<?php

namespace App\Models\Generic;

use App\Models\Talent\TalentProfessionalInfo;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    protected $table = 'reviews';
    protected $guarded = [];


    //    roles relation
    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id');
    }
}
