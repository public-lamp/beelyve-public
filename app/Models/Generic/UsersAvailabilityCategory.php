<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersAvailabilityCategory extends Model
{
    use HasFactory;

    protected $table = 'users_availability_categories';
    protected $guarded = [];

    //    availability slots
    public function availabilityCategory()
    {
        return $this->belongsTo(AvailabilityCategory::class, 'availability_category_id');
    }
}
