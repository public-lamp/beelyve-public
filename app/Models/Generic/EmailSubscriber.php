<?php

namespace App\Models\Generic;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailSubscriber extends Model
{
    use HasFactory;

    protected $table = 'email_subscribers';
    protected $guarded = [];

    //    subscriber user
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
