<?php

namespace App\Models\Generic;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeaturedUser extends Model
{
    use HasFactory;

    protected $table = 'featured_users';
    protected $guarded = [];

    //    featured Talent
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
