<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalentWorkFile extends Model
{
    use HasFactory;

    protected $table = 'talent_work_files';
    protected $guarded = [];


}
