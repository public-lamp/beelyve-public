<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisputeQuery extends Model
{
    use HasFactory;

    protected $table = 'dispute_queries';
    protected $guarded = [];
}
