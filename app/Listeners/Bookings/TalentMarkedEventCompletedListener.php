<?php

namespace App\Listeners\Bookings;

use App\Events\Bookings\TalentMarkedEventCompleted;
use App\Mail\Bookings\Moderator\TalentPresentEmail;
use App\Mail\Bookings\TalentCompletedEventEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class TalentMarkedEventCompletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TalentMarkedEventCompleted  $event
     * @return void
     */
    public function handle(TalentMarkedEventCompleted $event)
    {
        $booking = $event->talentBookingCompletedData['booking'];
        $moderator = $event->talentBookingCompletedData['booking']['moderator'];

        $talentMarkedEventCompletedEmailData = [
            'toRoleName' => 'moderator',
            'title' => 'Talent Marked Event Completed',
            'bodyText' => "Hi, " .$moderator['user_name']
                ."! Talent marked the booking event completed. To manage the booking event use the given link",
            'talentBookingManagementLink' => route('talent.booking.management', ['booking_id' => $booking['id']]),
            'moderatorBookingManagementLink' => route('moderator.booking.management', ['booking_id' => $booking['id']]),
        ];

        Mail::to($moderator['email'])->send(new TalentCompletedEventEmail($talentMarkedEventCompletedEmailData));
    }
}
