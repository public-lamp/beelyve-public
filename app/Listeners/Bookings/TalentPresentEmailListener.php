<?php

namespace App\Listeners\Bookings;


use App\Mail\Bookings\Moderator\TalentPresentEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class TalentPresentEmailListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the given event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(object $event)
    {
        $booking = $event->talentPresentData['booking'];
        $bookingStatus = $event->talentPresentData['booking']['bookingStatus'];
        $moderator = $event->talentPresentData['booking']['moderator'];
        $talent = $event->talentPresentData['booking']['talent'];
        $currentEventActivity  = $event->talentPresentData['eventActivity'];

        $talentPresentEmailData = [
            'toRoleName' => 'moderator',
            'title' => 'Talent Present',
            'bodyText' => "Hi, " .$moderator['first_name']. ' ' .$moderator['last_name']
                ."! Talent is present for one of your booking event. To manage the booking event use the given link",
            'talentBookingManagementLink' => route('talent.booking.management', ['booking_id' => $booking['id']]),
            'moderatorBookingManagementLink' => route('moderator.booking.management', ['booking_id' => $booking['id']]),
        ];

        Mail::to($moderator['email'])->send(new TalentPresentEmail($talentPresentEmailData));
    }

}
