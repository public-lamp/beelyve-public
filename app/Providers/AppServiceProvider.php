<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // to validate the signature of request in production environment with HTTPS
        if (env('APP_ENV') === 'production' || env('APP_ENV') === 'Production') {
            $this->app['request']->server->set('HTTPS', true);
        }
    }
}
