<?php

namespace App\Providers;

use App\Models\Generic\Booking;
use App\Policies\BookingPolicy;
use App\Policies\common\membership\MembershipSubscriptionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
         Booking::class => BookingPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('talent-can-mark-presence', [BookingPolicy::class, 'talentCanMarkPresence']);
        Gate::define('talent-can-mark-booking-complete', [BookingPolicy::class, 'talentCanMarkBookingComplete']);
        Gate::define('moderator-can-start-timer', [BookingPolicy::class, 'moderatorCanStartTimer']);
        Gate::define('moderator-can-approve-completion', [BookingPolicy::class, 'moderatorCanApproveCompletion']);

        // customization of account verification email
        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            return (new MailMessage)
                ->subject('Verify Email Address')
                ->markdown('app.emails.auth.verify-email',
                    [
                        'title' => 'Verify Email',
                        'actionText' => 'Verify your Email',
                        'message' => 'To complete email verification, please press the button below.',
                        'ignoreText' => 'If you did not create an account using this email address, safely ignore this email.
                                you can safety ignore this email. Only a person with access to your email can activate account',
                        'url' => $url,
                        'user' => $notifiable
                    ]
                );
        });
        // customization of account verification email, ends

    }
}
