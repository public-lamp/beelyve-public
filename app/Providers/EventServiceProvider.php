<?php

namespace App\Providers;


use App\Events\Bookings\TalentMarkedEventCompleted;
use App\Events\Bookings\TalentMarkedPresence;
use App\Listeners\Bookings\TalentMarkedEventCompletedListener;
use App\Listeners\Bookings\TalentPresentEmailListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        TalentMarkedPresence::class => [
            TalentPresentEmailListener::class,
        ],
        TalentMarkedEventCompleted::class => [
            TalentMarkedEventCompletedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
